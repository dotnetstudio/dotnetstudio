﻿using System;
using System.Threading;

namespace RalphsDotNet.Tool.System
{
    public class Synchronizer
    {
        public int WaiterCount { get; private set; }
        private readonly AutoResetEvent _handle = new AutoResetEvent(false);

        public void Release()
        {
            this._handle.Set();
        }

        public void Wait()
        {
            this.WaiterCount++;
            try
            {
                this._handle.WaitOne();
            }
            finally
            {
                this.WaiterCount--;
            }
        }

        public void Wait(TimeSpan timeout)
        {
            this.WaiterCount++;
            try
            {
                this._handle.WaitOne(timeout);
            }
            finally
            {
                this.WaiterCount--;
            }
        }
    }
}