﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading;

namespace RalphsDotNet.Tool.System
{
    public class Dispatcher : IDisposable
    {
        private static readonly Dictionary<Thread, Dispatcher> RunningDispatcher = new Dictionary<Thread, Dispatcher>();

        private readonly Synchronizer _synchronizer = new Synchronizer();
        private readonly Synchronizer _stopSynchronizer = new Synchronizer();
        private readonly Thread _thread;
        private bool _stopExecuted;
        private bool _stopRequested;

        protected Dispatcher(Thread thread)
        {
            this.HighQueue = new Queue<DispatcherOperation>();
            this.NormalQueue = new Queue<DispatcherOperation>();
            this.LowQueue = new Queue<DispatcherOperation>();
            this.ScheduleDictionary = new Dictionary<DateTime?, DispatcherOperation>();

            this._thread = thread;
        }

        public static Dispatcher Current
        {
            get { return ForThread(Thread.CurrentThread); }
        }

        protected Queue<DispatcherOperation> HighQueue { get; private set; }
        protected Queue<DispatcherOperation> NormalQueue { get; private set; }
        protected Queue<DispatcherOperation> LowQueue { get; private set; }
        protected Dictionary<DateTime?, DispatcherOperation> ScheduleDictionary { get; private set; }

        public int QueuedCount
        {
            get { return this.HighQueue.Count + this.NormalQueue.Count + this.LowQueue.Count; }
        }

        public int HighQueuedCount
        {
            get { return this.HighQueue.Count; }
        }

        public int NormalQueuedCount
        {
            get { return this.NormalQueue.Count; }
        }

        public int LowQueuedCount
        {
            get { return this.LowQueue.Count; }
        }

        public int ScheduleCount
        {
            get { return this.ScheduleDictionary.Count; }
        }

        public virtual void Dispose()
        {
            if (!this._stopExecuted)
                this.Stop();
        }

        public static void Run()
        {
            Dispatcher d;
            lock (RunningDispatcher)
            {
                var cThread = Thread.CurrentThread;

                if (RunningDispatcher.ContainsKey(cThread))
                    throw new DispatcherException(string.Format("thread {0} already hosts a dispatcher",
                        cThread.ManagedThreadId));

                d = new Dispatcher(cThread);
                RunningDispatcher.Add(cThread, d);
            }

            d.Loop();
        }

        public static Dispatcher ForThread(Thread thread)
        {
            Dispatcher d;
            RunningDispatcher.TryGetValue(thread, out d);

            return d;
        }

        public void Stop()
        {
            this._stopRequested = true;

            this._synchronizer.Release();

            this._stopSynchronizer.Wait();
        }

        public object Invoke(DispatcherPriority priority, Delegate dele, params object[] parameter)
        {
            var dO = this.BeginInvoke(priority, dele, parameter);

            dO.Wait();

            if (dO.Error != null)
                throw dO.Error;

            return dO.Result;
        }

        public DispatcherOperation Schedule(Delegate dele, DateTime utcRunTime, params object[] parameter)
        {
            var dO = new DispatcherOperation(this, dele, parameter);
            this.ScheduleDictionary.Add(utcRunTime, dO);

            this._synchronizer.Release();

            return dO;
        }

        public DispatcherOperation BeginInvoke(DispatcherPriority priority, Delegate dele, params object[] parameter)
        {
            var dO = new DispatcherOperation(this, dele, parameter);

            switch (priority)
            {
                case DispatcherPriority.High:
                    this.HighQueue.Enqueue(dO);
                    break;
                case DispatcherPriority.Normal:
                    this.NormalQueue.Enqueue(dO);
                    break;
                case DispatcherPriority.Low:
                    this.LowQueue.Enqueue(dO);
                    break;
            }

            this._synchronizer.Release();

            return dO;
        }

        protected virtual void OnStop()
        {
            lock (RunningDispatcher)
                RunningDispatcher.Remove(this._thread);

            this._stopExecuted = true;
            this._stopSynchronizer.Release();
        }

        protected virtual void Loop()
        {
            try
            {
                this.OnReady();

                while (!this._stopRequested)
                {
                    DispatcherOperation op;

                    do
                    {
                        op = null;
                        if (this.ScheduleDictionary.Count > 0)
                        {
                            var key = this.ScheduleDictionary.Keys.Where(k => k <= DateTime.UtcNow).Min();

                            if (key != null)
                            {
                                op = this.ScheduleDictionary[key];
                                this.ScheduleDictionary.Remove(key);
                            }
                        }

                        if (op == null && this.HighQueue.Count > 0)
                            op = this.HighQueue.Dequeue();

                        if (op == null && this.NormalQueue.Count > 0)
                            op = this.NormalQueue.Dequeue();

                        if (op == null && this.LowQueue.Count > 0)
                            op = this.LowQueue.Dequeue();

                        if (op != null)
                            op.Execute();

                    } while (op != null && !this._stopRequested);

                    if (!this._stopRequested)
                        this._synchronizer.Wait(TimeSpan.FromMilliseconds(100));
                }
            }
            finally
            {
                this.OnStop();
            }
        }

        public event UnhandledExceptionEventHandler UnhandledException;

        public event DispatcherReadyEventHandler Ready;

        protected virtual void OnUnhandledException(Exception ex, bool isTerminating)
        {
            if (this.UnhandledException != null)
                this.UnhandledException(this, new UnhandledExceptionEventArgs(ex, isTerminating));
        }

        protected virtual void OnReady()
        {
            if (this.Ready != null)
                this.Ready(this);
        }
    }

    public delegate void DispatcherReadyEventHandler(Dispatcher sender);

    public class DispatcherOperation
    {
        private readonly Delegate _dele;
        private readonly Dispatcher _dispatcher;
        private readonly object[] _parameter;
        private readonly Synchronizer _killSynchronizer = new Synchronizer();
        private readonly Synchronizer _waitSynchronizer = new Synchronizer();
        private bool _killExecuted;
        private bool _killRequested;

        internal DispatcherOperation(Dispatcher dispatcher, Delegate dele, object[] parameter)
        {
            this.Status = DispatcherOperationStatus.Pending;
            this._dispatcher = dispatcher;
            this._dele = dele;
            this._parameter = parameter;
        }

        public object Result { get; private set; }
        public Exception Error { get; private set; }

        public DispatcherOperationStatus Status { get; private set; }
        public event DispatcherOperationExecutingEventHandler Executing;
        public event DispatcherOperationFinishedEventHandler Finished;

        internal Exception Execute()
        {
            if (this.Status == DispatcherOperationStatus.Pending)
            {
                try
                {
                    if (this.OnExecuting())
                    {
                        this.Status = DispatcherOperationStatus.Executing;
                        this.Result = this._dele.DynamicInvoke(this._parameter);
                    }
                    else
                    {
                        this.Status = DispatcherOperationStatus.Aborted;
                    }
                }
                catch (ThreadAbortException)
                {
                    if (this._killRequested)
                    {
                        Thread.ResetAbort();

                        this.Status = DispatcherOperationStatus.Killed;
                        this._killExecuted = true;
                        this._killSynchronizer.Release();
                    }
                    else
                    {
                        this.Status = DispatcherOperationStatus.Failed;
                        throw;
                    }
                }
                catch (Exception ex)
                {
                    this.Status = DispatcherOperationStatus.Failed;
                    this.Error = ex;
                }

                this.Status = DispatcherOperationStatus.Completed;
            }

            this.OnFinished();

            this._waitSynchronizer.Release();

            return this.Error;
        }

        public void Abort()
        {
            this.Status = DispatcherOperationStatus.Aborted;
        }

        public void Kill()
        {
            this._killRequested = true;
            this._killSynchronizer.Wait();
        }

        public void Wait()
        {
            this._waitSynchronizer.Wait();
        }

        public void Wait(TimeSpan timeout)
        {
            this._waitSynchronizer.Wait(timeout);
        }

        protected bool OnExecuting(Exception ex = null)
        {
            var args = new CancelEventArgs();
            if (this.Executing != null)
                this.Executing(this, args);

            return !args.Cancel;
        }

        protected void OnFinished()
        {
            if (this.Finished != null)
                this.Finished(this, new DispatcherOperationFinishedEventArgs(this.Error));
        }
    }

    public delegate void DispatcherOperationExecutingEventHandler(DispatcherOperation sender, CancelEventArgs args);

    public delegate void DispatcherOperationFinishedEventHandler(
        DispatcherOperation sender, DispatcherOperationFinishedEventArgs args);

    public class DispatcherOperationFinishedEventArgs : EventArgs
    {
        public DispatcherOperationFinishedEventArgs(Exception ex = null)
        {
            this.Error = ex;
        }

        public Exception Error { get; private set; }
    }

    public enum DispatcherPriority
    {
        High,
        Normal,
        Low,
        Schedule
    }

    public enum DispatcherOperationStatus
    {
        Pending,
        Executing,
        Completed,
        Failed,
        Aborted,
        Killed
    }

    public class DispatcherException : Exception
    {
        public DispatcherException(string m) : base(m)
        {
        }
    }
}