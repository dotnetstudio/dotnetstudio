﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace RalphsDotNet.Extension
{
    public static class AttributeExtensions
    {
        public static IEnumerable<T> GetCustomAttributes<T>(this Type type, bool inherit = true) where T : Attribute
        {
            var types = new List<Type> { type };

            if (inherit)
                types.AddRange(type.GetInterfaces());

            return types.SelectMany(t => t.GetCustomAttributes(typeof(T), inherit)).OfType<T>();
        }

        //public static IEnumerable<T> GetCustomAttributes<T>(this MemberInfo mi) where T : Attribute
        //{
        //    return mi.GetCustomAttributes(typeof(T), true).OfType<T>();
        //}
    }
}
