﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Text;

namespace RalphsDotNet.Extension
{
    public static class StreamExtensions
    {
        /// <summary>
        ///     Deserializes a stream containing a data contract object.
        /// </summary>
        /// <typeparam name="T">Type of the object which should be deserialized</typeparam>
        /// <param name="stream">Stream containing the serialized object.</param>
        /// <returns>Deserialized object.</returns>
        public static T DeserializeDataContract<T>(this Stream stream) where T : class
        {
            return stream.DeserializeDataContract<T>(new System.Type[] {});
        }

        /// <summary>
        ///     Deserializes a stream containing a data contract object.
        /// </summary>
        /// <typeparam name="T">Type of the object which should be deserialized</typeparam>
        /// <param name="stream">Stream containing the serialized object.</param>
        /// <param name="knownTypes">list of known types</param>
        /// <returns>Deserialized object.</returns>
        public static T DeserializeDataContract<T>(this Stream stream, IEnumerable<System.Type> knownTypes)
            where T : class
        {
            var serializer = new DataContractSerializer(typeof (T), knownTypes);
            return serializer.ReadObject(stream) as T;
        }

        /// <summary>
        ///     Gets the string content of a stream.
        /// </summary>
        /// <param name="stream">Stream containing string.</param>
        /// <returns>Thre string</returns>
        public static string AsString(this Stream stream)
        {
            return stream.AsString(Encoding.Default);
        }

        /// <summary>
        ///     Gets the string content of a stream.
        /// </summary>
        /// <param name="stream">Stream containing string.</param>
        /// <param name="encoding">Encoding of the contained string.</param>
        /// <returns>Thre string</returns>
        public static string AsString(this Stream stream, Encoding encoding)
        {
            var sb = new StringBuilder();

            stream.Position = 0;

            while (stream.Position < stream.Length - 1)
            {
                int readLength =
                    Convert.ToInt32(stream.Position + 4096 < stream.Length ? 4096 : stream.Length - stream.Position);
                var buf = new byte[readLength];

                stream.Read(buf, 0, readLength);

                sb.Append(encoding.GetString(buf));
            }

            return sb.ToString();
        }

        public static byte[] GetBytes(this Stream stream)
        {
            using (stream)
            {
                if (stream == null) return null;
                long pos = stream.CanSeek ? stream.Position : 0;
                var ret = new byte[stream.Length - pos];
                stream.Read(ret, 0, ret.Length);
                if (stream.CanSeek)
                    stream.Seek(pos, SeekOrigin.Begin);
                return ret;
            }
        }
    }
}