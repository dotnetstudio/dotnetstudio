﻿using System.Collections.Generic;
using System.IO;

namespace RalphsDotNet.Extension
{
    public static class ByteArrayExtensions
    {
        /// <summary>
        ///     Deserializes a string containing a data contract object.
        /// </summary>
        /// <typeparam name="T">Type of the object which should be deserialized</typeparam>
        /// <param name="serialized">Serialized bytes.</param>
        /// <returns>Deserialized object.</returns>
        public static T DeserializeDataContract<T>(this byte[] serialized) where T : class
        {
            return serialized.DeserializeDataContract<T>(new System.Type[] {});
        }

        /// <summary>
        ///     Deserializes a string containing a data contract object.
        /// </summary>
        /// <typeparam name="T">Type of the object which should be deserialized</typeparam>
        /// <param name="serialized">Serialized bytes.</param>
        /// <returns>Deserialized object.</returns>
        public static T DeserializeDataContract<T>(this byte[] serialized, IEnumerable<System.Type> knownTypes)
            where T : class
        {
            using (Stream ms = new MemoryStream(serialized))
            {
                return ms.DeserializeDataContract<T>(knownTypes);
            }
        }
    }
}