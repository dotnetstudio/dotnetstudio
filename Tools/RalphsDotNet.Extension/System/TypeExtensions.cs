﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace RalphsDotNet.Extension
{
    public static class TypeExtensions
    {
        public static string GetResolvingName(this System.Type type)
        {
            return string.Concat(type.FullName, ", ", type.Assembly.FullName.Split(',')[0]);
        }

        public static MethodInfo GetMethodExtended(this System.Type type, string methodName,
            params System.Type[] parameterTypes)
        {
            return type.GetMethodExtendedInternal(methodName, parameterTypes);
        }

        private static MethodInfo GetMethodExtendedInternal(this System.Type type, string methodName,
            System.Type[] parameterTypes)
        {
            return
                type.GetMethod(methodName,
                    BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static, null,
                    parameterTypes, null) ??
                (type.BaseType != null ? type.BaseType.GetMethodExtendedInternal(methodName, parameterTypes) : null);
        }

        public static MethodInfo GetMethodExtended(this System.Type type, string methodName)
        {
            return
                type.GetMethod(methodName,
                    BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static) ??
                (type.BaseType != null ? type.BaseType.GetMethodExtended(methodName) : null);
        }

        public static IEnumerable<T> GetCustomAttributes<T>(this MemberInfo mi) where T : Attribute
        {
            return mi.GetCustomAttributes(typeof (T), true).OfType<T>();
        }
    }
}