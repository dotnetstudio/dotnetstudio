﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Xml;

namespace RalphsDotNet.Extension
{
    public static class ObjectExtensions
    {
        /// <summary>
        ///     Serailizes an object into a stream.
        /// </summary>
        /// <param name="obj">Object to be serialized.</param>
        /// <param name="stream">Stream object should be serialized in.</param>
        /// <param name="knownTypes">known types for serialization</param>
        public static void SerializeDataContract(this object obj, Stream stream, IEnumerable<System.Type> knownTypes)
        {
            if (!obj.GetType().IsClass || !obj.GetType().GetCustomAttributes(true).Any(a => a is DataContractAttribute))
                throw (new ArgumentException("object to serialize is not a class or no data contract"));

            var serializer = new DataContractSerializer(obj.GetType(), knownTypes);

            using (XmlWriter writer = XmlWriter.Create(stream, new XmlWriterSettings {Indent = true}))
                serializer.WriteObject(writer, obj);
        }

        /// <summary>
        ///     Serailizes an object into a stream.
        /// </summary>
        /// <param name="obj">Object to be serialized.</param>
        /// <param name="stream">Stream object should be serialized in.</param>
        public static void SerializeDataContract(this object obj, Stream stream)
        {
            obj.SerializeDataContract(stream, new System.Type[] {});
        }

        /// <summary>
        ///     Serailizes an object into a byte[].
        /// </summary>
        /// <param name="obj">Object to be serialized.</param>
        /// <returns>Bytes containing serialized object.</returns>
        /// <param name="knownTypes">known types for serialization</param>
        public static byte[] SerializeDataContract(this object obj, IEnumerable<System.Type> knownTypes)
        {
            using (var ms = new MemoryStream())
            {
                obj.SerializeDataContract(ms, knownTypes);

                var buf = new byte[ms.Length];
                ms.Seek(0, SeekOrigin.Begin);
                ms.Read(buf, 0, Convert.ToInt32(ms.Length));

                return buf;
            }
        }

        /// <summary>
        ///     Serailizes an object into a byte[].
        /// </summary>
        /// <param name="obj">Object to be serialized.</param>
        /// <returns>Bytes containing serialized object.</returns>
        public static byte[] SerializeDataContract(this object obj)
        {
            return obj.SerializeDataContract(new System.Type[] {});
        }

        /// <summary>
        ///     Sets a property value on the object.
        /// </summary>
        /// <param name="obj">Affected object.</param>
        /// <param name="propertyName">Name of the property to set.</param>
        /// <param name="value">Value to set.</param>
        public static void SetPropertyValue(this object obj, string propertyName, object value)
        {
            if (!obj.GetType().IsClass)
                throw (new ArgumentException("object is not a class"));

            PropertyInfo pi = obj.GetType()
                .GetProperty(propertyName, BindingFlags.Instance | BindingFlags.Public | BindingFlags.FlattenHierarchy);

            if (pi == null)
                throw (new ArgumentException(string.Concat("the object of type <", obj.GetType(),
                    "> contains no public property <", propertyName, ">")));

            pi.SetValue(obj, value, null);
        }

        /// <summary>
        ///     Gets a property value of the object.
        /// </summary>
        /// <param name="obj">Affected object.</param>
        /// <param name="propertyName">Name of the property to get.</param>
        /// <returns>Value of the property.</returns>
        public static object GetPropertyValue(this object obj, string propertyName)
        {
            if (!obj.GetType().IsClass)
                throw (new ArgumentException("object is not a class"));

            PropertyInfo pi = obj.GetType()
                .GetProperty(propertyName, BindingFlags.Instance | BindingFlags.Public | BindingFlags.FlattenHierarchy);

            if (pi == null)
                throw (new ArgumentException(string.Concat("the object of type <", obj.GetType(),
                    "> contains no public property <", propertyName, ">")));

            return pi.GetValue(obj, null);
        }

        public static IEnumerable<System.Type> GetKnownTypes(this object obj)
        {
            var knownTypes = new List<System.Type>();

            if (!obj.GetType().IsValueType && !(obj is string))
            {
                knownTypes.Add(obj.GetType());

                foreach (object member in from pi in obj.GetType().GetProperties()
                    where pi.GetCustomAttributes(typeof (DataMemberAttribute), true).Any()
                    select pi.GetValue(obj, null))
                {
                    if (member is IEnumerable && !(member is string))
                    {
                        foreach (
                            var knownType in
                                ((IEnumerable) member).GetKnownTypesFromEnumerable()
                                    .Where(knownType => !knownTypes.Contains(knownType)))
                        {
                            knownTypes.Add(knownType);
                        }
                    }
                    else if (member != null)
                    {
                        foreach (
                            System.Type knownType in
                                member.GetKnownTypes().Where(knownType => !knownTypes.Contains(knownType)))
                        {
                            knownTypes.Add(knownType);
                        }
                    }
                }
            }

            return knownTypes;
        }

        public static IEnumerable<System.Type> GetKnownTypesFromEnumerable(this IEnumerable enumerable)
        {
            var knownTypes = new List<System.Type>();

            foreach (object entry in enumerable)
            {
                knownTypes.AddRange(entry.GetKnownTypes());
            }

            return knownTypes;
        }
    }
}