using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;

namespace RalphsDotNet.Extension
{
    /// <summary>
    ///     General extensions for
    ///     <see>
    ///         <cref>System.String</cref>
    ///     </see>
    ///     class
    /// </summary>
    public static class StringExtensions
    {
        /// <summary>
        ///     Resolves an assembly string.
        /// </summary>
        /// <returns>
        ///     The assembly.
        /// </returns>
        /// <param name='s'>
        ///     string containing the assembly information.
        ///     <example>
        ///         <code>
        /// string myAssembly = "System.Xml";
        /// Assembly a = myAssembly.FindAssembly();
        /// </code>
        ///     </example>
        /// </param>
        public static Assembly FindAssembly(this string s)
        {
            if (!string.IsNullOrEmpty(s))
            {
                return AppDomain.CurrentDomain.GetAssemblies().FirstOrDefault(x => x.FullName == s) ??
                       AppDomain.CurrentDomain.Load(new AssemblyName(s));
            }

            return null;
        }

        /// <summary>
        ///     Resolves a type string.
        /// </summary>
        /// <returns>
        ///     The type.
        /// </returns>
        /// <param name='s'>
        ///     string containing the type information.
        ///     <example>
        ///         <code>
        /// string myType = "System.Int32, System";
        /// Type t = myType.FindType();
        /// </code>
        ///     </example>
        /// </param>
        public static System.Type FindType(this string s)
        {
            string[] sParts = s.Split(',');
            string typeName = sParts[0];
            string assemblyName = sParts.Length > 1 ? sParts[1] : null;

            System.Type result = null;

            Assembly ass = string.IsNullOrEmpty(assemblyName)
                ? AppDomain.CurrentDomain.GetAssemblies().FirstOrDefault(x => x.GetType(typeName) != null)
                : assemblyName.FindAssembly();

            if (ass != null)
            {
                result = ass.GetType(typeName);
            }

            return result;
        }

        /// <summary>
        ///     Gets the SHA1 hash of as string.
        /// </summary>
        /// <returns>
        ///     The hash.
        /// </returns>
        /// <param name='text'>
        ///     String, in the most cases password to hash.
        /// </param>
        public static string GetPasswordHash(this string text)
        {
            var sha1 = new SHA1CryptoServiceProvider();

            string result = null;

            byte[] arrayData = Encoding.ASCII.GetBytes(text);
            byte[] arrayResult = sha1.ComputeHash(arrayData);
// ReSharper disable once LoopCanBePartlyConvertedToQuery
            foreach (byte t in arrayResult)
            {
                string temp = Convert.ToString(t, 16);
                if (temp.Length == 1)
                    temp = "0" + temp;
                result += temp;
            }
            return result;
        }

        /// <summary>
        ///     Gets the bytes of a string using the default encoding
        /// </summary>
        /// <param name="s">The string.</param>
        /// <returns>Bytes of the string.</returns>
        public static byte[] GetBytes(this string s)
        {
            return s.GetBytes(Encoding.Default);
        }

        /// <summary>
        ///     Gets the bytes of a string.
        /// </summary>
        /// <param name="s">The string.</param>
        /// <param name="encoding">The encoding to use.</param>
        /// <returns>Bytes of the string</returns>
        public static byte[] GetBytes(this string s, Encoding encoding)
        {
            return encoding.GetBytes(s);
        }

        /// <summary>
        ///     Gets a stream of a string using the default encoding.
        /// </summary>
        /// <param name="s">The string.</param>
        /// <returns>Stream containing the string.</returns>
        public static Stream GetStream(this string s)
        {
            return s.GetStream(Encoding.Default);
        }

        /// <summary>
        ///     Gets a stream of a string.
        /// </summary>
        /// <param name="s">The string.</param>
        /// <param name="encoding">The encoding to use.</param>
        /// <returns>Stream containing the string.</returns>
        public static Stream GetStream(this string s, Encoding encoding)
        {
            return new MemoryStream(s.GetBytes(encoding));
        }

        public static string GetUniqueName(this string name, IEnumerable<string> existingNames)
        {
            string finalName = name;
            int counter = 0;
            string[] names = existingNames as string[] ?? existingNames.ToArray();
            while (names.Contains(finalName))
            {
                finalName = string.Format(string.Concat(name, " ({0})"), ++counter);
            }

            return finalName;
        }
    }
}