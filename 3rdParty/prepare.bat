@echo off

set NHIBERNATE="nhibernate-core-3.3.3.SP1"
set FLUENTNHIBERNATE="fluent-nhibernate-1.4.0.1"
set MYSQLBATCHER="NHibernate.MySQLBatcher-master"
set MYSQLCONNECTOR="mysql-connector-net-6.8.3-noinstall"
set IRONPYTHON="IronPython-2.7.4"
set HELIXTOOLKIT="HelixToolkit-2014.2.11.1"
set OXYPLOT="OxyPlot-2014.1.319.1"
set CLOO="Cloo-0.9.1"
set MONGODBDRIVER="CSharpDriver-1.9"
set EXTENDEDWPFTOOLKIT="Extended WPF Toolkit Binaries"
set AVALONEDIT="icsharpcode-AvalonEdit-683ee10"
set FAMFAMFAM="famfamfam_silk_icons_v013"

set "HOME=%cd%"
cd ..
set "SRC=%cd%"
cd %HOME%
cd Patches
set "PATCHES=%cd%"
cd %HOME%


echo "EXTRACTING NHIBERNATE"
cd %HOME%
7za.exe x %NHIBERNATE%.zip
rename %NHIBERNATE% nhibernate-core

echo "PATCHING NHIBERNATE"
cd %HOME%\nhibernate-core\src
%HOME%\patch.exe -p0 < %PATCHES%\nhibernate-core\NHibernate.csproj.patch
%HOME%\patch.exe -p0 < %SRC%\Service.Data.NHibernate\RalphsDotNet.Service.Data.NHibernate\Patches\NHibernate\PersistentList.cs.patch
%HOME%\patch.exe -p0 < %SRC%\Service.Data.NHibernate\RalphsDotNet.Service.Data.NHibernate\Patches\NHibernate\ProxySupport\ReflectHelper.cs.patch
%HOME%\patch.exe -p0 < %SRC%\Service.Data.NHibernate\RalphsDotNet.Service.Data.NHibernate\Patches\NHibernate\SqliteInmemorySupport\SchemaMetadataUpdater.cs.patch
%HOME%\patch.exe -p0 < %SRC%\Service.Data.NHibernate\RalphsDotNet.Service.Data.NHibernate\Patches\NHibernate\SqliteInmemorySupport\SchemaUpdate.cs.patch
%HOME%\patch.exe -p0 < %SRC%\Service.Data.NHibernate\RalphsDotNet.Service.Data.NHibernate\Patches\NHibernate\SqliteInmemorySupport\SessionFactoryImpl.cs.patch
echo "PATCHING NHIBERNATE COMPLETE"
echo "did it patch correct?"
pause
cd %HOME%

echo "PREPARING NHIBERNATE"
copy prepare-nhibernate.cm_ nhibernate-core\prepare-nhibernate.cmd
cd %HOME%\nhibernate-core
call prepare-nhibernate.cmd
cd %HOME%


echo "EXTRACTING FLUENT NHIBERNATE"
cd %HOME%
7za x %FLUENTNHIBERNATE%.zip
rename %FLUENTNHIBERNATE% fluent-nhibernate

echo "PATCHING FLUENT NHIBERNATE"
cd %HOME%\fluent-nhibernate\src
%HOME%\patch.exe -p0 < %PATCHES%\fluent-nhibernate\FluentNHibernate.csproj.patch
%HOME%\patch.exe -p0 < %PATCHES%\fluent-nhibernate\FluentNHibernate.sln.patch
%HOME%\patch.exe -p0 < %SRC%\Service.Data.NHibernate\RalphsDotNet.Service.Data.NHibernate\Patches\FluentNHibernate\ForeignKeys\ForeignKeyConvention.cs.patch
%HOME%\patch.exe -p0 < %SRC%\Service.Data.NHibernate\RalphsDotNet.Service.Data.NHibernate\Patches\FluentNHibernate\ForeignKeys\BuiltSuffixForeignKeyConvention.cs.patch
%HOME%\patch.exe -p0 < %SRC%\Service.Data.NHibernate\RalphsDotNet.Service.Data.NHibernate\Patches\FluentNHibernate\ForeignKeys\BuiltFuncForeignKeyConvention.cs.patch
echo "PATCHING FLUENT NHIBERNATE COMPLETE"
echo "did it patch correct?"
pause
%windir%\microsoft.net\framework\v4.0.30319\msbuild /m FluentNHibernate.sln /t:Rebuild /p:Configuration=Release /p:DebugType=PdbOnly "/p:DefineConstants=TRACE"
echo "did FluentNHibernate build correct? ignore the tests!"
pause
cd %HOME%


echo "EXTRACTING MYSQLCONNECTOR"
cd %HOME%
md mysql-connector-net
7za x -o%HOME%\mysql-connector-net %MYSQLCONNECTOR%.zip
cd %HOME%


echo "EXTRACTING MYSQLBATCHER"
cd %HOME%
7za x %MYSQLBATCHER%.zip
rename %MYSQLBATCHER% NHibernate.MySQLBatcher

echo "PATCHING MYSQLBATCHER"
cd %HOME%\NHibernate.MySQLBatcher\src
%HOME%\patch.exe -p0 < %PATCHES%\NHibernate.MySQLBatcher\NHibernate.MySQLBatcher.csproj.patch
echo "PATCHING MYSQLBATCHER COMPLETE"
echo "did it patch correct?"
pause
%windir%\microsoft.net\framework\v4.0.30319\msbuild /m NHibernate.MySQLBatcher.sln /t:Rebuild /p:Configuration=Release /p:DebugType=PdbOnly "/p:DefineConstants=TRACE"
echo "did MySQLBatcher build correct? ignore the tests!"
pause
cd %HOME%


echo "EXTRACTING IRONPYTHON"
cd %HOME%
7za x %IRONPYTHON%.zip
rename %IRONPYTHON% IronPython
cd %HOME%


echo "EXTRACTING HELIXTOOLKIT"
cd %HOME%
md HelixToolkit
7za x -o%HOME%\HelixToolkit %HELIXTOOLKIT%.zip
cd %HOME%


echo "EXTRACTING OXYPLOT"
cd %HOME%
md OxyPlot
7za x -o%HOME%\OxyPlot %OXYPLOT%.zip
cd %HOME%


echo "EXTRACTING CLOO"
cd %HOME%
md Cloo
7za x -o%HOME%\Cloo %CLOO%.zip
cd %HOME%\Cloo
%windir%\microsoft.net\framework\v4.0.30319\msbuild /m Cloo.VS2010.sln /t:Rebuild /p:Configuration=Release /p:DebugType=PdbOnly "/p:DefineConstants=TRACE"
echo "did Cloo build correct? ignore the tests!"
pause
cd %HOME%


echo "EXTRACTING MONGODBDRIVER"
cd %HOME%
md MongoDB
7za x -o%HOME%\MongoDB %MONGODBDRIVER%.zip
cd %HOME%


echo "EXTRACTING EXTENDEDWPFTOOLKIT"
cd %HOME%
md ExtendedWpfToolkit
7za x -o%HOME%\ExtendedWpfToolkit %EXTENDEDWPFTOOLKIT%.zip
rename %EXTENDEDWPFTOOLKIT% Cloo
cd %HOME%


echo "EXTRACTING AVALONEDIT"
cd %HOME%
7za x %AVALONEDIT%.zip
rename %AVALONEDIT% icsharpcode-AvalonEdit
echo "PATCHING AVALONEDIT"
cd %HOME%\icsharpcode-AvalonEdit
%HOME%\patch.exe -p0 < %PATCHES%\icsharpcode-AvalonEdit\TextEditor.cs.patch
echo "PATCHING AVALONEDIT COMPLETE"
echo "did it patch correct?"
pause
cd %HOME%\icsharpcode-AvalonEdit\packages
call buildpackages.cmd
@echo off
echo "did AvalonEdit build correct? Ignore the documentation build errors!"
pause
cd %HOME%


echo "EXTRACTING FAMFAMFAM"
cd %HOME%
md FamFamFam
7za x -o%HOME%\FamFamFam %FAMFAMFAM%.zip
cd %HOME%


echo "COMPLETE"
pause
