﻿using RalphsDotNet.Data.Base;

namespace RalphsDotNet.Data.Scripting
{
    [DeleteOrphans]
    public interface IScript : IDataObject, IHasName
    {
        [Index]
        ScriptLanguage Language { get; set; }

        [Text]
        string Code { get; set; }
    }

    public interface IScriptRepository : IDataRepository<IScript>
    {
        IScript Create(string name, ScriptLanguage language, string code);
    }

    // ReSharper disable InconsistentNaming
    public enum ScriptLanguage
    {
        ASPX,
        BAT,
        Boo,
        Coco,
        C,
        CPP,
        CSharp,
        HTML,
        Java,
        JavaScript,
        Patch,
        PHP,
        Python,
        Tex,
        VBNET,
        XML,
        OpenCl
    }

    // ReSharper restore InconsistentNaming
}