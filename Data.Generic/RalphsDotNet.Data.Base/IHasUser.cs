namespace RalphsDotNet.Data.Base
{
	public interface IHasUser
	{
        IUser User { get; set; }
	}
}

