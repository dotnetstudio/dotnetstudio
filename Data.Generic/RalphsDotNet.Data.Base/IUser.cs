namespace RalphsDotNet.Data.Base
{
    public interface IUser : IDataObject
    {
        string Email { get; set; }

        string FirstName { get; set; }

        string LastName { get; set; }

        ILanguage Language { get; set; }

        string Certificate { get; set; }

        string PasswordHash { get; set; }

        bool Verified { get; set; }
    }

    public interface IUserRepository : IDataRepository<IUser>
    {
        IUser Create(string email, string firstName, string lastName, string password, ILanguage language);
    }
}

