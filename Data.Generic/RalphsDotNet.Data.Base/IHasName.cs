﻿namespace RalphsDotNet.Data.Base
{
    public interface IHasName
    {
        [Index]
        string Name { get; set; }
    }
}
