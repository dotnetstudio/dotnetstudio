﻿namespace RalphsDotNet.Data.Base
{
    public interface IHasLanguage
    {
        ILanguage Language { get; set; }
    }
}
