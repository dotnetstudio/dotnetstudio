namespace RalphsDotNet.Data.Base
{
    public interface ISetting : IDataObject
    {
        string Type { get; set; }

        string Value { get; set; }
    }

    public interface ISettingRepository : IDataRepository<ISetting>
    {
        ISetting Create(string type, string value);
    }
}

