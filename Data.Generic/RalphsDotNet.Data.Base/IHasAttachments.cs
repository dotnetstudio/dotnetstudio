using System.Collections.Generic;

namespace RalphsDotNet.Data
{
    public interface IHasAttachments
    {
        [CascadeAll, OrderBy(Property = "Name")]
        IList<IAttachment> Attachments { get; set; }
    }
}

