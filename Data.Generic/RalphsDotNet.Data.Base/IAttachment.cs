using RalphsDotNet.Data.Base;

namespace RalphsDotNet.Data
{
    [DeleteOrphans]
    public interface IAttachment : IDataObject, IHasName
    {
        string Decription { get; set; }

        [Index]
        string FileName { get; set; }

        byte[] FileContent { get; set; }
    }

    public interface IAttachmentRepository : IDataRepository<IAttachment>
    {
        IAttachment Create(string name, string fileName);
    }
}