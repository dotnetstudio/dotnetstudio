﻿namespace RalphsDotNet.Data.Base
{
    public interface IHasSession
    {
        ISession Session { get; set; }
    }
}
