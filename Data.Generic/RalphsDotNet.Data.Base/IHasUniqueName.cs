﻿namespace RalphsDotNet.Data.Base
{
    public interface IHasUniqueName
    {
        [Index]
        [Unique]
        string Name { get; set; }
    }
}
