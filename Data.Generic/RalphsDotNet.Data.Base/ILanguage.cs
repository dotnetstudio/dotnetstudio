namespace RalphsDotNet.Data.Base
{
    public interface ILanguage : IDataObject
    {
        string ISO { get; set; }
    }

    public interface ILanguageRepository : IDataRepository<ILanguage>
    {
        ILanguage GetOrCreate(string iso);
        ILanguage Create(string iso);
    }
}

