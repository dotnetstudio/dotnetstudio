using System;

namespace RalphsDotNet.Data.Base
{
    public interface ISession : IDataObject, IHasUser
    {
        DateTime CreationTime { get; set; }

        DateTime LastActivityTime { get; set; }
    }

    public interface ISessionRepository : IDataRepository<ISession>
    {
        ISession Create(IUser user);
        void Update(ISession session);
    }
}

