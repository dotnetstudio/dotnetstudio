﻿using RalphsDotNet.Service.Data;

namespace RalphsDotNet.Data.Scripting.Repositories
{
    public class ScriptRepository : DataRepository<IScript>, IScriptRepository
    {
        public IScript Create(string name, ScriptLanguage language, string code)
        {
            var obj = this.CreateInternal();
            obj.Language = language;
            obj.Code = code;
            return obj;
        }
    }
}
