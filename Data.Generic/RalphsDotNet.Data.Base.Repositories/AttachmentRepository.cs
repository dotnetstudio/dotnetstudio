using RalphsDotNet.Service.Data;

namespace RalphsDotNet.Data
{
    public class AttachmentRepository : DataRepository<IAttachment>, IAttachmentRepository
    {
        public IAttachment Create(string name, string fileName)
        {
            var obj = this.CreateInternal();
            obj.Name = name;
            obj.FileName = fileName;
            return obj;
        }
    }
}

