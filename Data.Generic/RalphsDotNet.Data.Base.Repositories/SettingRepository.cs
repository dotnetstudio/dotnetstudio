using RalphsDotNet.Service.Data;

namespace RalphsDotNet.Data.Base
{
    public class SettingRepository : DataRepository<ISetting>, ISettingRepository
    {
        public ISetting Create(string type, string value)
        {
            var setting = this.CreateInternal();
            setting.Type = type;
            setting.Value = value;

            return setting;
        }
    }
}

