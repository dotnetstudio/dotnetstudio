using RalphsDotNet.Extension;
using RalphsDotNet.Service.Data;

namespace RalphsDotNet.Data.Base
{
    public class UserRepository : DataRepository<IUser>, IUserRepository
    {
        public IUser Create(string email, string firstName, string lastName, string password, ILanguage language)
        {
            var user = this.CreateInternal();

            user.Email = email;
            user.FirstName = firstName;
            user.LastName = lastName;
            user.PasswordHash = password.GetPasswordHash();
            user.Language = language;

            return user;
        }
    }
}