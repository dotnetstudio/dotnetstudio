using System.Linq;
using RalphsDotNet.Service.Data;

namespace RalphsDotNet.Data.Base
{
    public class LanguageRepository : DataRepository<ILanguage>, ILanguageRepository
    {
        public ILanguage GetOrCreate(string iso)
        {
            var lang = this.All.FirstOrDefault(l => l.ISO == iso) ?? this.Create(iso);

            return lang;
        }

        public ILanguage Create(string iso)
        {
            var language = this.CreateInternal();
            language.ISO = iso;

            return language;
        }
    }
}

