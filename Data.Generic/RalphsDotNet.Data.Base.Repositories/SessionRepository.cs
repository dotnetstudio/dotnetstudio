using System;
using RalphsDotNet.Service.Data;

namespace RalphsDotNet.Data.Base
{
    public class SessionRepository : DataRepository<ISession>, ISessionRepository
    {
        protected override ISession CreateInternal()
        {
            var obj = base.CreateInternal();
            obj.CreationTime = DateTime.UtcNow;
            this.Update(obj);
            return obj;
        }

        public ISession Create(IUser user)
        {
            var session = this.CreateInternal();
            session.User = user;

            return session;
        }

        public void Update(ISession session)
        {
            session.LastActivityTime = DateTime.UtcNow;
        }
    }
}

