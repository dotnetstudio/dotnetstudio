﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;
using System.Xml;
using ICSharpCode.AvalonEdit.Document;
using ICSharpCode.AvalonEdit.Highlighting;
using ICSharpCode.AvalonEdit.Highlighting.Xshd;
using ICSharpCode.AvalonEdit.Search;
using RalphsDotNet.Apps.SimulationStudio.Base;
using RalphsDotNet.Data;
using RalphsDotNet.Service.Simulation.Data;
using RalphsDotNet.Gui.Wpf.ActivityEngine;
using RalphsDotNet.Gui.Wpf.Controls;

namespace RalphsDotNet.Apps.SimulationStudio.Activities.Controls
{
    /// <summary>
    ///     Interaction logic for ScriptProfileEditor.xaml
    /// </summary>
    public partial class ScriptProfileEditor
    {
        private const string ScriptFileName = "_evaluation_script.py";

        public static readonly DependencyProperty ActiveScriptProperty =
            DependencyProperty.Register("ActiveScript", typeof (IAttachment), typeof (ScriptProfileEditor));

        private readonly IAttachmentRepository _attachmentRepository;
        private FindAndReplaceDialog _findReplaceDialog;

        public ScriptProfileEditor()
        {
            this._attachmentRepository = this.App.DataService.GetRepository<IAttachment, IAttachmentRepository>();

            this.InitializeComponent();
        }

        public IApplication App
        {
            get { return Application.Current as IApplication; }
        }

        public IAttachment ActiveScript
        {
            get { return this.GetValue(ActiveScriptProperty) as IAttachment; }
            set { this.SetValue(ActiveScriptProperty, value); }
        }

        public IExtensionProfile Profile
        {
            get { return this.DataContext as IExtensionProfile; }
        }

        protected override void OnInitialized(EventArgs e)
        {
            this.LoadSyntaxHighlighting();
            SearchPanel.Install(this._ScriptEditor.TextArea);

            this._ScriptEditor.Options.HighlightCurrentLine = true;

            base.OnInitialized(e);
        }

        private void LoadSyntaxHighlighting()
        {
            using (
                var s =
                    typeof(IApplication)
                        .Assembly.GetManifestResourceStream(
                            "RalphsDotNet.Apps.SimulationStudio.Base.Resources.Python_Light.xshd"))
            {
                using (var reader = new XmlTextReader(s))
                {
                    this._ScriptEditor.SyntaxHighlighting = HighlightingLoader.Load(reader,
                        HighlightingManager.Instance);
                }
            }
        }

        private void WriteActiveScript()
        {
            this.ActiveScript.FileContent = Encoding.Default.GetBytes(this._ScriptEditor.Document.Text);
            this.OnProfileChanged();
        }

        private void ReadActiveScript()
        {
            try
            {
                this._ScriptEditor.Document = this.ActiveScript.FileContent != null
                    ? new TextDocument(Encoding.Default.GetString(this.ActiveScript.FileContent))
                    : new TextDocument();
            }
            catch (Exception ex)
            {
                OutputProvider.WriteException(ex);
            }
        }

        private void SaveScriptButton_OnClick(object sender, RoutedEventArgs e)
        {
            this.WriteActiveScript();
        }

        private void ScriptEditor_OnKeyDown(object sender, KeyEventArgs e)
        {
            if (Keyboard.IsKeyDown(Key.S) && (Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl)))
            {
                this.WriteActiveScript();
            }
            else if (Keyboard.IsKeyDown(Key.R) && (Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl)))
            {
                this.ShowReplaceDialog();
            }
        }

        private void ScriptEditor_OnLostFocus(object sender, RoutedEventArgs e)
        {
            this.WriteActiveScript();
        }

        private void CreateSearchAndReplaceDialog()
        {
            if (this._findReplaceDialog == null)
            {
                this._findReplaceDialog = new FindAndReplaceDialog(this._ScriptEditor)
                {
                    Title = this.ActiveScript.Name
                };
                this._findReplaceDialog.Show();
                this._findReplaceDialog.Activate();
                this._findReplaceDialog.Closed += this._findReplaceDialog_Closed;
            }
            else
            {
                this._findReplaceDialog.Title = this.ActiveScript.Name;
                this._findReplaceDialog.Activate();
            }
        }

        private void ShowFindDialog()
        {
            this.CreateSearchAndReplaceDialog();
            this._findReplaceDialog.ShowFind();
            this._findReplaceDialog.TakeSelection();
        }

        private void ShowReplaceDialog()
        {
            this.CreateSearchAndReplaceDialog();
            this._findReplaceDialog.ShowReplace();
            this._findReplaceDialog.TakeSelection();
        }

        private void _findReplaceDialog_Closed(object sender, EventArgs e)
        {
            this._findReplaceDialog.Closed -= this._findReplaceDialog_Closed;
            this._findReplaceDialog = null;
        }

        private void DeleteButton_OnClickProfileButton_OnClick(object sender, RoutedEventArgs e)
        {
            this.Profile.Attachments.Remove(this.ActiveScript);
            this.OnProfileChanged();

            this.SetActiveScript();
        }

        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            base.OnPropertyChanged(e);

            if (e.Property == DataContextProperty)
            {
                if (this.Profile != null)
                    this.SetActiveScript();
                else
                    this.ActiveScript = null;
            }
            if (e.Property == ActiveScriptProperty)
            {
                if (e.OldValue as IAttachment != null)
                    ((IAttachment) e.OldValue).PropertyChanged -= this.ActiveScript_PropertyChanged;

                if (this.ActiveScript != null)
                {
                    this.ActiveScript.PropertyChanged += this.ActiveScript_PropertyChanged;
                    this.ReadActiveScript();
                }

                if (this._findReplaceDialog != null)
                    this._findReplaceDialog.Close();
            }
        }

        private void SetActiveScript(IAttachment script = null)
        {
            if (script == null)
                script = this.Profile.Attachments.FirstOrDefault();

            this.ActiveScript = script;
        }

        private IAttachment AddScript()
        {
            var script = this._attachmentRepository.Create(Guid.NewGuid().ToString(), ScriptFileName);
            this.Profile.Attachments.Add(script);
            this.OnProfileChanged();

            return script;
        }

        private void ActiveScript_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            this.Dispatcher.BeginInvoke(new Action(this.OnProfileChanged));
        }

        public event RoutedEventHandler ProfileChanged;

        private void OnProfileChanged()
        {
            if (this.ProfileChanged != null)
                this.ProfileChanged(this, new RoutedEventArgs());
        }

        private void AddScriptButton_OnClick(object sender, RoutedEventArgs e)
        {
            this.SetActiveScript(this.AddScript());
        }

        private void UndoButton_OnClick(object sender, RoutedEventArgs e)
        {
            this._ScriptEditor.Undo();
        }

        private void RedoButton_OnClick(object sender, RoutedEventArgs e)
        {
            this._ScriptEditor.Redo();
        }

        private void CutButton_OnClick(object sender, RoutedEventArgs e)
        {
            this._ScriptEditor.Cut();
        }

        private void CopyButton_OnClick(object sender, RoutedEventArgs e)
        {
            this._ScriptEditor.Copy();
        }

        private void PasteButton_OnClick(object sender, RoutedEventArgs e)
        {
            this._ScriptEditor.Paste();
        }

        private void FindButton_OnClick(object sender, RoutedEventArgs e)
        {
            this.ShowFindDialog();
        }

        private void PythonHelpButton_OnClick(object sender, RoutedEventArgs e)
        {
            Process.Start("https://docs.python.org/2/reference/");
        }

        private void IronPythonHelpButton_OnClick(object sender, RoutedEventArgs e)
        {
            Process.Start("http://ironpython.net/documentation/dotnet/");
        }

        private void HelixToolkitHelpButton_OnClick(object sender, RoutedEventArgs e)
        {
            Process.Start("http://www.nudoq.org/#!/Packages/HelixToolkit.Wpf/HelixToolkit.Wpf/MeshBuilder");
        }

        private void OxyPlotHelpButton_OnClick(object sender, RoutedEventArgs e)
        {
            Process.Start("http://www.oxyplot.org/ExampleBrowser/");
        }
    }
}