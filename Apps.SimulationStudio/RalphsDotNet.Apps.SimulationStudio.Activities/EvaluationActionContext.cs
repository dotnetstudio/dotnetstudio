﻿using System;
using RalphsDotNet.Service.Simulation.Model;
using RalphsDotNet.Gui.Wpf.ActivityEngine;

namespace RalphsDotNet.Apps.SimulationStudio.Activities
{
    public class EvaluationActionContext : EvaluationScriptContext
    {
        public void Execute(IIteration iteration, string script, string[] libraries)
        {
            this.Iteration = iteration;
            this.Script = string.Concat(string.Join(Environment.NewLine, libraries), Environment.NewLine, script);

            try
            {
                this.ExecuteInternal();
            }
            catch (Exception ex)
            {
                OutputProvider.WriteException(ex);
            }
        }

        private void ExecuteInternal()
        {
            if (this.Script != null)
            {
                this.ScriptSource =
                    ScriptEngine.CreateScriptSourceFromString(string.Concat(this.PreparationScript, this.Script));
            }

            if (this.Script != null && this.Iteration != null)
            {
                this.ScriptScope.SetVariable("Context", this);
                this.ScriptScope.SetVariable("Shared", Shared);
                this.ScriptSource.Execute(this.ScriptScope);
            }
        }
    }
}