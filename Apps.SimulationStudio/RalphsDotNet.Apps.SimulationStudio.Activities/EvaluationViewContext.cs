﻿using System;
using System.Threading;
using RalphsDotNet.Service.Simulation.Model;
using RalphsDotNet.Tool.System;
using RalphsDotNet.Gui.Wpf.ActivityEngine;
using Dispatcher = System.Windows.Threading.Dispatcher;
using DispatcherOperation = System.Windows.Threading.DispatcherOperation;
using DispatcherOperationStatus = System.Windows.Threading.DispatcherOperationStatus;
using DispatcherPriority = System.Windows.Threading.DispatcherPriority;

namespace RalphsDotNet.Apps.SimulationStudio.Activities
{
    public class EvaluationViewContext : EvaluationScriptContext
    {
        private readonly Synchronizer _abortSynchronizer = new Synchronizer();
        private readonly Dispatcher _dispatcher;
        private readonly Synchronizer _executionSynchronizer = new Synchronizer();
        private Thread _controllThread;
        private DispatcherOperation _executingDispatcherOperation;
        private Exception _executingException;
        private bool _isViewInitialized;
        private string _oldScript;
        private bool _oldScriptFailed;
        private bool _updateLock;

        public EvaluationViewContext(HelixModel helixModel, OxyModel oxyModel)
        {
            this.HelixModel = helixModel;
            this.OxyModel = oxyModel;
            this.RefreshRate = 40;

            this._dispatcher = Dispatcher.CurrentDispatcher;
            this.InitializeControlDispatcher();
        }

        public Tool.System.Dispatcher ControllDispatcher
        {
            get { return Tool.System.Dispatcher.ForThread(this._controllThread); }
        }

        public HelixModel HelixModel { get; private set; }
        public OxyModel OxyModel { get; private set; }

        public int RefreshRate { get; set; }

        private void InitializeControlDispatcher()
        {
            this._controllThread = new Thread(this.RunDispatcher);
            this._controllThread.Start();
            
            while (this.ControllDispatcher == null)
                new ManualResetEvent(false).WaitOne(TimeSpan.FromTicks(10));
        }

        private void RunDispatcher()
        {
            try
            {
                Tool.System.Dispatcher.Run();
            }
            catch (ThreadAbortException)
            {
                throw;
            }
            catch
            {
                this.RunDispatcher();
            }
        }

        public void UpdateModel(IIteration iteration, string script, string[] libraries)
        {
            if (script != null)
            {
                this.Iteration = iteration;
                this.Script = string.Concat(string.Join(Environment.NewLine, libraries), Environment.NewLine, script);

                this.UpdateModelsInternal();
            }
        }

        private void UpdateModelsInternal()
        {
            if (this._executingDispatcherOperation == null && !this._updateLock)
            {
                this._updateLock = true;
                try
                {
                    if (this._oldScript != this.Script)
                    {
                        this.ScriptSource =
                            ScriptEngine.CreateScriptSourceFromString(string.Concat(this.PreparationScript, this.Script));
                        this._isViewInitialized = false;
                        this._oldScriptFailed = false;
                    }

                    if (this.Iteration != null && (!this._oldScriptFailed || this._oldScript != this.Script) &&
                        this.ControllDispatcher != null)
                    {
                        this._executingDispatcherOperation = this._dispatcher.BeginInvoke(DispatcherPriority.Normal,
                            new Action(
                                () =>
                                {
                                    try
                                    {
                                        this.ScriptScope.SetVariable("IsInitialized", this._isViewInitialized);
                                        this.ScriptScope.SetVariable("Shared", Shared);
                                        this.ScriptScope.SetVariable("Context", this);

                                        this.ScriptSource.Execute(this.ScriptScope);
                                    }
                                    catch (ThreadAbortException)
                                    {
                                        // need to fake a thread abort since there is no other posibility to abort the python script but this one
                                        // because of wpf 3d the python script has also to run in gui thread

                                        this._executingException =
                                            new TimeoutException("view script execution exceeded a second");

                                        Thread.ResetAbort();

                                        this._abortSynchronizer.Release();
                                    }
                                    catch (Exception ex)
                                    {
                                        this._executingException = ex;
                                    }
                                }));

                        this._executionSynchronizer.Release();

                        this.ControllDispatcher.BeginInvoke(Tool.System.DispatcherPriority.Normal, new Action(() =>
                        {
                            this._executionSynchronizer.Wait();

                            this._executingDispatcherOperation.Wait(TimeSpan.FromMilliseconds(1000));

                            if (this._executingException != null &&
                                this._executingDispatcherOperation.Status == DispatcherOperationStatus.Executing)
                            {
                                this._dispatcher.Thread.Abort();

                                if (this._dispatcher.Thread.ThreadState == ThreadState.AbortRequested)
                                    this._abortSynchronizer.Wait();
                            }

                            if (this._executingDispatcherOperation.Status == DispatcherOperationStatus.Completed &&
                                this._executingException == null)
                            {
                                this._dispatcher.Invoke(this.PostUpdate);
                            }
                            else if (this._executingDispatcherOperation.Status == DispatcherOperationStatus.Pending)
                            {
                                this._executingDispatcherOperation.Abort();
                                OutputProvider.WriteLine("skipped view update");
                            }
                            else if (this._executingException != null)
                            {
                                if (this._executingException as TimeoutException == null)
                                    this._oldScriptFailed = true;

                                OutputProvider.WriteException(this._executingException);
                            }
                            else
                            {
                                this._executingDispatcherOperation.Abort();
                            }

                            this._oldScript = this.Script;

                            this._executingException = null;
                            this._executingDispatcherOperation = null;
                        }));
                    }
                }
                finally
                {
                    this._updateLock = false;
                }
            }
        }

        private void PostUpdate()
        {
            this._isViewInitialized = (bool) this.ScriptScope.GetVariable("IsInitialized");

            this.OxyModel.InvalidatePlot(true);
        }

        public override void Dispose()
        {
            base.Dispose();

            if (this._controllThread.IsAlive)
            {
                this.ControllDispatcher.Stop();

                this._controllThread.Abort();
            }
        }
    }
}