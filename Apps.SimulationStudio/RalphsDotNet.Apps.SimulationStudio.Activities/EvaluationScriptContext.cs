﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using IronPython.Hosting;
using Microsoft.Scripting;
using Microsoft.Scripting.Hosting;
using RalphsDotNet.Service.Simulation.Model;

namespace RalphsDotNet.Apps.SimulationStudio.Activities
{
    public class EvaluationScriptContext : INotifyPropertyChanged, IDisposable
    {
        private Dictionary<string, object> _sharedSymbols;

        static EvaluationScriptContext()
        {
            Shared = new Dictionary<string, object>();
        }

        public EvaluationScriptContext()
        {
            this.Reset();
        }

        protected static Dictionary<string, object> Shared { get; private set; }

        protected static ScriptEngine ScriptEngine { get; private set; }
        protected string PreparationScript { get; private set; }
        protected ScriptScope ScriptScope { get; private set; }
        protected ScriptSource ScriptSource { get; set; }

        public IIteration Iteration { get; set; }
        public string Script { get; set; }

        public virtual void Dispose()
        {
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void Reset()
        {
            this.ResetInternal();
        }

        private void ResetInternal()
        {
            using (
                var s =
                    typeof (EvaluationScriptContext)
                        .Assembly.GetManifestResourceStream(
                            "RalphsDotNet.Apps.SimulationStudio.Activities.Resources.EvaluationScript_Preparation.py"))
            {
                var sr = new StreamReader(s);
                this.PreparationScript = sr.ReadToEnd();
            }

            var referencesScript = string.Empty;
            using (
                var s =
                    typeof (EvaluationScriptContext)
                        .Assembly.GetManifestResourceStream(
                            "RalphsDotNet.Apps.SimulationStudio.Activities.Resources.EvaluationScript_References.py"))
            {
                var sr = new StreamReader(s);
                referencesScript = sr.ReadToEnd();
            }

            //a dictionary that receives the import simbols
            this._sharedSymbols = new Dictionary<string, object>();

            ScriptEngine = Python.CreateEngine();
            var importSource = ScriptEngine.CreateScriptSourceFromString(referencesScript,
                SourceCodeKind.Statements);
            var importCompiled = importSource.Compile();
            var importScope = ScriptEngine.CreateScope(this._sharedSymbols);
            importCompiled.Execute(importScope);

            this.ScriptScope = ScriptEngine.CreateScope(this._sharedSymbols);

            var searchPaths = ScriptEngine.GetSearchPaths();
            // ReSharper disable AssignNullToNotNullAttribute
            searchPaths.Add(Path.Combine(Path.GetDirectoryName(typeof (EvaluationScriptContext).Assembly.Location),
                "Resources", "PyLib"));
            // ReSharper restore AssignNullToNotNullAttribute
            ScriptEngine.SetSearchPaths(searchPaths);

            this.Script = null;
            this.Iteration = null;
        }

        protected virtual void OnPropertyChanged(string propertyName)
        {
            var handler = this.PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}