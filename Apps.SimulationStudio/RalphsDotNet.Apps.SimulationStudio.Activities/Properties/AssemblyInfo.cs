﻿using System.Reflection;
using RalphsDotNet.Apps.SimulationStudio.Activities;
using RalphsDotNet.Apps.SimulationStudio.Base;

[assembly: AssemblyTitle("RalphsDotNet.Apps.SimulationStudio.Activities")]
[assembly: AssemblyProduct("RalphsDotNet.Apps.SimulationStudio.Activities")]

[assembly: AddinActivity(typeof(EvaluationActivity), "EvaluationActivityString")]