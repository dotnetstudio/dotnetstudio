﻿import clr
clr.AddReference("System.Core")
clr.AddReference("PresentationCore")
clr.AddReference("HelixToolkit.Wpf")
clr.AddReference("OxyPlot")
clr.AddReference("RalphsDotNet.Service.Simulation.Data")
