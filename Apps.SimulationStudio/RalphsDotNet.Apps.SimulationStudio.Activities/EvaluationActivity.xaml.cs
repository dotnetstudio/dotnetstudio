﻿using System;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using RalphsDotNet.Data;
using RalphsDotNet.Service.Simulation.Data;
using RalphsDotNet.Gui.Wpf.ActivityEngine;

namespace RalphsDotNet.Apps.SimulationStudio.Activities
{
    /// <summary>
    ///     Interaction logic for ViewActivity.xaml
    /// </summary>
    public partial class EvaluationActivity
    {
        private const string ViewProfileName = "_evaluation_view";
        private const string ActionProfileName = "_evaluation_action";
        private const string LibraryProfileName = "_evaluation_library";

        public static readonly DependencyProperty ViewProfileProperty =
            DependencyProperty.Register("ViewProfile", typeof (IExtensionProfile), typeof (EvaluationActivity));

        public static readonly DependencyProperty ActionProfileProperty =
            DependencyProperty.Register("ActionProfile", typeof (IExtensionProfile), typeof (EvaluationActivity));

        public static readonly DependencyProperty LibraryProfileProperty =
            DependencyProperty.Register("LibraryProfile", typeof (IExtensionProfile), typeof (EvaluationActivity));

        public static readonly DependencyProperty ActiveViewProperty =
            DependencyProperty.Register("ActiveView", typeof (IAttachment), typeof (EvaluationActivity));

        public static readonly DependencyProperty ViewOutputProperty =
            DependencyProperty.Register("ViewOutput", typeof (string), typeof (EvaluationActivity));

        private readonly EvaluationActionContext _actionContext = new EvaluationActionContext();
        private bool _modelLock;

        private EvaluationViewContext _viewContext;

        public EvaluationActivity()
        {
            this.InitializeComponent();
        }

        public override Guid ProfileId
        {
            get { return Guid.Parse("f12576ba-7be3-4903-8620-090162e356cd"); }
        }

        public IExtensionProfile ViewProfile
        {
            get { return this.GetValue(ViewProfileProperty) as IExtensionProfile; }
            set { this.SetValue(ViewProfileProperty, value); }
        }

        public IExtensionProfile ActionProfile
        {
            get { return this.GetValue(ActionProfileProperty) as IExtensionProfile; }
            set { this.SetValue(ActionProfileProperty, value); }
        }

        public IExtensionProfile LibraryProfile
        {
            get { return this.GetValue(LibraryProfileProperty) as IExtensionProfile; }
            set { this.SetValue(LibraryProfileProperty, value); }
        }

        public IAttachment ActiveView
        {
            get { return this.GetValue(ActiveViewProperty) as IAttachment; }
            set { this.SetValue(ActiveViewProperty, value); }
        }

        public string ViewOutput
        {
            get { return this.GetValue(ViewOutputProperty) as string; }
            set { this.SetValue(ViewOutputProperty, value); }
        }

        protected override void OnInitialized(EventArgs e)
        {
            this._viewContext = new EvaluationViewContext(this._HelixModel, this._OxyModel);

            base.OnInitialized(e);
        }

        protected override void OnGuiResumeSimulation()
        {
            base.OnGuiResumeSimulation();

            this._viewContext.Reset();
        }

        private void LoadProfiles()
        {
            this.ViewProfile = this.Profiles.FirstOrDefault(p => p.Name == ViewProfileName) ??
                               this.CreateProfile(ViewProfileName);

            try
            {
                if (this.ActiveView == null)
                    this.ActiveView = this.ViewProfile.Attachments.FirstOrDefault();
            }
            catch (Exception ex)
            {
                OutputProvider.WriteException(ex);
                this.Host.Back();
            }

            if (this.ActionProfile == null)
                this.ActionProfile = this.Profiles.FirstOrDefault(p => p.Name == ActionProfileName) ??
                                     this.CreateProfile(ActionProfileName);

            if (this.LibraryProfile == null)
                this.LibraryProfile = this.Profiles.FirstOrDefault(p => p.Name == LibraryProfileName) ??
                                      this.CreateProfile(LibraryProfileName);
        }

        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            base.OnPropertyChanged(e);

            if (e.Property == ProfilesProperty)
            {
                this.LoadProfiles();
            }
        }

        protected override void OnUpdate()
        {
            base.OnUpdate();

            this.BuildView();
        }

        private void BuildView()
        {
            if (!this._modelLock && this.ActiveView != null)
            {
                this._modelLock = true;

                if (this.ActiveView != null && this.ActiveView.FileContent != null && this.LibraryProfile != null)
                {
                    this._viewContext.UpdateModel(this.ActualIteration,
                        Encoding.Default.GetString(this.ActiveView.FileContent),
                        this.LibraryProfile.Attachments.Where(a => a.FileContent != null).OrderBy(a => a.Name)
                            .Select(a => Encoding.Default.GetString(a.FileContent))
                            .ToArray());
                }

                this._modelLock = false;
            }
        }

        public override void Dispose()
        {
            base.Dispose();

            this._viewContext.Dispose();
            this._actionContext.Dispose();
        }

        private void HelixScriptProfileEditor_OnProfileChanged(object sender, RoutedEventArgs e)
        {
            this.SaveProfiles();

            this.BuildView();
        }

        private void ActionButton_OnClick(object sender, RoutedEventArgs e)
        {
            var action = (IAttachment) ((Button) (sender)).DataContext;

            if (action.FileContent != null)
            {
                this.PauseSimulation();
                this._actionContext.Execute(this.ActualIteration, Encoding.Default.GetString(action.FileContent),
                    this.LibraryProfile.Attachments.Where(a => a.FileContent != null).OrderBy(a => a.Name)
                        .Select(a => Encoding.Default.GetString(a.FileContent))
                        .ToArray());
                this.ResumeSimulation(this.ActualIteration);
            }
        }
    }
}