﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Media3D;

namespace RalphsDotNet.Apps.SimulationStudio.Activities
{
    public class HelixModel : ModelVisual3D
    {
        public static readonly DependencyProperty BackgroundProperty =
            DependencyProperty.Register("Background", typeof(Brush), typeof(HelixModel), new PropertyMetadata(new SolidColorBrush(Colors.DarkGray)));

        public SolidColorBrush Background
        {
            get { return this.GetValue(BackgroundProperty) as SolidColorBrush; }
            set { this.SetValue(BackgroundProperty, value); }
        }
    }
}
