﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Threading;
using Microsoft.Win32;
using RalphsDotNet.Apps.SimulationStudio.Activities;
using RalphsDotNet.Apps.SimulationStudio.Base;
using RalphsDotNet.Apps.SimulationStudio.Logic;
using RalphsDotNet.Gui.ActivityModel;
using RalphsDotNet.Service;
using RalphsDotNet.Service.Data.Interface;
using RalphsDotNet.Service.Simulation.Data;
using RalphsDotNet.Service.Simulation.Interface;
using RalphsDotNet.Service.Simulation.Model;
using RalphsDotNet.Gui.Wpf.ActivityEngine;

namespace RalphsDotNet.Apps.SimulationStudio
{
    /// <summary>
    ///     Interaction logic for App.xaml
    /// </summary>
    public partial class App : IApplication
    {
        private const string AppLogicId = "AppLogic";
        public PythonConsoleContext PythonConsoleContext { get; private set; }
        private readonly DispatcherTimer _iterationTimer;
        private IIteration _actualIteration;
        private IEnumerable<IExtensionProfile> _configurations;
        private IDataService _dataService;
        private bool _hasPendingChanges;
        private bool _isPaused;
        private bool _isReadOnly;
        private bool _isDebugging;
        private ISimulationDescription _simulation;
        private ISimulationService _simulationService;

        public App()
        {
            Debug.Listeners.Add(new Tracer());
            Console.SetOut(OutputProvider.Instance);

            this.PropertyChanged += this.App_PropertyChanged;

            this.PythonConsoleContext = new PythonConsoleContext();

            this._iterationTimer = new DispatcherTimer(DispatcherPriority.Background) {Interval = new TimeSpan(100)};
            this._iterationTimer.Tick += this._iterationTimer_Tick;
        }

        public new static App Current
        {
            get { return Application.Current as App; }
        }

        public ILogicService Logic { get; private set; }

        public bool IsReadOnly
        {
            get { return this._isReadOnly; }
            set
            {
                if (this._isReadOnly != value)
                {
                    this._isReadOnly = value;

                    this.OnPropertyChanged("IsReadOnly");
                }
            }
        }

        public bool IsDebugging
        {
            get { return this._isDebugging; }
            private set
            {
                if (this._isDebugging != value)
                {
                    this._isDebugging = value;

                    this.OnPropertyChanged("IsDebugging");
                }
            }
        }

        public bool HasPendingChanges
        {
            get { return this._hasPendingChanges; }
            set
            {
                if (this._hasPendingChanges != value)
                {
                    this._hasPendingChanges = value;

                    this.OnPropertyChanged("HasPendingChanges");
                }
            }
        }

        public bool IsClosingAllowed
        {
            get
            {

                if (this.HasPendingChanges)
                {
                    var result = MessageBox.Show("there are unsaved changes, save?", "unsaved changes",
                        MessageBoxButton.YesNoCancel, MessageBoxImage.Question);

                    if(result == MessageBoxResult.Yes)
                        this.Save();
                    else if (result == MessageBoxResult.Cancel)
                        return false;
                }

                return (this.Logic.SimulationService != null && this.Logic.SimulationService.IsRunning &&
                        MessageBox.Show("do you really want to stop the running simulation?", "simulation is running",
                            MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes) ||
                       this.Logic.SimulationService == null || !this.Logic.SimulationService.IsRunning;
            }
        }

        public ActivityReferenceCollection AddinReferences { get; private set; }

        public IEnumerable<IExtensionProfile> Configurations
        {
            get { return this._configurations; }
            private set
            {
                if (!Equals(this._configurations, value))
                {
                    this._configurations = value;

                    this.OnPropertyChanged("Configurations");
                }
            }
        }

        public ISimulationService SimulationService
        {
            get { return this._simulationService; }
            private set
            {
                if (!Equals(this._simulationService, value))
                {
                    this._simulationService = value;

                    this.OnPropertyChanged("SimulationService");
                }
            }
        }

        public IDataService DataService
        {
            get { return this._dataService; }
            private set
            {
                if (!Equals(this._dataService, value))
                {
                    this._dataService = value;

                    this.OnPropertyChanged("DataService");
                }
            }
        }

        public ISimulationDescription Simulation
        {
            get { return this._simulation; }
            private set
            {
                if (!Equals(this._simulation, value))
                {
                    this._simulation = value;

                    this.OnPropertyChanged("Simulation");
                }
            }
        }

        public IIteration ActualIteration
        {
            get { return this._actualIteration; }
            private set
            {
                if (!Equals(this._actualIteration, value))
                {
                    this._actualIteration = value;

                    this.OnPropertyChanged("ActualIteration");
                }
            }
        }

        public void CreateOrOpenProject(string sdbPath)
        {
            this.ServiceInvoke(this.Logic, () => this.Logic.CreateOrOpenProject(sdbPath));

            this.DataService = this.Logic.DataService;

            this.Reload();

            this.OnProjectOpened();
        }

        public void CloseProject()
        {
            if (this.SimulationService.IsRunning)
                this.StopSimulation();

            this.DataService = null;
            this.Simulation = null;

            this.ServiceInvoke(this.Logic, () => this.Logic.CloseProject());

            this.OnProjectClosed();
        }

        public void StartSimulation()
        {
            this.Save();
            this.ServiceInvoke(this.Logic, () => this.Logic.StartSimulation());

            this.OnSimulationStarted();
        }

        public void StartSimulation(string path)
        {
            this.ServiceInvoke(this.Logic, () => this.Logic.StartSimulation(path));

            this.OnSimulationStarted();
        }

        public void StopSimulation()
        {
            this.ServiceInvoke(this.Logic, () => this.Logic.StopSimulation());

            this.OnSimulationStopped();
        }

        public void DebugSimulation()
        {
            this.ServiceInvoke(this.Logic, () => this.Logic.DebugSimulation());

            this.OnSimulationStarted();
        }

        public void StepSimulation()
        {
            this.ServiceInvoke(this.Logic, () => this.Logic.StepSimulation());

            this.OnSimulationStepped();
        }

        public void StepSimulation(IIteration iteration)
        {
            this.ServiceInvoke(this.Logic, () => this.Logic.StepSimulation(iteration));

            this.OnSimulationStepped();
        }

        public void PauseSimulation()
        {
            this.ServiceInvoke(this.Logic, () => this.SimulationService.Pause());

            this.OnSimulationPaused();
        }

        public void ResumeSimulation()
        {
            this.ServiceInvoke(this.Logic, () => this.SimulationService.Resume());

            this.OnSimulationResumed();
        }

        public IExtensionProfile CreateConfiguration()
        {
            IExtensionProfile ret = null;
            this.ServiceInvoke(this.Logic, () => { ret = this.Logic.CreateConfiguration(); });

            if (ret == null)
                OutputProvider.WriteLine("nothing configured actually -> no configuration created");
            else
                this.Reload();

            return ret;
        }

        public IExtensionProfile CreateConfigurationSnapshot()
        {
            IExtensionProfile ret = null;
            this.ServiceInvoke(this.Logic, () => { ret = this.Logic.CreateConfigurationSnapshot(); });

            if (ret == null)
                OutputProvider.WriteLine("no simulation running -> no configuration created");
            else
                this.Reload();

            return ret;
        }

        public void LoadConfiguration(IExtensionProfile configuration)
        {
            this.Logic.LoadConfiguration(configuration);
            this.HasPendingChanges = true;
        }

        public void DeleteConfiguration(IExtensionProfile configuration)
        {
            this.Logic.DeleteConfiguration(configuration);
        }

        public void Save()
        {
            try
            {
                this.ServiceInvoke(this.Logic, () => this.Logic.Save());
                this.HasPendingChanges = false;
            }
            catch (Exception ex)
            {
                OutputProvider.WriteException(ex);
                this.Reload();
            }
        }

        public void SaveAndReload()
        {
            try
            {
                this.Save();
                this.Reload();
            }
            catch (Exception ex)
            {
                OutputProvider.WriteException(ex);
                this.Reload();
            }
        }

        public event SimulationStartedHandler SimulationStarted;
        public event SimulationStoppedHandler SimulationSopped;
        public event SimulationPausedHandler SimulationPaused;
        public event SimulationResumedHandler SimulationResumed;
        public event SimulationSteppedHandler SimulationStepped;

        public event ProjectOpenedHandler ProjectOpened;
        public event ProjectClosedHandler ProjectClosed;

        private void _iterationTimer_Tick(object sender, EventArgs e)
        {
            try
            {
                if (this.SimulationService.ErrorCode != 0)
                {
                    OutputProvider.WriteException(new SimulationException(this.SimulationService.ErrorCode));
                    this.SimulationService.ResetError();

                    this.OnSimulationPaused();
                }

                if (!this.SimulationService.IsRunning)
                {
                    this.OnSimulationStopped();
                }
                else
                {
                    this.IsDebugging = this.SimulationService.IsDebugging;
                    this.ActualIteration = this.SimulationService.ActualIteration;

                    if (this.SimulationService.IsPaused)
                    {
                        if (!this._isPaused)
                            this.OnSimulationPaused();
                    }
                    else
                    {
                        if (this._isPaused)
                            this.OnSimulationResumed();
                    }
                }
            }
            catch (Exception ex)
            {
                OutputProvider.WriteException(ex);
                this.StopSimulation();
            }
        }


        private void App_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Simulation")
                this.OnSimulationChanged();
            else if (e.PropertyName == "ActualIteration")
                this.OnActualIterationChanged();
        }

        protected virtual void OnSimulationChanged()
        {
            this.PythonConsoleContext.Simulation = this.Simulation;
        }

        protected virtual void OnActualIterationChanged()
        {
            this.PythonConsoleContext.ActualIteration = this.ActualIteration;
        }

        protected virtual void OnProjectOpened()
        {
            if (this.ProjectOpened != null)
                this.ProjectOpened();

            OutputProvider.WriteLine("project opened");
        }

        protected virtual void OnProjectClosed()
        {
            if (this.ProjectClosed != null)
                this.ProjectClosed();

            OutputProvider.WriteLine("project closed");
        }

        protected virtual void OnSimulationStarted()
        {
            if (this.SimulationStarted != null)
                this.SimulationStarted();

            this._iterationTimer.Start();

            OutputProvider.WriteLine("simulation started");
        }

        protected virtual void OnSimulationStopped()
        {
            this._iterationTimer.Stop();

            this.IsDebugging = false;
            this.ActualIteration = null;

            if (this.SimulationSopped != null)
                this.SimulationSopped();

            OutputProvider.WriteLine("simulation stoped");
        }

        protected virtual void OnSimulationPaused()
        {
            this._isPaused = true;

            if (this.SimulationPaused != null)
                this.SimulationPaused();

            OutputProvider.WriteLine("siumulation paused");
        }

        protected virtual void OnSimulationResumed()
        {
            this._isPaused = false;

            if (this.SimulationResumed != null)
                this.SimulationResumed();

            OutputProvider.WriteLine("siumulation resumed");
        }

        protected virtual void OnSimulationStepped()
        {
            if (this.SimulationStepped != null)
                this.SimulationStepped();

            OutputProvider.WriteLine("siumulation stepped");
        }

        public void Reload()
        {
            try
            {
                this.ServiceInvoke(this.Logic, () =>
                {
                    this.Simulation = null;
                    this.Configurations = null;
                    this.Simulation = this.Logic.Simulation;
                    this.Configurations = this.Logic.Configurations;
                });
            }
            catch (Exception ex)
            {
                OutputProvider.WriteException(ex);
            }
        }

        protected override void OnStartup(StartupEventArgs e)
        {
            this.ConfigureLogic(e.Args.Contains("--cpu"));

            this.LoadAddins();

            var args = e.Args.Where(a => a != "--cpu");

            base.OnStartup(e);

            if (args.Any())
            {
                if (e.Args.First().EndsWith(".sdbt"))
                {
                    var fd = new SaveFileDialog();
                    fd.Filter = MainActivity.SimulationDatabaseFilter;
                    fd.DefaultExt = MainActivity.SimulationDatabaseExtension;
                    if (fd.ShowDialog().GetValueOrDefault(false))
                    {
                        var template = e.Args.First();
                        var file = fd.FileName;

                        if (File.Exists(file))
                            File.Delete(file);

                        if (!string.IsNullOrEmpty(template))
                            File.Copy(template, file);

                        this.CreateOrOpenProject(file);
                    }
                }
                else
                {
                    this.CreateOrOpenProject(e.Args.First());
                }
            }
        }

        protected override bool OnClosing()
        {
            return this.IsClosingAllowed;
        }

        private void ConfigureLogic(bool useCpu)
        {
            ServiceProvider.ServiceConfigurations.AddOrReplace(new LogicServiceConfiguration
            {
                Id = AppLogicId,
                ServiceAssemblyName = typeof (LogicService).Assembly.GetName().Name,
                UseCpu = useCpu
            });
            this.Logic = ServiceProvider.GetService<ILogicService>(AppLogicId);

            this.SimulationService = this.Logic.SimulationService;
        }

        private void LoadAddins()
        {
            AppDomain.CurrentDomain.AssemblyResolve += this.CurrentDomain_AssemblyResolve;
            this.AddinReferences = new ActivityReferenceCollection();

            var addinsPath = Path.Combine(Path.GetDirectoryName(this.GetType().Assembly.Location), "Addins");

            if (Directory.Exists(addinsPath))
            {
                foreach (
                    var addinPath in
                        Directory.GetDirectories(addinsPath)
                            .Select(a => Directory.GetFiles(a, "addin.dll").FirstOrDefault())
                            .Where(a => !string.IsNullOrEmpty(a)))
                {
                    AppDomain.CurrentDomain.Load(AssemblyName.GetAssemblyName(addinPath));
                }
            }

            foreach (
                var ass in
                    AppDomain.CurrentDomain.GetAssemblies()
                        .Where(
                            a =>
                                a.GetCustomAttributes(typeof (AddinActivityAttribute), true)
                                    .OfType<AddinActivityAttribute>()
                                    .Any()))
            {
                var uri =
                    new Uri(
                        string.Format("pack://application:,,,/{0};component/Resources/Language/en.xaml",
                            ass.GetName().Name), UriKind.RelativeOrAbsolute);
                Application.Current.Resources.MergedDictionaries.Add(new ResourceDictionary {Source = uri});

                foreach (var attr in ass.GetCustomAttributes(true).OfType<AddinActivityAttribute>())
                    this.AddinReferences.Add(new ActivityReference
                    {
                        Activity = attr.ActivityType,
                        Name = attr.ActivityName,
                        IsEnabled = false
                    });
            }
        }

        private Assembly CurrentDomain_AssemblyResolve(object sender, ResolveEventArgs args)
        {
            var addinsPath = Path.Combine(Path.GetDirectoryName(this.GetType().Assembly.Location), "Addins");
            var assemblyName = new AssemblyName(args.Name).Name;

            if (assemblyName != "addin.resources" && Directory.Exists(addinsPath))
            {
                foreach (
                    var addinPath in
                        Directory.GetDirectories(addinsPath).Where(a => Directory.GetFiles(a, "addin.dll").Any()))
                {
                    var assemblyPath = Path.Combine(addinPath, assemblyName + ".dll");

                    if (File.Exists(assemblyPath))
                        return Assembly.LoadFrom(assemblyPath);
                }
            }

            return null;
        }

        protected override void OnExit(ExitEventArgs e)
        {
            this.Logic.Dispose();
            ServiceProvider.ServiceConfigurations.Remove(AppLogicId);

            base.OnExit(e);
        }
    }
}