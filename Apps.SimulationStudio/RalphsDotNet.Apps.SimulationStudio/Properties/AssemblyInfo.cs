﻿using System.Reflection;
using RalphsDotNet.Apps.SimulationStudio.Activities;
using RalphsDotNet.Gui.ActivityModel;

[assembly: AssemblyTitle("Simulation Studio")]
[assembly: AssemblyProduct("Simulation Studio")]

[assembly: ActivityHostConfiguration("HeaderImageSource", "HeaderTextString", "SubHeaderTextString", "FooterTextString", typeof(MainActivity))]