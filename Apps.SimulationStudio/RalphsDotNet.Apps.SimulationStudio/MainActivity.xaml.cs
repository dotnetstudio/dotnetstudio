﻿using System;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using Microsoft.Win32;
using RalphsDotNet.Apps.SimulationStudio.Base;
using RalphsDotNet.Apps.SimulationStudio.Controls;
using RalphsDotNet.Apps.SimulationStudio.Logic;
using RalphsDotNet.Data;
using RalphsDotNet.Gui.ActivityModel;
using RalphsDotNet.Service.Simulation.Data;
using RalphsDotNet.Gui.Wpf.ActivityEngine;
using Xceed.Wpf.AvalonDock.Layout;

namespace RalphsDotNet.Apps.SimulationStudio.Activities
{
    /// <summary>
    ///     Interaction logic for MainActivity.xaml
    /// </summary>
    public partial class MainActivity
    {
        public const string SimulationDatabaseFilter = "Simulation Database (*.sdb)|*.sdb";
        public const string SimulationDatabaseExtension = "*.sdb";

        public const string SimulationDatabaseTemplateFilter = "Simulation Database (*.sdbt)|*.sdbt";
        public const string SimulationDatabaseTemplateExtension = "*.sdbt";

        private const string SimulationContainerStreamFilter = "Simulation Container Stream (*.scs)|*.scs";
        private const string SimulationContainerStreamExtension = "*.scs";
        
        public static readonly DependencyProperty SimulationDescriptionTypeProperty =
            DependencyProperty.Register("SimulationDescriptionType", typeof (Type), typeof (MainActivity));

        public static readonly DependencyProperty SystemDescriptionTypeProperty =
            DependencyProperty.Register("SystemDescriptionType", typeof (Type), typeof (MainActivity));

        public static readonly DependencyProperty NumberPropertyDescriptionTypeProperty =
            DependencyProperty.Register("NumberPropertyDescriptionType", typeof (Type), typeof (MainActivity));

        private readonly ActivityReferenceBase _newProjectReference;
        private readonly ActivityReferenceBase _newProjectFromTemplateReference;
        private readonly ActivityReferenceBase _openProjectReference;
        private readonly ActivityReferenceBase _closeProjectReference;
        private readonly ActivityReferenceBase _saveProjectReference;
        private readonly ActivityReferenceBase _startSimulationReference;
        private readonly ActivityReferenceBase _debugSimulationReference;
        private readonly ActivityReferenceBase _recordSimulationReference;
        private readonly ActivityReferenceBase _pauseSimulationReference;
        private readonly ActivityReferenceBase _resumeSimulationReference;
        private readonly ActivityReferenceBase _stepSimulationReference;
        private readonly ActivityReferenceBase _stopSimulationReference;
        private readonly ActivityReferenceBase _resetSimulationReference;
        private readonly ActivityReferenceBase _createConfigurationReference;

        public MainActivity()
        {
            this.SimulationDescriptionType = ModelEmitter.ResolveType(typeof (ISimulationDescription));
            this.SystemDescriptionType = ModelEmitter.ResolveType(typeof (ISystemDescription));
            this.NumberPropertyDescriptionType = ModelEmitter.ResolveType(typeof (INumberPropertyDescription));

            this.InitializeComponent();

            this._newProjectReference = new ProgressActivityReference
            {
                IsEnabled = true,
                Name = "MainActivityNewProjectString",
                ProgressAction = this.NewProjectAction,
            };

            this._newProjectFromTemplateReference = new ProgressActivityReference
            {
                IsEnabled = true,
                Name = "MainActivityNewProjectFromTemplateString",
                ProgressAction = this.NewProjectFromTemplateAction,
            };

            this._openProjectReference = new ProgressActivityReference
            {
                IsEnabled = true,
                Name = "MainActivityOpenProjectString",
                ProgressAction = this.OpenProjectAction
            };

            this._closeProjectReference = new ProgressActivityReference
            {
                IsEnabled = false,
                Name = "MainActivityCloseProjectString",
                ProgressAction = this.CloseProjectAction
            };

            this._saveProjectReference = new ProgressActivityReference
            {
                IsEnabled = false,
                Name = "MainActivitySaveProjectString",
                ProgressAction = this.SaveProject
            };

            this._startSimulationReference = new ProgressActivityReference
            {
                IsEnabled = false,
                Name = "MainActivityStartSimulationString",
                ProgressAction = this.StartSimulationAction
            };

            this._debugSimulationReference = new ProgressActivityReference
            {
                IsEnabled = false,
                Name = "MainActivityDebugSimulationString",
                ProgressAction = this.DebugSimulationAction
            };

            this._recordSimulationReference = new ProgressActivityReference
            {
                IsEnabled = false,
                Name = "MainActivityRecordSimulationString",
                ProgressAction = this.RecordSimulationAction
            };

            this._pauseSimulationReference = new ProgressActivityReference
            {
                IsEnabled = false,
                Name = "AddinActivityPauseSimulationString",
                ProgressAction = this.PauseSimulation
            };

            this._resumeSimulationReference = new ProgressActivityReference
            {
                IsEnabled = false,
                Name = "AddinActivityResumeSimulationString",
                ProgressAction = this.ResumeSimulation
            };

            this._stepSimulationReference = new ProgressActivityReference
            {
                IsEnabled = false,
                Name = "MainActivityStepSimulationString",
                ProgressAction = this.StepSimulation
            };

            this._stopSimulationReference = new ProgressActivityReference
            {
                IsEnabled = false,
                Name = "MainActivityStopSimulationString",
                ProgressAction = this.StopSimulationAction
            };

            this._resetSimulationReference = new ProgressActivityReference
            {
                IsEnabled = false,
                Name = "AddinActivityResetSimulationString",
                ProgressAction = this.ResetSimulation
            };

            this._createConfigurationReference = new ProgressActivityReference
            {
                IsEnabled = false,
                Name = "AddinActivityCreateConfigurationString",
                ProgressAction = this.CreateConfiguration
            };
            
            this.References = new ActivityReferenceCollection
            {
                this._newProjectReference,
                this._newProjectFromTemplateReference,
                this._openProjectReference,
                this._closeProjectReference,
                this._saveProjectReference,
                this._startSimulationReference,
                this._debugSimulationReference,
                this._recordSimulationReference,
                this._pauseSimulationReference,
                this._resumeSimulationReference,
                this._stepSimulationReference,
                this._stopSimulationReference,
                this._resetSimulationReference,
                this._createConfigurationReference
            };

            foreach (var r in this.App.AddinReferences)
                this.References.Add(r);

            this.App.SimulationStarted += this.App_SimulationStarted;
            this.App.SimulationPaused += this.App_SimulationPaused;
            this.App.SimulationResumed += this.App_SimulationResumed;
            this.App.SimulationSopped += this.App_SimulationSopped;
            this.App.ProjectOpened += App_ProjectOpened;
            this.App.ProjectClosed += App_ProjectClosed;
        }

        private void SaveProject(ProgressActivityReference activityref, ProgressHandler handler)
        {
            handler.Text = "SavingProgressString";
            handler.IsIndeterminate = true;

            this.App.Save();
        }

        void App_ProjectOpened()
        {

            this._newProjectReference.IsEnabled = false;
            this._newProjectFromTemplateReference.IsEnabled = false;
            this._openProjectReference.IsEnabled = false;
            this._closeProjectReference.IsEnabled = true;
            this._startSimulationReference.IsEnabled = true;
            this._debugSimulationReference.IsEnabled = true;
            this._recordSimulationReference.IsEnabled = true;
            this._saveProjectReference.IsEnabled = true;
        }

        void App_ProjectClosed()
        {
            this.OnGuiCloseProject();
        }

        private void OnGuiCloseProject()
        {
            this.CloseDocuments();

            this._newProjectReference.IsEnabled = true;
            this._newProjectFromTemplateReference.IsEnabled = true;
            this._openProjectReference.IsEnabled = true;
            this._closeProjectReference.IsEnabled = false;
            this._startSimulationReference.IsEnabled = false;
            this._debugSimulationReference.IsEnabled = false;
            this._recordSimulationReference.IsEnabled = false;
            this._pauseSimulationReference.IsEnabled = false;
            this._resumeSimulationReference.IsEnabled = false;
            this._stepSimulationReference.IsEnabled = false;
            this._stopSimulationReference.IsEnabled = false;
            this._resetSimulationReference.IsEnabled = false;
            this._createConfigurationReference.IsEnabled = false;
            this._saveProjectReference.IsEnabled = false;

            this.HideAddins();
        }

        public Type SimulationDescriptionType
        {
            get { return this.GetValue(SimulationDescriptionTypeProperty) as Type; }
            set { this.SetValue(SimulationDescriptionTypeProperty, value); }
        }

        public Type SystemDescriptionType
        {
            get { return this.GetValue(SystemDescriptionTypeProperty) as Type; }
            set { this.SetValue(SystemDescriptionTypeProperty, value); }
        }

        public Type NumberPropertyDescriptionType
        {
            get { return this.GetValue(NumberPropertyDescriptionTypeProperty) as Type; }
            set { this.SetValue(NumberPropertyDescriptionTypeProperty, value); }
        }

        public IApplication App
        {
            get { return Application.Current as IApplication; }
        }

        public void CreateOrOpenProject(string file, string template, bool deleteIfExists)
        {
            if (deleteIfExists && File.Exists(file))
                File.Delete(file);

            if (!string.IsNullOrEmpty(template))
                File.Copy(template, file);

            this.App.CreateOrOpenProject(file);
        }

        private void App_SimulationStarted()
        {
            this._startSimulationReference.IsEnabled = false;
            this._debugSimulationReference.IsEnabled = false;
            this._recordSimulationReference.IsEnabled = false;
            this._pauseSimulationReference.IsEnabled = true;
            this._resumeSimulationReference.IsEnabled = false;
            this._stepSimulationReference.IsEnabled = false;
            this._stopSimulationReference.IsEnabled = true;
            this._resetSimulationReference.IsEnabled = true;
            this._createConfigurationReference.IsEnabled = true;

            this.ShowAddins();

            this.App.IsReadOnly = true;

            if (!this.App.SimulationService.IsDebugging && this.App.AddinReferences.Count > 0)
                this.Host.LoadActivity(this.App.AddinReferences.OfType<ActivityReference>().First().Activity);

            this._InfoPane.Show();
        }

        private void App_SimulationPaused()
        {
            this._pauseSimulationReference.IsEnabled = false;
            if (!this.App.SimulationService.IsDebugging) this._resumeSimulationReference.IsEnabled = true;
            this._stepSimulationReference.IsEnabled = true;
        }

        private void App_SimulationResumed()
        {
            this._pauseSimulationReference.IsEnabled = true;
            this._resumeSimulationReference.IsEnabled = false;
            this._stepSimulationReference.IsEnabled = false;
        }

        private void App_SimulationSopped()
        {
            this.OnGuiStopSimulation();
        }

        private void OnGuiStopSimulation()
        {
            this._startSimulationReference.IsEnabled = true;
            this._debugSimulationReference.IsEnabled = true;
            this._recordSimulationReference.IsEnabled = true;
            this._pauseSimulationReference.IsEnabled = false;
            this._resumeSimulationReference.IsEnabled = false;
            this._stepSimulationReference.IsEnabled = false;
            this._stopSimulationReference.IsEnabled = false;
            this._resetSimulationReference.IsEnabled = false;
            this._createConfigurationReference.IsEnabled = false;

            this.HideAddins();

            this._InfoPane.Hide();

            this.App.IsReadOnly = false;
        }

        protected override void OnInitialized(EventArgs e)
        {
            base.OnInitialized(e);

            this._InfoPane.Hide();
        }

        private void CloseDocuments(ILayoutContainer container = null)
        {
            if (container == null)
                container = this._LayoutRoot;

            foreach (var child in container.Children.ToArray())
            {
                if (child as LayoutDocument != null)
                    ((LayoutDocument) child).Close();
                else if (child as ILayoutContainer != null)
                    this.CloseDocuments(child as ILayoutContainer);
            }
        }

        private LayoutDocumentPane GetDocumentPane(ILayoutContainer container = null)
        {
            if (container == null)
                container = this._LayoutRoot;

            LayoutDocumentPane documentPane = null;

            foreach (var child in container.Children.ToArray())
            {
                if (child as LayoutDocumentPane != null)
                    return child as LayoutDocumentPane;

                if (child as ILayoutContainer != null)
                {
                    documentPane = this.GetDocumentPane(child as ILayoutContainer);
                    if (documentPane != null) break;
                }
            }

            return documentPane;
        }

        private LayoutDocument GetDocument(object dataContext, ILayoutContainer container = null)
        {
            if (container == null)
                container = this._LayoutRoot;

            LayoutDocument document = null;

            foreach (var child in container.Children.ToArray())
            {
                if (child as LayoutDocument != null &&
                    ((FrameworkElement) ((LayoutDocument) child).Content).DataContext == dataContext)
                    return child as LayoutDocument;

                if (child as ILayoutContainer != null)
                {
                    document = this.GetDocument(dataContext, child as ILayoutContainer);
                    if (document != null) break;
                }
            }

            return document;
        }

        private void NewProjectAction(ProgressActivityReference activityref, ProgressHandler handler)
        {
            handler.IsIndeterminate = true;
            handler.Text = "MainActivityNewProjectProgressString";

            this.CreateOrOpenProject(new SaveFileDialog(), null, true);
        }

        private void NewProjectFromTemplateAction(ProgressActivityReference activityref, ProgressHandler handler)
        {
            handler.IsIndeterminate = true;
            handler.Text = "MainActivityNewProjectProgressString";

            var ofd = new OpenFileDialog {Filter = SimulationDatabaseTemplateFilter, DefaultExt = SimulationDatabaseTemplateExtension};
            if (ofd.ShowDialog().GetValueOrDefault(false))
            {
                this.CreateOrOpenProject(new SaveFileDialog(), ofd.FileName, true);
            }
        }

        private void OpenProjectAction(ProgressActivityReference activityref, ProgressHandler handler)
        {
            handler.IsIndeterminate = true;
            handler.Text = "MainActivityOpenProjectProgressString";

            this.CreateOrOpenProject(new OpenFileDialog(), null, false);
        }

        private void CloseProjectAction(ProgressActivityReference activityref, ProgressHandler handler)
        {
            handler.IsIndeterminate = true;
            handler.Text = "MainActivityCloseProjectProgressString";

            var isClosingAllowed = this.App.IsClosingAllowed;
            if (isClosingAllowed)
            {
                this.App.CloseProject();
            }
        }

        private void ShowAddins()
        {
            foreach (var r in this.App.AddinReferences)
                r.IsEnabled = true;
        }

        private void HideAddins()
        {
            foreach (var r in this.App.AddinReferences)
                r.IsEnabled = false;
        }

        private void CreateOrOpenProject(FileDialog fd, string template, bool deleteIfExists)
        {
            try
            {
                fd.Filter = SimulationDatabaseFilter;
                fd.DefaultExt = SimulationDatabaseExtension;
                if (fd.ShowDialog().GetValueOrDefault(false))
                {
                    this.CreateOrOpenProject(fd.FileName, template, deleteIfExists);
                }
            }
            catch (Exception ex)
            {
                this.OnGuiCloseProject();

                OutputProvider.WriteLine("got an error on opening project");
                OutputProvider.WriteException(ex);
            }
        }

        private void StartSimulationAction(ProgressActivityReference activityref, ProgressHandler handler)
        {
            try
            {
                handler.Text = "MainActivityStartSimulationProgressString";
                handler.IsIndeterminate = true;

                this.App.StartSimulation();
            }
            catch (Exception ex)
            {
                this.OnGuiStopSimulation();

                OutputProvider.WriteLine("got an error on starting simulation");
                OutputProvider.WriteException(ex);
            }
        }

        private void DebugSimulationAction(ProgressActivityReference activityref, ProgressHandler handler)
        {
            handler.IsIndeterminate = true;
            handler.Text = "MainActivityDebugSimulationProgressString";

            try
            {
                this.App.DebugSimulation();
            }
            catch (Exception ex)
            {
                this.OnGuiStopSimulation();

                OutputProvider.WriteLine("got an error on starting simulation");
                OutputProvider.WriteException(ex);
            }
        }

        private void RecordSimulationAction(ProgressActivityReference activityref, ProgressHandler handler)
        {
            handler.IsIndeterminate = true;
            handler.Text = "MainActivityRecordSimulationProgressString";

            try
            {
                var fd = new SaveFileDialog
                {
                    Filter = SimulationContainerStreamFilter,
                    DefaultExt = SimulationContainerStreamExtension
                };
                if (fd.ShowDialog().GetValueOrDefault(false))
                {
                    if (File.Exists(fd.FileName))
                        File.Delete(fd.FileName);

                    this.App.StartSimulation(fd.FileName);
                }
            }
            catch (Exception ex)
            {
                this.OnGuiStopSimulation();

                OutputProvider.WriteLine("got an error on starting simulation");
                OutputProvider.WriteException(ex);
            }
        }

//RecordSimulationAction

        private void StopSimulationAction(ProgressActivityReference activityref, ProgressHandler handler)
        {
            handler.IsIndeterminate = true;
            handler.Text = "MainActivityStopSimulationProgressString";

            this.App.StopSimulation();
        }

        private void ResetSimulation(ProgressActivityReference activityref, ProgressHandler handler)
        {
            handler.IsIndeterminate = true;
            handler.Text = "AddinActivityResetSimulationProgressString";

            this.ResetSimulation();
        }

        private void CreateConfiguration(ProgressActivityReference activityref, ProgressHandler handler)
        {
            handler.IsIndeterminate = true;
            handler.Text = "AddinActivityCreateConfigurationProgressString";

            this.App.CreateConfigurationSnapshot();
        }

        private void ResetSimulation()
        {
            var isDebugging = this.App.SimulationService.IsDebugging;
            this.App.StopSimulation();

            if (isDebugging)
                this.App.DebugSimulation();
            else
                this.App.StartSimulation();
        }

        private void ResumeSimulation(ProgressActivityReference activityref, ProgressHandler handler)
        {
            handler.IsIndeterminate = true;
            handler.Text = "AddinActivityResumeSimulationProgressString";

            this.App.ResumeSimulation();
        }

        private void StepSimulation(ProgressActivityReference activityref, ProgressHandler handler)
        {
            handler.IsIndeterminate = true;
            handler.Text = "AddinActivityStepSimulationProgressString";

            this.App.StepSimulation();
        }

        private void PauseSimulation(ProgressActivityReference activityref, ProgressHandler handler)
        {
            handler.IsIndeterminate = true;
            handler.Text = "AddinActivityPauseSimulationProgressString";

            this.App.PauseSimulation();
        }

        private void SimulationDescriptionNavigationControl_OnEditClick(object sender, ItemEditEventArgs args)
        {
            var documentPane = this.GetDocumentPane();
            var doc = this.GetDocument(args.DataObject);

            if (doc == null)
            {
                doc = new LayoutDocument();
                doc.Closing += Doc_Closing;

                if (args.DataObject as ISimulationDescription != null)
                {
                    doc.Title = "Simulation";
                    doc.Content = new SimulationDescriptionEditControl {DataContext = args.DataObject};
                }
                else if (args.DataObject as ISystemDescription != null)
                {
                    doc.Title = ((ISystemDescription) args.DataObject).Name;
                    doc.Content = new SystemDescriptionEditControl {DataContext = args.DataObject};
                }
                else if (args.DataObject as ISimulationRawScript != null)
                {
                    doc.Title = ((ISimulationRawScript) args.DataObject).Name;
                    doc.Content = new SimulationScriptEditControl {DataContext = args.DataObject};
                }
                else if (args.DataObject as ISimulationScript != null)
                {
                    doc.Title = ((ISimulationScript)args.DataObject).Name;
                    doc.Content = new SimulationScriptEditControl { DataContext = args.DataObject };
                }
                else if (args.DataObject as IAttachment != null)
                {
                    doc.Title = ((IAttachment)args.DataObject).Name;
                    doc.Content = new AttachmentEditControl { DataContext = args.DataObject };
                }
                else if (args.DataObject as INumberPropertyDescription != null)
                {
                    var obj = (INumberPropertyDescription) args.DataObject;
                    doc.Title = string.Format("{0} - {1}", obj.SystemDescription.Name, obj.Name);
                    doc.Content = new NumberPropertyDescriptionEditControl {DataContext = args.DataObject};
                }
                else if (args.DataObject as ISystemPrepareInteractionScriptMapping != null)
                {
                    var obj = (ISystemPrepareInteractionScriptMapping) args.DataObject;
                    doc.Title = string.Format("{0} - {1}", obj.SystemDescription.Name, obj.Name);
                    doc.Content = new SystemPrepareInteractionScriptEditControl {DataContext = args.DataObject};
                }
                else if (args.DataObject as ISystemInteractionScriptMapping != null)
                {
                    var obj = (ISystemInteractionScriptMapping) args.DataObject;
                    doc.Title = string.Format("{0} - {1}", obj.SystemDescription.Name, obj.Name);
                    doc.Content = new SystemInteractionScriptEditControl {DataContext = args.DataObject};
                }
                else if (args.DataObject as ISystemModifyScriptMapping != null)
                {
                    var obj = (ISystemModifyScriptMapping) args.DataObject;
                    doc.Title = string.Format("{0} - {1}", obj.SystemDescription.Name, obj.Name);
                    doc.Content = new SystemModifyScriptEditControl {DataContext = args.DataObject};
                }
                else if (args.DataObject as IExtensionProfile != null &&
                         (args.DataObject as IExtensionProfile).ProfileId == LogicService.ConfigurationProfileId)
                {
                    doc.Title = ((IExtensionProfile) args.DataObject).Name;
                    doc.Content = new ConfigurationEditControl {DataContext = args.DataObject};
                }

                if (doc != null)
                {
                    documentPane.Children.Add(doc);

                    if (doc.Content as SimulationScriptEditControl != null)
                    {
                        var dBinding = new Binding("IsDebugging") {Source = this.App};
                        ((EditControl) doc.Content).SetBinding(
                            SimulationScriptEditControl.IsDebuggingProperty, dBinding);
                        var aiBinding = new Binding("ActualIteration") {Source = this.App};
                        ((EditControl) doc.Content).SetBinding(
                            SimulationScriptEditControl.DebuggingActualIterationProperty, aiBinding);
                    }
                }
            }

            if (doc != null)
                doc.IsActive = true;
        }

        private void SimulationDescriptionControl_OnDeleteClick(object sender, ItemDeleteEventArgs args)
        {
            var doc = this.GetDocument(args.DataObject);

            if (doc != null)
                doc.Close();
        }

        private static void Doc_Closing(object sender, CancelEventArgs e)
        {
            if (((LayoutDocument) sender).Content as IDisposable != null)
                (((LayoutDocument) sender).Content as IDisposable).Dispose();
        }
    }
}