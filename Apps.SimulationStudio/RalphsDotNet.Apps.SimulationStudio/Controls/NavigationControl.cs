﻿using System.Windows;
using System.Windows.Controls;
using RalphsDotNet.Apps.SimulationStudio.Base;
using IDataObject = RalphsDotNet.Data.IDataObject;

namespace RalphsDotNet.Apps.SimulationStudio.Controls
{
    public class NavigationControl : UserControl
    {
        public IApplication App
        {
            get { return Application.Current as IApplication; }
        }

        public event ItemEditHandler EditClick;

        public event ItemDeleteHandler DeleteClick;
        
        protected void OnEdit(IDataObject obj)
        {
            this.OnEdit(this, new ItemEditEventArgs(obj));
        }
        protected void OnDelete(IDataObject obj)
        {
            this.App.HasPendingChanges = true;
            this.App.Reload();
            this.OnDelete(this, new ItemDeleteEventArgs(obj));
        }

        protected void OnEdit(object sender, ItemEditEventArgs e)
        {
            if (this.EditClick != null)
                this.EditClick(sender, e);
        }

        protected void OnDelete(object sender, ItemDeleteEventArgs e)
        {
            if (this.DeleteClick != null)
                this.DeleteClick(sender, e);
        }

        protected bool AskReallyDelete()
        {
            return
                MessageBox.Show(this.FindResource("EditControlSureQuestionText") as string,
                    this.FindResource("EditControlSureQuestionTitle") as string, MessageBoxButton.YesNo,
                    MessageBoxImage.Question) ==
                MessageBoxResult.Yes;
        }
    }
}