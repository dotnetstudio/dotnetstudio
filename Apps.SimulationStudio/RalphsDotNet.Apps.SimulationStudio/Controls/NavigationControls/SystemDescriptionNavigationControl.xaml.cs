﻿using System;
using System.Windows;
using RalphsDotNet.Service.Simulation.Data;

namespace RalphsDotNet.Apps.SimulationStudio.Controls
{
    /// <summary>
    ///     Interaction logic for SystemDescriptionNavigationControl.xaml
    /// </summary>
    public partial class SystemDescriptionNavigationControl
    {
        private INumberPropertyDescriptionRepository _numberPropertyDescriptionRepository;
        private ISystemInteractionScriptMappingRepository _systemInteractionScriptMappingRepository;
        private ISystemModifyScriptMappingRepository _systemModifyScriptMappingRepository;
        private ISystemPrepareInteractionScriptMappingRepository _systemPrepareInteractionScriptMappingRepository;

        public SystemDescriptionNavigationControl()
        {
            this.InitializeComponent();
        }

        public INumberPropertyDescriptionRepository NumberPropertyDescriptionRepository
        {
            get
            {
                if (this._numberPropertyDescriptionRepository == null && this.App.DataService != null)
                {
                    this._numberPropertyDescriptionRepository =
                        this.App.DataService
                            .GetRepository<INumberPropertyDescription, INumberPropertyDescriptionRepository>();
                }

                return this._numberPropertyDescriptionRepository;
            }
        }

        public ISystemPrepareInteractionScriptMappingRepository SystemPrepareInteractionScriptMappingRepository
        {
            get
            {
                if (this._systemPrepareInteractionScriptMappingRepository == null && this.App.DataService != null)
                {
                    this._systemPrepareInteractionScriptMappingRepository =
                        this.App.DataService
                            .GetRepository
                            <ISystemPrepareInteractionScriptMapping, ISystemPrepareInteractionScriptMappingRepository>();
                }

                return this._systemPrepareInteractionScriptMappingRepository;
            }
        }

        public ISystemInteractionScriptMappingRepository SystemInteractionScriptMappingRepository
        {
            get
            {
                if (this._systemInteractionScriptMappingRepository == null && this.App.DataService != null)
                {
                    this._systemInteractionScriptMappingRepository =
                        this.App.DataService
                            .GetRepository<ISystemInteractionScriptMapping, ISystemInteractionScriptMappingRepository>();
                }

                return this._systemInteractionScriptMappingRepository;
            }
        }

        public ISystemModifyScriptMappingRepository SystemModifyScriptMappingRepository
        {
            get
            {
                if (this._systemModifyScriptMappingRepository == null && this.App.DataService != null)
                {
                    this._systemModifyScriptMappingRepository =
                        this.App.DataService
                            .GetRepository<ISystemModifyScriptMapping, ISystemModifyScriptMappingRepository>();
                }

                return this._systemModifyScriptMappingRepository;
            }
        }

        public ISystemDescription SystemDescription
        {
            get { return this.DataContext as ISystemDescription; }
        }

        private void AddNumberPropertyButton_OnClick(object sender, RoutedEventArgs e)
        {
            if (this.SystemDescription != null)
            {
                var obj = this.NumberPropertyDescriptionRepository.Create(Guid.NewGuid().ToString(),
                    this.SystemDescription);
                this.OnEdit(obj);
            }
        }

        private void EditButton_OnClick(object sender, RoutedEventArgs e)
        {
            this.OnEdit(this.SystemDescription);
        }

        private void DeleteButton_OnClick(object sender, RoutedEventArgs e)
        {
            if (this.AskReallyDelete())
            {
                var systemDescription = this.SystemDescription;
                this.App.Simulation.SystemDescriptions.Remove(systemDescription);
                this.OnDelete(systemDescription);
            }
        }

        private void ChildControl_OnEditClick(object sender, ItemEditEventArgs args)
        {
            this.OnEdit(this, args);
        }

        private void AddSystemPrepareInteractionScriptButton_OnClick(object sender, RoutedEventArgs e)
        {
            var obj = this.SystemPrepareInteractionScriptMappingRepository.Create(Guid.NewGuid().ToString(),
                this.SystemDescription);
            this.OnEdit(obj);
        }

        private void AddSystemInteractionScriptButton_OnClick(object sender, RoutedEventArgs e)
        {
            var obj = this.SystemInteractionScriptMappingRepository.Create(Guid.NewGuid().ToString(),
                this.SystemDescription);
            this.OnEdit(obj);
        }

        private void AddSystemModifyScriptButton_OnClick(object sender, RoutedEventArgs e)
        {
            var obj = this.SystemModifyScriptMappingRepository.Create(Guid.NewGuid().ToString(), this.SystemDescription);
            this.OnEdit(obj);
        }

        private void ChildControl_OnDeleteClick(object sender, ItemDeleteEventArgs args)
        {
            this.OnDelete(this, args);
        }
    }
}