﻿using System;
using System.Windows;
using RalphsDotNet.Data;
using RalphsDotNet.Service.Simulation.Data;

namespace RalphsDotNet.Apps.SimulationStudio.Controls
{
    /// <summary>
    ///     Interaction logic for SimulationDescriptionNavigationControl.xaml
    /// </summary>
    public partial class SimulationDescriptionNavigationControl
    {
        private ISimulationRawScriptRepository _rawScriptRepository;
        private IAttachmentRepository _attachmentRepository;
        private ISimulationSystemScriptRepository _scriptRepository;
        private ISystemDescriptionRepository _systemDescriptionRepository;

        public SimulationDescriptionNavigationControl()
        {
            this.InitializeComponent();
        }

        public ISystemDescriptionRepository SystemDescriptionRepository
        {
            get
            {
                if (this._systemDescriptionRepository == null && this.App.DataService != null)
                {
                    this._systemDescriptionRepository =
                        this.App.DataService.GetRepository<ISystemDescription, ISystemDescriptionRepository>();
                }

                return this._systemDescriptionRepository;
            }
        }

        public ISimulationSystemScriptRepository SystemScriptRepository
        {
            get
            {
                if (this._scriptRepository == null && this.App.DataService != null)
                {
                    this._scriptRepository =
                        this.App.DataService.GetRepository<ISimulationSystemScript, ISimulationSystemScriptRepository>();
                }

                return this._scriptRepository;
            }
        }

        public ISimulationRawScriptRepository RawScriptRepository
        {
            get
            {
                if (this._rawScriptRepository == null && this.App.DataService != null)
                {
                    this._rawScriptRepository =
                        this.App.DataService.GetRepository<ISimulationRawScript, ISimulationRawScriptRepository>();
                }

                return this._rawScriptRepository;
            }
        }

        public IAttachmentRepository AttachmentRepository
        {
            get
            {
                if (this._attachmentRepository == null && this.App.DataService != null)
                {
                    this._attachmentRepository =
                        this.App.DataService.GetRepository<IAttachment, IAttachmentRepository>();
                }

                return this._attachmentRepository;
            }
        }

        public ISimulationDescription SimulationDescription
        {
            get { return this.DataContext as ISimulationDescription; }
        }

        private void AddSystemButton_OnClick(object sender, RoutedEventArgs e)
        {
            var obj = this.SystemDescriptionRepository.Create(this.SimulationDescription, Guid.NewGuid().ToString());
            this.OnEdit(obj);
        }

        private void AddSystemScriptButton_OnClick(object sender, RoutedEventArgs e)
        {
            var obj = this.SystemScriptRepository.Create(Guid.NewGuid().ToString(), this.App.Simulation, string.Empty);
            this.OnEdit(obj);
        }

        private void AddRawScriptButton_OnClick(object sender, RoutedEventArgs e)
        {
            var obj = this.RawScriptRepository.Create(Guid.NewGuid().ToString(), this.App.Simulation, string.Empty);
            this.OnEdit(obj);
        }

        private void AddConfigurationButton_OnClick(object sender, RoutedEventArgs e)
        {
            var obj = this.App.CreateConfiguration();
            this.OnEdit(obj);
        }

        private void EditButton_OnClick(object sender, RoutedEventArgs e)
        {
            this.OnEdit(this.SimulationDescription);
        }

        private void ScriptNavigationControl_OnEditClick(object sender, ItemEditEventArgs args)
        {
            this.OnEdit(this, args);
        }

        private void ChildControl_OnEditClick(object sender, ItemEditEventArgs args)
        {
            this.OnEdit(this, args);
        }

        private void ChildControl_OnDeleteClick(object sender, ItemDeleteEventArgs args)
        {
            this.OnDelete(this, args);
        }

        private void ScriptNavigationControl_OnDeleteClick(object sender, ItemDeleteEventArgs args)
        {
            this.OnDelete(this, args);
        }

        private void AddAttachmentButton_OnClick(object sender, RoutedEventArgs e)
        {
            var obj = this.AttachmentRepository.Create(Guid.NewGuid().ToString(), string.Empty);
            this.SimulationDescription.Attachments.Add(obj);
            this.OnEdit(obj);
        }
    }
}