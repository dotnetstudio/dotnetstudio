﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows;
using Microsoft.Win32;
using RalphsDotNet.Data;
using RalphsDotNet.Gui.Wpf.ActivityEngine;

namespace RalphsDotNet.Apps.SimulationStudio.Controls
{
    /// <summary>
    ///     Interaction logic for SystemDescriptionNavigationControl.xaml
    /// </summary>
    public partial class AttachmentNavigationControl
    {
        public AttachmentNavigationControl()
        {
            this.InitializeComponent();
        }

        public IAttachment Attachment
        {
            get { return this.DataContext as IAttachment; }
        }

        private void EditButton_OnClick(object sender, RoutedEventArgs e)
        {
            this.OnEdit(this.Attachment);
        }

        private void DeleteButton_OnClick(object sender, RoutedEventArgs e)
        {
            if (this.AskReallyDelete())
            {
                var attachment = this.Attachment;
                var simulation = this.App.Simulation;

                if (attachment != null)
                    simulation.Attachments.Remove(attachment);
                else
                    simulation.Attachments.Remove(attachment);

                this.OnDelete(attachment);
            }
        }

        private void ExecuteButton_OnClick(object sender, RoutedEventArgs e)
        {
            if (this.Attachment.FileContent != null)
            {
                var path = Path.Combine(Path.GetTempPath(), this.Attachment.FileName);
                this.App.ProgressInvoke(() =>
                {
                    File.WriteAllBytes(path, this.Attachment.FileContent);

                    OutputProvider.WriteLine(path);

                    while (!File.Exists(path)) Thread.Sleep(10);

                    try
                    {
                        Process.Start(new ProcessStartInfo { FileName = path, Verb = "preview", CreateNoWindow = true, WorkingDirectory = Path.GetTempPath() });
                    }
                    catch (Exception)
                    {
                        try
                        {
                            Process.Start(new ProcessStartInfo { FileName = path, Verb = "open", CreateNoWindow = true, WorkingDirectory = Path.GetTempPath() });
                        }
                        catch (Exception)
                        {
                            Process.Start(new ProcessStartInfo { FileName = path, Verb = "openas", CreateNoWindow = true, WorkingDirectory = Path.GetTempPath() });
                        }
                    }
                    
                }, "AttachmentNavigationControlDownloadProgressString");
            }
        }
    }
}