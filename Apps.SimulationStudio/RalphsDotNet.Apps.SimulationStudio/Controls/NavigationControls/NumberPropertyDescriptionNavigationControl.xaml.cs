﻿using System.Windows;
using RalphsDotNet.Service.Simulation.Data;

namespace RalphsDotNet.Apps.SimulationStudio.Controls
{
    /// <summary>
    ///     Interaction logic for SystemDescriptionNavigationControl.xaml
    /// </summary>
    public partial class NumberPropertyDescriptionNavigationControl
    {
        public NumberPropertyDescriptionNavigationControl()
        {
            this.InitializeComponent();
        }

        public INumberPropertyDescription NumberDescription
        {
            get { return this.DataContext as INumberPropertyDescription; }
        }

        private void EditButton_OnClick(object sender, RoutedEventArgs e)
        {
            this.OnEdit(this.NumberDescription);
        }

        private void DeleteButton_OnClick(object sender, RoutedEventArgs e)
        {
            var numberDescription = this.NumberDescription;
            var system = numberDescription.SystemDescription;
            system.NumberPropertyDescriptions.Remove(numberDescription);
            this.OnDelete(numberDescription);
        }
    }
}