﻿using System.Windows;
using RalphsDotNet.Service.Simulation.Data;

namespace RalphsDotNet.Apps.SimulationStudio.Controls
{
    /// <summary>
    ///     Interaction logic for SystemDescriptionNavigationControl.xaml
    /// </summary>
    public partial class SystemScriptNavigationControl
    {
        public SystemScriptNavigationControl()
        {
            this.InitializeComponent();
        }

        public ISystemScriptMapping SystemScriptMapping
        {
            get { return this.DataContext as ISystemScriptMapping; }
        }

        private void EditButton_OnClick(object sender, RoutedEventArgs e)
        {
            this.OnEdit(this.SystemScriptMapping);
        }

        private void DeleteButton_OnClick(object sender, RoutedEventArgs e)
        {
            var systemScriptMapping = this.SystemScriptMapping;
            var system = systemScriptMapping.SystemDescription;

            if (systemScriptMapping as ISystemPrepareInteractionScriptMapping != null)
                system.SystemPrepareInteractionScripts.Remove(
                    systemScriptMapping as ISystemPrepareInteractionScriptMapping);
            else if (systemScriptMapping as ISystemModifyScriptMapping != null)
                system.SystemModifyScripts.Remove(systemScriptMapping as ISystemModifyScriptMapping);
            else if (systemScriptMapping as ISystemInteractionScriptMapping != null)
                system.SystemInteractionScripts.Remove(systemScriptMapping as ISystemInteractionScriptMapping);

            this.OnDelete(systemScriptMapping);
        }
    }
}