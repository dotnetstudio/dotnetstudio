﻿using System.Windows;
using RalphsDotNet.Service.Simulation.Data;

namespace RalphsDotNet.Apps.SimulationStudio.Controls
{
    /// <summary>
    ///     Interaction logic for SystemDescriptionNavigationControl.xaml
    /// </summary>
    public partial class SimulationScriptNavigationControl
    {
        public SimulationScriptNavigationControl()
        {
            this.InitializeComponent();
        }

        public ISimulationScript SystemScript
        {
            get { return this.DataContext as ISimulationScript; }
        }

        private void EditButton_OnClick(object sender, RoutedEventArgs e)
        {
            this.OnEdit(this.SystemScript);
        }

        private void DeleteButton_OnClick(object sender, RoutedEventArgs e)
        {
            if(this.AskReallyDelete())
            {
                var systemScript = this.SystemScript;
                var simulation = this.App.Simulation;

                if (systemScript as ISimulationRawScript != null)
                    simulation.RawScripts.Remove(systemScript as ISimulationRawScript);
                else
                    simulation.SystemScripts.Remove(systemScript as ISimulationSystemScript);

                this.OnDelete(systemScript);
            }
        }
    }
}