﻿using System.Windows;
using RalphsDotNet.Service.Simulation.Data;

namespace RalphsDotNet.Apps.SimulationStudio.Controls
{
    /// <summary>
    ///     Interaction logic for SystemDescriptionNavigationControl.xaml
    /// </summary>
    public partial class ConfigurationNavigationControl
    {
        public ConfigurationNavigationControl()
        {
            this.InitializeComponent();
        }

        public IExtensionProfile Configuration
        {
            get { return this.DataContext as IExtensionProfile; }
        }

        private void EditButton_OnClick(object sender, RoutedEventArgs e)
        {
            this.OnEdit(this.Configuration);
        }

        private void DeleteButton_OnClick(object sender, RoutedEventArgs e)
        {
            var configuration = this.Configuration;
            this.App.ProgressInvoke(() => this.App.DeleteConfiguration(configuration), "DeletingProgressString");
            this.OnDelete(configuration);
        }

        private void ActivateButton_OnClick(object sender, RoutedEventArgs e)
        {
            this.App.ProgressInvoke(() =>
            {
                this.App.LoadConfiguration(this.Configuration);
                this.App.SaveAndReload();
            }, "ConfigurationNavigationControlActivateProgressString");
        }
    }
}