﻿using System;
using System.ComponentModel;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using RalphsDotNet.Apps.SimulationStudio.Base;
using IDataObject = RalphsDotNet.Data.IDataObject;

namespace RalphsDotNet.Apps.SimulationStudio.Controls
{
    public class EditControl : UserControl, IDisposable
    {
        public static readonly DependencyProperty IsReadOnlyProperty =
            DependencyProperty.Register("IsReadOnly", typeof (bool), typeof (EditControl));

        public IApplication App
        {
            get { return Application.Current as IApplication; }
        }

        public bool IsReadOnly
        {
            get { return (bool) this.GetValue(IsReadOnlyProperty); }
            set { this.SetValue(IsReadOnlyProperty, value); }
        }

        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            base.OnPropertyChanged(e);

            if (e.Property == DataContextProperty)
            {
                if (e.OldValue as IDataObject != null)
                    (e.OldValue as IDataObject).PropertyChanged -= this.DataContext_PropertyChanged;
                
                if (this.DataContext as IDataObject != null)
                    ((IDataObject) this.DataContext).PropertyChanged += this.DataContext_PropertyChanged;
            }
        }

        private void DataContext_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (Thread.CurrentThread == this.Dispatcher.Thread && this.DataContext as IDataObject != null)
            {
                this.OnDataContextPropertyChanged(e);

                this.App.HasPendingChanges = true;
            }
        }

        protected virtual void OnDataContextPropertyChanged(PropertyChangedEventArgs e)
        {
        }

        public virtual void Dispose()
        {
        }
    }
}