﻿using System;
using RalphsDotNet.Data;

namespace RalphsDotNet.Apps.SimulationStudio.Controls
{
    public class ItemDeleteEventArgs : EventArgs
    {
        public IDataObject DataObject { get; private set; }
        public ItemDeleteEventArgs(IDataObject dataObject)
        {
            this.DataObject = dataObject;
        }
    }

    public delegate void ItemDeleteHandler(object sender, ItemDeleteEventArgs args);
}