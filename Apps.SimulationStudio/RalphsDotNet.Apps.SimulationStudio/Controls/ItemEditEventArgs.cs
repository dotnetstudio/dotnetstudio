﻿using System;
using RalphsDotNet.Data;

namespace RalphsDotNet.Apps.SimulationStudio.Controls
{
    public class ItemEditEventArgs : EventArgs
    {
        public IDataObject DataObject { get; private set; }
        public ItemEditEventArgs(IDataObject dataObject)
        {
            this.DataObject = dataObject;
        }
    }

    public delegate void ItemEditHandler(object sender, ItemEditEventArgs args);
}