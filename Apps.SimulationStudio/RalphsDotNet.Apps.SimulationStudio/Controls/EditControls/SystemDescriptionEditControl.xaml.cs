﻿using System.ComponentModel;
using System.Threading;
using System.Windows;
using RalphsDotNet.Service.Simulation.Data;

namespace RalphsDotNet.Apps.SimulationStudio.Controls
{
    /// <summary>
    ///     Interaction logic for SystemDescriptionEditControl.xaml
    /// </summary>
    public partial class SystemDescriptionEditControl
    {
        public SystemDescriptionEditControl()
        {
            this.InitializeComponent();
        }
    }
}