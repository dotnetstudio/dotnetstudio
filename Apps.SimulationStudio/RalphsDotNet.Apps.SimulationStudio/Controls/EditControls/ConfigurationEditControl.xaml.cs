﻿using RalphsDotNet.Service.Simulation.Data;

namespace RalphsDotNet.Apps.SimulationStudio.Controls
{
    /// <summary>
    ///     Interaction logic for SystemDescriptionEditControl.xaml
    /// </summary>
    public partial class ConfigurationEditControl
    {
        public ConfigurationEditControl()
        {
            this.InitializeComponent();
        }

        public IExtensionProfile Configuration
        {
            get { return this.DataContext as IExtensionProfile; }
        }
    }
}