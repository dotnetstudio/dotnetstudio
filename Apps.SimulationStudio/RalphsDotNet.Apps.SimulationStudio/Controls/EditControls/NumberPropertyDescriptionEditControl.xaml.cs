﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Input;
using System.Xml;
using ICSharpCode.AvalonEdit.Highlighting;
using ICSharpCode.AvalonEdit.Highlighting.Xshd;
using RalphsDotNet.Apps.SimulationStudio.Base;
using RalphsDotNet.Service.Simulation.Data;

namespace RalphsDotNet.Apps.SimulationStudio.Controls
{
    /// <summary>
    ///     Interaction logic for SystemDescriptionEditControl.xaml
    /// </summary>
    public partial class NumberPropertyDescriptionEditControl
    {
        public NumberPropertyDescriptionEditControl()
        {
            this.InitializeComponent();

            this.LoadSyntaxHighlighting();
        }

        public INumberPropertyDescription NumberPropertyDescription
        {
            get { return this.DataContext as INumberPropertyDescription; }
        }

        private void LoadSyntaxHighlighting()
        {
            using (
                var s =
                    typeof(IApplication)
                        .Assembly.GetManifestResourceStream("RalphsDotNet.Apps.SimulationStudio.Base.Resources.Python_Light.xshd"))
            {
                using (var reader = new XmlTextReader(s))
                {
                    this._DistributionEditor.SyntaxHighlighting = HighlightingLoader.Load(reader,
                        HighlightingManager.Instance);
                }
            }
        }

        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            base.OnPropertyChanged(e);

            if (e.Property == DataContextProperty)
            {
                if (e.OldValue as INumberPropertyDescription != null)
                {
                    this.SaveDistribution();
                    this._DistributionEditor.Document.Text = string.Empty;
                }

                if (this.NumberPropertyDescription != null)
                {
                    this._DistributionEditor.Document.Text = this.NumberPropertyDescription.Distribution ?? string.Empty;
                }
            }
        }

        protected override void OnDataContextPropertyChanged(PropertyChangedEventArgs e)
        {
            base.OnDataContextPropertyChanged(e);

            if (e.PropertyName == "Distribution")
            {
                this._DistributionEditor.Document.Text = this.NumberPropertyDescription.Distribution;
            }
        }

        private void SaveDistribution()
        {
            if (this.NumberPropertyDescription != null)
                this.NumberPropertyDescription.Distribution = this._DistributionEditor.Document.Text;
        }

        private void DistributionEditor_OnLostFocus(object sender, RoutedEventArgs e)
        {
            this.SaveDistribution();
        }

        private void DistributionEditor_OnKeyDown(object sender, KeyEventArgs e)
        {
            if (Keyboard.IsKeyDown(Key.S) && (Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl)))
            {
                this.SaveDistribution();
                e.Handled = true;
            }
            else if (Keyboard.IsKeyDown(Key.Tab))
            {
                this.MoveFocus(new TraversalRequest(FocusNavigationDirection.Next));
                e.Handled = true;
            }
        }

        private string _oldDistributionText;
        private void DistributionEditor_OnTextChanged(object sender, EventArgs e)
        {
            if (this._DistributionEditor.Document.Text != this._oldDistributionText)
            {
                var pos = this._DistributionEditor.CaretOffset;
                var text = this._DistributionEditor.Document.Text;
                this._oldDistributionText = text
                    .Replace("\r\n", "")
                    .Replace("\n", "")
                    .Replace("\t", "");
                this._DistributionEditor.Document.Text = this._oldDistributionText;
                this._DistributionEditor.CaretOffset = pos - (text.Length - this._oldDistributionText.Length);
            }
        }
    }
}