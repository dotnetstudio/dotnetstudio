﻿using System.Windows;
using RalphsDotNet.Service.Simulation.Data;

namespace RalphsDotNet.Apps.SimulationStudio.Controls
{
    /// <summary>
    ///     Interaction logic for SystemPrepareInteractionScriptEditControl.xaml
    /// </summary>
    public partial class SystemPrepareInteractionScriptEditControl
    {
        public SystemPrepareInteractionScriptEditControl()
        {
            this.InitializeComponent();
        }
    }
}