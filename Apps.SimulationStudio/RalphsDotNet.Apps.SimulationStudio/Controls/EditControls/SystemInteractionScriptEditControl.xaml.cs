﻿using System.Windows;

namespace RalphsDotNet.Apps.SimulationStudio.Controls
{
    /// <summary>
    ///     Interaction logic for SystemPrepareInteractionScriptEditControl.xaml
    /// </summary>
    public partial class SystemInteractionScriptEditControl
    {
        public SystemInteractionScriptEditControl()
        {
            this.InitializeComponent();
        }
    }
}