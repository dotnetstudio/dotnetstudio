﻿using System.IO;
using System.Windows;
using Microsoft.Win32;
using RalphsDotNet.Data;
using RalphsDotNet.Service.Simulation.Data;

namespace RalphsDotNet.Apps.SimulationStudio.Controls
{
    /// <summary>
    ///     Interaction logic for SystemDescriptionEditControl.xaml
    /// </summary>
    public partial class AttachmentEditControl
    {
        public AttachmentEditControl()
        {
            this.InitializeComponent();
        }

        public IAttachment Attachment
        {
            get { return this.DataContext as IAttachment; }
        }

        private void Upload_OnClick(object sender, RoutedEventArgs e)
        {
            var oFD = new OpenFileDialog();
            if (oFD.ShowDialog().GetValueOrDefault(false))
            {
                var info = new FileInfo(oFD.FileName);

                if (info.Length <= 32*1024*1024)
                {
                    this.Attachment.FileName = Path.GetFileName(oFD.FileName);

                    this.App.ProgressInvoke(() =>
                    {
                        this.Attachment.FileContent = File.ReadAllBytes(oFD.FileName);
                    }, "AttachmentEditControlUploadProgressString");
                }
                else
                {
                    MessageBox.Show("attachment filesize limited to 32MB", "file too big", MessageBoxButton.OK,
                        MessageBoxImage.Error);
                }
            }
        }
    }
}