﻿using System;

namespace RalphsDotNet.Apps.SimulationStudio.Base
{
    public class SimulationException : Exception
    {
        public SimulationException(int errorCode) : base(string.Format("Error: {0}", errorCode))
        {
        }
    }
}