﻿import math

import clr
clr.AddReference("System.Core")
clr.AddReference("PresentationCore")
clr.AddReference("HelixToolkit.Wpf")
clr.AddReference("OxyPlot")
clr.AddReference("RalphsDotNet.Service.Simulation.Data")

import System
from System import Linq

clr.ImportExtensions(Linq)

from RalphsDotNet.Service.Simulation.Data import *