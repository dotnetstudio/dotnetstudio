﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Threading;
using RalphsDotNet.Data;
using RalphsDotNet.Gui.ActivityModel;
using RalphsDotNet.Service.Simulation.Data;
using RalphsDotNet.Service.Simulation.Model;
using RalphsDotNet.Gui.Wpf.ActivityEngine;

namespace RalphsDotNet.Apps.SimulationStudio.Base
{
    public class AddinActivityBase : Activity
    {
        public static readonly DependencyProperty IsPausedProperty =
            DependencyProperty.Register("IsPaused", typeof (bool), typeof (AddinActivityBase));

        public static readonly DependencyProperty ActualIterationProperty =
            DependencyProperty.Register("ActualIteration", typeof (IIteration),
                typeof (AddinActivityBase));

        public static readonly DependencyProperty SimulationIdProperty =
            DependencyProperty.Register("SimulationId", typeof (string), typeof (AddinActivityBase));

        public static readonly DependencyProperty ProfilesProperty =
            DependencyProperty.Register("Profiles", typeof (IExtensionProfile[]), typeof (AddinActivityBase));

        public static readonly DependencyProperty RefreshIterationIntervalProperty =
            DependencyProperty.Register("RefreshIterationInterval", typeof (TimeSpan), typeof (AddinActivityBase),
                new PropertyMetadata(TimeSpan.FromMilliseconds(100)));

        public static readonly DependencyProperty SimulationObstacleMilisecondsProperty =
            DependencyProperty.Register("SimulationObstacleMiliseconds", typeof (int), typeof (AddinActivityBase));

        public static readonly DependencyProperty SampleRateProperty =
            DependencyProperty.Register("SampleRate", typeof (int), typeof (AddinActivityBase));

        private IAttachmentRepository _attachmentRepository;

        private DispatcherTimer _iterationTimer;
        private IExtensionProfileRepository _profileRepository;

        public AddinActivityBase()
        {
            this.CanPause = false;

            this.InitReferences();
        }

        public virtual Guid ProfileId
        {
            get { return Guid.Empty; }
        }

        public IApplication App
        {
            get { return Application.Current as IApplication; }
        }

        public bool IsPaused
        {
            get { return (bool) this.GetValue(IsPausedProperty); }
            set { this.SetValue(IsPausedProperty, value); }
        }

        public IIteration ActualIteration
        {
            get { return this.GetValue(ActualIterationProperty) as IIteration; }
            set { this.SetValue(ActualIterationProperty, value); }
        }

        public string SimulationId
        {
            get { return this.GetValue(SimulationIdProperty) as string; }
            set { this.SetValue(SimulationIdProperty, value); }
        }

        public IExtensionProfile[] Profiles
        {
            get { return this.GetValue(ProfilesProperty) as IExtensionProfile[]; }
            set { this.SetValue(ProfilesProperty, value); }
        }

        public TimeSpan RefreshIterationInterval
        {
            get { return (TimeSpan) this.GetValue(RefreshIterationIntervalProperty); }
            set { this.SetValue(RefreshIterationIntervalProperty, value); }
        }

        public int SimulationObstacleMiliseconds
        {
            get { return (int) this.GetValue(SimulationObstacleMilisecondsProperty); }
            set { this.SetValue(SimulationObstacleMilisecondsProperty, value); }
        }

        public int SampleRate
        {
            get { return (int) this.GetValue(SampleRateProperty); }
            set { this.SetValue(SampleRateProperty, value); }
        }

        private void InitReferences()
        {
            this.References.Add(new ProgressActivityReference
            {
                IsEnabled = true,
                Name = "AddinActivityPauseSimulationString",
                ProgressAction = this.PauseSimulation
            });

            this.References.Add(new ProgressActivityReference
            {
                IsEnabled = false,
                Name = "AddinActivityResumeSimulationString",
                ProgressAction = this.ResumeSimulation
            });

            this.References.Add(new ProgressActivityReference
            {
                IsEnabled = false,
                Name = "AddinActivityStepSimulationString",
                ProgressAction = this.StepSimulation
            });

            this.References.Add(new ProgressActivityReference
            {
                IsEnabled = true,
                Name = "AddinActivityResetSimulationString",
                ProgressAction = this.ResetSimulationAction
            });

            this.References.Add(new ProgressActivityReference
            {
                IsEnabled = true,
                Name = "AddinActivityCreateConfigurationString",
                ProgressAction = this.CreateConfiguration
            });

            foreach (
                var r in this.App.AddinReferences.OfType<ActivityReference>().Where(r => r.Activity != this.GetType()))
                this.References.Add(r);
        }

        protected override void OnActivityInitialize()
        {
            base.OnActivityInitialize();

            this.InitData();

            this._iterationTimer = new DispatcherTimer(DispatcherPriority.Background)
            {
                Interval = this.RefreshIterationInterval
            };
            this._iterationTimer.Tick += this._iterationTimer_Tick;

            this.SimulationObstacleMiliseconds = this.App.SimulationService.ObstacleMiliseconds;
            this.SampleRate = this.App.SimulationService.SampleRate;
        }

        protected override void OnActivityActivated()
        {
            base.OnActivityActivated();

            this.Resume();
        }

        protected override void OnActivityDeactivated()
        {
            base.OnActivityDeactivated();

            this.Pause();
        }

        private void InitData()
        {
            this._profileRepository =
                this.App.DataService.GetRepository<IExtensionProfile, IExtensionProfileRepository>();
            this._attachmentRepository = this.App.DataService.GetRepository<IAttachment, IAttachmentRepository>();

            this.LoadProfiles();
        }

        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            base.OnPropertyChanged(e);

            if (e.Property == RefreshIterationIntervalProperty)
            {
                this._iterationTimer.Interval = this.RefreshIterationInterval;
            }
            else if (e.Property == SimulationObstacleMilisecondsProperty)
            {
                this.App.SimulationService.ObstacleMiliseconds = this.SimulationObstacleMiliseconds;
            }
            else if (e.Property == SampleRateProperty)
            {
                this.App.SimulationService.SampleRate = this.SampleRate;
            }
        }

        private void LoadProfiles()
        {
            if (this.ProfileId != Guid.Empty)
                this.Profiles = this._profileRepository.All.Where(p => p.ProfileId == this.ProfileId).ToArray();
        }

        private void _iterationTimer_Tick(object sender, EventArgs e)
        {
            if (this.App.SimulationService != null)
            {
                try
                {
                    if (this.App.SimulationService.ErrorCode != 0)
                    {
                        OutputProvider.WriteException(new SimulationException(this.App.SimulationService.ErrorCode));
                        this.App.SimulationService.ResetError();
                        this.OnGuiPauseSimulation();
                    }

                    if (this.App.SimulationService.IsRunning)
                    {
                        this.ActualIteration = this.App.SimulationService.ActualIteration;
                        this.SimulationId = this.App.SimulationService.SimulationId.ToString();
                        this.SimulationObstacleMiliseconds = this.App.SimulationService.ObstacleMiliseconds;
                        this.SampleRate = this.App.SimulationService.SampleRate;

                        this.OnUpdate();

                        if (this.App.SimulationService.IsPaused)
                        {
                            if (!this.IsPaused)
                                this.OnGuiPauseSimulation();
                        }
                        else
                        {
                            if (this.IsPaused)
                                this.OnGuiResumeSimulation();
                        }
                    }
                    else
                    {
                        this.GuiStopSimulation();
                    }
                }
                catch (Exception ex)
                {
                    OutputProvider.WriteException(ex);
                    this.GuiStopSimulation();
                    this.App.StopSimulation();
                }
            }
        }

        protected virtual void OnUpdate()
        {
        }

        private void CheckProfileId()
        {
            if (this.ProfileId == Guid.Empty)
                throw new ArgumentException("need a valid ProfileId for beeing able to work with profiles");
        }

        protected IExtensionProfile CreateProfile(string name)
        {
            this.CheckProfileId();

            var profile = this._profileRepository.Create(this.ProfileId, name);
            this._profileRepository.SaveOrUpdate(profile);
            this.LoadProfiles();
            return profile;
        }

        protected void DeleteProfile(IExtensionProfile profile)
        {
            this.CheckProfileId();

            this._profileRepository.Delete(profile);
            this.LoadProfiles();
        }

        protected IAttachment CreateAttachment(string name, string fileName)
        {
            this.CheckProfileId();

            return this._attachmentRepository.Create(name, fileName);
        }

        protected void SaveProfiles()
        {
            this.CheckProfileId();

            this._profileRepository.SaveOrUpdate(this.Profiles);
            this.LoadProfiles();
        }

        private void PauseSimulation(ProgressActivityReference activityref, ProgressHandler handler)
        {
            handler.IsIndeterminate = true;
            handler.Text = "AddinActivityPauseSimulationProgressString";

            this.PauseSimulation();
        }

        protected void PauseSimulation()
        {
            this.App.SimulationService.Pause();

            this.OnGuiPauseSimulation();

            OutputProvider.WriteLine("siumulation paused");
        }

        private void ResumeSimulation(ProgressActivityReference activityref, ProgressHandler handler)
        {
            handler.IsIndeterminate = true;
            handler.Text = "AddinActivityResumeSimulationProgressString";

            this.ResumeSimulation();
        }

        private void StepSimulation(ProgressActivityReference activityref, ProgressHandler handler)
        {
            handler.IsIndeterminate = true;
            handler.Text = "AddinActivityStepSimulationProgressString";

            this.StepSimulation();
        }

        protected void ResumeSimulation(IIteration modifiedIteration = null)
        {
            this.App.SimulationService.Resume(modifiedIteration);

            this.OnGuiResumeSimulation();

            OutputProvider.WriteLine("siumulation resumed");
        }

        protected void StepSimulation(IIteration modifiedIteration = null)
        {
            this.App.StepSimulation(modifiedIteration);

            this.OnGuiResumeSimulation();

            OutputProvider.WriteLine("siumulation stepped");
        }

        private void ResetSimulationAction(ProgressActivityReference activityref, ProgressHandler handler)
        {
            handler.IsIndeterminate = true;
            handler.Text = "AddinActivityResetSimulationProgressString";

            this.ResetSimulation();
        }

        private void CreateConfiguration(ProgressActivityReference activityref, ProgressHandler handler)
        {
            handler.IsIndeterminate = true;
            handler.Text = "AddinActivityCreateConfigurationProgressString";

            this.App.CreateConfigurationSnapshot();
        }

        protected void ResetSimulation()
        {
            this._iterationTimer.Stop();
            var isDebugging = this.App.SimulationService.IsDebugging;
            this.App.StopSimulation();

            if (isDebugging)
                this.App.DebugSimulation();
            else
                this.App.StartSimulation();

            this.OnGuiResumeSimulation();

            OutputProvider.WriteLine("siumulation resumed");
            this._iterationTimer.Start();
        }

        private void GuiStopSimulation()
        {
            this._iterationTimer.Stop();
            this.Host.Back();
        }

        protected virtual void OnGuiResumeSimulation()
        {
            this.App.SimulationService.ObstacleMiliseconds = this.SimulationObstacleMiliseconds;
            this.App.SimulationService.SampleRate = this.SampleRate;

            this.References[0].IsEnabled = true;
            this.References[1].IsEnabled = false;
            this.References[2].IsEnabled = false;

            this.IsPaused = false;
        }

        protected virtual void OnGuiPauseSimulation()
        {
            this.References[0].IsEnabled = false;
            if (!this.App.SimulationService.IsDebugging) this.References[1].IsEnabled = true;
            this.References[2].IsEnabled = true;

            this.IsPaused = true;
        }

        public override void Pause()
        {
            base.Pause();

            if (this._iterationTimer != null)
                this._iterationTimer.Stop();
        }

        public override void Resume()
        {
            base.Resume();

            if (this.App.SimulationService.IsPaused)
            {
                this.OnGuiPauseSimulation();
            }
            else
            {
                this.OnGuiResumeSimulation();
            }
            if (!this.App.SimulationService.IsRunning)
            {
                this.GuiStopSimulation();
            }

            if (this._iterationTimer != null)
                this._iterationTimer.Start();
        }

        public override void Dispose()
        {
            base.Dispose();

            if (this._iterationTimer != null)
                this._iterationTimer.Stop();
            this._iterationTimer = null;
        }
    }

    [AttributeUsage(AttributeTargets.Assembly, AllowMultiple = true)]
    public class AddinActivityAttribute : Attribute
    {
        public AddinActivityAttribute(Type t, string name)
        {
            this.ActivityType = t;
            this.ActivityName = name;
        }

        public Type ActivityType { get; private set; }

        public string ActivityName { get; private set; }
    }
}