﻿using System.Collections.Generic;
using System.ComponentModel;
using RalphsDotNet.Gui.ActivityModel;
using RalphsDotNet.Service.Data.Interface;
using RalphsDotNet.Service.Simulation.Data;
using RalphsDotNet.Service.Simulation.Interface;
using RalphsDotNet.Service.Simulation.Model;
using RalphsDotNet.Gui.Wpf.ActivityEngine;

namespace RalphsDotNet.Apps.SimulationStudio.Base
{
    public interface IApplication : IApplicationController, INotifyPropertyChanged
    {
        bool IsReadOnly { get; set; }

        bool IsDebugging { get; }

        bool IsClosingAllowed { get; }

        IActivityHost ActivityHost { get; }

        ActivityReferenceCollection AddinReferences { get; }

        IEnumerable<IExtensionProfile> Configurations { get; }

        ISimulationService SimulationService { get; }
        IDataService DataService { get; }

        ISimulationDescription Simulation { get; }

        IIteration ActualIteration { get; }

        bool HasPendingChanges { get; set; }

        PythonConsoleContext PythonConsoleContext { get; }

        void CreateOrOpenProject(string sdbPath);

        void CloseProject();

        void StartSimulation();
        void StartSimulation(string path);
        void StopSimulation();
        void DebugSimulation();
        void StepSimulation();
        void StepSimulation(IIteration iteration);
        void PauseSimulation();
        void ResumeSimulation();

        IExtensionProfile CreateConfiguration();
        IExtensionProfile CreateConfigurationSnapshot();
        void LoadConfiguration(IExtensionProfile configuration);
        void DeleteConfiguration(IExtensionProfile configuration);

        void Save();
        void Reload();
        void SaveAndReload();

        event SimulationStartedHandler SimulationStarted;
        event SimulationStoppedHandler SimulationSopped;
        event SimulationPausedHandler SimulationPaused;
        event SimulationResumedHandler SimulationResumed;
        event SimulationSteppedHandler SimulationStepped;
        event ProjectOpenedHandler ProjectOpened;
        event ProjectClosedHandler ProjectClosed;
    }

    public delegate void SimulationStartedHandler();

    public delegate void SimulationStoppedHandler();

    public delegate void SimulationPausedHandler();

    public delegate void SimulationResumedHandler();

    public delegate void SimulationSteppedHandler();

    public delegate void ProjectOpenedHandler();

    public delegate void ProjectClosedHandler();
}