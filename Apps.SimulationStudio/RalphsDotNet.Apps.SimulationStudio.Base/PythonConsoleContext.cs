using RalphsDotNet.Service.Simulation.Data;
using RalphsDotNet.Service.Simulation.Model;

namespace RalphsDotNet.Apps.SimulationStudio
{
    public class PythonConsoleContext
    {
        public ISimulationDescription Simulation { get; set; }

        public IIteration ActualIteration { get; set; }
    }
}