﻿using System;
using ICSharpCode.AvalonEdit.Document;
using ICSharpCode.AvalonEdit.Highlighting;

namespace RalphsDotNet.Apps.SimulationStudio.Base.Controls.PythonConsoleControl
{
    /// <summary>
    ///     Only colourize when text is input
    /// </summary>
    public class PythonConsoleHighlightingColorizer : HighlightingColorizer
    {
        private readonly TextDocument _document;

        /// <summary>
        ///     Creates a new HighlightingColorizer instance.
        /// </summary>
        /// <param name="ruleSet">The root highlighting rule set.</param>
        /// <param name="document"></param>
        public PythonConsoleHighlightingColorizer(IHighlightingDefinition ruleSet, TextDocument document)
            : base(ruleSet)
        {
            if (document == null)
                throw new ArgumentNullException("document");
            this._document = document;
        }

        /// <inheritdoc />
        protected override void ColorizeLine(DocumentLine line)
        {
            var highlighter = this.CurrentContext.TextView.Services.GetService(typeof (IHighlighter)) as IHighlighter;
            var lineString = this._document.GetText(line);
            if (highlighter != null)
            {
                if (lineString.Length < 3 || lineString.Substring(0, 3) == ">>>" || lineString.Substring(0, 3) == "...")
                {
                    var hl = highlighter.HighlightLine(line.LineNumber);
                    foreach (var section in hl.Sections)
                    {
                        this.ChangeLinePart(section.Offset, section.Offset + section.Length,
                            visualLineElement => this.ApplyColorToElement(visualLineElement, section.Color));
                    }
                }
            }
        }
    }
}