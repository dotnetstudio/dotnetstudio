﻿using System;
using System.Collections.Generic;

namespace RalphsDotNet.Apps.SimulationStudio.Base.Controls.PythonConsoleControl
{
    /// <summary>
    ///     Stores the command line history for the PythonConsole.
    /// </summary>
    public class CommandLineHistory
    {
        private readonly List<string> _lines = new List<string>();
        private int _position;

        /// <summary>
        ///     Gets the current command line. By default this will be the last command line entered.
        /// </summary>
        public string Current
        {
            get
            {
                if ((this._position >= 0) && (this._position < this._lines.Count))
                {
                    return this._lines[this._position];
                }
                return null;
            }
        }

        /// <summary>
        ///     Adds the command line to the history.
        /// </summary>
        public void Add(string line)
        {
            if (!String.IsNullOrEmpty(line))
            {
                var index = this._lines.Count - 1;
                if (index >= 0)
                {
                    if (this._lines[index] != line)
                    {
                        this._lines.Add(line);
                    }
                }
                else
                {
                    this._lines.Add(line);
                }
            }
            this._position = this._lines.Count;
        }

        /// <summary>
        ///     Moves to the next command line.
        /// </summary>
        /// <returns>False if the current position is at the end of the command line history.</returns>
        public bool MoveNext()
        {
            var nextPosition = this._position + 1;
            if (nextPosition < this._lines.Count)
            {
                ++this._position;
            }
            return nextPosition < this._lines.Count;
        }

        /// <summary>
        ///     Moves to the previous command line.
        /// </summary>
        /// <returns>False if the current position is at the start of the command line history.</returns>
        public bool MovePrevious()
        {
            if (this._position >= 0)
            {
                if (this._position == 0)
                {
                    return false;
                }
                --this._position;
            }
            return this._position >= 0;
        }
    }
}