﻿using System.Windows.Media;
using ICSharpCode.AvalonEdit;

namespace RalphsDotNet.Apps.SimulationStudio.Base.Controls.PythonConsoleControl
{
    public class PythonConsolePad
    {
        private readonly PythonConsoleHost _host;
        private readonly TextEditor _textEditor;

        public PythonConsolePad()
        {
            this._textEditor = new TextEditor();
            this._host = new PythonConsoleHost(new PythonTextEditor(this._textEditor));
            this._host.Run();
            this._textEditor.FontFamily = new FontFamily("Consolas");
            this._textEditor.FontSize = 12;
        }

        public TextEditor Control
        {
            get { return this._textEditor; }
        }

        public PythonConsoleHost Host
        {
            get { return this._host; }
        }

        public PythonConsole Console
        {
            get { return this._host.Console; }
        }

        public void Dispose()
        {
            this._host.Dispose();
        }
    }
}