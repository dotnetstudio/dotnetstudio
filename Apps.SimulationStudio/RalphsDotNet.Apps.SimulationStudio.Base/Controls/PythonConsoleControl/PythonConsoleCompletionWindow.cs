﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Threading;
using ICSharpCode.AvalonEdit.CodeCompletion;
using ICSharpCode.AvalonEdit.Document;
using ICSharpCode.AvalonEdit.Editing;
using ICSharpCode.AvalonEdit.Rendering;

namespace RalphsDotNet.Apps.SimulationStudio.Base.Controls.PythonConsoleControl
{
    public delegate void DescriptionUpdateDelegate(string description);

    /// <summary>
    ///     The code completion window.
    /// </summary>
    public class PythonConsoleCompletionWindow : CompletionWindowBase
    {
        private readonly PythonConsoleCompletionDataProvider _completionDataProvider;
        private readonly CompletionList _completionList = new CompletionList();
        private readonly PythonTextEditor _textEditor;
        private readonly DispatcherTimer _updateDescription;
        private readonly TimeSpan _updateDescriptionInterval;
        private ToolTip _toolTip = new ToolTip();

        /// <summary>
        ///     Creates a new code completion window.
        /// </summary>
        public PythonConsoleCompletionWindow(TextArea textArea, PythonTextEditor textEditor)
            : base(textArea)
        {
            // keep height automatic
            this._completionDataProvider = textEditor.CompletionProvider;
            this._textEditor = textEditor;
            this.CloseAutomatically = true;
            this.SizeToContent = SizeToContent.Height;
            this.MaxHeight = 300;
            this.Width = 175;
            this.Content = this._completionList;
            // prevent user from resizing window to 0x0
            this.MinHeight = 15;
            this.MinWidth = 30;

            this._toolTip.PlacementTarget = this;
            this._toolTip.Placement = PlacementMode.Right;
            this._toolTip.Closed += this.toolTip_Closed;

            this._completionList.InsertionRequested += this.completionList_InsertionRequested;
            this._completionList.SelectionChanged += this.completionList_SelectionChanged;
            this.AttachEvents();

            this._updateDescription = new DispatcherTimer();
            this._updateDescription.Tick += this.completionList_UpdateDescription;
            this._updateDescriptionInterval = TimeSpan.FromSeconds(0.3);

            var eventInfo = typeof (TextView).GetEvent("ScrollOffsetChanged");
            var methodDelegate = Delegate.CreateDelegate(eventInfo.EventHandlerType, this, "TextViewScrollOffsetChanged");
            eventInfo.RemoveEventHandler(this.TextArea.TextView, methodDelegate);
        }

        #region ToolTip handling

        private void toolTip_Closed(object sender, RoutedEventArgs e)
        {
            // Clear content after tooltip is closed.
            // We cannot clear is immediately when setting IsOpen=false
            // because the tooltip uses an animation for closing.
            if (this._toolTip != null)
                this._toolTip.Content = null;
        }

        private void completionList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var item = this._completionList.SelectedItem;
            if (item == null)
            {
                this._updateDescription.Stop();
            }
            this._updateDescription.Interval = this._updateDescriptionInterval;
            this._updateDescription.Start();
        }

        private void completionList_UpdateDescription(Object sender, EventArgs e)
        {
            this._updateDescription.Stop();
            this._textEditor.UpdateCompletionDescription();
        }

        /// <summary>
        ///     Update the description of the current item. This is typically called from a separate thread from the main UI
        ///     thread.
        /// </summary>
        internal void UpdateCurrentItemDescription()
        {
            if (this._textEditor.StopCompletion())
            {
                this._updateDescription.Interval = this._updateDescriptionInterval;
                this._updateDescription.Start();
                return;
            }
            var stub = "";
            var item = "";
            var isInstance = false;
            this._textEditor.TextEditor.Dispatcher.Invoke(new Action(() =>
            {
                var data = (this._completionList.SelectedItem as PythonCompletionData);
                if (data == null || this._toolTip == null)
                    return;
                stub = data.Stub;
                item = data.Text;
                isInstance = data.IsInstance;
            }));
            // Send to the completion thread to generate the description, providing callback.
            this._completionDataProvider.GenerateDescription(stub, item, this.completionList_WriteDescription,
                isInstance);
        }

        private void completionList_WriteDescription(string description)
        {
            this._textEditor.TextEditor.Dispatcher.Invoke(new Action(() =>
            {
                if (this._toolTip != null)
                {
                    if (description != null)
                    {
                        this._toolTip.Content = description;
                        this._toolTip.IsOpen = true;
                    }
                    else
                    {
                        this._toolTip.IsOpen = false;
                    }
                }
            }));
        }

        #endregion

        /// <summary>
        ///     Gets the completion list used in this completion window.
        /// </summary>
        public CompletionList CompletionList
        {
            get { return this._completionList; }
        }

        /// <summary>
        ///     Gets/Sets whether the completion window should close automatically.
        ///     The default value is true.
        /// </summary>
        public bool CloseAutomatically { get; set; }

        /// <inheritdoc />
        protected override bool CloseOnFocusLost
        {
            get { return this.CloseAutomatically; }
        }

        /// <summary>
        ///     When this flag is set, code completion closes if the caret moves to the
        ///     beginning of the allowed range. This is useful in Ctrl+Space and "complete when typing",
        ///     but not in dot-completion.
        ///     Has no effect if CloseAutomatically is false.
        /// </summary>
        public bool CloseWhenCaretAtBeginning { get; set; }

        private void completionList_InsertionRequested(object sender, EventArgs e)
        {
            this.Close();
            // The window must close before Complete() is called.
            // If the Complete callback pushes stacked input handlers, we don't want to pop those when the CC window closes.
            var item = this._completionList.SelectedItem;
            if (item != null)
                item.Complete(this.TextArea,
                    new AnchorSegment(this.TextArea.Document, this.StartOffset, this.EndOffset - this.StartOffset), e);
        }

        private void AttachEvents()
        {
            this.TextArea.Caret.PositionChanged += this.CaretPositionChanged;
            this.TextArea.MouseWheel += this.textArea_MouseWheel;
            this.TextArea.PreviewTextInput += this.textArea_PreviewTextInput;
        }

        /// <inheritdoc />
        protected override void DetachEvents()
        {
            this.TextArea.Caret.PositionChanged -= this.CaretPositionChanged;
            this.TextArea.MouseWheel -= this.textArea_MouseWheel;
            this.TextArea.PreviewTextInput -= this.textArea_PreviewTextInput;
            base.DetachEvents();
        }

        /// <inheritdoc />
        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            if (this._toolTip != null)
            {
                this._toolTip.IsOpen = false;
                this._toolTip = null;
            }
        }

        /// <inheritdoc />
        protected override void OnKeyDown(KeyEventArgs e)
        {
            base.OnKeyDown(e);
            if (!e.Handled)
            {
                this._completionList.HandleKey(e);
            }
        }

        private void textArea_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = RaiseEventPair(this, PreviewTextInputEvent, TextInputEvent,
                new TextCompositionEventArgs(e.Device, e.TextComposition));
        }

        private void textArea_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            e.Handled = RaiseEventPair(this.GetScrollEventTarget(),
                PreviewMouseWheelEvent, MouseWheelEvent,
                new MouseWheelEventArgs(e.MouseDevice, e.Timestamp, e.Delta));
        }

        private UIElement GetScrollEventTarget()
        {
            if (this._completionList == null)
                return this;
            return this._completionList.ScrollViewer ?? this._completionList.ListBox ?? (UIElement) this._completionList;
        }

        private void CaretPositionChanged(object sender, EventArgs e)
        {
            var offset = this.TextArea.Caret.Offset;
            if (offset == this.StartOffset)
            {
                if (this.CloseAutomatically && this.CloseWhenCaretAtBeginning)
                {
                    this.Close();
                }
                else
                {
                    this._completionList.SelectItem(string.Empty);
                }
                return;
            }
            if (offset < this.StartOffset || offset > this.EndOffset)
            {
                if (this.CloseAutomatically)
                {
                    this.Close();
                }
            }
            else
            {
                var document = this.TextArea.Document;
                if (document != null)
                {
                    this._completionList.SelectItem(document.GetText(this.StartOffset, offset - this.StartOffset));
                }
            }
        }
    }
}