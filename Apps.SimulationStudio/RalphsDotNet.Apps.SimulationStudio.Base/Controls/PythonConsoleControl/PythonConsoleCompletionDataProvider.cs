﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using ICSharpCode.AvalonEdit.CodeCompletion;
using IronPython.Runtime;
using Microsoft.Scripting;
using Microsoft.Scripting.Hosting.Shell;

namespace RalphsDotNet.Apps.SimulationStudio.Base.Controls.PythonConsoleControl
{
    /// <summary>
    ///     Provides code completion for the Python Console window.
    /// </summary>
    public class PythonConsoleCompletionDataProvider
    {
        private readonly CommandLine _commandLine;
        internal volatile bool AutocompletionInProgress = false;

        private bool _excludeCallables;

        public PythonConsoleCompletionDataProvider(CommandLine commandLine) //IMemberProvider memberProvider)
        {
            this._commandLine = commandLine;
        }

        public bool ExcludeCallables
        {
            get { return this._excludeCallables; }
            set { this._excludeCallables = value; }
        }

        /// <summary>
        ///     Generates completion data for the specified text. The text should be everything before
        ///     the dot character that triggered the completion. The text can contain the command line prompt
        ///     '>>>' as this will be ignored.
        /// </summary>
        public ICompletionData[] GenerateCompletionData(string line)
        {
            var items = new List<PythonCompletionData>(); //DefaultCompletionData

            var name = this.GetName(line);
            // A very simple test of callables!
            if (this._excludeCallables && name.Contains(')')) return null;

            if (!String.IsNullOrEmpty(name))
            {
                var stream = this._commandLine.ScriptScope.Engine.Runtime.IO.OutputStream;
                try
                {
                    this.AutocompletionInProgress = true;
                    // Another possibility:
                    //commandLine.ScriptScope.Engine.Runtime.IO.SetOutput(new System.IO.MemoryStream(), Encoding.UTF8);
                    //object value = commandLine.ScriptScope.Engine.CreateScriptSourceFromString(name, SourceCodeKind.Expression).Execute(commandLine.ScriptScope);
                    //IList<string> members = commandLine.ScriptScope.Engine.Operations.GetMemberNames(value);
                    var type = this.TryGetType(name);
                    // Use Reflection for everything except in-built Python types and COM pbjects. 
                    if (type != null && type.Namespace != "IronPython.Runtime" && (type.Name != "__ComObject"))
                    {
                        this.PopulateFromClrType(items, type, name);
                    }
                    else
                    {
                        var dirCommand = "dir(" + name + ")";
                        var value =
                            this._commandLine.ScriptScope.Engine.CreateScriptSourceFromString(dirCommand,
                                SourceCodeKind.Expression).Execute(this._commandLine.ScriptScope) as List;
                        this.AutocompletionInProgress = false;
                        items.AddRange(
                            value.Select(
                                member => new PythonCompletionData((string) member, name, this._commandLine, false)));
                    }
                }
                catch (ThreadAbortException tae)
                {
                    if (tae.ExceptionState is KeyboardInterruptException) Thread.ResetAbort();
                }
                catch
                {
                    // Do nothing.
                }
                this._commandLine.ScriptScope.Engine.Runtime.IO.SetOutput(stream, Encoding.UTF8);
                this.AutocompletionInProgress = false;
            }
            return items.OfType<ICompletionData>().ToArray();
        }

        protected Type TryGetType(string name)
        {
            var tryGetType = name + ".GetType()";
            object type = null;
            try
            {
                type =
                    this._commandLine.ScriptScope.Engine.CreateScriptSourceFromString(tryGetType,
                        SourceCodeKind.Expression).Execute(this._commandLine.ScriptScope);
            }
            catch (ThreadAbortException tae)
            {
                if (tae.ExceptionState is KeyboardInterruptException) Thread.ResetAbort();
            }
            catch
            {
                // Do nothing.
            }
            return type as Type;
        }

        protected void PopulateFromClrType(List<PythonCompletionData> items, Type type, string name)
        {
            var methodInfo = type.GetMethods();
            var propertyInfo = type.GetProperties();
            var fieldInfo = type.GetFields();
            var completionsList = (from methodInfoItem in methodInfo
                where
                    (methodInfoItem.IsPublic) && (methodInfoItem.Name.IndexOf("get_") != 0) &&
                    (methodInfoItem.Name.IndexOf("set_") != 0) && (methodInfoItem.Name.IndexOf("add_") != 0) &&
                    (methodInfoItem.Name.IndexOf("remove_") != 0) && (methodInfoItem.Name.IndexOf("__") != 0)
                select methodInfoItem.Name).ToList();
            completionsList.AddRange(propertyInfo.Select(propertyInfoItem => propertyInfoItem.Name));
            completionsList.AddRange(fieldInfo.Select(fieldInfoItem => fieldInfoItem.Name));
            completionsList.Sort();
            var last = "";
            for (var i = completionsList.Count - 1; i > 0; --i)
            {
                if (completionsList[i] == last) completionsList.RemoveAt(i);
                else last = completionsList[i];
            }
            items.AddRange(
                completionsList.Select(completion => new PythonCompletionData(completion, name, this._commandLine, true)));
        }

        /// <summary>
        ///     Generates completion data for the specified text. The text should be everything before
        ///     the dot character that triggered the completion. The text can contain the command line prompt
        ///     '>>>' as this will be ignored.
        /// </summary>
        public void GenerateDescription(string stub, string item, DescriptionUpdateDelegate updateDescription,
            bool isInstance)
        {
            var stream = this._commandLine.ScriptScope.Engine.Runtime.IO.OutputStream;
            var description = "";
            if (!String.IsNullOrEmpty(item))
            {
                try
                {
                    this.AutocompletionInProgress = true;
                    // Another possibility:
                    //commandLine.ScriptScope.Engine.Runtime.IO.SetOutput(new System.IO.MemoryStream(), Encoding.UTF8);
                    //object value = commandLine.ScriptScope.Engine.CreateScriptSourceFromString(item, SourceCodeKind.Expression).Execute(commandLine.ScriptScope);
                    //description = commandLine.ScriptScope.Engine.Operations.GetDocumentation(value);
                    var docCommand = "";
                    if (isInstance) docCommand = "type(" + stub + ")" + "." + item + ".__doc__";
                    else docCommand = stub + "." + item + ".__doc__";
                    object value =
                        this._commandLine.ScriptScope.Engine.CreateScriptSourceFromString(docCommand,
                            SourceCodeKind.Expression).Execute(this._commandLine.ScriptScope);
                    description = (string) value;
                    this.AutocompletionInProgress = false;
                }
                catch (ThreadAbortException tae)
                {
                    if (tae.ExceptionState is KeyboardInterruptException) Thread.ResetAbort();
                    this.AutocompletionInProgress = false;
                }
                catch
                {
                    this.AutocompletionInProgress = false;
                    // Do nothing.
                }
                this._commandLine.ScriptScope.Engine.Runtime.IO.SetOutput(stream, Encoding.UTF8);
                updateDescription(description);
            }
        }


        private string GetName(string text)
        {
            text = text.Replace("\t", "   ");
            var startIndex = text.LastIndexOf(' ');
            return text.Substring(startIndex + 1).Trim('.');
        }
    }
}