﻿using System;
using System.IO;
using System.Windows;
using System.Xml;
using ICSharpCode.AvalonEdit.Highlighting;
using ICSharpCode.AvalonEdit.Highlighting.Xshd;
using Microsoft.Scripting;

namespace RalphsDotNet.Apps.SimulationStudio.Base.Controls.PythonConsoleControl
{
    /// <summary>
    ///     Interaction logic for PythonConsoleControl.xaml
    /// </summary>
    public partial class PythonConsoleControl
    {
        private readonly PythonConsolePad _pad;

        private Action<PythonConsoleHost> _hostAction;

        public PythonConsoleControl()
        {
            this.InitializeComponent();
            this._pad = new PythonConsolePad();
            this._Grid.Children.Add(this._pad.Control);
            // Load our custom highlighting definition
            IHighlightingDefinition pythonHighlighting;
            using (
                var s =
                    typeof (IApplication).Assembly.GetManifestResourceStream(
                        "RalphsDotNet.Apps.SimulationStudio.Base.Resources.Python_Light.xshd"))
            {
                if (s == null)
                    throw new InvalidOperationException("Could not find embedded resource");
                using (XmlReader reader = new XmlTextReader(s))
                {
                    pythonHighlighting = HighlightingLoader.Load(reader, HighlightingManager.Instance);
                }
            }
            // and register it in the HighlightingManager
            HighlightingManager.Instance.RegisterHighlighting("Python Highlighting", new[] {".cool"}, pythonHighlighting);
            this._pad.Control.SyntaxHighlighting = pythonHighlighting;
            var transformers = this._pad.Control.TextArea.TextView.LineTransformers;
            for (var i = 0; i < transformers.Count; ++i)
            {
                if (transformers[i] is HighlightingColorizer)
                    transformers[i] = new PythonConsoleHighlightingColorizer(pythonHighlighting,
                        this._pad.Control.Document);
            }

            this.WithHost(h => this.PrepareConsole());
        }

        public PythonConsole Console
        {
            get { return this._pad.Console; }
        }

        public PythonConsoleHost Host
        {
            get { return this._pad.Host; }
        }

        public PythonConsolePad Pad
        {
            get { return this._pad; }
        }

        /// <summary>
        ///     Performs the specified action on the console host but only once the console
        ///     has initialized.
        /// </summary>
        public void WithHost(Action<PythonConsoleHost> hostAction)
        {
            this._hostAction = hostAction;
            this.Host.ConsoleCreated += this.Host_ConsoleCreated;
        }

        private void Host_ConsoleCreated(object sender, EventArgs e)
        {
            this.Console.ConsoleInitialized += this.Console_ConsoleInitialized;
        }

        private void Console_ConsoleInitialized(object sender, EventArgs e)
        {
            this._hostAction.Invoke(this.Host);
        }

        private void PrepareConsole()
        {
            var script = string.Empty;
            using (
                var s =
                    this.GetType()
                        .Assembly.GetManifestResourceStream(
                            "RalphsDotNet.Apps.SimulationStudio.Base.Resources.Console_Preparation.py"))
            {
                var sr = new StreamReader(s);
                script = sr.ReadToEnd();
            }

            var scriptSource = this.Console.ScriptScope.Engine.CreateScriptSourceFromString(script,
                SourceCodeKind.Statements);

            scriptSource.Execute(this.Console.ScriptScope);

            this.Console.ScriptScope.SetVariable("Context", ((IApplication)Application.Current).PythonConsoleContext);
        }
    }
}