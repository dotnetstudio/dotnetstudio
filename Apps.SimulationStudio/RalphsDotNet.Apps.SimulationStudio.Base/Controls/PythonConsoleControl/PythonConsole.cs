﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using ICSharpCode.AvalonEdit.Document;
using ICSharpCode.AvalonEdit.Editing;
using IronPython.Runtime;
using Microsoft.Scripting;
using Microsoft.Scripting.Hosting;
using Microsoft.Scripting.Hosting.Shell;
using Style = Microsoft.Scripting.Hosting.Shell.Style;

namespace RalphsDotNet.Apps.SimulationStudio.Base.Controls.PythonConsoleControl
{
    public delegate void ConsoleInitializedEventHandler(object sender, EventArgs e);

    /// <summary>
    ///     Custom IronPython console. The command dispacher runs on a separate UI thread from the REPL
    ///     and also from the WPF control.
    /// </summary>
    public class PythonConsole : IConsole, IDisposable
    {
        private const int LineReceivedEventIndex = 0;
        private readonly CommandLine _commandLine;
        private readonly CommandLineHistory _commandLineHistory = new CommandLineHistory();

        // This is the thread upon which all commands execute unless the dipatcher is overridden.
        private readonly Thread _dispatcherThread;
        private readonly ManualResetEvent _disposedEvent = new ManualResetEvent(false);
        private readonly ManualResetEvent _lineReceivedEvent = new ManualResetEvent(false);
        private readonly List<string> _previousLines = new List<string>();
        private readonly string _prompt;
        private readonly PythonTextEditor _textEditor;
        private readonly WaitHandle[] _waitHandles;
        private bool _allowCtrlSpaceAutocompletion;
        private bool _allowFullAutocompletion = true;
        private bool _consoleInitialized;
        private bool _disableAutocompletionForCallables = true;
        private Dispatcher _dispatcher;
        private Window _dispatcherWindow;
        private volatile bool _executing;

        // The index into the waitHandles array where the lineReceivedEvent is stored.

        private int _promptLength = 4;

        private string _scriptText = String.Empty;

        public PythonConsole(PythonTextEditor textEditor, CommandLine commandLine)
        {
            this._waitHandles = new WaitHandle[] {this._lineReceivedEvent, this._disposedEvent};

            this._commandLine = commandLine;
            this._textEditor = textEditor;
            textEditor.CompletionProvider = new PythonConsoleCompletionDataProvider(commandLine)
            {
                ExcludeCallables = this._disableAutocompletionForCallables
            };
            textEditor.PreviewKeyDown += this.textEditor_PreviewKeyDown;
            textEditor.TextEntering += this.textEditor_TextEntering;
            this._dispatcherThread = new Thread(this.DispatcherThreadStartingPoint);
            this._dispatcherThread.SetApartmentState(ApartmentState.STA);
            this._dispatcherThread.IsBackground = true;
            this._dispatcherThread.Start();

            // Only required when running outside REP loop.
            this._prompt = ">>> ";

            // Set commands:
            this._textEditor.TextArea.Dispatcher.Invoke(new Action(() =>
            {
                CommandBinding pasteBinding = null;
                CommandBinding copyBinding = null;
                CommandBinding cutBinding = null;
                CommandBinding undoBinding = null;
                CommandBinding deleteBinding = null;
                foreach (CommandBinding commandBinding in (this._textEditor.TextArea.CommandBindings))
                {
                    if (commandBinding.Command == ApplicationCommands.Paste) pasteBinding = commandBinding;
                    if (commandBinding.Command == ApplicationCommands.Copy) copyBinding = commandBinding;
                    if (commandBinding.Command == ApplicationCommands.Cut) cutBinding = commandBinding;
                    if (commandBinding.Command == ApplicationCommands.Undo) undoBinding = commandBinding;
                    if (commandBinding.Command == ApplicationCommands.Delete) deleteBinding = commandBinding;
                }
                // Remove current bindings completely from control. These are static so modifying them will cause other
                // controls' behaviour to change too.
                if (pasteBinding != null) this._textEditor.TextArea.CommandBindings.Remove(pasteBinding);
                if (copyBinding != null) this._textEditor.TextArea.CommandBindings.Remove(copyBinding);
                if (cutBinding != null) this._textEditor.TextArea.CommandBindings.Remove(cutBinding);
                if (undoBinding != null) this._textEditor.TextArea.CommandBindings.Remove(undoBinding);
                if (deleteBinding != null) this._textEditor.TextArea.CommandBindings.Remove(deleteBinding);
                this._textEditor.TextArea.CommandBindings.Add(new CommandBinding(ApplicationCommands.Paste, this.OnPaste,
                    this.CanPaste));
                this._textEditor.TextArea.CommandBindings.Add(new CommandBinding(ApplicationCommands.Copy, this.OnCopy,
                    PythonEditingCommandHandler.CanCutOrCopy));
                this._textEditor.TextArea.CommandBindings.Add(new CommandBinding(ApplicationCommands.Cut,
                    PythonEditingCommandHandler.OnCut, this.CanCut));
                this._textEditor.TextArea.CommandBindings.Add(new CommandBinding(ApplicationCommands.Undo, this.OnUndo,
                    this.CanUndo));
                this._textEditor.TextArea.CommandBindings.Add(new CommandBinding(ApplicationCommands.Delete,
                    PythonEditingCommandHandler.OnDelete(ApplicationCommands.NotACommand), this.CanDeleteCommand));
            }));
            var codeContext = DefaultContext.Default;
            // Set dispatcher to run on a UI thread independent of both the Control UI thread and thread running the REPL.
            ClrModule.SetCommandDispatcher(codeContext, this.DispatchCommand);
        }

        public bool AllowFullAutocompletion
        {
            get { return this._allowFullAutocompletion; }
            set { this._allowFullAutocompletion = value; }
        }

        public bool DisableAutocompletionForCallables
        {
            get { return this._disableAutocompletionForCallables; }
            set
            {
                if (this._textEditor.CompletionProvider != null)
                    this._textEditor.CompletionProvider.ExcludeCallables = value;
                this._disableAutocompletionForCallables = value;
            }
        }

        public bool AllowCtrlSpaceAutocompletion
        {
            get { return this._allowCtrlSpaceAutocompletion; }
            set { this._allowCtrlSpaceAutocompletion = value; }
        }

        public ScriptScope ScriptScope
        {
            get { return this._commandLine.ScriptScope; }
        }

        /// <summary>
        ///     Indicates whether there is a line already read by the console and waiting to be processed.
        /// </summary>
        public bool IsLineAvailable
        {
            get
            {
                lock (this._previousLines)
                {
                    return this._previousLines.Count > 0;
                }
            }
        }

        /// <summary>
        ///     Returns true if the cursor is in a readonly text editor region.
        /// </summary>
        private bool IsInReadOnlyRegion
        {
            get { return this.IsCurrentLineReadOnly || this.IsInPrompt; }
        }

        /// <summary>
        ///     Only the last line in the text editor is not read only.
        /// </summary>
        private bool IsCurrentLineReadOnly
        {
            get { return this._textEditor.Line < this._textEditor.TotalLines; }
        }

        /// <summary>
        ///     Determines whether the current cursor position is in a prompt.
        /// </summary>
        private bool IsInPrompt
        {
            get { return this._textEditor.Column - this._promptLength - 1 < 0; }
        }

        /// <summary>
        ///     Returns true if the user can delete at the current cursor position.
        /// </summary>
        private bool CanDelete
        {
            get
            {
                if (this._textEditor.SelectionLength > 0) return this.SelectionIsDeletable;
                return !this.IsInReadOnlyRegion;
            }
        }

        /// <summary>
        ///     Returns true if the user can backspace at the current cursor position.
        /// </summary>
        private bool CanBackspace
        {
            get
            {
                if (this._textEditor.SelectionLength > 0) return this.SelectionIsDeletable;
                var cursorIndex = this._textEditor.Column - this._promptLength - 1;
                return !this.IsCurrentLineReadOnly &&
                       (cursorIndex > 0 || (cursorIndex == 0 && this._textEditor.SelectionLength > 0));
            }
        }

        private bool SelectionIsDeletable
        {
            get
            {
                return (!this._textEditor.SelectionIsMultiline
                        && !this.IsCurrentLineReadOnly
                        && (this._textEditor.SelectionStartColumn - this._promptLength - 1 >= 0)
                        && (this._textEditor.SelectionEndColumn - this._promptLength - 1 >= 0));
            }
        }

        public TextWriter Output
        {
            get { return null; }
            set { }
        }

        public TextWriter ErrorOutput
        {
            get { return null; }
            set { }
        }

        /// <summary>
        ///     Returns the next line typed in by the console user. If no line is available this method
        ///     will block.
        /// </summary>
        public string ReadLine(int autoIndentSize)
        {
            var indent = String.Empty;
            if (autoIndentSize > 0)
            {
                indent = String.Empty.PadLeft(autoIndentSize);
                this.Write(indent, Style.Prompt);
            }

            var line = this.ReadLineFromTextEditor();
            if (line != null)
            {
                return indent + line;
            }
            return null;
        }

        /// <summary>
        ///     Writes text to the console.
        /// </summary>
        public void Write(string text, Style style)
        {
            this._textEditor.Write(text);
            if (style == Style.Prompt)
            {
                this._promptLength = text.Length;
                if (!this._consoleInitialized)
                {
                    this._consoleInitialized = true;
                    if (this.ConsoleInitialized != null) this.ConsoleInitialized(this, EventArgs.Empty);
                }
            }
        }

        /// <summary>
        ///     Writes text followed by a newline to the console.
        /// </summary>
        public void WriteLine(string text, Style style)
        {
            this.Write(text + Environment.NewLine, style);
        }

        /// <summary>
        ///     Writes an empty line to the console.
        /// </summary>
        public void WriteLine()
        {
            this.Write(Environment.NewLine, Style.Out);
        }

        public void Dispose()
        {
            this._disposedEvent.Set();
            this._textEditor.PreviewKeyDown -= this.textEditor_PreviewKeyDown;
            this._textEditor.TextEntering -= this.textEditor_TextEntering;
        }

        #region CommandHandling

        private const string LineSelectedType = "MSDEVLineSelect";
        // This is the type VS 2003 and 2005 use for flagging a whole line copy

        protected void CanPaste(object target, CanExecuteRoutedEventArgs args)
        {
            args.CanExecute = !this.IsInReadOnlyRegion;
        }

        protected void CanCut(object target, CanExecuteRoutedEventArgs args)
        {
            if (!this.CanDelete)
            {
                args.CanExecute = false;
            }
            else
                PythonEditingCommandHandler.CanCutOrCopy(target, args);
        }

        protected void CanDeleteCommand(object target, CanExecuteRoutedEventArgs args)
        {
            if (!this.CanDelete)
            {
                args.CanExecute = false;
            }
            else
                PythonEditingCommandHandler.CanDelete(target, args);
        }

        protected void CanUndo(object target, CanExecuteRoutedEventArgs args)
        {
            args.CanExecute = false;
        }

        protected void OnPaste(object target, ExecutedRoutedEventArgs args)
        {
            if (target != this._textEditor.TextArea) return;
            var textArea = this._textEditor.TextArea;
            if (textArea != null && textArea.Document != null)
            {
                Debug.WriteLine(Clipboard.GetText(TextDataFormat.Html));

                // convert text back to correct newlines for this document
                var newLine = TextUtilities.GetNewLineFromDocument(textArea.Document, textArea.Caret.Line);
                var text = TextUtilities.NormalizeNewLines(Clipboard.GetText(), newLine);
                var commands = text.Split(new[] {newLine}, StringSplitOptions.None);
                var scriptText = "";
                if (commands.Length > 1)
                {
                    text = newLine;
                    foreach (var command in commands)
                    {
                        text += "... " + command + newLine;
                        scriptText += command.Replace("\t", "   ") + newLine;
                    }
                }

                if (!string.IsNullOrEmpty(text))
                {
                    var fullLine = textArea.Options.CutCopyWholeLine && Clipboard.ContainsData(LineSelectedType);
                    var rectangular = Clipboard.ContainsData(RectangleSelection.RectangularSelectionDataType);
                    if (fullLine)
                    {
                        var currentLine = textArea.Document.GetLineByNumber(textArea.Caret.Line);
                        if (textArea.ReadOnlySectionProvider.CanInsert(currentLine.Offset))
                        {
                            textArea.Document.Insert(currentLine.Offset, text);
                        }
                    }
                    else if (rectangular && textArea.Selection.IsEmpty)
                    {
                        if (!RectangleSelection.PerformRectangularPaste(textArea, textArea.Caret.Position, text, false))
                            this._textEditor.Write(text, false);
                    }
                    else
                    {
                        this._textEditor.Write(text, false);
                    }
                }
                textArea.Caret.BringCaretToView();
                args.Handled = true;

                if (commands.Length > 1)
                {
                    lock (this._scriptText)
                    {
                        this._scriptText = scriptText;
                    }
                    this._dispatcherWindow.Dispatcher.BeginInvoke(new Action(this.ExecuteStatements));
                }
            }
        }

        protected void OnCopy(object target, ExecutedRoutedEventArgs args)
        {
            if (target != this._textEditor.TextArea) return;
            if (this._textEditor.SelectionLength == 0 && this._executing)
            {
                // Send the 'Ctrl-C' abort 
                //if (!IsInReadOnlyRegion)
                //{
                this.MoveToHomePosition();
                //textEditor.Column = GetLastTextEditorLine().Length + 1;
                //textEditor.Write(Environment.NewLine);
                //}
                this._dispatcherThread.Abort(new KeyboardInterruptException(""));
                args.Handled = true;
            }
            else PythonEditingCommandHandler.OnCopy(target, args);
        }

        protected void OnUndo(object target, ExecutedRoutedEventArgs args)
        {
        }

        #endregion

        public event ConsoleInitializedEventHandler ConsoleInitialized;

        protected void DispatchCommand(Delegate command)
        {
            if (command != null)
            {
                // Slightly involved form to enable keyboard interrupt to work.
                this._executing = true;
                var operation = this._dispatcher.BeginInvoke(DispatcherPriority.Normal, command);
                while (this._executing)
                {
                    if (operation.Status != DispatcherOperationStatus.Completed)
                        operation.Wait(TimeSpan.FromSeconds(1));
                    if (operation.Status == DispatcherOperationStatus.Completed)
                        this._executing = false;
                }
            }
        }

        private void DispatcherThreadStartingPoint()
        {
            this._dispatcherWindow = new Window();
            this._dispatcher = this._dispatcherWindow.Dispatcher;
            while (true)
            {
                try
                {
                    Dispatcher.Run();
                }
                catch (ThreadAbortException tae)
                {
                    if (tae.ExceptionState is KeyboardInterruptException)
                    {
                        Thread.ResetAbort();
                        this._executing = false;
                    }
                }
            }
        }

        public void SetDispatcher(Dispatcher dispatcher)
        {
            this._dispatcher = dispatcher;
        }

        /// <summary>
        ///     Run externally provided statements in the Console Engine.
        /// </summary>
        /// <param name="statements"></param>
        public void RunStatements(string statements)
        {
            this.MoveToHomePosition();
            lock (this._scriptText)
            {
                this._scriptText = statements;
            }
            this._dispatcher.BeginInvoke(new Action(this.ExecuteStatements));
        }

        /// <summary>
        ///     Run on the statement execution thread.
        /// </summary>
        private void ExecuteStatements()
        {
            lock (this._scriptText)
            {
                this._textEditor.Write("\r\n");
                var scriptSource = this._commandLine.ScriptScope.Engine.CreateScriptSourceFromString(this._scriptText,
                    SourceCodeKind.Statements);
                var error = "";
                try
                {
                    this._executing = true;
                    scriptSource.Execute(this._commandLine.ScriptScope);
                }
                catch (ThreadAbortException tae)
                {
                    if (tae.ExceptionState is KeyboardInterruptException) Thread.ResetAbort();
                    error = "KeyboardInterrupt" + Environment.NewLine;
                }
                catch (SyntaxErrorException exception)
                {
                    var eo = this._commandLine.ScriptScope.Engine.GetService<ExceptionOperations>();
                    error = eo.FormatException(exception);
                }
                catch (Exception exception)
                {
                    var eo = this._commandLine.ScriptScope.Engine.GetService<ExceptionOperations>();
                    error = eo.FormatException(exception) + Environment.NewLine;
                }
                this._executing = false;
                if (error != "") this._textEditor.Write(error);
                this._textEditor.Write(this._prompt);
            }
        }

        /// <summary>
        ///     Gets the text that is yet to be processed from the console. This is the text that is being
        ///     typed in by the user who has not yet pressed the enter key.
        /// </summary>
        public string GetCurrentLine()
        {
            var fullLine = this.GetLastTextEditorLine();
            return fullLine.Substring(this._promptLength);
        }

        /// <summary>
        ///     Gets the lines that have not been returned by the ReadLine method. This does not
        ///     include the current line.
        /// </summary>
        public string[] GetUnreadLines()
        {
            return this._previousLines.ToArray();
        }

        private string GetLastTextEditorLine()
        {
            return this._textEditor.GetLine(this._textEditor.TotalLines - 1);
        }

        private string ReadLineFromTextEditor()
        {
            var result = WaitHandle.WaitAny(this._waitHandles);
            if (result == LineReceivedEventIndex)
            {
                lock (this._previousLines)
                {
                    var line = this._previousLines[0];
                    this._previousLines.RemoveAt(0);
                    if (this._previousLines.Count == 0)
                    {
                        this._lineReceivedEvent.Reset();
                    }
                    return line;
                }
            }
            return null;
        }

        /// <summary>
        ///     Processes characters entered into the text editor by the user.
        /// </summary>
        private void textEditor_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Delete:
                    if (!this.CanDelete) e.Handled = true;
                    return;
                case Key.Tab:
                    if (this.IsInReadOnlyRegion) e.Handled = true;
                    return;
                case Key.Back:
                    if (!this.CanBackspace) e.Handled = true;
                    return;
                case Key.Home:
                    this.MoveToHomePosition();
                    e.Handled = true;
                    return;
                case Key.Down:
                    if (!this.IsInReadOnlyRegion) this.MoveToNextCommandLine();
                    e.Handled = true;
                    return;
                case Key.Up:
                    if (!this.IsInReadOnlyRegion) this.MoveToPreviousCommandLine();
                    e.Handled = true;
                    return;
            }
        }

        /// <summary>
        ///     Processes characters entering into the text editor by the user.
        /// </summary>
        private void textEditor_TextEntering(object sender, TextCompositionEventArgs e)
        {
            if (e.Text.Length > 0)
            {
                if (!char.IsLetterOrDigit(e.Text[0]))
                {
                    // Whenever a non-letter is typed while the completion window is open,
                    // insert the currently selected element.
                    this._textEditor.RequestCompletioninsertion(e);
                }
            }

            if (this.IsInReadOnlyRegion)
            {
                e.Handled = true;
            }
            else
            {
                if (e.Text[0] == '\n')
                {
                    this.OnEnterKeyPressed();
                }

                if (e.Text[0] == '.' && this._allowFullAutocompletion)
                {
                    this._textEditor.ShowCompletionWindow();
                }

                if ((e.Text[0] == ' ') && (Keyboard.Modifiers == ModifierKeys.Control))
                {
                    e.Handled = true;
                    if (this._allowCtrlSpaceAutocompletion) this._textEditor.ShowCompletionWindow();
                }
            }
        }

        /// <summary>
        ///     Move cursor to the end of the line before retrieving the line.
        /// </summary>
        private void OnEnterKeyPressed()
        {
            this._textEditor.StopCompletion();
            if (this._textEditor.WriteInProgress) return;
            lock (this._previousLines)
            {
                // Move cursor to the end of the line.
                this._textEditor.Column = this.GetLastTextEditorLine().Length + 1;

                // Append line.
                var currentLine = this.GetCurrentLine();
                this._previousLines.Add(currentLine);
                this._commandLineHistory.Add(currentLine);

                this._lineReceivedEvent.Set();
            }
        }

        /// <summary>
        ///     The home position is at the start of the line after the prompt.
        /// </summary>
        private void MoveToHomePosition()
        {
            this._textEditor.Line = this._textEditor.TotalLines;
            this._textEditor.Column = this._promptLength + 1;
        }

        /// <summary>
        ///     Shows the previous command line in the command line history.
        /// </summary>
        private void MoveToPreviousCommandLine()
        {
            if (this._commandLineHistory.MovePrevious())
            {
                this.ReplaceCurrentLineTextAfterPrompt(this._commandLineHistory.Current);
            }
        }

        /// <summary>
        ///     Shows the next command line in the command line history.
        /// </summary>
        private void MoveToNextCommandLine()
        {
            this._textEditor.Line = this._textEditor.TotalLines;
            if (this._commandLineHistory.MoveNext())
            {
                this.ReplaceCurrentLineTextAfterPrompt(this._commandLineHistory.Current);
            }
        }

        /// <summary>
        ///     Replaces the current line text after the prompt with the specified text.
        /// </summary>
        private void ReplaceCurrentLineTextAfterPrompt(string text)
        {
            var currentLine = this.GetCurrentLine();
            this._textEditor.Replace(this._promptLength, currentLine.Length, text);

            // Put cursor at end.
            this._textEditor.Column = this._promptLength + text.Length + 1;
        }
    }
}