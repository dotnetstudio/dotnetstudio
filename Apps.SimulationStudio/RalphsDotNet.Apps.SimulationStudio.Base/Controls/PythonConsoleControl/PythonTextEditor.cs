﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading;
using System.Windows.Input;
using System.Windows.Threading;
using ICSharpCode.AvalonEdit;
using ICSharpCode.AvalonEdit.Document;
using ICSharpCode.AvalonEdit.Editing;
using Microsoft.Scripting;

namespace RalphsDotNet.Apps.SimulationStudio.Base.Controls.PythonConsoleControl
{
    /// <summary>
    ///     Interface console to AvalonEdit and handle autocompletion.
    /// </summary>
    public class PythonTextEditor
    {
        public delegate string StringAction();

        private const int CompletionEventIndex = 0;
        private const int DescriptionEventIndex = 1;

        private readonly AutoResetEvent _completionRequestedEvent = new AutoResetEvent(false);
        private readonly Thread _completionThread;
        private readonly WaitHandle[] _completionWaitHandles;
        private readonly AutoResetEvent _descriptionRequestedEvent = new AutoResetEvent(false);
        private readonly StringBuilder _writeBuffer = new StringBuilder();
        internal TextArea TextArea;
        internal TextEditor TextEditor;
        private PythonConsoleCompletionDataProvider _completionProvider;
        private PythonConsoleCompletionWindow _completionWindow;
        private volatile bool _writeInProgress;

        public PythonTextEditor(TextEditor textEditor)
        {
            this.TextEditor = textEditor;
            this.TextArea = textEditor.TextArea;
            this._completionWaitHandles = new WaitHandle[]
            {this._completionRequestedEvent, this._descriptionRequestedEvent};
            this._completionThread = new Thread(this.Completion) {Priority = ThreadPriority.Lowest, IsBackground = true};
            //completionThread.SetApartmentState(ApartmentState.STA);
            this._completionThread.Start();
        }

        public bool WriteInProgress
        {
            get { return this._writeInProgress; }
        }

        public ICollection<CommandBinding> CommandBindings
        {
            get { return (this.TextArea.ActiveInputHandler as TextAreaDefaultInputHandler).CommandBindings; }
        }

        public int Column
        {
            get { return this.TextArea.Caret.Column; }
            set { this.TextArea.Caret.Column = value; }
        }

        /// <summary>
        ///     Gets the current cursor line.
        /// </summary>
        public int Line
        {
            get { return this.TextArea.Caret.Line; }
            set { this.TextArea.Caret.Line = value; }
        }

        /// <summary>
        ///     Gets the total number of lines in the text editor.
        /// </summary>
        public int TotalLines
        {
            get { return this.TextArea.Document.LineCount; }
        }

        public int SelectionStart
        {
            get { return this.TextArea.Selection.SurroundingSegment.Offset; }
        }

        public int SelectionLength
        {
            get { return this.TextArea.Selection.Length; }
        }

        public bool SelectionIsMultiline
        {
            get { return this.TextArea.Selection.IsMultiline; }
        }

        public int SelectionStartColumn
        {
            get
            {
                var startOffset = this.TextArea.Selection.SurroundingSegment.Offset;
                return startOffset - this.TextArea.Document.GetLineByOffset(startOffset).Offset + 1;
            }
        }

        public int SelectionEndColumn
        {
            get
            {
                var endOffset = this.TextArea.Selection.SurroundingSegment.EndOffset;
                return endOffset - this.TextArea.Document.GetLineByOffset(endOffset).Offset + 1;
            }
        }

        public PythonConsoleCompletionDataProvider CompletionProvider
        {
            get { return this._completionProvider; }
            set { this._completionProvider = value; }
        }

        public Thread CompletionThread
        {
            get { return this._completionThread; }
        }

        public PythonConsoleCompletionWindow CompletionWindow
        {
            get { return this._completionWindow; }
        }

        public void Write(string text)
        {
            this.Write(text, false);
        }

        public void Write(string text, bool allowSynchronous)
        {
            //text = text.Replace("\r\r\n", "\r\n");
            text = text.Replace("\r\r\n", "\r");
            text = text.Replace("\r\n", "\r");
            if (allowSynchronous)
            {
                this.MoveToEnd();
                this.PerformTextInput(text);
                return;
            }
            lock (this._writeBuffer)
            {
                this._writeBuffer.Append(text);
            }
            if (!this._writeInProgress)
            {
                this._writeInProgress = true;
                ThreadPool.QueueUserWorkItem(this.CheckAndOutputWriteBuffer);
                Stopwatch.StartNew();
            }
        }

        private void CheckAndOutputWriteBuffer(Object stateInfo)
        {
            var writeCompletedEvent = new AutoResetEvent(false);
            Action action = delegate
            {
                string toWrite;
                lock (this._writeBuffer)
                {
                    toWrite = this._writeBuffer.ToString();
                    this._writeBuffer.Remove(0, this._writeBuffer.Length);
                    //writeBuffer.Clear();
                }
                this.MoveToEnd();
                this.PerformTextInput(toWrite);
                writeCompletedEvent.Set();
            };
            while (true)
            {
                // Clear writeBuffer and write out.
                this.TextArea.Dispatcher.BeginInvoke(action, DispatcherPriority.Normal);
                // Check if writeBuffer has refilled in the meantime; if so clear and write out again.
                writeCompletedEvent.WaitOne();
                lock (this._writeBuffer)
                {
                    if (this._writeBuffer.Length == 0)
                    {
                        this._writeInProgress = false;
                        break;
                    }
                }
            }
        }

        private void MoveToEnd()
        {
            var lineCount = this.TextArea.Document.LineCount;
            if (this.TextArea.Caret.Line != lineCount) this.TextArea.Caret.Line = this.TextArea.Document.LineCount;
            var column = this.TextArea.Document.Lines[lineCount - 1].Length + 1;
            if (this.TextArea.Caret.Column != column) this.TextArea.Caret.Column = column;
        }

        private void PerformTextInput(string text)
        {
            if (text == "\n" || text == "\r\n")
            {
                var newLine = TextUtilities.GetNewLineFromDocument(this.TextArea.Document, this.TextArea.Caret.Line);
                this.TextEditor.AppendText(newLine);
                //using (textArea.Document.RunUpdate())
                //{

                //    textArea.Selection.ReplaceSelectionWithText(textArea, newLine);
                //}
            }
            else
                this.TextEditor.AppendText(text);
            this.TextArea.Caret.BringCaretToView();
        }

        /// <summary>
        ///     Gets the text for the specified line.
        /// </summary>
        public string GetLine(int index)
        {
            return (string) this.TextArea.Dispatcher.Invoke(new StringAction(delegate
            {
                var line = this.TextArea.Document.Lines[index];
                return this.TextArea.Document.GetText(line);
            }));
        }

        /// <summary>
        ///     Replaces the text at the specified index on the current line with the specified text.
        /// </summary>
        public void Replace(int index, int length, string text)
        {
            //int currentLine = textArea.Caret.Line - 1;
            var currentLine = this.TextArea.Document.LineCount - 1;
            var startOffset = this.TextArea.Document.Lines[currentLine].Offset;
            this.TextArea.Document.Replace(startOffset + index, length, text);
        }

        public event TextCompositionEventHandler TextEntering
        {
            add { this.TextArea.TextEntering += value; }
            remove { this.TextArea.TextEntering -= value; }
        }

        public event TextCompositionEventHandler TextEntered
        {
            add { this.TextArea.TextEntered += value; }
            remove { this.TextArea.TextEntered -= value; }
        }

        public event KeyEventHandler PreviewKeyDown
        {
            add { this.TextArea.PreviewKeyDown += value; }
            remove { this.TextArea.PreviewKeyDown -= value; }
        }

        public bool StopCompletion()
        {
            if (this._completionProvider.AutocompletionInProgress)
            {
                // send Ctrl-C abort
                this._completionThread.Abort(new KeyboardInterruptException(""));
                return true;
            }
            return false;
        }

        public void ShowCompletionWindow()
        {
            this._completionRequestedEvent.Set();
        }

        public void UpdateCompletionDescription()
        {
            this._descriptionRequestedEvent.Set();
        }

        /// <summary>
        ///     Perform completion actions on the background completion thread.
        /// </summary>
        private void Completion()
        {
            while (true)
            {
                var action = WaitHandle.WaitAny(this._completionWaitHandles);
                if (action == CompletionEventIndex && this._completionProvider != null)
                    this.BackgroundShowCompletionWindow();
                if (action == DescriptionEventIndex && this._completionProvider != null &&
                    this._completionWindow != null) this.BackgroundUpdateCompletionDescription();
            }
        }

        /// <summary>
        ///     Obtain completions (this runs in its own thread)
        /// </summary>
        internal void BackgroundShowCompletionWindow() //ICompletionItemProvider
        {
            // provide AvalonEdit with the data:
            var itemForCompletion = "";
            this.TextArea.Dispatcher.Invoke(new Action(() =>
            {
                var line = this.TextArea.Document.Lines[this.TextArea.Caret.Line - 1];
                itemForCompletion = this.TextArea.Document.GetText(line);
            }));

            var completions = this._completionProvider.GenerateCompletionData(itemForCompletion);

            if (completions != null && completions.Length > 0)
                this.TextArea.Dispatcher.BeginInvoke(new Action(delegate
                {
                    this._completionWindow = new PythonConsoleCompletionWindow(this.TextArea, this);
                    var data = this._completionWindow.CompletionList.CompletionData;
                    foreach (var completion in completions)
                    {
                        data.Add(completion);
                    }
                    this._completionWindow.Show();
                    this._completionWindow.Closed += delegate { this._completionWindow = null; };
                }));
        }

        internal void BackgroundUpdateCompletionDescription()
        {
            this._completionWindow.UpdateCurrentItemDescription();
        }

        public void RequestCompletioninsertion(TextCompositionEventArgs e)
        {
            if (this._completionWindow != null) this._completionWindow.CompletionList.RequestInsertion(e);
            // if autocompletion still in progress, terminate
            this.StopCompletion();
        }
    }
}