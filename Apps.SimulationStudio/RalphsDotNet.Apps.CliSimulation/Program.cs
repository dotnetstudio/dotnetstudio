﻿using System;
using System.IO;
using System.Linq;
using System.Threading;
using RalphsDotNet.Apps.SimulationStudio.Logic;
using RalphsDotNet.Service;
using RalphsDotNet.Tool.System;

namespace RalphsDotNet.Apps.CliSimulation
{
    internal class Program
    {
        private const string AppLogicId = "AppLogic";

        private static Thread _controllThread;

        public static ILogicService Logic { get; private set; }

        public static Dispatcher ControllDispatcher
        {
            get { return Dispatcher.ForThread(_controllThread); }
        }

        private static void Main(string[] args)
        {
            if (args.Length >= 1 && !args.Contains("-h"))
            {
                var sdbPath = args.Last();

                string outPath = null;
                if (args.Contains("-o"))
                {
                    var outPathIndex = args.ToList().IndexOf("-o") + 1;

                    if (args.Length > outPathIndex)
                        outPath = args[outPathIndex];
                    else
                    {
                        PrintHelp();
                        return;
                    }
                }

                int snapshot = -1;
                if (args.Contains("-s"))
                {
                    var outPathIndex = args.ToList().IndexOf("-s") + 1;

                    if (args.Length > outPathIndex)
                        snapshot = Convert.ToInt32(args[outPathIndex]);
                    else
                    {
                        PrintHelp();
                        return;
                    }
                }

                bool useCpu = args.Contains("--cpu");

                if (File.Exists(sdbPath))
                {
                    try
                    {
                        ConfigureLogic(useCpu);

                        InitializeControllDispatcher();

                        ControllDispatcher.Invoke(DispatcherPriority.Normal,
                            new Action(() => Logic.CreateOrOpenProject(sdbPath)));

                        if (snapshot > -1)
                        {
                            ControllDispatcher.Schedule(new Action<int>(SnapshotAndResume),
                                DateTime.UtcNow.AddMilliseconds(500), snapshot);

                            ControllDispatcher.Invoke(DispatcherPriority.Normal,
                                !string.IsNullOrEmpty(outPath)
                                    ? new Action(() => Logic.StartSimulation(outPath, 0))
                                    : new Action(() => Logic.StartSimulation(0)));
                        }
                        else
                        {
                            ControllDispatcher.Invoke(DispatcherPriority.Normal,
                                !string.IsNullOrEmpty(outPath)
                                    ? new Action(() => Logic.StartSimulation(outPath))
                                    : new Action(() => Logic.StartSimulation()));
                        }

                        CommandLoop();

                        ControllDispatcher.Invoke(DispatcherPriority.Normal, new Action(() => Logic.StopSimulation()));
                    }
                    finally
                    {
                        Logic.Dispose();
                        UninitializeControllDispatcher();
                    }
                }
                else
                {
                    Console.WriteLine(".sdb file is not existing");
                }
            }
            else
            {
                Console.WriteLine("argument count is not valid");
                PrintHelp();
            }
        }

        private static void SnapshotAndResume(int snapshot)
        {
            if (Logic.SimulationService.IsPaused)
            {
                    Logic.CreateConfigurationSnapshot();
                    Logic.SimulationService.Resume(snapshot);
            }

            ControllDispatcher.Schedule(new Action<int>(SnapshotAndResume), DateTime.UtcNow.AddMilliseconds(100), snapshot);
        }

        private static void InitializeControllDispatcher()
        {
            _controllThread = new Thread(RunControllDispatcher);
            _controllThread.Start();
            while(ControllDispatcher == null) Thread.Sleep(10);
        }

        private static void RunControllDispatcher()
        {
            try
            {
                Dispatcher.Run();
            }
            catch (ThreadAbortException)
            {
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());

                RunControllDispatcher();
            }
        }

        private static void UninitializeControllDispatcher()
        {
            lock (_controllThread)
            {
                if (_controllThread.IsAlive)
                {
                    ControllDispatcher.Stop();
                }
            }
        }

        private static void ConfigureLogic(bool useCpu)
        {
            ServiceProvider.ServiceConfigurations.AddOrReplace(new LogicServiceConfiguration
            {
                Id = AppLogicId,
                ServiceAssemblyName = typeof (LogicService).Assembly.GetName().Name,
                UseCpu = useCpu
            });
            Logic = ServiceProvider.GetService<ILogicService>(AppLogicId);
        }

        private static void PrintHelp()
        {
            Console.WriteLine("usage: RalphsDotNet.Apps.CliSimulation.exe [-o <.ss outpath> <.sdb path>");
        }

        private static void PrintCommandList()
        {
            Console.WriteLine("?:\thelp");
            Console.WriteLine("i:\tactual iteration");
            Console.WriteLine("q:\tstop simulation and quit");
        }

        private static void CommandLoop()
        {
            var cmd = '?';

            while (cmd != 'q')
            {
                switch (cmd)
                {
                    case '?':
                        Console.WriteLine();
                        PrintCommandList();
                        break;
                    case 'i':
                        Console.WriteLine();
                        Console.WriteLine(Logic.SimulationService.ActualIteration != null
                            ? Logic.SimulationService.ActualIteration.Number
                            : 0);
                        break;
                    case 'q':
                        break;
                    default:
                        Console.WriteLine();
                        Console.WriteLine("unknown command");
                        break;
                }

                if (cmd != 'q')
                {
                    Console.WriteLine();
                    cmd = Console.ReadKey().KeyChar;
                }
            }
        }
    }
}