﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace RalphsDotNet.Apps.SimulationStudio.Logic
{
    [DataContract]
    public class SystemConfiguration
    {
        public SystemConfiguration()
        {
            this.NumberProperties = new List<Tuple<Guid, string>>();
        }

        [DataMember]
        public int Amount { get; set; }

        [DataMember]
        public List<Tuple<Guid, string>> NumberProperties { get; set; }
    }
}