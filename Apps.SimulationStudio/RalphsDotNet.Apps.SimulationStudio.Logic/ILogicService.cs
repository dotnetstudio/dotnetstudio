﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;
using RalphsDotNet.Data;
using RalphsDotNet.Service;
using RalphsDotNet.Service.Data.Interface;
using RalphsDotNet.Service.DataStream.Interface;
using RalphsDotNet.Service.Simulation.Data;
using RalphsDotNet.Service.Simulation.Interface;
using RalphsDotNet.Service.Simulation.Model;

namespace RalphsDotNet.Apps.SimulationStudio.Logic
{
    [DataContract]
    public class LogicServiceConfiguration : ServiceConfiguration
    {
        private bool _useCpu;

        [DataMember(Order = 100)]
        public bool UseCpu
        {
            get { return this._useCpu; }
            set
            {
                if (this._useCpu != value)
                {
                    this._useCpu = value;
                    this.OnPropertyChanged("UseCpu");
                }
            }
        }

        [Browsable(false)]
        public override Type ServiceType
        {
            get { return typeof (ILogicService); }
        }
    }

    public interface ILogicService : IService, INotifyPropertyChanged
    {
        IDataService DataService { get; }
        IDataStreamService DataStreamService { get; }
        ISimulationService SimulationService { get; }
        DataStreamBase DataStream { get; }
        ISimulationDescription Simulation { get; }
        IEnumerable<IExtensionProfile> Configurations { get; }

        void StartSimulation();
        void StartSimulation(int pauseAfter);
        void StartSimulation(string streamPath);
        void StartSimulation(string streamPath, int pauseAfter);
        void DebugSimulation();
        void StopSimulation();
        void StepSimulation();
        void StepSimulation(IIteration iteration);

        IExtensionProfile CreateConfigurationSnapshot();
        IExtensionProfile CreateConfiguration();
        void LoadConfiguration(IExtensionProfile configuration);
        void DeleteConfiguration(IExtensionProfile configuration);

        void Save();
        void CreateOrOpenProject(string sdbPath);
        void Reload();
        void CloseProject();
    }
}