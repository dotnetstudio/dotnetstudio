﻿using System.Runtime.Serialization;

namespace RalphsDotNet.Apps.SimulationStudio.Logic
{
    [DataContract]
    public class SimulationConfiguration
    {
        [DataMember]
        public int SampleRate { get; set; }

        [DataMember]
        public int ObstacleMiliseconds { get; set; }

        [DataMember]
        public bool SupportDouble { get; set; }
    }
}