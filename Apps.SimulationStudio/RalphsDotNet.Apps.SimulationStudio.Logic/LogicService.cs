﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Threading;
using RalphsDotNet.Data;
using RalphsDotNet.Service;
using RalphsDotNet.Service.Data;
using RalphsDotNet.Service.Data.Interface;
using RalphsDotNet.Service.DataStream;
using RalphsDotNet.Service.DataStream.Interface;
using RalphsDotNet.Service.OpenCl;
using RalphsDotNet.Service.OpenCl.Interface;
using RalphsDotNet.Service.Simulation;
using RalphsDotNet.Service.Simulation.Data;
using RalphsDotNet.Service.Simulation.Interface;
using RalphsDotNet.Service.Simulation.Model;

namespace RalphsDotNet.Apps.SimulationStudio.Logic
{
    public class LogicService : ILogicService
    {
        public const string DataServiceId = "Data";
        public const string DataStreamServiceId = "DataStream";
        public const string DataStreamId = "Stream";
        public const string OpenClServiceId = "OpenCl";
        public const string SimulationServiceId = "Simu";

        public static readonly Guid ConfigurationProfileId = Guid.Parse("919a6925-c8e0-4e10-9da5-72c382dd6bbc");
        private readonly ServiceBase _proxy;

        private Stream _fileStream;
        private string _sdbPath;

        public LogicService(ServiceBase proxy, ServiceConfiguration configuration)
        {
            this._proxy = proxy;
            this.ServiceConfiguration = configuration;

            this.ConfigureOpenClService();
            this.ConfigureDataStreamService();
            this.ConfigureSimulationService();
        }

        public IOpenClService OpenClService
        {
            get
            {
                return ServiceProvider.ServiceConfigurations[OpenClServiceId] != null
                    ? ServiceProvider.GetService<IOpenClService>(OpenClServiceId)
                    : null;
            }
        }

        public IEnumerable<IExtensionProfile> Configurations
        {
            get
            {
                return this.DataService != null
                    ? this.DataService.GetRepository<IExtensionProfile, IExtensionProfileRepository>()
                        .All.Where(p => p.ProfileId == ConfigurationProfileId)
                    : null;
            }
        }

        public IDataService DataService
        {
            get
            {
                return ServiceProvider.ServiceConfigurations[DataServiceId] != null
                    ? ServiceProvider.GetService<IDataService>(DataServiceId)
                    : null;
            }
        }

        public IDataStreamService DataStreamService
        {
            get
            {
                return ServiceProvider.ServiceConfigurations[DataStreamServiceId] != null
                    ? ServiceProvider.GetService<IDataStreamService>(DataStreamServiceId)
                    : null;
            }
        }

        public ISimulationService SimulationService
        {
            get
            {
                return ServiceProvider.ServiceConfigurations[SimulationServiceId] != null
                    ? ServiceProvider.GetService<ISimulationService>(SimulationServiceId)
                    : null;
            }
        }

        public DataStreamBase DataStream
        {
            get { return this.DataStreamService.GetStream(DataStreamId); }
        }

        public ISimulationDescription Simulation { get; private set; }

        public void Dispose()
        {
            this.DisposeServices();
        }

        public string Id
        {
            get { return this.ServiceConfiguration.Id; }
        }

        public ServiceConfiguration ServiceConfiguration { get; private set; }
        public event PropertyChangedEventHandler PropertyChanged;

        public void StartSimulation()
        {
            this.StartSimulation(null, -1, false);
        }

        public void StartSimulation(int pauseAfter)
        {
            this.StartSimulation(null, pauseAfter, false);
        }

        public void StartSimulation(string streamPath)
        {
            this.StartSimulation(streamPath, -1, false);
        }

        public void StartSimulation(string streamPath, int pauseAfter)
        {
            this.StartSimulation(streamPath, pauseAfter, false);
        }

        public void DebugSimulation()
        {
            this.StartSimulation(null, -1, true);
        }

        public void StopSimulation()
        {
            try
            {
                this.SimulationService.Stop();
            }
            finally
            {
                this.UnconfigureDataStream();
            }
        }

        public void StepSimulation()
        {
            this.SimulationService.Step();
        }

        public void StepSimulation(IIteration iteration)
        {
            this.SimulationService.Step(iteration);
        }

        public void CreateOrOpenProject(string sdbPath)
        {
            this._sdbPath = sdbPath;
            this.ConfigureDataService();

            var repo = this.DataService.GetRepository<ISimulationDescription, ISimulationDescriptionRepository>();

            this.Simulation = repo.All.FirstOrDefault();

            this.OnPropertyChanged("Configurations");
            if (this.Simulation == null)
            {
                this.Simulation = repo.Create();
                repo.SaveOrUpdate(this.Simulation);
            }

            this.OnPropertyChanged("Simulation");
        }

        public void DeleteConfiguration(IExtensionProfile configuration)
        {
            this.DataService.Delete(configuration);
            this.OnPropertyChanged("Configurations");
        }

        public IExtensionProfile CreateConfiguration()
        {
            if (this.Simulation.SystemDescriptions.Any()) // create only if there is something to create
            {
                var siSer = new DataContractSerializer(typeof (SimulationConfiguration));
                var sySer = new DataContractSerializer(typeof (SystemConfiguration));
                var arepo = this.DataService.GetRepository<IAttachment, IAttachmentRepository>();

                var configuration =
                    this.DataService.GetRepository<IExtensionProfile, IExtensionProfileRepository>()
                        .Create(ConfigurationProfileId, Guid.NewGuid().ToString());

                var siConf = new SimulationConfiguration
                {
                    SampleRate = this.Simulation.SampleRate,
                    ObstacleMiliseconds = this.Simulation.ObstacleMiliseconds,
                    SupportDouble = this.Simulation.SupportDouble
                };

                var att = arepo.Create(this.Simulation.Id.ToString(), "SimulationConfiguration");
                using (var ms = new MemoryStream())
                {
                    siSer.WriteObject(ms, siConf);
                    att.FileContent = ms.ToArray();
                }

                configuration.Attachments.Add(att);

                foreach (var system in this.Simulation.SystemDescriptions)
                {
                    var conf = new SystemConfiguration {Amount = system.Amount};

                    foreach (var np in system.NumberPropertyDescriptions)
                    {
                        conf.NumberProperties.Add(new Tuple<Guid, string>(np.Id, np.Distribution));
                    }

                    att = arepo.Create(system.Id.ToString(), string.Format("{0}.SystemConfiguration", system.Name));
                    using (var ms = new MemoryStream())
                    {
                        sySer.WriteObject(ms, conf);
                        att.FileContent = ms.ToArray();
                    }

                    configuration.Attachments.Add(att);
                }

                this.DataService.SaveOrUpdate(configuration);

                this.OnPropertyChanged("Configurations");

                return configuration;
            }

            return null;
        }

        public IExtensionProfile CreateConfigurationSnapshot()
        {
            if (this.SimulationService.IsRunning) // create only if there is something to create
            {
                var wasPaused = this.SimulationService.IsPaused;
                if (!wasPaused)
                    this.SimulationService.Pause();

                var siSer = new DataContractSerializer(typeof (SimulationConfiguration));
                var sySer = new DataContractSerializer(typeof (SystemConfiguration));
                var arepo = this.DataService.GetRepository<IAttachment, IAttachmentRepository>();

                var iteration = this.SimulationService.ActualIteration;

                var configuration =
                    this.DataService.GetRepository<IExtensionProfile, IExtensionProfileRepository>()
                        .Create(ConfigurationProfileId,
                            string.Format("{0:yyyy-MM-dd HH:mm:ss} {1}", DateTime.Now, iteration.Number));

                var siConf = new SimulationConfiguration
                {
                    SampleRate = this.SimulationService.SampleRate,
                    ObstacleMiliseconds = this.SimulationService.ObstacleMiliseconds,
                    SupportDouble = this.Simulation.SupportDouble
                };

                var att = arepo.Create(this.Simulation.Id.ToString(), "SimulationConfiguration");
                using (var ms = new MemoryStream())
                {
                    siSer.WriteObject(ms, siConf);
                    att.FileContent = ms.ToArray();
                }

                configuration.Attachments.Add(att);

                foreach (var systemDescription in this.Simulation.SystemDescriptions)
                {
                    var systems = iteration.Systems.Where(s => s.Name == systemDescription.Name).ToArray();
                    var conf = new SystemConfiguration {Amount = systems.Length};

                    foreach (var np in systemDescription.NumberPropertyDescriptions)
                    {
                        var distribution = string.Empty;

                        if (np.IsArray)
                        {
                            distribution += "[";

                            foreach (
                                var property in
                                    systems.Select(system => system.NumberArrayProperties.First(n => n.Name == np.Name))
                                )
                            {
                                distribution += "[";
                                distribution = property.Value.Aggregate(distribution,
                                    (current, t) =>
                                        current + string.Concat(t.ToString("e17", CultureInfo.InvariantCulture), ", "));
                                distribution = distribution.Remove(distribution.Length - 2);
                                distribution += "], ";
                            }
                            distribution = distribution.Remove(distribution.Length - 2);
                            distribution += "][int(k) - 1][int(i) - 1]";
                        }
                        else
                        {
                            distribution += "[";

                            foreach (var system in systems)
                                distribution +=
                                    string.Concat(
                                        system.NumberProperties.First(n => n.Name == np.Name)
                                            .Value.ToString("e17", CultureInfo.InvariantCulture),
                                        ", ");

                            if (systems.Length > 0) distribution = distribution.Remove(distribution.Length - 2);
                            distribution += "][int(k) - 1]";
                        }

                        conf.NumberProperties.Add(new Tuple<Guid, string>(np.Id,
                            distribution.Replace("NaN", "\"nan\"")
                                .Replace("Infinity", "+inf")
                                .Replace("-Infinity", "-inf")));
                    }

                    att = arepo.Create(systemDescription.Id.ToString(),
                        string.Format("{0}.SystemConfiguration", systemDescription.Name));
                    using (var ms = new MemoryStream())
                    {
                        sySer.WriteObject(ms, conf);
                        att.FileContent = ms.ToArray();
                    }

                    configuration.Attachments.Add(att);
                }

                this.DataService.SaveOrUpdate(configuration);

                this.OnPropertyChanged("Configurations");

                if (!wasPaused)
                    this.SimulationService.Resume();

                return configuration;
            }

            return null;
        }

        public void LoadConfiguration(IExtensionProfile configuration)
        {
            var siSer = new DataContractSerializer(typeof (SimulationConfiguration));
            var sySer = new DataContractSerializer(typeof (SystemConfiguration));

            foreach (var att in configuration.Attachments)
            {
                using (var ms = new MemoryStream(att.FileContent))
                {
                    if (att.Name == this.Simulation.Id.ToString())
                    {
                        var conf = siSer.ReadObject(ms) as SimulationConfiguration;

                        this.Simulation.SampleRate = conf.SampleRate;
                        this.Simulation.ObstacleMiliseconds = conf.ObstacleMiliseconds;
                        this.Simulation.SupportDouble = conf.SupportDouble;
                    }
                    else
                    {
                        var conf = sySer.ReadObject(ms) as SystemConfiguration;

                        if (conf != null)
                        {
                            var system =
                                this.Simulation.SystemDescriptions.FirstOrDefault(s => s.Id == Guid.Parse(att.Name));

                            if (system != null)
                            {
                                system.Amount = conf.Amount;

                                foreach (var cnp in conf.NumberProperties)
                                {
                                    var np = system.NumberPropertyDescriptions.FirstOrDefault(n => n.Id == cnp.Item1);

                                    if (np != null)
                                        np.Distribution = cnp.Item2;
                                }
                            }
                        }
                    }
                }
            }
        }

        public void Save()
        {
            if (this.DataService != null)
            {
                this.DataService.BeginTransaction();
                try
                {
                    this.DataService.SaveOrUpdate(this.Simulation);
                    this.DataService.SaveOrUpdate(this.Configurations);

                    this.DataService.CommitTransaction();
                }
                catch (Exception)
                {
                    this.DataService.RollbackTransaction();
                    throw;
                }
            }
        }

        public void Reload()
        {
            this.DataService.Reload(this.Simulation);
        }

        public void CloseProject()
        {
            try
            {
                if (this.SimulationService != null && this.SimulationService.IsRunning)
                {
                    this.StopSimulation();
                }

                this.Simulation = null;
            }
            finally
            {
                if (this.DataService != null)
                {
                    this.DataService.Dispose();
                    this.UnConfigureService(DataServiceId);

                    this.OnPropertyChanged("Configurations");
                }
            }

            this.OnPropertyChanged("Simulation");
        }

        public void StartSimulation(string streamPath, int pauseAfter, bool debug)
        {
            this.ConfigureDataStream(streamPath);

            try
            {
                if (debug)
                    this.SimulationService.Debug(this.Simulation,
                        this.DataService.GetRepository<IExtensionProfile, IExtensionProfileRepository>().All.ToArray());
                else
                    this.SimulationService.Run(this.Simulation,
                        this.DataService.GetRepository<IExtensionProfile, IExtensionProfileRepository>().All.ToArray(),
                        pauseAfter);

                // wait for an iteration or death
                while (this.SimulationService.ActualIteration == null && this.SimulationService.IsRunning)
                    new ManualResetEvent(false).WaitOne(TimeSpan.FromMilliseconds(1));
            }
            catch (Exception)
            {
                this.UnconfigureDataStream();
                throw;
            }
        }

        private void UnconfigureDataStream()
        {
            if (this.DataStream != null)
            {
                this.DataStream.Dispose();
                this._fileStream = null;
            }
        }

        private void ConfigureDataStream(string streamPath)
        {
            this._fileStream = !string.IsNullOrEmpty(streamPath) ? File.OpenWrite(streamPath) : null;
            this.DataStreamService.CreateStream(DataStreamId, this._fileStream);
        }

        private void ConfigureDataService()
        {
            ServiceProvider.ServiceConfigurations.AddOrReplace(new DataServiceConfiguration
            {
                Id = DataServiceId,
                ServiceAssemblyName = typeof (DataService).Assembly.GetName().Name,
                DataManagerType = typeof (NHibernateManager),
                ObjectAssemblies =
                    new[]
                    {
                        Assembly.Load("RalphsDotNet.Data.Base"),
                        Assembly.Load("RalphsDotNet.Service.Simulation.Data")
                    },
                RepositoryAssemblies =
                    new[]
                    {
                        Assembly.Load("RalphsDotNet.Data.Base.Repositories"),
                        Assembly.Load("RalphsDotNet.Service.Simulation.Data.Repositories")
                    },
                Values = new[]
                {
                    new ConfigurationValue
                    {
                        Key = NHibernateManager.ConfigDbconfig,
                        Value = "FluentNHibernate.Cfg.Db.SQLiteConfiguration, FluentNHibernate"
                    },
                    new ConfigurationValue
                    {
                        Key = NHibernateManager.ConfigDbvariant,
                        Value = "Standard"
                    },
                    new ConfigurationValue
                    {
                        Key = NHibernateManager.ConfigConnectionstring,
                        Value = string.Format("FullUri=file:{0}?cache=shared", this._sdbPath)
                    }
                }
            });
        }

        private void UnConfigureService(string id)
        {
            ServiceProvider.ServiceConfigurations.Remove(id);
        }

        private void ConfigureDataStreamService()
        {
            ServiceProvider.ServiceConfigurations.AddOrReplace(new DataStreamServiceConfiguration
            {
                Id = DataStreamServiceId,
                ServiceAssemblyName = typeof (DataStreamService).Assembly.GetName().Name,
                ObjectAssemblies =
                    new[]
                    {
                        Assembly.Load("RalphsDotNet.Service.Simulation.Data")
                    }
            });
            ServiceProvider.GetService<IDataStreamService>(DataStreamServiceId);
        }

        private void ConfigureSimulationService()
        {
            ServiceProvider.ServiceConfigurations.AddOrReplace(new Service.Simulation.Interface.SimulationConfiguration
            {
                Id = SimulationServiceId,
                DataStreamServiceId = DataStreamServiceId,
                DataStreamId = DataStreamId,
                OpenClServiceId = OpenClServiceId,
                ServiceAssemblyName = typeof (SimulationService).Assembly.GetName().Name,
                UseCpu = ((LogicServiceConfiguration)this.ServiceConfiguration).UseCpu
            });
        }

        private void ConfigureOpenClService()
        {
            ServiceProvider.ServiceConfigurations.AddOrReplace(new OpenClServiceConfiguration
            {
                Id = OpenClServiceId,
                ServiceAssemblyName = typeof (OpenClService).Assembly.GetName().Name
            });
        }

        private void DisposeServices()
        {
            this.CloseProject();
            if (this.SimulationService != null)
            {
                this.SimulationService.Dispose();
                this.UnConfigureService(SimulationServiceId);
            }
            if (this.OpenClService != null)
            {
                this.OpenClService.Dispose();
                this.UnConfigureService(OpenClServiceId);
            }
            if (this.DataStreamService != null)
            {
                this.DataStreamService.Dispose();
                this.UnConfigureService(DataStreamServiceId);
            }
        }

        protected virtual void OnPropertyChanged(string propertyName = null)
        {
            var handler = this.PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}