﻿using NHibernate.Collection;
using NHibernate.Engine;
using NHibernate.Persister.Collection;
using NHibernate.Type;
using NHibernate.UserTypes;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace RalphsDotNet.Service.Data
{
    public class ObservableListType<T> : CollectionType, IUserCollectionType
    {
        public ObservableListType(string role, string foreignKeyPropertyName, bool isEmbeddedInXml)
            : base(role, foreignKeyPropertyName, isEmbeddedInXml) { }

        public ObservableListType()
            : base(string.Empty, string.Empty, false) { }

        public IPersistentCollection Instantiate(ISessionImplementor session, ICollectionPersister persister)
        {
            return new PersistentObservableGenericList<T>(session);
        }

        public override IPersistentCollection Instantiate(ISessionImplementor session, ICollectionPersister persister, object key)
        {
            return new PersistentObservableGenericList<T>(session);
        }

        public override IPersistentCollection Wrap(ISessionImplementor session, object collection)
        {
            return new PersistentObservableGenericList<T>(session, (IList<T>)collection);
        }

        public IEnumerable GetElements(object collection)
        {
            return ((IEnumerable)collection);
        }

        public bool Contains(object collection, object entity)
        {
            return ((ICollection<T>)collection).Contains((T)entity);
        }


        protected override void Clear(object collection)
        {
            ((IList)collection).Clear();
        }

        public object ReplaceElements(object original, object target, ICollectionPersister persister, object owner, IDictionary copyCache, ISessionImplementor session)
        {
            var result = (ICollection<T>)target;
            result.Clear();
            foreach (var item in ((IEnumerable)original))
            {
                if (copyCache.Contains(item))
                    result.Add((T)copyCache[item]);
                else
                    result.Add((T)item);
            }
            return result;
        }

        public override object Instantiate(int anticipatedSize)
        {
            return new ObservableCollection<T>();
        }

        public override System.Type ReturnedClass
        {
            get
            {
                return typeof(PersistentObservableGenericList<T>);
            }
        }
    }
}
