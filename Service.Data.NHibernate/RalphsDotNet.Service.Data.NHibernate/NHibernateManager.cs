using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Common;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using FluentNHibernate;
using FluentNHibernate.Automapping;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using FluentNHibernate.Conventions;
using FluentNHibernate.Conventions.Instances;
using NHibernate;
using NHibernate.AdoNet;
using NHibernate.Cache;
using NHibernate.Linq;
using NHibernate.SqlCommand;
using NHibernate.Tool.hbm2ddl;
using NHibernate.Type;
using RalphsDotNet.Data;
using RalphsDotNet.Extension;

namespace RalphsDotNet.Service.Data
{
    /// <summary>
    ///     Service for the persisting data objects via FluentNHibernate.
    /// </summary>
    public class NHibernateManager : DataManager
    {
// ReSharper disable UnreachableCode
// ReSharper disable HeuristicUnreachableCode

        /* FluentNHibernate.Cfg.Db.MySQLConfiguration, FluentNHibernate -> Standard
         * FluentNHibernate.Cfg.Db.PostgreSQLConfiguration, FluentNHibernate -> PostgreSQL82
         * FluentNHibernate.Cfg.Db.MsSqlConfiguration, FluentNHibernate -> MsSql2008
         * RalphsDotNet.Service.Data.MonoSQLiteConfiguration, RalphsDotNet.Service.Data.NHibernate -> Standard  */
        public const string ConfigDbconfig = "DBCONFIG";
        public const string ConfigDbvariant = "DBVARIANT";
        public const string ConfigDbBatchSize = "DBBATCHSIZE";
        public const string ConfigDbBatcherFactory = "DBBATCHERFACTORY";
        public const string ConfigConnectionstring = "CONNECTIONSTRING";
        public const string ConfigAssemblies = "ASSEMBLIES";

        public int BatchSize { get; private set; }

        private NHibernate.Cfg.Configuration _configuration;
        private DbConnection _connection;
        private ISessionFactory _factory;
        private FluentConfiguration _fluentConfiguration;
        private ISession _session;
        private IStatelessSession _statelessSession;
        private readonly List<IDataObject> _batchTransactionSaveOrUpdateObjects = new List<IDataObject>();
        private readonly List<IDataObject> _batchTransactionDeleteObjects = new List<IDataObject>();

        private readonly Dictionary<Type, PropertyInfo[]> _batchTransactionChildInfos =
            new Dictionary<Type, PropertyInfo[]>();

        private readonly Dictionary<Type, PropertyInfo[]> _batchTransactionParentInfos =
            new Dictionary<Type, PropertyInfo[]>();

        public override bool ActiveTransaction
        {
            get
            {
                return (this._session != null && this._session.Transaction != null && this._session.Transaction.IsActive);
            }
        }

        public override bool ActiveBatchTransaction
        {
            get
            {
                return (this._statelessSession != null && this._statelessSession.Transaction != null &&
                        this._statelessSession.Transaction.IsActive);
            }
        }

        protected override void OnInitialize()
        {
            this.BuildSession();

            base.OnInitialize();
        }

        private void BuildSession()
        {
            // https://github.com/jagregory/fluent-nhibernate/wiki/Auto-mapping
            // expecting that DataService emited all types into one assembly and there is at least one type(if there is no type it makes all no sense)

            var batchSize = this.GetConfigValue(ConfigDbBatchSize);
            this.BatchSize = !string.IsNullOrEmpty(batchSize) ? Convert.ToInt32(batchSize) : 1000;

            var configurerType = this.GetConfigValue(ConfigDbconfig).FindType();
            var configurer =
                configurerType.GetProperty(this.GetConfigValue(ConfigDbvariant)).GetValue(null, null) as
                    IPersistenceConfigurer;
            configurer = configurerType.GetMethodExtended("ConnectionString", typeof (string))
                .Invoke(configurer, new object[] {this.GetConfigValue(ConfigConnectionstring)}) as
                IPersistenceConfigurer;
            configurer = configurerType.GetMethodExtended("Raw", typeof (string), typeof (string))
                .Invoke(configurer, new object[] {"connection.release_mode", "on_close"}) as
                IPersistenceConfigurer;

            if (!string.IsNullOrEmpty(this.GetConfigValue(ConfigDbBatcherFactory)))
                configurer = configurerType.GetMethodExtended("Raw", typeof (string), typeof (string))
                    .Invoke(configurer,
                        new object[] {"adonet.factory_class", this.GetConfigValue(ConfigDbBatcherFactory)}) as
                    IPersistenceConfigurer;

            //configurer = configurerType.GetMethodExtended("Raw", typeof (string), typeof (string))
            //    .Invoke(configurer,
            //        new object[] { "hibernate.connection.autocommit", "false" }) as
            //    IPersistenceConfigurer;

            configurer = configurerType.GetMethodExtended("Raw", typeof (string), typeof (string))
                .Invoke(configurer,
                    new object[] {"adonet.batch_size", this.BatchSize.ToString()}) as
                IPersistenceConfigurer;
#if DEBUG
            configurer = configurerType.GetMethodExtended("Raw", typeof (string), typeof (string))
                .Invoke(configurer, new object[] {"show_sql", "false"}) as
                IPersistenceConfigurer;
#else
            configurer = configurerType.GetMethodExtended("Raw", typeof (string), typeof (string))
                .Invoke(configurer, new object[] {"show_sql", "false"}) as
                IPersistenceConfigurer;
#endif

            this._fluentConfiguration = Fluently.Configure()
                .Database(configurer)
                .Mappings(
                    m =>
                    {
                        foreach (var assembly in this.DataTypeAssemblies)
                        {
                            m.AutoMappings.Add(AutoMap.Assembly(assembly, new AutomappingConfiguration())
                                .Conventions.Add(new ObservableListConvention())
                                .Conventions.Add(new TypeResolvingConvention(this))
                                .Conventions.Add(new ReferenceConvention())
                                .Conventions.Add(new TableConvention())
                                .Conventions.Add(new ForeignKeyNameConvention())
                                .Conventions.Add(new IndexAttributeReferenceConvention())
                                .Conventions.Add(new IndexAttributeConvention())
                                .Conventions.Add(new UniqueAttributeReferenceConvention())
                                .Conventions.Add(new UniqueAttributeConvention())
                                .IgnoreBase<DataObject>());
                        }
                    })
                .ExposeConfiguration(c => this._configuration = c)
#if DEBUG
                .ExposeConfiguration(c => c.SetInterceptor(new SqlStatementInterceptor()))
#endif
                ;

            this._fluentConfiguration.Cache(
                c => c.ProviderClass(typeof (HashtableCacheProvider).AssemblyQualifiedName).UseQueryCache());

            // TODO BUG no session at creating a second service
            this._factory = this._fluentConfiguration.BuildSessionFactory();

            this._statelessSession = this._factory.OpenStatelessSession();
            this._session = this._factory.OpenSession(new DirtyInterceptor());

            this._connection = (DbConnection) this._session.Connection;

            var update = new SchemaUpdate(this._configuration, this._connection);
            update.Execute(true, true);
        }

        public override void Dispose()
        {
            if (this.ActiveTransaction)
                this.RollbackTransaction();

            this.Close();
        }

        protected void Close()
        {
            if(this._statelessSession != null)
                this._statelessSession.Close();
            
            if(this._session != null)
                this._session.Close();

            if(this._factory != null)
                this._factory.Close();
        }

        public override void BeginTransaction()
        {
            this._session.BeginTransaction();
        }

        public override void BeginBatchTransaction()
        {
            this._statelessSession.BeginTransaction();
        }

        public override void CommitTransaction()
        {
            if (this.ActiveTransaction)
            {
                this._session.Transaction.Commit();
            }
        }

        public override void CommitBatchTransaction()
        {
            if (this.ActiveBatchTransaction)
            {
                this._statelessSession.Transaction.Commit();

                foreach (var obj in this._batchTransactionSaveOrUpdateObjects)
                {
                    obj.State = TransactionState.None;
                    obj.IsDirty = false;
                }

                foreach (var obj in this._batchTransactionDeleteObjects)
                    obj.Dispose();

                this._batchTransactionSaveOrUpdateObjects.Clear();
                this._batchTransactionDeleteObjects.Clear();
            }
        }

        public override void RollbackTransaction()
        {
            if (this.ActiveTransaction)
            {
                this._session.Transaction.Rollback();
                this._session.Clear();
            }
        }

        public override void RollbackBatchTransaction()
        {
            if (this.ActiveTransaction)
            {
                this._statelessSession.Transaction.Rollback();

                foreach (var obj in this._batchTransactionSaveOrUpdateObjects)
                {
                    obj.State = TransactionState.None;
                }

                foreach (var obj in this._batchTransactionDeleteObjects)
                {
                    obj.State = TransactionState.None;
                }

                this._batchTransactionSaveOrUpdateObjects.Clear();
            }
        }

        public override void SaveOrUpdate(IDataObject obj)
        {
            var activeTransaction = this.ActiveTransaction;

            if (!activeTransaction)
                this.BeginTransaction();

            try
            {
                this._session.SaveOrUpdate(obj);

                if (!activeTransaction)
                    this.CommitTransaction();
            }
            catch
            {
                if (!activeTransaction)
                {
                    this.RollbackTransaction();
                }

                throw;
            }
        }

        public override void SaveOrUpdate(IEnumerable<IDataObject> objs)
        {
            var activeTransaction = this.ActiveTransaction;

            if (!activeTransaction)
                this.BeginTransaction();

            try
            {
                foreach (var obj in objs)
                    this.SaveOrUpdate(obj);

                if (!activeTransaction)
                    this.CommitTransaction();
            }
            catch
            {
                if (!activeTransaction)
                {
                    this.RollbackTransaction();
                }

                throw;
            }
        }

        public override void BatchSaveOrUpdate(IEnumerable<IDataObject> objs)
        {
            var activeTransaction = this.ActiveBatchTransaction;

            if (!activeTransaction)
                this.BeginBatchTransaction();

            try
            {
                foreach (var obj in objs)
                    this.BatchSaveOrUpdate(obj);

                if (!activeTransaction)
                    this.CommitBatchTransaction();
            }
            catch
            {
                if (!activeTransaction)
                    this.RollbackBatchTransaction();

                throw;
            }
        }

        public override void Delete(IDataObject obj)
        {
            var activeTransaction = this.ActiveTransaction;

            if (!activeTransaction)
                this.BeginTransaction();

            try
            {
                this._session.Delete(obj);

                if (!activeTransaction)
                    this.CommitTransaction();
            }
            catch
            {
                if (!activeTransaction)
                {
                    this.RollbackTransaction();
                }

                throw;
            }
        }

        public override void Delete(IEnumerable<IDataObject> objs)
        {
            var activeTransaction = this.ActiveTransaction;

            if (!activeTransaction)
                this.BeginTransaction();

            try
            {
                foreach (var obj in objs)
                    this.Delete(obj);

                if (!activeTransaction)
                    this.CommitTransaction();
            }
            catch
            {
                if (!activeTransaction)
                {
                    this.RollbackTransaction();
                }

                throw;
            }
        }

        public override void BatchDelete(IEnumerable<IDataObject> objs)
        {
            var activeTransaction = this.ActiveBatchTransaction;

            if (!activeTransaction)
                this.BeginBatchTransaction();

            try
            {
                foreach (var obj in objs)
                    this.BatchDelete(obj);

                if (!activeTransaction)
                    this.CommitBatchTransaction();
            }
            catch
            {
                if (!activeTransaction)
                    this.RollbackBatchTransaction();

                throw;
            }
        }

        public override void BatchSaveOrUpdate(IDataObject obj)
        {
            var activeTransaction = this.ActiveBatchTransaction;

            if (!activeTransaction)
                this.BeginBatchTransaction();

            try
            {
                this.BatchAction(obj, o =>
                {
                    this._batchTransactionSaveOrUpdateObjects.Add(o);
                    o.State = TransactionState.ForSaveOrUpdate;

                    if (o.Id == Guid.Empty)
                        this._statelessSession.Insert(o);
                    else if (o.IsDirty)
                        this._statelessSession.Update(o);
                },
                    o => o.State == TransactionState.None, false);

                if (!activeTransaction)
                    this.CommitBatchTransaction();
            }
            catch
            {
                if (!activeTransaction)
                    this.RollbackBatchTransaction();

                throw;
            }
        }

        public override void BatchDelete(IDataObject obj)
        {
            var activeTransaction = this.ActiveBatchTransaction;

            if (!activeTransaction)
                this.BeginBatchTransaction();

            try
            {
                this.BatchAction(obj, o =>
                {
                    this._batchTransactionDeleteObjects.Add(o);
                    o.State = TransactionState.ForDelete;

                    if (o.Id != Guid.Empty)
                        this._statelessSession.Delete(o);
                },
                    o => o.State == TransactionState.None, false);

                if (!activeTransaction)
                    this.CommitBatchTransaction();
            }
            catch
            {
                if (!activeTransaction)
                    this.RollbackBatchTransaction();

                throw;
            }
        }

        private void BatchAction(IDataObject obj, Action<IDataObject> doAction, Func<IDataObject, bool> checkFunc,
            bool needsCascadeAll)
        {
            if (checkFunc(obj))
            {
                var t = obj.GetType();
                PropertyInfo[] childInfos = null;
                PropertyInfo[] parentInfos = null;

                if (this._batchTransactionChildInfos.ContainsKey(t))
                {
                    childInfos = this._batchTransactionChildInfos[t];
                    parentInfos = this._batchTransactionParentInfos[t];
                }
                else
                {
                    childInfos = t.GetProperties()
                        .Where(
                            pi =>
                                (!needsCascadeAll ||
                                 pi.GetCustomAttributes<CascadeAllAttribute>().Any()) &&
                                !typeof (IEnumerable).IsAssignableFrom(pi.PropertyType) &&
                                pi.PropertyType.GetInterfaces().Contains(typeof (IDataObject))).ToArray();

                    parentInfos =
                        t.GetProperties()
                            .Where(
                                pi =>
                                    (!needsCascadeAll ||
                                     pi.GetCustomAttributes<CascadeAllAttribute>().Any()) &&
                                    typeof (IEnumerable).IsAssignableFrom(pi.PropertyType) &&
                                    pi.PropertyType.GetGenericArguments().Any() &&
                                    pi.PropertyType.GetGenericArguments()
                                        .First()
                                        .GetInterfaces()
                                        .Contains(typeof (IDataObject))).ToArray();

                    this._batchTransactionChildInfos.Add(t, childInfos);
                    this._batchTransactionParentInfos.Add(t, parentInfos);
                }

                foreach (
                    var cObj in
                        childInfos.Select(pi => pi.GetValue(obj, null) as IDataObject)
                            .Where(o => o != null))
                {
                    this.BatchAction(cObj, doAction, checkFunc, needsCascadeAll);
                }

                doAction(obj);

                foreach (
                    var cObj in
                        parentInfos.Select(pi => pi.GetValue(obj, null) as IEnumerable)
                            .Where(l => l != null)
                            .SelectMany(l => l.OfType<IDataObject>())
                            .Where(o => o != null))
                {
                    this.BatchAction(cObj, doAction, checkFunc, needsCascadeAll);
                }
            }
        }

        public override void Reload(IEnumerable<IDataObject> objs)
        {
            foreach (var obj in objs)
                this.Reload(obj);
        }


        public override void Reload(IDataObject obj)
        {
            this._session.Refresh(obj);
        }

        public override IQueryable<T> Get<T>()
        {
            return this._session.Query<T>();
        }

        public override IQueryable<T> BatchGet<T>()
        {
            return this._statelessSession.Query<T>();
        }
// ReSharper restore UnreachableCode
// ReSharper restore HeuristicUnreachableCode
    }

    public class AutomappingConfiguration : DefaultAutomappingConfiguration
    {
        public override bool ShouldMap(Member member)
        {
            if (member.MemberInfo.GetCustomAttributes<IgnorePersistanceAttribute>().Any())
            {
                return false;
            }

            return base.ShouldMap(member);
        }
    }

    public class SqlStatementInterceptor : EmptyInterceptor
    {
        public override SqlString OnPrepareStatement(SqlString sql)
        {
            Debug.WriteLine(sql.ToString());
            return sql;
        }
    }


    // http://fluentnhibernate.wikia.com/wiki/Available_conventions
    public class TypeResolvingConvention : IReferenceConvention, IHasManyConvention, IHasOneConvention
    {
        private readonly NHibernateManager _manager;

        public TypeResolvingConvention(NHibernateManager manager)
        {
            this._manager = manager;
        }

        // IReferenceConvention

        //IHasManyConvention
        public void Apply(IOneToManyCollectionInstance instance)
        {
            var iType = instance.ChildType;

            if (iType.IsInterface && iType.GetInterfaces().Any(i => i == typeof (IDataObject)))
            {
                try
                {
                    instance.Relationship.CustomClass(this._manager.Resolve(iType));
                }
                catch (NullReferenceException)
                {
                    throw (new ArgumentException(string.Concat("could not find resolved type for <", iType.Name,
                        ">. did you forgot adding a repository to configuration?")));
                }
            }
        }

        // IHasOneConvention
        public void Apply(IOneToOneInstance instance)
        {
            var iType = instance.EntityType;

            if (iType.IsInterface && iType.GetInterfaces().Any(i => i == typeof (IDataObject)))
            {
                try
                {
                    instance.Class(this._manager.Resolve(iType));
                }
                catch (NullReferenceException)
                {
                    throw (new ArgumentException(string.Concat("could not find resolved type for <", iType.Name,
                        ">. did you forgot adding a repository to configuration?")));
                }
            }
        }

        public void Apply(IManyToOneInstance instance)
        {
            var iType = instance.Class.GetUnderlyingSystemType();

            if (iType.IsInterface && iType.GetInterfaces().Any(i => i == typeof (IDataObject)))
            {
                try
                {
                    instance.CustomClass(this._manager.Resolve(iType));
                }
                catch (NullReferenceException)
                {
                    throw (new ArgumentException(string.Concat("could not find resolved type for <", iType.Name,
                        ">. did you forgot adding a repository to configuration?")));
                }
            }
        }
    }

    public class ReferenceConvention : IReferenceConvention, IHasManyConvention, IHasOneConvention
    {
        // IReferenceConvention

        //IHasManyConvention
        public void Apply(IOneToManyCollectionInstance instance)
        {
            var iType = instance.ChildType;

            if (iType.GetInterfaces().Any(i => i == typeof (IDataObject)))
            {
                var cascade = instance.Member.GetCustomAttributes<CascadeAllAttribute>().Any();
                var deleteOrphans = iType.GetCustomAttributes<DeleteOrphansAttribute>().Any();
                var eagerLoading = iType.GetCustomAttributes<EagerLoadingAttribute>().Any();
                var orderByAttributes = instance.Member.GetCustomAttributes<OrderByAttribute>();

                foreach (var orderByAttribute in orderByAttributes)
                    instance.OrderBy(string.Concat(orderByAttribute.Property,
                        orderByAttribute.Order != Order.Ascending ? " DESC" : string.Empty));

                if (cascade && !deleteOrphans)
                    instance.Cascade.All();
                else if (cascade && deleteOrphans)
                    instance.Cascade.AllDeleteOrphan();
                else if (!cascade && deleteOrphans)
                    instance.Cascade.DeleteOrphan();
                else instance.Cascade.SaveUpdate();

                if (eagerLoading)
                    instance.Not.LazyLoad();
            }
        }

        // IHasOneConvention
        public void Apply(IOneToOneInstance instance)
        {
            var iType = instance.EntityType;

            if (iType.GetInterfaces().Any(i => i == typeof (IDataObject)))
            {
                var eagerLoading = iType.GetCustomAttributes<EagerLoadingAttribute>().Any();

                instance.Cascade.SaveUpdate();

                if (eagerLoading)
                    instance.Not.LazyLoad();
            }
        }

        public void Apply(IManyToOneInstance instance)
        {
            var iType = instance.Class.GetUnderlyingSystemType();

            if (iType.GetInterfaces().Any(i => i == typeof (IDataObject)))
            {
                var cascade = instance.Property.MemberInfo.GetCustomAttributes<CascadeAllAttribute>().Any();
                var eagerLoading = iType.GetCustomAttributes<EagerLoadingAttribute>().Any();

                if (cascade)
                    instance.Cascade.All();
                else instance.Cascade.SaveUpdate();

                if (eagerLoading)
                    instance.Not.LazyLoad();
            }
        }
    }

    public class TableConvention : IClassConvention
    {
        public void Apply(IClassInstance instance)
        {
            // set name
            var iFace =
                instance.EntityType.GetInterfaces()
                    .First(i => instance.EntityType.Name.EndsWith(string.Concat("_", i.Name)));
            instance.Table(string.Concat("tbl_", iFace.Name.Substring(1)));
        }
    }

    public class ForeignKeyNameConvention : ForeignKeyConvention
    {
        protected override string GetKeyName(Member property, Type type, MemberInfo member)
        {
            if (property != null)
            {
                return string.Concat(property.MemberInfo.Name, "_id");
            }

            if (member != null)
            {
                var attribute =
                    member.GetCustomAttributes<ForeignKeyAttribute>().FirstOrDefault();
                if (attribute != null)
                {
                    return string.Concat(attribute.Name, "_id");
                }
            }

            var iFace = type.GetInterfaces().First(i => type.Name.EndsWith(string.Concat("_", i.Name)));
            return string.Concat(iFace.Name.Remove(0, 1), "_id");
        }
    }

    public class IndexAttributeConvention : AttributePropertyConvention<IndexAttribute>
    {
        protected override void Apply(IndexAttribute attribute, IPropertyInstance instance)
        {
            var tblName = instance.Property.DeclaringType.Name.Split(new[] {"_I"}, StringSplitOptions.None).Last();
            instance.Index(!string.IsNullOrEmpty(attribute.Name)
                ? string.Concat("i_", tblName, "_", attribute.Name)
                : string.Concat("i_", tblName, "_", instance.Name));
        }
    }

    public class IndexAttributeReferenceConvention : IReferenceConvention
    {
        public void Apply(IManyToOneInstance instance)
        {
            var iType = instance.Class.GetUnderlyingSystemType();

            if (iType.GetInterfaces().Any(i => i == typeof (IDataObject)))
            {
                var tblName = instance.Property.DeclaringType.Name.Split(new[] {"_I"}, StringSplitOptions.None).Last();
                var attribute =
                    instance.Property.MemberInfo.GetCustomAttributes<IndexAttribute>().FirstOrDefault();
                if (attribute != null)
                {
                    instance.Index(!string.IsNullOrEmpty(attribute.Name)
                        ? string.Concat("i_", tblName, "_", attribute.Name)
                        : string.Concat("i_", tblName, "_", instance.Name));
                }
            }
        }
    }

    public class UniqueAttributeConvention : AttributePropertyConvention<UniqueAttribute>
    {
        protected override void Apply(UniqueAttribute attribute, IPropertyInstance instance)
        {
            var tblName = instance.Property.DeclaringType.Name.Split(new[] {"_I"}, StringSplitOptions.None).Last();
            instance.UniqueKey(!string.IsNullOrEmpty(attribute.Name)
                ? string.Concat("u_", tblName, "_", attribute.Name)
                : string.Concat("u_", tblName, "_", instance.Name));
        }
    }

    public class UniqueAttributeReferenceConvention : IReferenceConvention
    {
        public void Apply(IManyToOneInstance instance)
        {
            var iType = instance.Class.GetUnderlyingSystemType();

            if (iType.GetInterfaces().Any(i => i == typeof (IDataObject)))
            {
                var tblName = instance.Property.DeclaringType.Name.Split(new[] {"_I"}, StringSplitOptions.None).Last();
                var attribute =
                    instance.Property.MemberInfo.GetCustomAttributes<UniqueAttribute>().FirstOrDefault();
                if (attribute != null)
                {
                    instance.UniqueKey(!string.IsNullOrEmpty(attribute.Name)
                        ? string.Concat("u_", tblName, "_", attribute.Name)
                        : string.Concat("u_", tblName, "_", instance.Name));
                }
            }
        }
    }

    public class TextAttributeConvention : AttributePropertyConvention<TextAttribute>
    {
        protected override void Apply(TextAttribute attribute, IPropertyInstance instance)
        {
            if (instance.Type == typeof (String))
                instance.CustomSqlType("text");
        }
    }

    public class ObservableListConvention :
        IHasManyConvention, IHasManyToManyConvention, ICollectionConvention
    {
        // For one-to-many relations
        public void Apply(ICollectionInstance instance)
        {
            this.ApplyObservableListConvention(instance);
        }

        public void Apply(IOneToManyCollectionInstance instance)
        {
            this.ApplyObservableListConvention(instance);
        }

        // For many-to-many relations
        public void Apply(IManyToManyCollectionInstance instance)
        {
            this.ApplyObservableListConvention(instance);
        }

        // For collections of components or simple types

        private void ApplyObservableListConvention(ICollectionInstance instance)
        {
            var collectionType =
                typeof (ObservableListType<>)
                    .MakeGenericType(instance.ChildType);
            instance.CollectionType(collectionType);
        }
    }

    public class DirtyInterceptor : EmptyInterceptor
    {
        public override bool OnLoad(object entity, object id, object[] state, string[] propertyNames, IType[] types)
        {
            if (entity as IDataObject != null)
            {
                ((IDataObject) entity).IsDirty = false;
                ((IDataObject) entity).State = TransactionState.None;
            }

            return base.OnLoad(entity, id, state, propertyNames, types);
        }

        public override void PostFlush(ICollection entities)
        {
            foreach (var entity in entities)
            {
                if (entity as IDataObject != null)
                {
                    ((IDataObject) entity).IsDirty = false;
                    ((IDataObject) entity).State = TransactionState.None;
                }
            }

            base.PostFlush(entities);
        }

        public override bool OnSave(object entity, object id, object[] state, string[] propertyNames, IType[] types)
        {
            if (entity as IDataObject != null)
            {
                ((IDataObject) entity).State = TransactionState.ForSaveOrUpdate;
            }

            return base.OnSave(entity, id, state, propertyNames, types);
        }

        public override void OnDelete(object entity, object id, object[] state, string[] propertyNames, IType[] types)
        {
            if (entity as IDataObject != null)
            {
                ((IDataObject) entity).State = TransactionState.ForDelete;
            }

            base.OnDelete(entity, id, state, propertyNames, types);
        }
    }
}