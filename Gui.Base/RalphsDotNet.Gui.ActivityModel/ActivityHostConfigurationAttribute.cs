﻿using System;

namespace RalphsDotNet.Gui.ActivityModel
{
    [AttributeUsage(AttributeTargets.Assembly)]
    public class ActivityHostConfigurationAttribute : Attribute
    {
        public ActivityHostConfigurationAttribute(string headerImage, string headerText, string subHeaderText,
            string footerText, Type entryActivity)
        {
            this.HeaderImage = headerImage;
            this.HeaderText = headerText;
            this.SubHeaderText = subHeaderText;
            this.FooterText = footerText;
            this.EntryActivity = entryActivity;
        }

        public string HeaderImage { get; private set; }
        public string HeaderText { get; private set; }
        public string SubHeaderText { get; private set; }
        public string FooterText { get; private set; }
        public Type EntryActivity { get; private set; }
    }
}