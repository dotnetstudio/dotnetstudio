﻿using System.ComponentModel;

namespace RalphsDotNet.Gui.ActivityModel
{
    public class ProgressHandler : INotifyPropertyChanged
    {
        private int _maximum;
        private int _minimum;
        private string _text;
        private int _value;

        private bool _isIndeterminate;
        public bool IsIndeterminate
        {
            get { return this._isIndeterminate; }
              set
            {
                if(this._isIndeterminate != value)
                {
                    this._isIndeterminate = value;

                    this.OnPropertyChanged("IsIndeterminate");
                }
            }
        }

        public int Minimum
        {
            get { return this._minimum; }
            set
            {
                if (this._minimum != value)
                {
                    this._minimum = value;

                    this.OnPropertyChanged("Minimum");
                }
            }
        }

        public int Maximum
        {
            get { return this._maximum; }
            set
            {
                if (this._maximum != value)
                {
                    this._maximum = value;

                    this.OnPropertyChanged("Maximum");
                }
            }
        }

        public int Value
        {
            get { return this._value; }
            set
            {
                if (this._value != value)
                {
                    this._value = value;

                    this.OnPropertyChanged("Value");
                }
            }
        }

        public string Text
        {
            get { return this._text; }
            set
            {
                if (!Equals(this._text, value))
                {
                    this._text = value;

                    this.OnPropertyChanged("Text");
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            var handler = this.PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}