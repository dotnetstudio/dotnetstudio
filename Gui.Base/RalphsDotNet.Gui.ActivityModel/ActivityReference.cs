﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace RalphsDotNet.Gui.ActivityModel
{
    public delegate bool PreActivityAction(ActivityReferenceBase activityRef, out object parameter);

    public delegate void ProgressActivityAction(ProgressActivityReference activityRef, ProgressHandler handler);

    public abstract class ActivityReferenceBase : INotifyPropertyChanged
    {
        private bool _isEnabled = true;
        private string _name;

        public string Name
        {
            get { return this._name; }
            set
            {
                if (value != this._name)
                {
                    this._name = value;
                    this.OnPropertyChanged("Name");
                }
            }
        }

        public PreActivityAction PreAction { get; set; }

        public bool IsEnabled
        {
            get { return this._isEnabled; }
            set
            {
                if (value != this._isEnabled)
                {
                    this._isEnabled = value;
                    this.OnPropertyChanged("IsEnabled");
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public class ActivityReference : ActivityReferenceBase
    {
        public Type Activity { get; set; }
    }

    public class ProgressActivityReference : ActivityReferenceBase
    {
        public ProgressActivityAction ProgressAction { get; set; }
    }

    public class ActivityReferenceCollection : ObservableCollection<ActivityReferenceBase>
    {
    }

    public class ActivityReferenceContainer : INotifyPropertyChanged
    {
        public ActivityReferenceContainer(ActivityReferenceBase reference, string symbol)
        {
            this.Reference = reference;
            this.Reference.PropertyChanged += this.Reference_PropertyChanged;
            this.Symbol = symbol;
        }

        public ActivityReferenceBase Reference { get; private set; }

        public string Name
        {
            get { return this.Reference.Name; }
        }

        public string Symbol { get; private set; }

        public bool IsEnabled
        {
            get { return this.Reference.IsEnabled; }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void Reference_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            this.SendPropertyChanged(e.PropertyName);
        }

        private void SendPropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}