﻿using System;

namespace RalphsDotNet.Gui.ActivityModel
{
    public interface IActivityHost
    {
        object LastResult { get; }

        string HeaderImage { get; set; }
        string HeaderText { get; set; }
        string SubHeaderText { get; set; }
        string FooterText { get; set; }

        ProgressHandler ProgressHandler { get; }

        void ClearActivities();
        IActivity LoadActivity(Type activityType);
        IActivity LoadActivity(Type activityType, object parameter);
        void LoadActivity(IActivity activity);
        void LoadActivity(IActivity activity, object parameter);
        IActivity LoadActivity(ActivityReferenceBase reference);
        void Back();
    }
}
