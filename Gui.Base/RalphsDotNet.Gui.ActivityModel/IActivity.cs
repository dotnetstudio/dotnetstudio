﻿using System;
using System.ComponentModel;

namespace RalphsDotNet.Gui.ActivityModel
{
    public interface IActivity : IDisposable, INotifyPropertyChanged
    {
        ActivityReferenceCollection References { get; }

        bool IsActive { get; set; }
        bool CanPause { get; set; }
        object DataContext { get; set; }
        IActivityHost Host { get; set; }
        object Result { get; }

        void Pause();
        void Resume();
    }

    public interface IServiceActivity : INotifyPropertyChanged
    {
        object DataContext { get; set; }
        IActivityHost Host { get; set; }
    }
}
