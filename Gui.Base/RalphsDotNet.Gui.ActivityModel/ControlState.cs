﻿using System.Collections.Generic;
using System.ComponentModel;

namespace RalphsDotNet.Gui.ActivityModel
{
    public class ControlState : INotifyPropertyChanged
    {
        private bool _isActive;
        public string Name { get; set; }
        public ControlStateCheckHandler CheckHandler { get; set; }

        public bool IsActive
        {
            get { return this._isActive; }
            set
            {
                if (this._isActive != value)
                {
                    this._isActive = value;

                    this.OnPropertyChanged("IsActive");

                    if (this.IsActive)
                        this.OnStateEntered();
                    else
                        this.OnStateLeft();
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public event ControlStateEnteredHandler StateEntered;
        public event ControlStateLeftHandler StateLeft;

        private void OnPropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        private void OnStateEntered()
        {
            if (this.StateEntered != null)
                this.StateEntered(this);
        }

        private void OnStateLeft()
        {
            if (this.StateLeft != null)
                this.StateLeft(this);
        }
    }

    public class ControlStateCollection : List<ControlState>
    {
    }

    public delegate bool ControlStateCheckHandler(ControlState state);

    public delegate void ControlStateEnteredHandler(ControlState state);

    public delegate void ControlStateLeftHandler(ControlState state);
}