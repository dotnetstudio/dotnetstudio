﻿using RalphsDotNet.Service.Data;
using RalphsDotNet.Service.Simulation.Model;

namespace RalphsDotNet.Service.Simulation.Data.Repositories
{
    public abstract class PropertyRepository<T> : DataRepository<T> where T : IProperty
    {
        public T Create(string name)
        {
            var obj = this.CreateInternal();
            obj.Name = name;
            return obj;
        }
    }
}