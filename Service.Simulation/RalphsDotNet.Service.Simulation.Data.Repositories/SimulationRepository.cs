﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using RalphsDotNet.Service.Data;
using RalphsDotNet.Service.Simulation.Model;

namespace RalphsDotNet.Service.Simulation.Data.Repositories
{
    public class SimulationRepository : DataRepository<ISimulation>, ISimulationRepository
    {
        protected override ISimulation CreateInternal()
        {
            var obj = base.CreateInternal();
            return obj;
        }

        public ISimulation Create(DateTime startTime)
        {
            var obj = this.CreateInternal();
            obj.StarTime = startTime;
            return obj;
        }
    }
}