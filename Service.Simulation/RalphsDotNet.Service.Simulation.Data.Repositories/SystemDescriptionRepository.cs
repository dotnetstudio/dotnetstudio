﻿using System;
using System.Collections.ObjectModel;
using RalphsDotNet.Service.Data;

namespace RalphsDotNet.Service.Simulation.Data.Repositories
{
    public class SystemDescriptionRepository : DataRepository<ISystemDescription>, ISystemDescriptionRepository
    {
        protected override ISystemDescription CreateInternal()
        {
            var obj = base.CreateInternal();
            obj.SystemPrepareInteractionScripts = new ObservableCollection<ISystemPrepareInteractionScriptMapping>();
            obj.SystemModifyScripts = new ObservableCollection<ISystemModifyScriptMapping>();
            obj.SystemInteractionScripts = new ObservableCollection<ISystemInteractionScriptMapping>();
            obj.NumberPropertyDescriptions = new ObservableCollection<INumberPropertyDescription>();
            return obj;
        }

        public ISystemDescription Create(ISimulationDescription simulationDescription, string name)
        {
            if (simulationDescription == null)
                throw new ArgumentException("simulationDescription must be set");

            var obj = this.CreateInternal();
            obj.Name = name;
            obj.SimulationDescription = simulationDescription;
            simulationDescription.SystemDescriptions.Add(obj);
            return obj;
        }

        public ISystemDescription Create(ISimulationDescription simulationDescription, string name, int amount)
        {
            var obj = this.Create(simulationDescription, name);
            obj.Amount = amount;
            return obj;
        }
    }
}