﻿using System;
using RalphsDotNet.Service.Data;

namespace RalphsDotNet.Service.Simulation.Data.Repositories
{
    public abstract class PropertyDescriptionRepository<T> : DataRepository<T>, IPropertyDescriptionRepository<T>
        where T : IPropertyDescription
    {
        public virtual T Create(string name, ISystemDescription system)
        {
            if (system == null)
                throw new ArgumentException("system must be set");

            var obj = this.CreateInternal();
            obj.Name = name;
            obj.SystemDescription = system;
            return obj;
        }
    }
}