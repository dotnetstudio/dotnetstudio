﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using RalphsDotNet.Data.Scripting;
using RalphsDotNet.Service.Data;

namespace RalphsDotNet.Service.Simulation.Data.Repositories
{
    public class SimulationDescriptionRepository : DataRepository<ISimulationDescription>,
        ISimulationDescriptionRepository
    {
        protected override ISimulationDescription CreateInternal()
        {
            var obj= base.CreateInternal();
            obj.SystemDescriptions = new ObservableCollection<ISystemDescription>();
            obj.SystemScripts = new ObservableCollection<ISimulationSystemScript>();
            obj.RawScripts = new ObservableCollection<ISimulationRawScript>();
            return obj;
        }

        public ISimulationDescription Create()
        {
            return this.CreateInternal();
        }
    }
}