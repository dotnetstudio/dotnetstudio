﻿using System;
using System.Collections.ObjectModel;
using RalphsDotNet.Data;
using RalphsDotNet.Service.Data;

namespace RalphsDotNet.Service.Simulation.Data.Repositories
{
    public class ExtensionProfileRepository : DataRepository<IExtensionProfile>, IExtensionProfileRepository
    {
        protected override IExtensionProfile CreateInternal()
        {
            var obj = base.CreateInternal();
            obj.Attachments = new ObservableCollection<IAttachment>();
            return obj;
        }

        public IExtensionProfile Create(Guid profileId, string name)
        {
            var obj = this.CreateInternal();
            obj.ProfileId = profileId;
            obj.Name = name;

            return obj;
        }
    }
}