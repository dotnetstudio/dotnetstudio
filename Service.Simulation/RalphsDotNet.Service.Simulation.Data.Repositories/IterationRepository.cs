﻿using System.Collections.ObjectModel;
using RalphsDotNet.Service.Data;
using RalphsDotNet.Service.Simulation.Model;

namespace RalphsDotNet.Service.Simulation.Data.Repositories
{
    public class IterationRepository : DataRepository<IIteration>, IIterationRepository
    {
        protected override IIteration CreateInternal()
        {
            var obj = base.CreateInternal();
            obj.Systems = new ObservableCollection<ISystem>();
            return obj;
        }

        public IIteration Create(ISimulation simulation, ulong number)
        {
            var obj = this.CreateInternal();
            obj.SimulationId = simulation.Id;
            obj.Number = number;
            return obj;
        }
    }
}