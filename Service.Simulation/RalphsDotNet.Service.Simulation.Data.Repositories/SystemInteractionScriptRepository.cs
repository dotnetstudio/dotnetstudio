﻿using System;
using RalphsDotNet.Data.Scripting;
using RalphsDotNet.Service.Data;

namespace RalphsDotNet.Service.Simulation.Data.Repositories
{
    public class SystemInteractionScriptMappingRepository : DataRepository<ISystemInteractionScriptMapping>,
        ISystemInteractionScriptMappingRepository
    {
        public ISystemInteractionScriptMapping Create(string name, ISystemDescription system)
        {
            if (system == null)
                throw new ArgumentException("system must be set");

            var obj = this.CreateInternal();
            obj.Name = name;
            system.SystemInteractionScripts.Add(obj);
            obj.SystemDescription = system;
            return obj;
        }
    }
}