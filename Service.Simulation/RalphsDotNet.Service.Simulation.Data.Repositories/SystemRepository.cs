﻿using System;
using System.Collections.ObjectModel;
using RalphsDotNet.Service.Data;
using RalphsDotNet.Service.Simulation.Model;

namespace RalphsDotNet.Service.Simulation.Data.Repositories
{
    public class SystemRepository : DataRepository<ISystem>, ISystemRepository
    {
        protected override ISystem CreateInternal()
        {
            var obj = base.CreateInternal();
            obj.NumberArrayProperties = new ObservableCollection<INumberArrayProperty>();
            obj.NumberProperties = new ObservableCollection<INumberProperty>();
            return obj;
        }

        public ISystem Create(string name, IIteration iteration, int number)
        {
            if (iteration == null)
                throw new ArgumentException("iteration must be set");

            var obj = this.CreateInternal();
            obj.Name = name;
            iteration.Systems.Add(obj);
            obj.Number = number;
            return obj;
        }
    }
}