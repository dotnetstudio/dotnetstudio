﻿using System;
using RalphsDotNet.Data.Scripting;
using RalphsDotNet.Service.Data;

namespace RalphsDotNet.Service.Simulation.Data.Repositories
{
    public class SystemPrepareInteractionScriptMappingRepository : DataRepository<ISystemPrepareInteractionScriptMapping>,
        ISystemPrepareInteractionScriptMappingRepository
    {
        public ISystemPrepareInteractionScriptMapping Create(string name, ISystemDescription system)
        {
            if (system == null)
                throw new ArgumentException("system must be set");

            var obj = this.CreateInternal();
            obj.Name = name;
            system.SystemPrepareInteractionScripts.Add(obj);
            obj.SystemDescription = system;
            return obj;
        }
    }
}