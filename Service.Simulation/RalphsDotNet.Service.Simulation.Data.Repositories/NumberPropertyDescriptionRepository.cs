﻿using System;

namespace RalphsDotNet.Service.Simulation.Data.Repositories
{
    public class NumberPropertyDescriptionRepository : PropertyDescriptionRepository<INumberPropertyDescription>,
        INumberPropertyDescriptionRepository
    {
        public override INumberPropertyDescription Create(string name, ISystemDescription system)
        {
            if (system == null)
                throw new ArgumentException("system must be set");

            var obj = base.Create(name, system);
            system.NumberPropertyDescriptions.Add(obj);
            return obj;
        }

        public INumberPropertyDescription Create(string name, ISystemDescription system, string distribution)
        {
            var obj = this.Create(name, system);
            obj.Distribution = distribution;
            return obj;
        }
    }
}