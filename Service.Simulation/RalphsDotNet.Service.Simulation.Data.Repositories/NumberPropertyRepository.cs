﻿using System;
using RalphsDotNet.Service.Simulation.Model;

namespace RalphsDotNet.Service.Simulation.Data.Repositories
{
    public class NumberPropertyRepository : PropertyRepository<INumberProperty>,
        INumberPropertyRepository
    {
        public INumberProperty Create(string name, ISystem system)
        {
            if (system == null)
                throw new ArgumentException("system must be set");

            var obj = this.Create(name);
            if (system != null) system.NumberProperties.Add(obj);
            return obj;
        }

        public INumberProperty Create(string name, ISystem system, double value)
        {
            var obj = this.Create(name, system);
            obj.Value = value;
            return obj;
        }
    }
}