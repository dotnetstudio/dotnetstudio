﻿using RalphsDotNet.Data.Scripting;
using RalphsDotNet.Service.Data;

namespace RalphsDotNet.Service.Simulation.Data.Repositories
{
    public class SimulationSystemScriptRepository : DataRepository<ISimulationSystemScript>, ISimulationSystemScriptRepository
    {
        public ISimulationSystemScript Create(string name, ISimulationDescription simulationDescription, string code)
        {
            var obj = this.CreateInternal();
            obj.Name = name;
            if (simulationDescription != null) simulationDescription.SystemScripts.Add(obj);
            ((IScript) obj).Code = code;
            return obj;
        }

        protected override ISimulationSystemScript CreateInternal()
        {
            var obj = base.CreateInternal();
            obj.Language = ScriptLanguage.OpenCl;
            return obj;
        }
    }

    public class SimulationRawScriptRepository : DataRepository<ISimulationRawScript>, ISimulationRawScriptRepository
    {
        public ISimulationRawScript Create(string name, ISimulationDescription simulationDescription, string code)
        {
            var obj = this.CreateInternal();
            obj.Name = name;
            if(simulationDescription != null) simulationDescription.RawScripts.Add(obj);
            ((IScript) obj).Code = code;
            return obj;
        }

        protected override ISimulationRawScript CreateInternal()
        {
            var obj = base.CreateInternal();
            obj.Language = ScriptLanguage.OpenCl;
            return obj;
        }
    }
}