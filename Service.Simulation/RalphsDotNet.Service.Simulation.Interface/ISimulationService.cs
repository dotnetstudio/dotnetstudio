﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;
using RalphsDotNet.Service.Simulation.Data;
using RalphsDotNet.Service.Simulation.Model;

namespace RalphsDotNet.Service.Simulation.Interface
{
    [DataContract]
    public class SimulationConfiguration : ServiceConfiguration
    {
        private string _dataStreamId;
        private string _dataStreamServiceId;
        private string _openClServiceId;
        private bool _useCpu;

        [DataMember(Order = 100)]
        public string DataStreamServiceId
        {
            get { return this._dataStreamServiceId; }
            set
            {
                if (this._dataStreamServiceId != value)
                {
                    this._dataStreamServiceId = value;
                    this.OnPropertyChanged("DataStreamServiceId");
                }
            }
        }

        [DataMember(Order = 101)]
        public string DataStreamId
        {
            get { return this._dataStreamId; }
            set
            {
                if (this._dataStreamId != value)
                {
                    this._dataStreamId = value;
                    this.OnPropertyChanged("DataStreamId");
                }
            }
        }

        [DataMember(Order = 102)]
        public string OpenClServiceId
        {
            get { return this._openClServiceId; }
            set
            {
                if (this._openClServiceId != value)
                {
                    this._openClServiceId = value;
                    this.OnPropertyChanged("OpenClServiceId");
                }
            }
        }

        [DataMember(Order = 103)]
        public bool UseCpu
        {
            get { return this._useCpu; }
            set
            {
                if (this._useCpu != value)
                {
                    this._useCpu = value;
                    this.OnPropertyChanged("UseCpu");
                }
            }
        }

        [Browsable(false)]
        public override Type ServiceType
        {
            get { return typeof (ISimulationService); }
        }
    }

    public interface ISimulationService : IService, INotifyPropertyChanged
    {
        SimulationConfiguration SimulationConfiguration { get; }

        /// <summary>
        /// Gets the debug information for actual iteration
        /// </summary>
        Dictionary<Tuple<Guid, int>, double[,]> DebugInformation { get; }

        /// <summary>
        /// Gets the actual simulation id
        /// </summary>
        Guid SimulationId { get; }

        /// <summary>
        /// Gets the actual round
        /// </summary>
        IIteration ActualIteration { get; }

        /// <summary>
        /// != 0 if there hapened an error in simulation
        /// </summary>
        int ErrorCode { get; }

        /// <summary>
        /// Gets if the simulation is actually running
        /// </summary>
        bool IsRunning { get; }

        /// <summary>
        /// Gets if the running simulation is paused
        /// </summary>
        bool IsPaused { get; }

        /// <summary>
        /// Gets if the running simulation is debugging
        /// </summary>
        bool IsDebugging { get; }

        /// <summary>
        /// Slow down the simulations by breaking X miliseconds after each sample
        /// </summary>
        int ObstacleMiliseconds { get; set; }

        /// <summary>
        /// Change the sample rate
        /// </summary>
        int SampleRate { get; set; }

        /// <summary>
        /// Runs simulation
        /// </summary>
        void Run(ISimulationDescription simulationDescrition,
            IEnumerable<IExtensionProfile> extensionProfiles);

        /// <summary>
        /// Runs simulation pausing after n samples
        /// </summary>
        void Run(ISimulationDescription simulationDescrition,
            IEnumerable<IExtensionProfile> extensionProfiles, int pauseAfter);

        /// <summary>
        /// Debugs simulation
        /// </summary>
        void Debug(ISimulationDescription simulationDescrition,
            IEnumerable<IExtensionProfile> extensionProfiles);

        /// <summary>
        /// Stops actual simulation
        /// </summary>
        void Stop();

        /// <summary>
        /// Freezes actual simulation
        /// </summary>
        void Pause();

        /// <summary>
        /// Resets the error
        /// </summary>
        void ResetError();

        /// <summary>
        /// Unfreezes actual simulation
        /// </summary>
        void Resume();

        /// <summary>
        /// Unfreezes actual simulation
        /// </summary>
        void Resume(int pauseAfter);

        /// <summary>
        /// Unfreezes actual simulation
        /// </summary>
        void Resume(IIteration modifiedIteration);

        /// <summary>
        /// Unfreezes actual simulation
        /// </summary>
        void Resume(IIteration modifiedIteration, int pauseAfter);

        /// <summary>
        /// Step simulation
        /// </summary>
        void Step();

        /// <summary>
        /// Step simulation
        /// </summary>
        void Step(IIteration modifiedIteration);
    }

    public class SimulationServiceException : Exception
    {
        public SimulationServiceException(string msg) : base(msg)
        {
        }

        public SimulationServiceException(string msg, Exception inner) : base(msg, inner)
        {
        }
    }
}