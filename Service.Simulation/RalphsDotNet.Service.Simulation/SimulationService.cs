﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Threading;
using IronPython.Hosting;
using IronPython.Runtime.Exceptions;
using Microsoft.CSharp.RuntimeBinder;
using Microsoft.Scripting.Hosting;
using RalphsDotNet.Data;
using RalphsDotNet.Service.DataStream.Interface;
using RalphsDotNet.Service.OpenCl.Interface;
using RalphsDotNet.Service.Simulation.Data;
using RalphsDotNet.Service.Simulation.Interface;
using RalphsDotNet.Service.Simulation.Model;
using RalphsDotNet.Tool.System;

namespace RalphsDotNet.Service.Simulation
{
    public class SimulationService : ISimulationService
    {
        private readonly Synchronizer _pauseSynchronizer = new Synchronizer();
        private readonly ServiceBase _proxy;
        private readonly Random _rnd = new Random();
        private readonly Synchronizer _startingSynchronizer = new Synchronizer();
        private readonly Synchronizer _stopSynchronizer = new Synchronizer();

        private readonly Dictionary<string, List<string>> _systemNumberArrayPropertyNames =
            new Dictionary<string, List<string>>();

        private readonly Dictionary<string, List<string>> _systemNumberPropertyNames =
            new Dictionary<string, List<string>>();

        private IIteration _actualIteration;

        private DataStreamBase _dataStream;
        private IDataStreamService _dataStreamService;
        private IClMemoryObject _debugBuffer;
        private IClKernel _downloadNumberArrayPropertyKernel;
        private IClKernel _downloadNumberPropertyKernel;
        private int _errorCode;
        private IClMemoryObject<int> _errorCodeBuffer;
        private IEnumerable<IExtensionProfile> _extensionProfiles;
        private bool _hasInteraction;
        private bool _hasPostInteraction;
        private bool _hasPreInteraction;
        private IClKernel _interactionKernel;
        private bool _isRunning;
        private bool _isStarting;
        private IIteration _modifiedIteration;
        private IClMemoryObject _numberArrayPropertyTransferBuffer;
        private IClMemoryObject _numberPropertiesBuffer;
        private IClMemoryObject _numberPropertyNum;
        private IClMemoryObject _numberPropertyTransferBuffer;
        private string _openClCode;
        private IClContext _openClContext;
        private IClDevice _openClDevice;
        private IOpenClService _openClService;
        private List<string> _orderedNumberPropertyNames;
        private List<string> _orderedSystemDescriptionNames;
        private int _pauseAfter;
        private IClKernel _postInteractionKernel;
        private IClKernel _preInteractionKernel;
        private bool _shouldPause;
        private bool _shouldStep;
        private bool _shouldStop;
        private ISimulationDescription _simulationDescription;
        private Guid _simulationId;
        private Thread _simulationThread;
        private IClKernel _uploadNumberArrayPropertyKernel;
        private IClKernel _uploadNumberPropertyKernel;
        private ISystem[] _uploadedSystems;

        public SimulationService(ServiceBase proxy, ServiceConfiguration configuration)
        {
            this._proxy = proxy;
            this.ServiceConfiguration = configuration;
        }

        public bool SupportDouble { get; private set; }
        public Dictionary<Tuple<Guid, int>, double[,]> DebugInformation { get; private set; }

        public bool IsPaused { get; private set; }

        public bool IsDebugging { get; private set; }

        public int ObstacleMiliseconds { get; set; }

        public int SampleRate { get; set; }

        public string Id
        {
            get { return this.ServiceConfiguration.Id; }
        }

        public ServiceConfiguration ServiceConfiguration { get; private set; }

        public SimulationConfiguration SimulationConfiguration
        {
            get { return this.ServiceConfiguration as SimulationConfiguration; }
        }

        public Guid SimulationId
        {
            get { return this._simulationId; }
            private set
            {
                if (this._simulationId != value)
                {
                    this._simulationId = value;

                    this.OnPropertyChanged("SimulationId");
                }
            }
        }

        public IIteration ActualIteration
        {
            get { return this._actualIteration; }
            private set
            {
                if (this._actualIteration != value)
                {
                    this._actualIteration = value;

                    this.OnPropertyChanged("ActualIteration");
                }
            }
        }

        public int ErrorCode
        {
            get { return this._errorCode; }
            private set
            {
                if (this._errorCode != value)
                {
                    this._errorCode = value;

                    this.OnPropertyChanged("Iteration");
                }
            }
        }

        public bool IsRunning
        {
            get { return this._isRunning; }
            private set
            {
                if (this._isRunning != value)
                {
                    this._isRunning = value;

                    this.OnPropertyChanged("IsRunning");
                }
            }
        }

        public void Run(ISimulationDescription simulationDescrition, IEnumerable<IExtensionProfile> extensionProfiles)
        {
            this.Run(simulationDescrition, extensionProfiles, false, -1);
        }

        public void Run(ISimulationDescription simulationDescrition, IEnumerable<IExtensionProfile> extensionProfiles,
            int pauseAfter)
        {
            this.Run(simulationDescrition, extensionProfiles, false, pauseAfter);
        }

        public void Debug(ISimulationDescription simulationDescrition, IEnumerable<IExtensionProfile> extensionProfiles)
        {
            this.Run(simulationDescrition, extensionProfiles, true, -1);
        }

        public void Stop()
        {
            if (this.IsRunning)
            {
                this._shouldStop = true;

                this._stopSynchronizer.Wait();
            }
        }

        public void Pause()
        {
            if (this.IsRunning)
            {
                if (!this.IsPaused)
                {
                    this._pauseAfter = -1;
                    this._shouldPause = true;
                }

                this._pauseSynchronizer.Wait();
            }
        }

        public void ResetError()
        {
            this.ErrorCode = 0;
        }

        public void Resume()
        {
            this.Resume(null, -1);
        }

        public void Resume(int pauseAfter)
        {
            this.Resume(null, pauseAfter);
        }

        public void Resume(IIteration modifiedIteration)
        {
            this.Resume(modifiedIteration, -1);
        }

        public void Step()
        {
            this.Step(null);
        }

        public void Resume(IIteration modifiedIteration, int pauseAfter)
        {
            if (this.IsRunning && (pauseAfter > 0 || pauseAfter < 0))
            {
                this._modifiedIteration = modifiedIteration;
                this.ResetError();
                this._pauseAfter = pauseAfter;
                this._shouldPause = false;
            }
        }

        public void Step(IIteration modifiedIteration)
        {
            if (this.IsRunning)
            {
                this._modifiedIteration = modifiedIteration;
                this.ResetError();
                this._shouldStep = true;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void Dispose()
        {
            if (this._shouldStop)
                this.Stop();
        }

        public void Run(ISimulationDescription simulationDescrition, IEnumerable<IExtensionProfile> extensionProfiles,
            bool debug, int pauseAfter)
        {
            if (!this.IsRunning && !this._isStarting)
            {
                this._isStarting = true;
                this.ObstacleMiliseconds = -1;
                this.SampleRate = -1;
                this._modifiedIteration = null;
                this._pauseAfter = pauseAfter;
                this._simulationDescription = simulationDescrition;
                this._extensionProfiles = extensionProfiles;
                this.IsDebugging = debug;

                this._simulationThread = new Thread(this.RunInternal);
                this._simulationThread.Start();

                this._startingSynchronizer.Wait();
            }
        }

        private void CollectDescriptiveData()
        {
            this._orderedSystemDescriptionNames =
                this._simulationDescription.SystemDescriptions.Select(s => s.Name).OrderBy(n => n).ToList();

            this._orderedNumberPropertyNames =
                this._simulationDescription.SystemDescriptions.SelectMany(s => s.NumberPropertyDescriptions)
                    .Select(p => p.Name)
                    .Distinct()
                    .OrderBy(n => n)
                    .ToList();
        }

        private int BuildClKernel(int size)
        {
            // generate opencl code
            var debugVariableCount = 0;
            var preSystemCodes = new List<string>();
            var postInteractionCodes = new List<string>();
            var postSystemCodes = new List<string>();
            var interactionCodes = new List<string>();
            this._hasPreInteraction = this._hasInteraction = this._hasPostInteraction = false;
            this.DebugInformation = this.IsDebugging ? new Dictionary<Tuple<Guid, int>, double[,]>() : null;

            for (var i = 0; i < this._orderedSystemDescriptionNames.Count; i++)
            {
                var systemDescription =
                    this._simulationDescription.SystemDescriptions.First(
                        s => s.Name == this._orderedSystemDescriptionNames[i]);

                var preInteractionScripts =
                    systemDescription.SystemPrepareInteractionScripts.OrderBy(s => s.ExecutionOrder);
                var postInteractionScripts =
                    systemDescription.SystemModifyScripts.OrderBy(s => s.ExecutionOrder);
                var interactionScripts =
                    systemDescription.SystemInteractionScripts.OrderBy(s => s.ExecutionOrder);

                var numberproperties = this._orderedNumberPropertyNames.OrderByDescending(n => n.Length).ToArray();

                var prepareInteractionCode = string.Format("\tif(tsysType == {0})\n\t{{\n", i);
                foreach (
                    var preInteractionScript in
                        preInteractionScripts.Where(s => s.Script != null && s.Script.IsEnabled)
                    )
                {
                    this._hasPreInteraction = true;

                    var lines = preInteractionScript.Script.Code.Split(new[] {"\r\n", "\n"},
                        StringSplitOptions.None);

                    for (var l = 0; l < lines.Length; l++)
                    {
                        var tmp = lines[l].Replace("ERROR:", "return ");

                        if (this.IsDebugging && tmp.Trim().StartsWith("DEBUG:"))
                        {
                            var debugId = Tuple.Create(preInteractionScript.Script.Id, l);
                            if (!this.DebugInformation.ContainsKey(debugId))
                            {
                                this.DebugInformation.Add(debugId, null);
                                debugVariableCount++;
                            }

                            tmp = tmp.Replace("DEBUG:",
                                string.Format("debug[{0}] =", this.DebugInformation.Keys.ToList().IndexOf(debugId)));
                        }
                        else if (tmp.Trim().StartsWith("DEBUG:"))
                            tmp = string.Empty;

                        foreach (var np in numberproperties)
                        {
                            var j = this._orderedNumberPropertyNames.IndexOf(np) + 1;

                            tmp = tmp.Replace(string.Format("tsys.{0}", np),
                                string.Format("tsysNumberProperties[{0}]", j));

                            tmp = tmp.Replace("st.tsys", "tsysType");
                            for (var k = 0; k < this._orderedSystemDescriptionNames.Count; k++)
                                tmp = tmp.Replace(string.Format("st.{0}", this._orderedSystemDescriptionNames[k]),
                                    k.ToString());
                        }
                        prepareInteractionCode += string.Concat("\t\t", tmp, "\n");
                    }
                }
                prepareInteractionCode += "\n\t}";

                var interactionCode = string.Format("\tif(tsysType == {0})\n\t{{\n", i);
                foreach (
                    var interactionScript in
                        interactionScripts.Where(s => s.Script != null && s.Script.IsEnabled))
                {
                    this._hasInteraction = true;

                    interactionCode += string.Format("\t\tif(fsysType == {0})\n\t\t{{\n",
                        this._orderedSystemDescriptionNames.IndexOf(interactionScript.InteractionSystemDescription.Name));

                    var lines = interactionScript.Script.Code.Split(new[] {"\r\n", "\n"}, StringSplitOptions.None);

                    for (var l = 0; l < lines.Length; l++)
                    {
                        var tmp = lines[l].Replace("ERROR:", "return ");

                        if (this.IsDebugging && tmp.Trim().StartsWith("DEBUG:"))
                        {
                            var debugId = Tuple.Create(interactionScript.Script.Id, l);
                            if (!this.DebugInformation.ContainsKey(debugId))
                            {
                                this.DebugInformation.Add(debugId, null);
                                debugVariableCount++;
                            }

                            tmp = tmp.Replace("DEBUG:",
                                string.Format("debug[{0}] =", this.DebugInformation.Keys.ToList().IndexOf(debugId)));
                        }
                        else if (tmp.Trim().StartsWith("DEBUG:"))
                            tmp = string.Empty;

                        foreach (var np in numberproperties)
                        {
                            var j = this._orderedNumberPropertyNames.IndexOf(np) + 1;

                            tmp = tmp.Replace(string.Format("tsys.{0}", np),
                                string.Format("tsysNumberProperties[{0}]", j))
                                .Replace(string.Format("fsys.{0}", np),
                                    string.Format("fsysNumberProperties[{0}]", j))
                                .Replace(string.Format("tmp.{0}", np),
                                    string.Format("tmpNumberProperties[{0}]", j));

                            tmp = tmp.Replace("st.tsys", "tsysType");
                            tmp = tmp.Replace("st.fsys", "fsysType");
                            for (var k = 0; k < this._orderedSystemDescriptionNames.Count; k++)
                                tmp = tmp.Replace(string.Format("st.{0}", this._orderedSystemDescriptionNames[k]),
                                    k.ToString());
                        }
                        interactionCode += string.Concat("\t\t\t", tmp, "\n");
                    }
                    interactionCode += "\t\t}\n";
                }
                interactionCode += "\t}";

                var collectCode = string.Format("\tif(tsysType == {0})\n\t{{\n", i);
                var modifyCode = string.Format("\tif(tsysType == {0})\n\t{{\n", i);
                foreach (var postInteractionScript in postInteractionScripts)
                {
                    if (postInteractionScript.CollectScript != null && postInteractionScript.CollectScript.IsEnabled)
                    {
                        this._hasPostInteraction = true;

                        var lines = postInteractionScript.CollectScript.Code.Split(new[] {"\r\n", "\n"},
                            StringSplitOptions.None);

                        for (var l = 0; l < lines.Length; l++)
                        {
                            var tmp = lines[l].Replace("ERROR:", "return ");

                            if (this.IsDebugging && tmp.Trim().StartsWith("DEBUG:"))
                            {
                                var debugId = Tuple.Create(postInteractionScript.CollectScript.Id, l);
                                if (!this.DebugInformation.ContainsKey(debugId))
                                {
                                    this.DebugInformation.Add(debugId, null);
                                    debugVariableCount++;
                                }

                                tmp = tmp.Replace("DEBUG:",
                                    string.Format("debug[{0}] =", this.DebugInformation.Keys.ToList().IndexOf(debugId)));
                            }
                            else if (tmp.Trim().StartsWith("DEBUG:"))
                                tmp = string.Empty;

                            foreach (var np in numberproperties)
                            {
                                var j = this._orderedNumberPropertyNames.IndexOf(np) + 1;

                                tmp = tmp.Replace(string.Format("tsys.{0}", np),
                                    string.Format("tsysNumberProperties[{0}]", j))
                                    .Replace(string.Format("fsys.{0}", np),
                                        string.Format("fsysNumberProperties[{0}]", j))
                                    .Replace(string.Format("tmp.{0}", np),
                                        string.Format("tmpNumberProperties[{0}]", j));

                                tmp = tmp.Replace("st.tsys", "tsysType");
                                tmp = tmp.Replace("st.fsys", "fsysType");
                                for (var k = 0; k < this._orderedSystemDescriptionNames.Count; k++)
                                    tmp = tmp.Replace(string.Format("st.{0}", this._orderedSystemDescriptionNames[k]),
                                        k.ToString());
                            }
                            collectCode += string.Concat("\t\t", tmp, "\n");
                        }
                    }

                    if (postInteractionScript.ModifyScript != null && postInteractionScript.ModifyScript.IsEnabled)
                    {
                        this._hasPostInteraction = true;

                        var lines = postInteractionScript.ModifyScript.Code.Split(new[] {"\r\n", "\n"},
                            StringSplitOptions.None);

                        for (var l = 0; l < lines.Length; l++)
                        {
                            var tmp = lines[l].Replace("ERROR:", "return ");

                            if (this.IsDebugging && tmp.Trim().StartsWith("DEBUG:"))
                            {
                                var debugId = Tuple.Create(postInteractionScript.ModifyScript.Id, l);
                                if (!this.DebugInformation.ContainsKey(debugId))
                                {
                                    this.DebugInformation.Add(debugId, null);
                                    debugVariableCount++;
                                }

                                tmp = tmp.Replace("DEBUG:",
                                    string.Format("debug[{0}] =", this.DebugInformation.Keys.ToList().IndexOf(debugId)));
                            }
                            else if (tmp.Trim().StartsWith("DEBUG:"))
                                tmp = string.Empty;

                            foreach (var np in numberproperties)
                            {
                                var j = this._orderedNumberPropertyNames.IndexOf(np) + 1;

                                tmp = tmp.Replace(string.Format("tsys.{0}", np),
                                    string.Format("tsysNumberProperties[{0}]", j));

                                tmp = tmp.Replace("st.tsys", "tsysType");
                                for (var k = 0; k < this._orderedSystemDescriptionNames.Count; k++)
                                    tmp = tmp.Replace(string.Format("st.{0}", this._orderedSystemDescriptionNames[k]),
                                        k.ToString());
                            }
                            modifyCode += string.Concat("\t\t", tmp, "\n");
                        }
                    }
                }
                collectCode += "\n\t}";
                modifyCode += "\n\t}";

                preSystemCodes.Add(prepareInteractionCode);
                interactionCodes.Add(interactionCode);
                postInteractionCodes.Add(collectCode);
                postSystemCodes.Add(modifyCode);
            }

            this._openClCode = this.SupportDouble ? "#pragma OPENCL EXTENSION cl_khr_fp64 : enable\n\n" : string.Empty;

            this.AddClIncludes();

            var code = new StreamReader(
                this.GetType()
                    .Assembly.GetManifestResourceStream("RalphsDotNet.Service.Simulation.Template.clt"))
                .ReadToEnd();

            this._openClCode = this.ReplacePlaceholder(code,
                this.SupportDouble ? "double" : "float",
                string.Join("\n\n", this._simulationDescription.RawScripts.Where(s => s.IsEnabled).Select(s => s.Code)),
                string.Join("\n", preSystemCodes),
                string.Join("\n", interactionCodes),
                string.Join("\n", postInteractionCodes),
                string.Join("\n", postSystemCodes),
                size,
                this._orderedNumberPropertyNames.Count + 1,
                debugVariableCount);

            return debugVariableCount;
        }

        private string ReplacePlaceholder(string code, string numbertype, string rawcode, string preparecode, string interactioncode, string collectcode, string modifycode, int simusize, int propertycount, int debugsize)
        {
            code = code.Replace(":numbertype:", numbertype);
            code = code.Replace(":rawcode:", rawcode);
            code = code.Replace(":preparecode:", preparecode);
            code = code.Replace(":interactioncode:", interactioncode);
            code = code.Replace(":collectcode:", collectcode);
            code = code.Replace(":modifycode:", modifycode);
            code = code.Replace(":simusize:", simusize.ToString());
            code = code.Replace(":propertycount:", propertycount.ToString());
            code = code.Replace(":debugsize:", debugsize.ToString());

            return code;
        }

        private void AddClIncludes()
        {
            // random number generator
            this._openClCode +=
                new StreamReader(
                    this.GetType()
                        .Assembly.GetManifestResourceStream("RalphsDotNet.Service.Simulation.RND.cl"))
                    .ReadToEnd();
        }

        private void CreateOrReplaceMemoryObjects(IIteration iteration, int debugVariableCount)
        {
            this._openClContext.ClearMemoryObjects();

            var orderedSystems = iteration.Systems.OrderBy(s => s.Name).ThenBy(s => s.Number).ToArray();

            this._errorCodeBuffer = this._openClContext.AddMemoryObject(new ClBufferConfiguration
            {
                Name = "ErrorCode",
                DataType = typeof (int),
                Size = 1
            }) as IClMemoryObject<int>;

            var debugSize = debugVariableCount > 0
                ? orderedSystems.Count()*debugVariableCount*orderedSystems.Count()
                : 1;
            this._debugBuffer = this._openClContext.AddMemoryObject(new ClBufferConfiguration
            {
                Name = "Debug",
                DataType = this.SupportDouble ? typeof (double) : typeof (float),
                Size = debugSize
            });

            // create numberpropertiestransferbuffer
            this._numberPropertyTransferBuffer = this._openClContext.AddMemoryObject(new ClBufferConfiguration
            {
                Name = "NumberPropertyTransfer",
                DataType = this.SupportDouble ? typeof (double) : typeof (float),
                Size = orderedSystems.Count()
            });

            // create numberpropertiestransferbuffer
            this._numberArrayPropertyTransferBuffer = this._openClContext.AddMemoryObject(new ClBufferConfiguration
            {
                Name = "NumberArrayPropertyTransfer",
                DataType = this.SupportDouble ? typeof (double) : typeof (float),
                Size = orderedSystems.Count()*orderedSystems.Count()
            });

            this._numberPropertyNum = this._openClContext.AddMemoryObject(new ClBufferConfiguration
            {
                Name = "NumberPropertyNum",
                DataType = typeof (int),
                Size = 1
            });

            // create numberpropertiesbuffer
            this._numberPropertiesBuffer = this._openClContext.AddMemoryObject(new ClBufferConfiguration
            {
                Name = "NumberProperties",
                DataType = this.SupportDouble ? typeof (double) : typeof (float),
                Size = orderedSystems.Count()*(this._orderedNumberPropertyNames.Count() + 1)*orderedSystems.Count()
            });

            // get opencl kernels and add parameters
            this._uploadNumberPropertyKernel.ClearArguments();
            this._uploadNumberPropertyKernel.AddArgument(0, this._numberPropertyTransferBuffer);
            this._uploadNumberPropertyKernel.AddArgument(1, this._numberPropertiesBuffer);
            this._uploadNumberPropertyKernel.AddArgument(2, this._numberPropertyNum);

            this._uploadNumberArrayPropertyKernel.ClearArguments();
            this._uploadNumberArrayPropertyKernel.AddArgument(0, this._numberArrayPropertyTransferBuffer);
            this._uploadNumberArrayPropertyKernel.AddArgument(1, this._numberPropertiesBuffer);
            this._uploadNumberArrayPropertyKernel.AddArgument(2, this._numberPropertyNum);

            this._downloadNumberPropertyKernel.ClearArguments();
            this._downloadNumberPropertyKernel.AddArgument(0, this._numberPropertyTransferBuffer);
            this._downloadNumberPropertyKernel.AddArgument(1, this._numberPropertiesBuffer);
            this._downloadNumberPropertyKernel.AddArgument(2, this._numberPropertyNum);

            this._downloadNumberArrayPropertyKernel.ClearArguments();
            this._downloadNumberArrayPropertyKernel.AddArgument(0, this._numberArrayPropertyTransferBuffer);
            this._downloadNumberArrayPropertyKernel.AddArgument(1, this._numberPropertiesBuffer);
            this._downloadNumberArrayPropertyKernel.AddArgument(2, this._numberPropertyNum);

            this._preInteractionKernel.ClearArguments();
            this._preInteractionKernel.AddArgument(0, this._errorCodeBuffer);
            this._preInteractionKernel.AddArgument(1, this._numberPropertiesBuffer);
            this._preInteractionKernel.AddArgument(2, this._debugBuffer);

            this._postInteractionKernel.ClearArguments();
            this._postInteractionKernel.AddArgument(0, this._errorCodeBuffer);
            this._postInteractionKernel.AddArgument(1, this._numberPropertiesBuffer);
            this._postInteractionKernel.AddArgument(2, this._debugBuffer);

            this._interactionKernel.ClearArguments();
            this._interactionKernel.AddArgument(0, this._errorCodeBuffer);
            this._interactionKernel.AddArgument(1, this._numberPropertiesBuffer);
            this._interactionKernel.AddArgument(2, this._debugBuffer);
        }

        private void UploadIteration(IIteration iteration, int debugVariableCount)
        {
            this._uploadedSystems = iteration.Systems.OrderBy(s => s.Name).ThenBy(s => s.Number).ToArray();

            this._errorCodeBuffer.UploadData(new[] {0}, this._openClDevice, true);

            this.InitializeDebugBuffer(debugVariableCount);

            this.UploadIteration();
        }

        private void UploadIteration()
        {
            if (this.SupportDouble)
            {
                // collect and upload system information property
                (this._numberPropertyNum as IClMemoryObject<int>).UploadData(new[] {0}, this._openClDevice, true);

                var numberPropertyValues = new double[this._uploadedSystems.Length];

                for (var tsys = 0; tsys < this._uploadedSystems.Length; tsys++)
                {
                    numberPropertyValues[tsys] =
                        this._orderedSystemDescriptionNames.IndexOf(this._uploadedSystems[tsys].Name);
                }

                (this._numberPropertyTransferBuffer as IClMemoryObject<double>).UploadData(numberPropertyValues,
                    this._openClDevice, true);

                this._uploadNumberPropertyKernel.Execute(this._openClDevice, new long[] {this._uploadedSystems.Length},
                    true);

                for (var i = 0; i < this._orderedNumberPropertyNames.Count; i++)
                {
                    // collect and upload property
                    var anyIsArray =
                        this._simulationDescription.SystemDescriptions.Select(sd =>
                            sd.NumberPropertyDescriptions.FirstOrDefault(
                                n => n.Name == this._orderedNumberPropertyNames[i]))
                            .Where(n => n != null)
                            .Any(n => n.IsArray);

                    (this._numberPropertyNum as IClMemoryObject<int>).UploadData(new[] {i + 1}, this._openClDevice, true);

                    numberPropertyValues = anyIsArray
                        ? new double[this._uploadedSystems.Length*this._uploadedSystems.Length]
                        : new double[this._uploadedSystems.Length];

                    for (var tsys = 0; tsys < this._uploadedSystems.Length; tsys++)
                    {
                        if (this._systemNumberPropertyNames[this._uploadedSystems[tsys].Name].Contains(
                            this._orderedNumberPropertyNames[i]) ||
                            this._systemNumberArrayPropertyNames[this._uploadedSystems[tsys].Name].Contains(
                                this._orderedNumberPropertyNames[i]))
                        {
                            var sd =
                                this._simulationDescription.SystemDescriptions.First(
                                    s => s.Name == this._uploadedSystems[tsys].Name);
                            var np =
                                sd.NumberPropertyDescriptions.FirstOrDefault(
                                    n => n.Name == this._orderedNumberPropertyNames[i]);

                            if (np != null)
                            {
                                var thisIsArray = np.IsArray;

                                if (anyIsArray)
                                {
                                    if (thisIsArray)
                                    {
                                        var numberProperty =
                                            this._uploadedSystems[tsys].NumberArrayProperties.First(
                                                n => n.Name == this._orderedNumberPropertyNames[i]);

                                        for (var fsys = 0; fsys < this._uploadedSystems.Length; fsys++)
                                            numberPropertyValues[tsys*this._uploadedSystems.Length + fsys] =
                                                numberProperty.Value[fsys];
                                    }
                                    else
                                    {
                                        var numberProperty =
                                            this._uploadedSystems[tsys].NumberProperties.First(
                                                n => n.Name == this._orderedNumberPropertyNames[i]);

                                        for (var fsys = 0; fsys < this._uploadedSystems.Length; fsys++)
                                            numberPropertyValues[tsys*this._uploadedSystems.Length + fsys] =
                                                numberProperty.Value;
                                    }
                                }
                                else
                                {
                                    var numberProperty =
                                        this._uploadedSystems[tsys].NumberProperties.First(
                                            n => n.Name == this._orderedNumberPropertyNames[i]);

                                    numberPropertyValues[tsys] = numberProperty.Value;
                                }
                            }
                        }
                    }

                    if (anyIsArray)
                    {
                        (this._numberArrayPropertyTransferBuffer as IClMemoryObject<double>).UploadData(
                            numberPropertyValues,
                            this._openClDevice, true);

                        this._uploadNumberArrayPropertyKernel.Execute(this._openClDevice,
                            new long[] {this._uploadedSystems.Length}, true);
                    }
                    else
                    {
                        (this._numberPropertyTransferBuffer as IClMemoryObject<double>).UploadData(numberPropertyValues,
                            this._openClDevice, true);

                        this._uploadNumberPropertyKernel.Execute(this._openClDevice,
                            new long[] {this._uploadedSystems.Length}, true);
                    }
                }
            }
            else
            {
                // collect and upload system information property
                (this._numberPropertyNum as IClMemoryObject<int>).UploadData(new[] {0}, this._openClDevice, true);

                var numberPropertyValues = new float[this._uploadedSystems.Length];

                for (var tsys = 0; tsys < this._uploadedSystems.Length; tsys++)
                {
                    numberPropertyValues[tsys] =
                        this._orderedSystemDescriptionNames.IndexOf(this._uploadedSystems[tsys].Name);
                }

                (this._numberPropertyTransferBuffer as IClMemoryObject<float>).UploadData(numberPropertyValues,
                    this._openClDevice, true);

                this._uploadNumberPropertyKernel.Execute(this._openClDevice, new long[] { this._uploadedSystems.Length },
                    true);

                for (var i = 0; i < this._orderedNumberPropertyNames.Count; i++)
                {
                    // collect and upload property
                    var anyIsArray =
                        this._simulationDescription.SystemDescriptions.Select(sd =>
                            sd.NumberPropertyDescriptions.FirstOrDefault(
                                n => n.Name == this._orderedNumberPropertyNames[i]))
                            .Where(n => n != null)
                            .Any(n => n.IsArray);

                    (this._numberPropertyNum as IClMemoryObject<int>).UploadData(new[] {i + 1}, this._openClDevice, true);

                    numberPropertyValues = anyIsArray
                        ? new float[this._uploadedSystems.Length*this._uploadedSystems.Length]
                        : new float[this._uploadedSystems.Length];

                    for (var tsys = 0; tsys < this._uploadedSystems.Length; tsys++)
                    {
                        if (this._systemNumberPropertyNames[this._uploadedSystems[tsys].Name].Contains(
                            this._orderedNumberPropertyNames[i]) ||
                            this._systemNumberArrayPropertyNames[this._uploadedSystems[tsys].Name].Contains(
                                this._orderedNumberPropertyNames[i]))
                        {
                            var sd =
                                this._simulationDescription.SystemDescriptions.First(
                                    s => s.Name == this._uploadedSystems[tsys].Name);
                            var np =
                                sd.NumberPropertyDescriptions.FirstOrDefault(
                                    n => n.Name == this._orderedNumberPropertyNames[i]);

                            if (np != null)
                            {
                                var thisIsArray = np.IsArray;

                                if (anyIsArray)
                                {
                                    if (thisIsArray)
                                    {
                                        var numberProperty =
                                            this._uploadedSystems[tsys].NumberArrayProperties.First(
                                                n => n.Name == this._orderedNumberPropertyNames[i]);

                                        for (var fsys = 0; fsys < this._uploadedSystems.Length; fsys++)
                                            numberPropertyValues[tsys*this._uploadedSystems.Length + fsys] =
                                                (float) numberProperty.Value[fsys];
                                    }
                                    else
                                    {
                                        var numberProperty =
                                            this._uploadedSystems[tsys].NumberProperties.First(
                                                n => n.Name == this._orderedNumberPropertyNames[i]);

                                        for (var fsys = 0; fsys < this._uploadedSystems.Length; fsys++)
                                            numberPropertyValues[tsys*this._uploadedSystems.Length + fsys] =
                                                (float) numberProperty.Value;
                                    }
                                }
                                else
                                {
                                    var numberProperty =
                                        this._uploadedSystems[tsys].NumberProperties.First(
                                            n => n.Name == this._orderedNumberPropertyNames[i]);

                                    numberPropertyValues[tsys] = (float) numberProperty.Value;
                                }
                            }
                        }
                    }

                    if (anyIsArray)
                    {
                        (this._numberArrayPropertyTransferBuffer as IClMemoryObject<float>).UploadData(
                            numberPropertyValues,
                            this._openClDevice, true);

                        this._uploadNumberArrayPropertyKernel.Execute(this._openClDevice,
                            new long[] {this._uploadedSystems.Length}, true);
                    }
                    else
                    {
                        (this._numberPropertyTransferBuffer as IClMemoryObject<float>).UploadData(numberPropertyValues,
                            this._openClDevice, true);

                        this._uploadNumberPropertyKernel.Execute(this._openClDevice,
                            new long[] {this._uploadedSystems.Length}, true);
                    }
                }
            }
        }

        private void InitializeDebugBuffer(int debugVariableCount)
        {
            if (this.SupportDouble)
            {
                ((IClMemoryObject<double>) this._debugBuffer).UploadData(new double[
                    debugVariableCount > 0
                        ? this._uploadedSystems.Length*debugVariableCount*this._uploadedSystems.Length
                        : 1], this._openClDevice, true);
            }
            else
            {
                ((IClMemoryObject<float>) this._debugBuffer).UploadData(new float[
                    debugVariableCount > 0
                        ? this._uploadedSystems.Length*debugVariableCount*this._uploadedSystems.Length
                        : 1], this._openClDevice, true);
            }
        }

        private void LoadClKernel()
        {
            this._uploadNumberPropertyKernel = this._openClContext.GetKernel("UploadNumberProperty");
            this._uploadNumberArrayPropertyKernel = this._openClContext.GetKernel("UploadNumberArrayProperty");
            this._downloadNumberPropertyKernel = this._openClContext.GetKernel("DownloadNumberProperty");
            this._downloadNumberArrayPropertyKernel = this._openClContext.GetKernel("DownloadNumberArrayProperty");
            this._preInteractionKernel = this._openClContext.GetKernel("PreInteraction");
            this._postInteractionKernel = this._openClContext.GetKernel("PostInteraction");
            this._interactionKernel = this._openClContext.GetKernel("Interaction");
        }

        private void RunInternal()
        {
            try
            {
                this._dataStreamService =
                    ServiceProvider.GetService<IDataStreamService>(this.SimulationConfiguration.DataStreamServiceId);
                this._dataStream = this._dataStreamService.GetStream(this.SimulationConfiguration.DataStreamId);

                this.CollectDescriptiveData();

                // no simulationdescription? bang!
                if (this._simulationDescription == null)
                    throw new SimulationServiceException("no simulationdescrition avaliable, aborting simulation");

                foreach (var profile in this._extensionProfiles)
                {
                    this._dataStream.Write(profile);
                }

                this.SupportDouble = this._simulationDescription.SupportDouble;

                if (this.ObstacleMiliseconds < 0)
                    this.ObstacleMiliseconds = this._simulationDescription.ObstacleMiliseconds;

                if (this.SampleRate < 0)
                    this.SampleRate = this._simulationDescription.SampleRate;

                this.ActualIteration = this.CreateSimulation();

                var debugVariableCount = this.BuildClKernel(this.ActualIteration.Systems.Count);

                // get openclservice, clear and create an opencl context
                this._openClService =
                    ServiceProvider.GetService<IOpenClService>(this.SimulationConfiguration.OpenClServiceId);

                var sources = new List<string>
                {
                    this._openClCode
                };

                this._openClContext = this._openClService.CreateContext(sources.ToArray(),
                    "AMD;NVIDIA;Intel", this.SupportDouble ? new[] {"cl_khr_fp64"} : null,
                    this.SimulationConfiguration.UseCpu ? ClDeviceType.Cpu : ClDeviceType.GpuPreferred);

                if (this._openClContext != null)
                {
                    try
                    {
                        this._openClDevice =
                            this._openClContext.GetDevice(this._openClContext.GetDeviceNames().First());

                        if (this._openClDevice == null)
                            throw new SimulationServiceException("no supported gpu found");

                        this.LoadClKernel();

                        this.CreateOrReplaceMemoryObjects(this.ActualIteration, debugVariableCount);

                        this.IsRunning = true;
                        this._isStarting = false;
                        this._startingSynchronizer.Release();

                        if (this._pauseAfter == 0)
                        {
                            this._shouldPause = true;
                            this._pauseAfter = -1;
                        }

                        this.Iterate(this.ActualIteration, debugVariableCount);
                    }
                    finally
                    {
                        this._openClContext.Dispose();
                        this.SimulationId = Guid.Empty;
                    }
                }
            }
            catch (SimulationServiceException ex)
            {
                this._proxy.RuntimeError = ex;

                this._startingSynchronizer.Release();
            }
            catch (Exception ex)
            {
                this._proxy.RuntimeError = new SimulationServiceException("something went really wrong", ex);

                this._startingSynchronizer.Release();
            }
            finally
            {
                this.SimulationId = Guid.Empty;
                this.DebugInformation = null;
                this.ActualIteration = null;
                this._simulationDescription = null;
                this._extensionProfiles = null;
                this.ErrorCode = 0;
                this._shouldPause = false;
                this._shouldStep = false;
                this.IsPaused = false;
                this.IsRunning = false;
                this._isStarting = false;
                this._modifiedIteration = null;
                this.IsDebugging = false;

                if (this._shouldStop)
                    this._stopSynchronizer.Release();

                this._shouldStop = false;
            }
        }

        private void Iterate(IIteration iteration, int debugVariableCount)
        {
            ISystem[] orderedSystems = null;

            // run the whole thing until it gets killed
            var iterationNum = this.ActualIteration.Number;
            while (!this._shouldStop)
            {
                if (!this._shouldPause || this._shouldStep)
                {
                    this.IsPaused = false;

                    if (orderedSystems == null || this._modifiedIteration != null)
                    {
                        iteration = this._modifiedIteration ?? iteration;
                        orderedSystems = iteration.Systems.OrderBy(s => s.Name).ThenBy(s => s.Number).ToArray();
                        this.UploadIteration(iteration, debugVariableCount);
                        this._modifiedIteration = null;
                    }

                    for (var i = 0; i < this.GetSimulationSampleRate(); i++)
                    {
                        if (this._hasPreInteraction)
                            this._preInteractionKernel.Execute(this._openClDevice, new long[] {orderedSystems.Length},
                                false);
                        if (this._hasInteraction)
                            this._interactionKernel.Execute(this._openClDevice,
                                new long[] {orderedSystems.Length, orderedSystems.Length}, false);
                        if (this._hasPostInteraction)
                            this._postInteractionKernel.Execute(this._openClDevice, new long[] {orderedSystems.Length},
                                false);

                        iterationNum++;
                    }

                    this._dataStream.Write(iteration);

                    this._openClContext.WaitForQueueToFinish(this._openClDevice);

                    // take a nap my friend (but only if not debugging)
                    if (!this.IsDebugging)
                        for (var i = 0; i < this.ObstacleMiliseconds; i++)
                        {
                            if (!this._shouldStop)
                                new ManualResetEvent(false).WaitOne(TimeSpan.FromMilliseconds(1));
                            else
                                break;
                        }

                    if (this.IsDebugging) this.DownloadDebugInformation(debugVariableCount);
                    iteration = this.DownloadIteration(iterationNum);

                    if (this._pauseAfter == 1)
                    {
                        this._shouldPause = true;
                        this._pauseAfter = -1;
                    }
                    else if (this._pauseAfter > 0)
                        this._pauseAfter--;

                    if (this._shouldStep)
                        this._shouldStep = false;

                    if (this.IsDebugging)
                        this._shouldPause = true;
                }
                else
                {
                    this._dataStream.WaitForQueueToFinish();
                    this.IsPaused = true;
                    if (this._shouldPause)
                        this._pauseSynchronizer.Release();
                }
            }
        }

        private int GetSimulationSampleRate()
        {
            return this.SampleRate > 0 && !this.IsDebugging ? this.SampleRate : 1;
        }

        private IIteration DownloadIteration(ulong iterationNum)
        {
            var iteration = DataObject.Create<IIteration>();
            iteration.Number = iterationNum;
            iteration.Systems = new ObservableCollection<ISystem>();
            iteration.SimulationId = this._simulationId;
            var systems = new ISystem[this._uploadedSystems.Length];

            this.DownloadIteration(systems);

            foreach (var system in systems)
                iteration.Systems.Add(system);

            this.ActualIteration = iteration;

            if (this.ErrorCode != 0)
                this._shouldPause = true;

            return iteration;
        }

        private void DownloadIteration(ISystem[] systems)
        {
            // TODO maybe this function can be more beautyful
            this.ErrorCode = this._errorCodeBuffer.DownloadData(this._openClDevice, true)[0];
            var size = this._uploadedSystems.Length;

            if (this.SupportDouble)
            {
                // download system information property
                (this._numberPropertyNum as IClMemoryObject<int>).UploadData(new[] {0}, this._openClDevice, true);

                this._downloadNumberPropertyKernel.Execute(this._openClDevice, new long[] { size },
                    true);

                var numberPropertyValues =
                    (this._numberPropertyTransferBuffer as IClMemoryObject<double>).DownloadData(this._openClDevice,
                        true);

                // create systems
                for (var tsys = 0; tsys < size; tsys++)
                {
                    var systemType = Convert.ToInt32(numberPropertyValues[tsys]);

                    var system = DataObject.Create<ISystem>();
                    system.Name = this._systemNumberPropertyNames.Keys.ToArray()[systemType];
                    system.Number = tsys + 1;
                    system.NumberProperties = new ObservableCollection<INumberProperty>();
                    system.NumberArrayProperties = new ObservableCollection<INumberArrayProperty>();
                    systems[tsys] = system;
                }

                for (var i = 0; i < this._orderedNumberPropertyNames.Count; i++)
                {
                    // download property
                    var anyIsArray =
                        this._simulationDescription.SystemDescriptions.Select(sd =>
                            sd.NumberPropertyDescriptions.FirstOrDefault(
                                n => n.Name == this._orderedNumberPropertyNames[i]))
                            .Where(n => n != null)
                            .Any(n => n.IsArray);

                    (this._numberPropertyNum as IClMemoryObject<int>).UploadData(new[] {i + 1}, this._openClDevice, true);

                    if (anyIsArray)
                    {
                        this._downloadNumberArrayPropertyKernel.Execute(this._openClDevice,
                            new long[] { size },
                            true);

                        numberPropertyValues =
                            (this._numberArrayPropertyTransferBuffer as IClMemoryObject<double>).DownloadData(
                                this._openClDevice,
                                true);
                    }
                    else
                    {
                        this._downloadNumberPropertyKernel.Execute(this._openClDevice,
                            new long[] { size },
                            true);

                        numberPropertyValues =
                            (this._numberPropertyTransferBuffer as IClMemoryObject<double>).DownloadData(
                                this._openClDevice,
                                true);
                    }

                    for (var tsys = 0; tsys < size; tsys++)
                    {
                        if (this._systemNumberPropertyNames[systems[tsys].Name].Contains(
                            this._orderedNumberPropertyNames[i]) ||
                            this._systemNumberArrayPropertyNames[systems[tsys].Name].Contains(
                                this._orderedNumberPropertyNames[i]))
                        {
                            var sd =
                                this._simulationDescription.SystemDescriptions.First(s => s.Name == systems[tsys].Name);
                            var np =
                                sd.NumberPropertyDescriptions.FirstOrDefault(
                                    n => n.Name == this._orderedNumberPropertyNames[i]);

                            if (np != null)
                            {
                                var thisIsArray = np.IsArray;

                                if (anyIsArray)
                                {
                                    if (thisIsArray)
                                    {
                                        var numberProperty = DataObject.Create<INumberArrayProperty>();
                                        numberProperty.Name = this._orderedNumberPropertyNames[i];
                                        numberProperty.Value = new double[size];

                                        for (var fsys = 0; fsys < size; fsys++)
                                            numberProperty.Value[fsys] =
                                                numberPropertyValues[tsys * size + fsys];
                                        systems[tsys].NumberArrayProperties.Add(numberProperty);
                                    }
                                    else
                                    {
                                        var numberProperty = DataObject.Create<INumberProperty>();
                                        numberProperty.Name = this._orderedNumberPropertyNames[i];
                                        numberProperty.Value =
                                            numberPropertyValues[tsys * size + tsys];
                                        systems[tsys].NumberProperties.Add(numberProperty);
                                    }
                                }
                                else
                                {
                                    var numberProperty = DataObject.Create<INumberProperty>();
                                    numberProperty.Name = this._orderedNumberPropertyNames[i];
                                    numberProperty.Value =
                                        numberPropertyValues[tsys];
                                    systems[tsys].NumberProperties.Add(numberProperty);
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                // download system information property
                (this._numberPropertyNum as IClMemoryObject<int>).UploadData(new[] {0}, this._openClDevice, true);

                this._downloadNumberPropertyKernel.Execute(this._openClDevice, new long[] { size },
                    true);

                var numberPropertyValues =
                    (this._numberPropertyTransferBuffer as IClMemoryObject<float>).DownloadData(this._openClDevice,
                        true);

                // create systems
                for (var tsys = 0; tsys < size; tsys++)
                {
                    var systemType = Convert.ToInt32(numberPropertyValues[tsys]);

                    var system = DataObject.Create<ISystem>();
                    system.Name = this._systemNumberPropertyNames.Keys.ToArray()[systemType];
                    system.Number = tsys + 1;
                    system.NumberProperties = new ObservableCollection<INumberProperty>();
                    system.NumberArrayProperties = new ObservableCollection<INumberArrayProperty>();
                    systems[tsys] = system;
                }

                for (var i = 0; i < this._orderedNumberPropertyNames.Count; i++)
                {
                    // download property
                    var anyIsArray =
                        this._simulationDescription.SystemDescriptions.Select(sd =>
                            sd.NumberPropertyDescriptions.FirstOrDefault(
                                n => n.Name == this._orderedNumberPropertyNames[i]))
                            .Where(n => n != null)
                            .Any(n => n.IsArray);

                    (this._numberPropertyNum as IClMemoryObject<int>).UploadData(new[] {i + 1}, this._openClDevice, true);

                    if (anyIsArray)
                    {
                        this._downloadNumberArrayPropertyKernel.Execute(this._openClDevice,
                            new long[] { size },
                            true);

                        numberPropertyValues =
                            (this._numberArrayPropertyTransferBuffer as IClMemoryObject<float>).DownloadData(
                                this._openClDevice,
                                true);
                    }
                    else
                    {
                        this._downloadNumberPropertyKernel.Execute(this._openClDevice,
                            new long[] { size },
                            true);

                        numberPropertyValues =
                            (this._numberPropertyTransferBuffer as IClMemoryObject<float>).DownloadData(
                                this._openClDevice,
                                true);
                    }

                    for (var tsys = 0; tsys < size; tsys++)
                    {
                        if (this._systemNumberPropertyNames[systems[tsys].Name].Contains(
                            this._orderedNumberPropertyNames[i]) ||
                            this._systemNumberArrayPropertyNames[systems[tsys].Name].Contains(
                                this._orderedNumberPropertyNames[i]))
                        {
                            var sd =
                                this._simulationDescription.SystemDescriptions.First(s => s.Name == systems[tsys].Name);
                            var np =
                                sd.NumberPropertyDescriptions.FirstOrDefault(
                                    n => n.Name == this._orderedNumberPropertyNames[i]);

                            if (np != null)
                            {
                                var thisIsArray = np.IsArray;

                                if (anyIsArray)
                                {
                                    if (thisIsArray)
                                    {
                                        var numberProperty = DataObject.Create<INumberArrayProperty>();
                                        numberProperty.Name = this._orderedNumberPropertyNames[i];
                                        numberProperty.Value = new double[size];

                                        for (var fsys = 0; fsys < size; fsys++)
                                            numberProperty.Value[fsys] =
                                                numberPropertyValues[tsys * size + fsys];
                                        systems[tsys].NumberArrayProperties.Add(numberProperty);
                                    }
                                    else
                                    {
                                        var numberProperty = DataObject.Create<INumberProperty>();
                                        numberProperty.Name = this._orderedNumberPropertyNames[i];
                                        numberProperty.Value =
                                            numberPropertyValues[tsys * size + tsys];
                                        systems[tsys].NumberProperties.Add(numberProperty);
                                    }
                                }
                                else
                                {
                                    var numberProperty = DataObject.Create<INumberProperty>();
                                    numberProperty.Name = this._orderedNumberPropertyNames[i];
                                    numberProperty.Value =
                                        numberPropertyValues[tsys];
                                    systems[tsys].NumberProperties.Add(numberProperty);
                                }
                            }
                        }
                    }
                }
            }
        }

        private void DownloadDebugInformation(int debugVariableCount)
        {
            var size = this._uploadedSystems.Length;
            Array debugValues;

            if (this.SupportDouble)
                debugValues = (this._debugBuffer as IClMemoryObject<double>).DownloadData(this._openClDevice, true);
            else
                debugValues = (this._debugBuffer as IClMemoryObject<float>).DownloadData(this._openClDevice, true);

            //Parallel.For(0, debugVariableCount, i =>
            for (var i = 0; i < debugVariableCount; i++)
            {
                var arr = new double[this._uploadedSystems.Length, this._uploadedSystems.Length];

                for (var tsys = 0; tsys < this._uploadedSystems.Length; tsys++)
                {
                    for (var fsys = 0; fsys < this._uploadedSystems.Length; fsys++)
                    {
                        var index = size*size*i + size*fsys + tsys;
                        if (this.SupportDouble)
                            arr[tsys, fsys] = ((double[]) debugValues)[index];
                        else
                            arr[tsys, fsys] = ((float[]) debugValues)[index];
                    }
                }

                this.DebugInformation[this.DebugInformation.Keys.ToArray()[i]] = arr;
            } //);
        }

        private IIteration CreateSimulation()
        {
            // here we create a new simulation and populate it with the initial values
            var simulation = DataObject.Create<ISimulation>();
            this.SimulationId = simulation.Id = Guid.NewGuid();

            var scriptEngine = Python.CreateEngine();
            var scriptScope = scriptEngine.CreateScope();
            var distributionScriptSources = new Dictionary<string, ScriptSource>();

            var overallAmount = this._simulationDescription.SystemDescriptions.Select(sd => sd.Amount).Sum();

            var iteration = DataObject.Create<IIteration>();
            iteration.Number = 0;
            iteration.Systems = new ObservableCollection<ISystem>();
            iteration.SimulationId = this._simulationId;

            this._systemNumberPropertyNames.Clear();

            foreach (var sd in this._simulationDescription.SystemDescriptions.OrderBy(d => d.Name))
            {
                var numberPropertyNames = new List<string>();
                var numberArrayPropertyNames = new List<string>();
                foreach (var npd in sd.NumberPropertyDescriptions)
                {
                    if (!npd.IsArray && !numberPropertyNames.Contains(npd.Name))
                        numberPropertyNames.Add(npd.Name);
                    else if (npd.IsArray && !numberArrayPropertyNames.Contains(npd.Name))
                        numberArrayPropertyNames.Add(npd.Name);

                    if (!string.IsNullOrEmpty(npd.Distribution))
                        distributionScriptSources.Add(string.Concat(sd.Name, "_", npd.Name),
                            scriptEngine.CreateScriptSourceFromString(
                                string.Format("import math\ndef f(x):\treturn {0}\ny = float(f(x))", npd.Distribution)));
                }

                if (!this._systemNumberPropertyNames.ContainsKey(sd.Name))
                    this._systemNumberPropertyNames.Add(sd.Name, numberPropertyNames);

                if (!this._systemNumberArrayPropertyNames.ContainsKey(sd.Name))
                    this._systemNumberArrayPropertyNames.Add(sd.Name, numberArrayPropertyNames);

                for (var i = 1; i <= sd.Amount; i++)
                {
                    var system = DataObject.Create<ISystem>();
                    system.Name = sd.Name;
                    system.Number = i;
                    system.NumberProperties = new ObservableCollection<INumberProperty>();
                    system.NumberArrayProperties = new ObservableCollection<INumberArrayProperty>();
                    iteration.Systems.Add(system);

                    foreach (var npd in sd.NumberPropertyDescriptions)
                    {
                        try
                        {
                            if (npd.IsArray)
                            {
                                var value = new double[overallAmount];

                                ScriptSource ss;
                                distributionScriptSources.TryGetValue(string.Concat(sd.Name, "_", npd.Name), out ss);

                                for (var j = 1; j <= overallAmount; j++)
                                {
                                    if (ss != null)
                                    {
                                        scriptScope.SetVariable("x", this._rnd.NextDouble());
                                        scriptScope.SetVariable("k", Convert.ToDouble(i));
                                        scriptScope.SetVariable("i", Convert.ToDouble(j));
                                        scriptScope.SetVariable("n", Convert.ToDouble(sd.Amount));
                                        ss.Execute(scriptScope);
                                        value[j - 1] = scriptScope.GetVariable<double>("y");
                                    }
                                }

                                var np = DataObject.Create<INumberArrayProperty>();
                                np.Name = npd.Name;
                                np.Value = value;
                                system.NumberArrayProperties.Add(np);
                            }
                            else
                            {
                                var value = double.NaN;

                                ScriptSource ss;
                                distributionScriptSources.TryGetValue(string.Concat(sd.Name, "_", npd.Name), out ss);
                                if (ss != null)
                                {
                                    scriptScope.SetVariable("x", this._rnd.NextDouble());
                                    scriptScope.SetVariable("k", Convert.ToDouble(i));
                                    scriptScope.SetVariable("n", Convert.ToDouble(sd.Amount));
                                    ss.Execute(scriptScope);
                                    value = scriptScope.GetVariable<double>("y");
                                }

                                var np = DataObject.Create<INumberProperty>();
                                np.Name = npd.Name;
                                np.Value = value;
                                system.NumberProperties.Add(np);
                            }
                        }
                        catch (Exception ex)
                        {
                            throw (new RuntimeException(string.Format("error on creating number {0} of system {1}", npd.Name, sd.Name), ex));
                        }
                    }
                }
            }

            this._dataStream.Write(iteration);

            return iteration;
        }

        private void OnPropertyChanged(string p)
        {
            if (this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(p));
        }
    }
}