﻿// Pre: a<M, b<M
// Post: r=(a+b) mod M
ulong RND_AddMod64(ulong a, ulong b, ulong M)
{
	ulong v = a + b;
	if ((v >= M) || (v<a))
		v = v - M;
	return v;
}

// Pre: a<M,b<M
// Post: r=(a*b) mod M
// This could be done more efficently, but it is portable, and should
// be easy to understand. It can be replaced with any of the better
// modular multiplication algorithms (for example if you know you have
// double precision available or something).
ulong RND_MulMod64(ulong a, ulong b, ulong M)
{
	ulong r = 0;
	while (a != 0){
		if (a & 1)
			r = RND_AddMod64(r, b, M);
		b = RND_AddMod64(b, b, M);
		a = a >> 1;
	}
	return r;
}

// Pre: a<M, e>=0
// Post: r=(a^b) mod M
// This takes at most ~64^2 modular additions, so probably about 2^15 or so instructions on
// most architectures
ulong RND_PowMod64(ulong a, ulong e, ulong M)
{
	ulong sqr = a, acc = 1;
	while (e != 0){
		if (e & 1)
			acc = RND_MulMod64(acc, sqr, M);
		sqr = RND_MulMod64(sqr, sqr, M);
		e = e >> 1;
	}
	return acc;
}

uint2 RND_SkipImpl_Mod64(uint2 curr, ulong A, ulong M, ulong distance)
{
	ulong m = RND_PowMod64(A, distance, M);
	ulong x = curr.x*(ulong)A + curr.y;
	x = RND_MulMod64(x, m, M);
	return (uint2)((uint)(x / A), (uint)(x%A));
}

uint2 RND_SeedImpl_Mod64(ulong A, ulong M, uint vecSize, uint vecOffset, ulong streamBase, ulong streamGap)
{
	// This is an arbitrary constant for starting LCG jumping from. I didn't
	// want to start from 1, as then you end up with the two or three first values
	// being a bit poor in ones - once you've decided that, one constant is as
	// good as any another. There is no deep mathematical reason for it, I just
	// generated a random number.
	enum{ RND_BASEID = 4077358422479273989UL };

	ulong dist = streamBase + (get_global_id(0)*vecSize + vecOffset)*streamGap;
	ulong m = RND_PowMod64(A, dist, M);

	ulong x = RND_MulMod64(RND_BASEID, m, M);
	return (uint2)((uint)(x / A), (uint)(x%A));
}

//! Represents the state of a particular generator
typedef struct{ uint x; uint c; } rnd64x_state_t;

enum{ RND64X_A = 4294883355U };
enum{ RND64X_M = 18446383549859758079UL };

void RND64X_Step(rnd64x_state_t *s)
{
	uint X = s->x, C = s->c;

	uint Xn = RND64X_A*X + C;
	uint carry = (uint)(Xn<C);				// The (Xn<C) will be zero or one for scalar
	uint Cn = mad_hi(RND64X_A, X, carry);

	s->x = Xn;
	s->c = Cn;
}

void RND64X_Skip(rnd64x_state_t *s, ulong distance)
{
	uint2 tmp = RND_SkipImpl_Mod64((uint2)(s->x, s->c), RND64X_A, RND64X_M, distance);
	s->x = tmp.x;
	s->c = tmp.y;
}

void RND64X_SeedStreams(rnd64x_state_t *s, ulong baseOffset, ulong perStreamOffset)
{
	uint2 tmp = RND_SeedImpl_Mod64(RND64X_A, RND64X_M, 1, 0, baseOffset, perStreamOffset);
	s->x = tmp.x;
	s->c = tmp.y;
}

//! Return a 32-bit integer in the range [0..2^32)
uint RND64X_NextUint(rnd64x_state_t *s)
{
	uint res = s->x ^ s->c;
	RND64X_Step(s);
	return res;
}
