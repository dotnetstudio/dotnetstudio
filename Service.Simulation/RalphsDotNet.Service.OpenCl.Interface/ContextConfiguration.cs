﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RalphsDotNet.Extension.System;

namespace RalphsDotNet.Service.OpenCl.Interface
{
    [DataContract]
    public class ContextConfiguration
    {
        [DataMember(Order = 0)]
        public DeviceType DeviceType { get; set; }

        [DataMember(Order = 1)]
        public string PlatformSearchWord { get; set; }

        [DataMember(Order = 2)]
        public string[] SourceFiles { get; set; }

        [DataMember(Order = 3)]
        public MemoryObjectConfiguration[] MemoryObjects { get; set; }

        [DataMember(Order = 4)]
        public KernelConfiguration[] Kernels { get; set; }
    }

    [DataContract]
    public class KernelConfiguration
    {
        [DataMember(Order = 0)]
        public string Name { get; set; }

        [DataMember(Order = 1)]
        public string[] Arguments { get; set; }
    }

    [DataContract]
    public abstract class MemoryObjectConfiguration
    {
        [DataMember(Order = 0)]
        public string Name { get; set; }

        [DataMember(Order = 1)]
        private string DataTypeName { get; set; }

        public Type DataType
        {
            get
            {
                return this.DataTypeName.FindType();
            }
            set
            {
                this.DataTypeName = string.Format("{0}, {1}", value.FullName, value.Assembly.GetName().Name);
            }
        }
    }

    [DataContract]
    public class BufferConfiguration : MemoryObjectConfiguration
    {
        [DataMember(Order = 2)]
        public long Size { get; set; }
    }

    [DataContract]
    public class ImageConfiguration : MemoryObjectConfiguration
    {
        [DataMember(Order = 2)]
        public int ChannelAmount { get; set; }
    }

    [DataContract]
    public class Image2DConfiguration : ImageConfiguration
    {
        [DataMember(Order = 3)]
        public long Width { get; set; }

        [DataMember(Order = 4)]
        public long Height { get; set; }
    }

    [DataContract]
    public class Image3DConfiguration : ImageConfiguration
    {
        [DataMember(Order = 3)]
        public long Width { get; set; }

        [DataMember(Order = 4)]
        public long Height { get; set; }

        [DataMember(Order = 5)]
        public long Depth { get; set; }
    }
}
