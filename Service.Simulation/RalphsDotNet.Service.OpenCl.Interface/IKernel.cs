﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace RalphsDotNet.Service.OpenCl.Interface
{
    public interface IKernel : IDisposable
    {
        string Name { get; }
        ObservableDictionary<int, IMemoryObject> Arguments { get; }
        IContext Context { get; }

        void ExecuteSingle(IDevice device);

        void Execute(IDevice device, long[] globalWorkSize);

        void Execute(IDevice device, long[] globalWorkOffset, long[] globalWorkSize, long[] localWorkSize);
    }
}
