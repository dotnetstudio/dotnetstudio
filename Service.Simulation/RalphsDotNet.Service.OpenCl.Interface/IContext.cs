﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RalphsDotNet.Service.OpenCl.Interface
{
    public interface IContext : IDisposable
    {
        IDevice[] Devices { get; }
        IKernel[] Kernels { get; }

        IMemoryObject[] MemoryObjects { get; }

        void AddMemoryObject(IMemoryObject mo);
        void RemoveMemoryObject(IMemoryObject mo);
    }
}
