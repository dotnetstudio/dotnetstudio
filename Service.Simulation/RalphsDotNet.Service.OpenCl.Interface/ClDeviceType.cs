﻿namespace RalphsDotNet.Service.OpenCl.Interface
{
    public enum ClDeviceType
    {
        Default,
        All,
        Gpu,
        GpuPreferred,
        Cpu,
        Accelerator
    }
}
