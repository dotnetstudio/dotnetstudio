﻿using System;
using System.Collections.Generic;

namespace RalphsDotNet.Service.OpenCl.Interface
{
    public interface IOpenClService : IService
    {
        IClContext CreateContext(IEnumerable<string> sources);

        IClContext CreateContext(IEnumerable<string> sources, ClDeviceType deviceType);

        IClContext CreateContext(IEnumerable<string> sources, string platformSearchWord,
            string[] requiredExtensions, ClDeviceType deviceType);

        IClContext CreateContext(ClContextConfiguration configuration);
    }
}
