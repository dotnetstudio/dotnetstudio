﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RalphsDotNet.Service.OpenCl.Interface
{
    public interface IMemoryObject : IDisposable
    {
        MemoryType Type { get; }
        string Name { get; }
        IContext Context { get; }
    }

    public enum MemoryType
    {
        Buffer,
        Image
    }
}
