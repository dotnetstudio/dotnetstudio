﻿using System;

namespace RalphsDotNet.Service.OpenCl.Interface
{
    public enum MemoryType
    {
        Buffer,
        Image
    }

    public interface IClMemoryObject : IServiceObject
    {
        long Size { get; }
        MemoryType Type { get; }
        string Name { get; }
        IClContext Context { get; }
    }

    public interface IClMemoryObject<T> : IClMemoryObject where T : struct
    {
        void UploadData(T[] data, IClDevice device, bool blocking);
        T[] DownloadData(IClDevice device, bool blocking);
    }
}
