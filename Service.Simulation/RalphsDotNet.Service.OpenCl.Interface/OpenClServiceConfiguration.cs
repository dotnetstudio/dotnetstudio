﻿namespace RalphsDotNet.Service.OpenCl.Interface
{
    public class OpenClServiceConfiguration : ServiceConfiguration
    {
        public override System.Type ServiceType
        {
            get { return typeof(IOpenClService); }
        }
    }
}
