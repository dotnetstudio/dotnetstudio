﻿using System;
using System.Collections.ObjectModel;

namespace RalphsDotNet.Service.OpenCl.Interface
{
    public interface IClKernel : IServiceObject
    {
        string Name { get; }
        IClContext Context { get; }

        int[] GetArgumentIndices();
        void AddArgument(int index, IClMemoryObject obj);
        void RemoveArgument(int index);
        void ClearArguments();

        void ExecuteSingle(IClDevice device, bool blocking);

        void Execute(IClDevice device, long[] globalWorkSize, bool blocking);

        void Execute(IClDevice device, long[] globalWorkOffset, long[] globalWorkSize,
            long[] localWorkSize, bool blocking);
    }
}
