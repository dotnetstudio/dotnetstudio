﻿namespace RalphsDotNet.Service.OpenCl.Interface
{
    public interface IClDevice : IServiceObject
    {
        string Name { get; }
        string Vendor { get; }

        string[] GetProperties();
        object GetProperty(string propertyName);
    }
}
