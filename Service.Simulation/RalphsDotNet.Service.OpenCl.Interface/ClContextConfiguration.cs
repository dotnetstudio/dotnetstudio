﻿using System;
using System.Runtime.Serialization;
using RalphsDotNet.Extension;

namespace RalphsDotNet.Service.OpenCl.Interface
{
    [DataContract]
    public class ClContextConfiguration
    {
        [DataMember(Order = 0)]
        public ClDeviceType DeviceType { get; set; }

        // TODO introduce limitationfilter like workgoursize, etc
        [DataMember(Order = 1)]
        public string PlatformSearchPattern { get; set; }

        [DataMember(Order = 2)]
        public string[] SourceFiles { get; set; }

        [DataMember(Order = 3)]
        public ClMemoryObjectConfiguration[] MemoryObjects { get; set; }

        [DataMember(Order = 4)]
        public ClKernelConfiguration[] Kernels { get; set; }

        [DataMember(Order = 5)]
        public string[] RequiredExtensions { get; set; }
    }

    [DataContract]
    public class ClKernelConfiguration
    {
        [DataMember(Order = 0)]
        public string Name { get; set; }

        [DataMember(Order = 1)]
        public string[] Arguments { get; set; }
    }

    [DataContract]
    public abstract class ClMemoryObjectConfiguration
    {
        [DataMember(Order = 0)]
        public string Name { get; set; }

        [DataMember(Order = 1)]
        private string DataTypeName { get; set; }

        public Type DataType
        {
            get { return this.DataTypeName.FindType(); }
            set { this.DataTypeName = string.Format("{0}, {1}", value.FullName, value.Assembly.GetName().Name); }
        }
    }

    [DataContract]
    public class ClBufferConfiguration : ClMemoryObjectConfiguration
    {
        [DataMember(Order = 2)]
        public long Size { get; set; }
    }

    [DataContract]
    public class ClImageConfiguration : ClMemoryObjectConfiguration
    {
        [DataMember(Order = 2)]
        public int ChannelAmount { get; set; }
    }

    [DataContract]
    public class ClImage2DConfiguration : ClImageConfiguration
    {
        [DataMember(Order = 3)]
        public int Width { get; set; }

        [DataMember(Order = 4)]
        public int Height { get; set; }
    }

    [DataContract]
    public class ClImage3DConfiguration : ClImageConfiguration
    {
        [DataMember(Order = 3)]
        public int Width { get; set; }

        [DataMember(Order = 4)]
        public int Height { get; set; }

        [DataMember(Order = 5)]
        public int Depth { get; set; }
    }
}