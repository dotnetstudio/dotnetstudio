﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RalphsDotNet.Service.OpenCl.Interface
{
    public interface IDevice
    {
        string Name { get; }
        string Vendor { get; }
    }
}
