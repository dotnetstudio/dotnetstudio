﻿using System;

namespace RalphsDotNet.Service.OpenCl.Interface
{
    public interface IClContext : IServiceObject
    {
        string Name { get; }

        string[] GetKernelNames();
        IClKernel GetKernel(string name);

        string[] GetDeviceNames();
        IClDevice GetDevice(string name);

        string[] GetMemoryObjectNames();
        IClMemoryObject AddMemoryObject(ClMemoryObjectConfiguration moConf);
        IClMemoryObject GetMemoryObject(string name);
        void RemoveMemoryObject(string name);
        void ClearMemoryObjects();

        void WaitForQueueToFinish(IClDevice device);
    }
}
