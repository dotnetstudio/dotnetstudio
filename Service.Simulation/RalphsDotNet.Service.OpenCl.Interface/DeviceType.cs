﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RalphsDotNet.Service.OpenCl.Interface
{
    public enum DeviceType : int
    {
        Default = 0,
        All = 1,
        Gpu = 2,
        Cpu = 3,
        Accelerator = 4
    }
}
