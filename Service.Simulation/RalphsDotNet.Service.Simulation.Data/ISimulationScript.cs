﻿using RalphsDotNet.Data;
using RalphsDotNet.Data.Scripting;

namespace RalphsDotNet.Service.Simulation.Data
{
    [Abstract]
    public interface ISimulationScript : IScript
    {
        [Index]
        bool IsEnabled { get; set; }

        [Index]
        bool IsValid { get; set; }
    }

    public interface ISimulationSystemScript : ISimulationScript
    {
    }

    public interface ISimulationSystemScriptRepository : IDataRepository<ISimulationSystemScript>
    {
        ISimulationSystemScript Create(string name, ISimulationDescription simulationDescription, string code);
    }

    public interface ISimulationRawScript : ISimulationScript
    {
    }

    public interface ISimulationRawScriptRepository : IDataRepository<ISimulationRawScript>
    {
        ISimulationRawScript Create(string name, ISimulationDescription simulationDescription, string code);
    }
}