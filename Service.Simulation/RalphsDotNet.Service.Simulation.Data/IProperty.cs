﻿using RalphsDotNet.Data;
using RalphsDotNet.Data.Base;

namespace RalphsDotNet.Service.Simulation.Model
{
    [Abstract]
    public interface IProperty : IDataObject, IHasName
    {
    }

    public interface IPropertyRepository<T> : IDataRepository<T> where T : IProperty
    {
        T Create(string name);
    }

    [Abstract]
    public interface IValueProperty<T> : IProperty
    {
        T Value { get; set; }
    }

    public interface INumberProperty : IValueProperty<double>
    {
    }

    public interface INumberPropertyRepository : IPropertyRepository<INumberProperty>
    {
        INumberProperty Create(string name, ISystem system);
        INumberProperty Create(string name, ISystem system, double value);
    }

    public interface INumberArrayProperty : IValueProperty<double[]>
    {
    }

    public interface INumberArrayPropertyRepository : IPropertyRepository<INumberArrayProperty>
    {
        INumberArrayProperty Create(string name, ISystem system);
        INumberArrayProperty Create(string name, ISystem system, double[] value);
    }
}