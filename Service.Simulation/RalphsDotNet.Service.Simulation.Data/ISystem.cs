﻿using RalphsDotNet.Data;
using RalphsDotNet.Service.Simulation.Data;
using System.Collections.Generic;

namespace RalphsDotNet.Service.Simulation.Model
{
    public interface ISystem : IDataObject
    {
        [Index(Name = "SystemNumber")]
        string Name { get; set; }

        [Index(Name = "SystemNumber")]
        int Number { get; set; }

        [CascadeAll, ForeignKey("System"), EagerLoading]
        IList<INumberArrayProperty> NumberArrayProperties { get; set; }

        [CascadeAll, ForeignKey("System"), EagerLoading]
        IList<INumberProperty> NumberProperties { get; set; }
    }

    public interface ISystemRepository : IDataRepository<ISystem>
    {
        ISystem Create(string name, IIteration iteration, int number);
    }
}
