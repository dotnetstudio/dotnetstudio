﻿using System.Collections.Generic;
using RalphsDotNet.Data;
using RalphsDotNet.Data.Base;

namespace RalphsDotNet.Service.Simulation.Data
{
    [DeleteOrphans]
    public interface ISystemDescription : IDataObject, IHasUniqueName
    {
        ISimulationDescription SimulationDescription { get; set; }

        int Amount { get; set; }

        [CascadeAll, ForeignKey("SystemDescription"), OrderBy(Property = "ExecutionOrder")]
        IList<ISystemPrepareInteractionScriptMapping> SystemPrepareInteractionScripts { get; set; }

        [CascadeAll, ForeignKey("SystemDescription"), OrderBy(Property = "ExecutionOrder")]
        IList<ISystemInteractionScriptMapping> SystemInteractionScripts { get; set; }

        [CascadeAll, ForeignKey("SystemDescription"), OrderBy(Property = "ExecutionOrder")]
        IList<ISystemModifyScriptMapping> SystemModifyScripts { get; set; }

        [CascadeAll, ForeignKey("SystemDescription"), OrderBy(Property = "Name")]
        IList<INumberPropertyDescription> NumberPropertyDescriptions { get; set; }
    }

    public interface ISystemDescriptionRepository : IDataRepository<ISystemDescription>
    {
        ISystemDescription Create(ISimulationDescription simulationDescription, string name);

        ISystemDescription Create(ISimulationDescription simulationDescription, string name, int amount);
    }
}