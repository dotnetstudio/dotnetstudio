﻿using System.Collections.Generic;
using RalphsDotNet.Data;
using RalphsDotNet.Data.Scripting;

namespace RalphsDotNet.Service.Simulation.Data
{
    public interface ISimulationDescription : IDataObject, IHasAttachments
    {
        int SampleRate { get; set; }
        int ObstacleMiliseconds { get; set; }
        bool SupportDouble { get; set; }

        [CascadeAll, ForeignKey("SimulationDescription"), OrderBy(Property = "Name")]
        IList<ISystemDescription> SystemDescriptions { get; set; }

        [CascadeAll, OrderBy(Property = "Name")]
        IList<ISimulationSystemScript> SystemScripts { get; set; }

        [CascadeAll, OrderBy(Property = "Name")]
        IList<ISimulationRawScript> RawScripts { get; set; }
    }

    public interface ISimulationDescriptionRepository : IDataRepository<ISimulationDescription>
    {
        ISimulationDescription Create();
    }
}