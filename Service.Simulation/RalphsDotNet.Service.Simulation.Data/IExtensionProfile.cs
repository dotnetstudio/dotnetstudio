﻿using System;
using RalphsDotNet.Data;

namespace RalphsDotNet.Service.Simulation.Data
{
    public interface IExtensionProfile : IDataObject, IHasAttachments
    {
        [Unique(Name = "ProfileName")]
        Guid ProfileId { get; set; }

        [Unique(Name = "ProfileName")]
        string Name { get; set; }
    }

    public interface IExtensionProfileRepository : IDataRepository<IExtensionProfile>
    {
        IExtensionProfile Create(Guid profileId, string name);
    }
}