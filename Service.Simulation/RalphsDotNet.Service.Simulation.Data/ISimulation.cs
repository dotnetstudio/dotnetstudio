﻿using System;
using RalphsDotNet.Data;

namespace RalphsDotNet.Service.Simulation.Data
{
    public interface ISimulation : IDataObject
    {
        [Index]
        DateTime StarTime { get; set; }

        DateTime EndTime { get; set; }

        ulong IterationCount { get; set; }
    }

    public interface ISimulationRepository : IDataRepository<ISimulation>
    {
        ISimulation Create(DateTime startTime);
    }
}