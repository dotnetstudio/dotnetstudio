﻿using RalphsDotNet.Data;
using RalphsDotNet.Data.Base;

namespace RalphsDotNet.Service.Simulation.Data
{
    [Abstract, DeleteOrphans]
    public interface ISystemScriptMapping : IDataObject, IHasName
    {
        [Index]
        ISystemDescription SystemDescription { get; set; }

        [Index]
        int ExecutionOrder { get; set; }
    }

    public interface ISystemPrepareInteractionScriptMapping : ISystemScriptMapping
    {
        [Text]
        ISimulationSystemScript Script { get; set; }
    }

    public interface ISystemPrepareInteractionScriptMappingRepository :
        IDataRepository<ISystemPrepareInteractionScriptMapping>
    {
        ISystemPrepareInteractionScriptMapping Create(string name, ISystemDescription system);
    }

    public interface ISystemModifyScriptMapping : ISystemScriptMapping
    {
        [Text]
        ISimulationSystemScript CollectScript { get; set; }

        [Text]
        ISimulationSystemScript ModifyScript { get; set; }
    }

    public interface ISystemModifyScriptMappingRepository :
        IDataRepository<ISystemModifyScriptMapping>
    {
        ISystemModifyScriptMapping Create(string name, ISystemDescription system);
    }

    public interface ISystemInteractionScriptMapping : ISystemScriptMapping
    {
        [Index]
        ISystemDescription InteractionSystemDescription { get; set; }

        [Text]
        ISimulationSystemScript Script { get; set; }
    }

    public interface ISystemInteractionScriptMappingRepository : IDataRepository<ISystemInteractionScriptMapping>
    {
        ISystemInteractionScriptMapping Create(string name, ISystemDescription system);
    }
}