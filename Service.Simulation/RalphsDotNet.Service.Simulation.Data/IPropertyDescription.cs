﻿using RalphsDotNet.Data;

namespace RalphsDotNet.Service.Simulation.Data
{
    [Abstract, DeleteOrphans]
    public interface IPropertyDescription : IDataObject
    {
// System or Group
        [Index]
        [Unique(Name = "SystemName")]
        ISystemDescription SystemDescription { get; set; }

        [Index]
        [Unique(Name = "SystemName")]
        string Name { get; set; }
    }

    public interface IPropertyDescriptionRepository<T> : IDataRepository<T> where T : IPropertyDescription
    {
        T Create(string name, ISystemDescription system);
    }

    [Abstract]
    public interface IValuePropertyDescription<T> : IPropertyDescription
    {
    }

    public interface INumberPropertyDescription : IValuePropertyDescription<double>
    {
        bool IsArray { get; set; }
        string Distribution { get; set; }
    }

    public interface INumberPropertyDescriptionRepository : IPropertyDescriptionRepository<INumberPropertyDescription>
    {
        INumberPropertyDescription Create(string name, ISystemDescription system, string distribution);
    }
}