﻿using System;
using System.Collections.Generic;
using System.Data;
using RalphsDotNet.Data;
using RalphsDotNet.Service.Simulation.Data;

namespace RalphsDotNet.Service.Simulation.Model
{
    public interface IIteration : IDataObject
    {
        Guid SimulationId { get; set; }

        ulong Number { get; set; }

        [CascadeAll, ForeignKey("Iteration"), EagerLoading]
        IList<ISystem> Systems { get; set; } 
    }

    public interface IIterationRepository : IDataRepository<IIteration>
    {
        IIteration Create(ISimulation simulation, ulong number);
    }
}
