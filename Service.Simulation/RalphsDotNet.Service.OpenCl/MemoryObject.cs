﻿using Cloo;
using RalphsDotNet.Service.OpenCl.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace RalphsDotNet.Service.OpenCl
{
    public abstract class MemoryObject : IMemoryObject
    {
        protected static ComputeImageChannelType GetChannelType(Type type)
        {
            if (type == typeof(byte))
                return ComputeImageChannelType.UnsignedInt8;
            else if (type == typeof(sbyte))
                return ComputeImageChannelType.SignedInt8;
            else if (type == typeof(ushort))
                return ComputeImageChannelType.UnsignedInt16;
            else if (type == typeof(short))
                return ComputeImageChannelType.SignedInt16;
            else if (type == typeof(uint))
                return ComputeImageChannelType.UnsignedInt32;
            else if (type == typeof(int))
                return ComputeImageChannelType.SignedInt32;
            else if (type == typeof(float))
                return ComputeImageChannelType.Float;
            else throw (new ArgumentException(string.Format("{0} is not supported", type.Name)));
        }

        protected static ComputeImageChannelOrder GetChannelOrder(int channelAmount)
        {
            switch (channelAmount)
            {
                case 1:
                    return ComputeImageChannelOrder.R;
                case 2:
                    return ComputeImageChannelOrder.RG;
                case 3:
                    return ComputeImageChannelOrder.Rgb;
                case 4:
                    return ComputeImageChannelOrder.Rgba;
                default:
                    throw (new ArgumentException("an image can have only between 1 and 4 channels (red, green, blue, alpha)"));
            }
        }

        internal ComputeMemory ComputeMemory { get; set; }

        public abstract MemoryType Type { get; }
        public string Name { get; private set; }
        
        private Context context;
        internal Context ContextInternal
        {
            get { return this.context; }
            set
            {
                if (this.context != value)
                {
                    if (this.context != null)
                    {
                        this.context.RemoveMemoryObjectInternal(this);
                        this.ComputeMemory.Dispose();
                    }

                    this.context = value;

                    if (this.context != null)
                    {
                        this.context.AddMemoryObjectInternal(this);
                        this.CreateComputeMemory();
                    }
                }
            }
        }

        public IContext Context { get { return this.ContextInternal; } }

        protected MemoryObject(string name)
        {
            this.Name = name;
        }

        public abstract void CreateComputeMemory();

        public abstract void Dispose();
    }
    
    public abstract class MemoryObject<T> : MemoryObject where T : struct
    {
        public MemoryObject(string name) : base(name) { }

        public abstract void UploadData(T[] data, IDevice device, bool blocking);

        public abstract T[] DownloadData(IDevice device, bool blocking);
    }

    public class Buffer<T> : MemoryObject<T> where T : struct
    {
        private long size;

        internal ComputeBuffer<T> ComputeBuffer { get { return this.ComputeMemory as ComputeBuffer<T>; } }

        public override MemoryType Type { get { return MemoryType.Buffer; } }

        public Buffer(string name, long size)
            : base(name)
        {
            this.size = size;
        }

        public override void UploadData(T[] data, IDevice device, bool blocking)
        {
            if (data == null)
                data = new T[this.size];

            this.ContextInternal.CommandQueues.Where(cq => cq.Device == device).First().ComputeCommandQueue.WriteToBuffer<T>(data, this.ComputeBuffer, blocking, null);
        }

        public override T[] DownloadData(IDevice device, bool blocking)
        {
            T[] data = new T[this.size];
            this.ContextInternal.CommandQueues.Where(cq => cq.Device == device).First().ComputeCommandQueue.ReadFromBuffer<T>(this.ComputeBuffer, ref data, blocking, null);

            return data;
        }

        public override void CreateComputeMemory()
        {
            this.ComputeMemory = new ComputeBuffer<T>(this.ContextInternal.ComputeContext, ComputeMemoryFlags.None, size);
        }

        public override void Dispose()
        {
            this.ComputeBuffer.Dispose();
        }
    }

    public abstract class Image2D<T> : MemoryObject<T> where T : struct
    {
        protected int width, height, channelAmount;

        internal ComputeImage2D ComputeImage2D { get { return this.ComputeMemory as ComputeImage2D; } }

        public override MemoryType Type { get { return MemoryType.Image; } }

        protected Image2D(string name, int channelAmount, int width, int height)
            : base(name)
        {
            this.width = width; this.height = height; this.channelAmount = channelAmount;
        }

        public override void CreateComputeMemory()
        {
            this.ComputeMemory = new ComputeImage2D(this.ContextInternal.ComputeContext, ComputeMemoryFlags.ReadWrite, new ComputeImageFormat(GetChannelOrder(channelAmount), GetChannelType(typeof(T))), width, height, 0, IntPtr.Zero);
        }

        public override void Dispose()
        {
            this.ComputeImage2D.Dispose();
        }
    }

    public abstract class Image3D<T> : MemoryObject<T> where T : struct
    {
        protected int width, height, depth, channelAmount;

        internal ComputeImage3D ComputeImage3D { get { return this.ComputeMemory as ComputeImage3D; } }

        public override MemoryType Type { get { return MemoryType.Image; } }
        
        protected Image3D(string name, int channelAmount, int width, int height, int depth)
            : base(name)
        {
            this.width = width; this.height = height; this.depth = depth; this.channelAmount = channelAmount;
        }

        public override void CreateComputeMemory()
        {
            this.ComputeMemory = new ComputeImage3D(this.ContextInternal.ComputeContext, ComputeMemoryFlags.ReadWrite, new ComputeImageFormat(GetChannelOrder(channelAmount), GetChannelType(typeof(T))), width, height, depth, 0, 0, IntPtr.Zero);
        }

        public override void Dispose()
        {
            this.ComputeImage3D.Dispose();
        }
    }

    public class Image2DUnsignedInt8 : Image2D<byte>
    {
        public Image2DUnsignedInt8(string name, int channelAmount, int width, int height)
            : base(name, channelAmount, width, height) { }

        public override void UploadData(byte[] data, IDevice device, bool blocking)
        {
            if (data == null)
                data = new byte[this.width * this.height];

            IntPtr ptr = Marshal.AllocHGlobal(data.Length * sizeof(byte));
            Marshal.Copy(data, 0, ptr, data.Length);
            this.ContextInternal.CommandQueues.Where(cq => cq.Device == device).First().ComputeCommandQueue.WriteToImage(ptr, this.ComputeImage2D, true, null);
            Marshal.FreeHGlobal(ptr);
        }

        public override byte[] DownloadData(IDevice device, bool blocking)
        {
            byte[] data = new byte[this.width * this.height];

            IntPtr ptr = Marshal.AllocHGlobal(data.Length * sizeof(byte));
            this.ContextInternal.CommandQueues.Where(cq => cq.Device == device).First().ComputeCommandQueue.ReadFromImage(this.ComputeImage2D, ptr, true, null);
            Marshal.Copy(ptr, data, 0, data.Length);
            Marshal.FreeHGlobal(ptr);

            return data;
        }
    }

    public class Image2DSignedInt8 : Image2D<sbyte>
    {
        public Image2DSignedInt8(string name, int channelAmount, int width, int height)
            : base(name, channelAmount, width, height) { }

        public override void UploadData(sbyte[] data, IDevice device, bool blocking)
        {
            if (data == null)
                data = new sbyte[this.width * this.height];

            IntPtr ptr = Marshal.AllocHGlobal(data.Length * sizeof(sbyte));
            Marshal.Copy(data.Cast<byte>().ToArray(), 0, ptr, data.Length);
            this.ContextInternal.CommandQueues.Where(cq => cq.Device == device).First().ComputeCommandQueue.WriteToImage(ptr, this.ComputeImage2D, true, null);
            Marshal.FreeHGlobal(ptr);
        }

        public override sbyte[] DownloadData(IDevice device, bool blocking)
        {
            byte[] data = new byte[this.width * this.height];

            IntPtr ptr = Marshal.AllocHGlobal(data.Length * sizeof(sbyte));
            this.ContextInternal.CommandQueues.Where(cq => cq.Device == device).First().ComputeCommandQueue.ReadFromImage(this.ComputeImage2D, ptr, true, null);
            Marshal.Copy(ptr, data, 0, data.Length);
            Marshal.FreeHGlobal(ptr);

            return data.Cast<sbyte>().ToArray();
        }
    }

    public class Image2DUnsignedInt16 : Image2D<ushort>
    {
        public Image2DUnsignedInt16(string name, int channelAmount, int width, int height)
            : base(name, channelAmount, width, height) { }

        public override void UploadData(ushort[] data, IDevice device, bool blocking)
        {
            if (data == null)
                data = new ushort[this.width * this.height];

            IntPtr ptr = Marshal.AllocHGlobal(data.Length * sizeof(ushort));
            Marshal.Copy(data.Cast<short>().ToArray(), 0, ptr, data.Length);
            this.ContextInternal.CommandQueues.Where(cq => cq.Device == device).First().ComputeCommandQueue.WriteToImage(ptr, this.ComputeImage2D, true, null);
            Marshal.FreeHGlobal(ptr);
        }

        public override ushort[] DownloadData(IDevice device, bool blocking)
        {
            short[] data = new short[this.width * this.height];

            IntPtr ptr = Marshal.AllocHGlobal(data.Length * sizeof(ushort));
            this.ContextInternal.CommandQueues.Where(cq => cq.Device == device).First().ComputeCommandQueue.ReadFromImage(this.ComputeImage2D, ptr, true, null);
            Marshal.Copy(ptr, data, 0, data.Length);
            Marshal.FreeHGlobal(ptr);

            return data.Cast<ushort>().ToArray();
        }
    }

    public class Image2DSignedInt16 : Image2D<short>
    {
        public Image2DSignedInt16(string name, int channelAmount, int width, int height)
            : base(name, channelAmount, width, height) { }

        public override void UploadData(short[] data, IDevice device, bool blocking)
        {
            if (data == null)
                data = new short[this.width * this.height];

            IntPtr ptr = Marshal.AllocHGlobal(data.Length * sizeof(short));
            Marshal.Copy(data, 0, ptr, data.Length);
            this.ContextInternal.CommandQueues.Where(cq => cq.Device == device).First().ComputeCommandQueue.WriteToImage(ptr, this.ComputeImage2D, true, null);
            Marshal.FreeHGlobal(ptr);
        }

        public override short[] DownloadData(IDevice device, bool blocking)
        {
            short[] data = new short[this.width * this.height];

            IntPtr ptr = Marshal.AllocHGlobal(data.Length * sizeof(short));
            this.ContextInternal.CommandQueues.Where(cq => cq.Device == device).First().ComputeCommandQueue.ReadFromImage(this.ComputeImage2D, ptr, true, null);
            Marshal.Copy(ptr, data, 0, data.Length);
            Marshal.FreeHGlobal(ptr);

            return data;
        }
    }

    public class Image2DUnsignedInt32 : Image2D<uint>
    {
        public Image2DUnsignedInt32(string name, int channelAmount, int width, int height)
            : base(name, channelAmount, width, height) { }

        public override void UploadData(uint[] data, IDevice device, bool blocking)
        {
            if (data == null)
                data = new uint[this.width * this.height];

            IntPtr ptr = Marshal.AllocHGlobal(data.Length * sizeof(uint));
            Marshal.Copy(data.Cast<int>().ToArray(), 0, ptr, data.Length);
            this.ContextInternal.CommandQueues.Where(cq => cq.Device == device).First().ComputeCommandQueue.WriteToImage(ptr, this.ComputeImage2D, true, null);
            Marshal.FreeHGlobal(ptr);
        }

        public override uint[] DownloadData(IDevice device, bool blocking)
        {
            int[] data = new int[this.width * this.height];

            IntPtr ptr = Marshal.AllocHGlobal(data.Length * sizeof(uint));
            this.ContextInternal.CommandQueues.Where(cq => cq.Device == device).First().ComputeCommandQueue.ReadFromImage(this.ComputeImage2D, ptr, true, null);
            Marshal.Copy(ptr, data, 0, data.Length);
            Marshal.FreeHGlobal(ptr);

            return data.Cast<uint>().ToArray();
        }
    }

    public class Image2DSignedInt32 : Image2D<int>
    {
        public Image2DSignedInt32(string name, int channelAmount, int width, int height)
            : base(name, channelAmount, width, height) { }

        public override void UploadData(int[] data, IDevice device, bool blocking)
        {
            if (data == null)
                data = new int[this.width * this.height];

            IntPtr ptr = Marshal.AllocHGlobal(data.Length * sizeof(int));
            Marshal.Copy(data, 0, ptr, data.Length);
            this.ContextInternal.CommandQueues.Where(cq => cq.Device == device).First().ComputeCommandQueue.WriteToImage(ptr, this.ComputeImage2D, true, null);
            Marshal.FreeHGlobal(ptr);
        }

        public override int[] DownloadData(IDevice device, bool blocking)
        {
            int[] data = new int[this.width * this.height];

            IntPtr ptr = Marshal.AllocHGlobal(data.Length * sizeof(int));
            this.ContextInternal.CommandQueues.Where(cq => cq.Device == device).First().ComputeCommandQueue.ReadFromImage(this.ComputeImage2D, ptr, true, null);
            Marshal.Copy(ptr, data, 0, data.Length);
            Marshal.FreeHGlobal(ptr);

            return data;
        }
    }

    public class Image2DFloat : Image2D<float>
    {
        public Image2DFloat(string name, int channelAmount, int width, int height)
            : base(name, channelAmount, width, height) { }

        public override void UploadData(float[] data, IDevice device, bool blocking)
        {
            if (data == null)
                data = new float[this.width * this.height];

            IntPtr ptr = Marshal.AllocHGlobal(data.Length * sizeof(float));
            Marshal.Copy(data, 0, ptr, data.Length);
            this.ContextInternal.CommandQueues.Where(cq => cq.Device == device).First().ComputeCommandQueue.WriteToImage(ptr, this.ComputeImage2D, true, null);
            Marshal.FreeHGlobal(ptr);
        }

        public override float[] DownloadData(IDevice device, bool blocking)
        {
            float[] data = new float[this.width * this.height];

            IntPtr ptr = Marshal.AllocHGlobal(data.Length * sizeof(float));
            this.ContextInternal.CommandQueues.Where(cq => cq.Device == device).First().ComputeCommandQueue.ReadFromImage(this.ComputeImage2D, ptr, true, null);
            Marshal.Copy(ptr, data, 0, data.Length);
            Marshal.FreeHGlobal(ptr);

            return data;
        }
    }

    public class Image3DUnsignedInt8 : Image3D<byte>
    {
        public Image3DUnsignedInt8(string name, int channelAmount, int width, int height, int depth)
            : base(name, channelAmount, width, height, depth) { }

        public override void UploadData(byte[] data, IDevice device, bool blocking)
        {
            if (data == null)
                data = new byte[this.width * this.height * this.depth];

            IntPtr ptr = Marshal.AllocHGlobal(data.Length * sizeof(byte));
            Marshal.Copy(data, 0, ptr, data.Length);
            this.ContextInternal.CommandQueues.Where(cq => cq.Device == device).First().ComputeCommandQueue.WriteToImage(ptr, this.ComputeImage3D, true, null);
            Marshal.FreeHGlobal(ptr);
        }

        public override byte[] DownloadData(IDevice device, bool blocking)
        {
            byte[] data = new byte[this.width * this.height * this.depth];

            IntPtr ptr = Marshal.AllocHGlobal(data.Length * sizeof(byte));
            this.ContextInternal.CommandQueues.Where(cq => cq.Device == device).First().ComputeCommandQueue.ReadFromImage(this.ComputeImage3D, ptr, true, null);
            Marshal.Copy(ptr, data, 0, data.Length);
            Marshal.FreeHGlobal(ptr);

            return data;
        }
    }

    public class Image3DSignedInt8 : Image3D<sbyte>
    {
        public Image3DSignedInt8(string name, int channelAmount, int width, int height, int depth)
            : base(name, channelAmount, width, height, depth) { }

        public override void UploadData(sbyte[] data, IDevice device, bool blocking)
        {
            if (data == null)
                data = new sbyte[this.width * this.height * this.depth];

            IntPtr ptr = Marshal.AllocHGlobal(data.Length * sizeof(sbyte));
            Marshal.Copy(data.Cast<byte>().ToArray(), 0, ptr, data.Length);
            this.ContextInternal.CommandQueues.Where(cq => cq.Device == device).First().ComputeCommandQueue.WriteToImage(ptr, this.ComputeImage3D, true, null);
            Marshal.FreeHGlobal(ptr);
        }

        public override sbyte[] DownloadData(IDevice device, bool blocking)
        {
            byte[] data = new byte[this.width * this.height * this.depth];

            IntPtr ptr = Marshal.AllocHGlobal(data.Length * sizeof(sbyte));
            this.ContextInternal.CommandQueues.Where(cq => cq.Device == device).First().ComputeCommandQueue.ReadFromImage(this.ComputeImage3D, ptr, true, null);
            Marshal.Copy(ptr, data, 0, data.Length);
            Marshal.FreeHGlobal(ptr);

            return data.Cast<sbyte>().ToArray();
        }
    }

    public class Image3DUnsignedInt16 : Image3D<ushort>
    {
        public Image3DUnsignedInt16(string name, int channelAmount, int width, int height, int depth)
            : base(name, channelAmount, width, height, depth) { }

        public override void UploadData(ushort[] data, IDevice device, bool blocking)
        {
            if (data == null)
                data = new ushort[this.width * this.height * this.depth];

            IntPtr ptr = Marshal.AllocHGlobal(data.Length * sizeof(ushort));
            Marshal.Copy(data.Cast<short>().ToArray(), 0, ptr, data.Length);
            this.ContextInternal.CommandQueues.Where(cq => cq.Device == device).First().ComputeCommandQueue.WriteToImage(ptr, this.ComputeImage3D, true, null);
            Marshal.FreeHGlobal(ptr);
        }

        public override ushort[] DownloadData(IDevice device, bool blocking)
        {
            short[] data = new short[this.width * this.height * this.depth];

            IntPtr ptr = Marshal.AllocHGlobal(data.Length * sizeof(ushort));
            this.ContextInternal.CommandQueues.Where(cq => cq.Device == device).First().ComputeCommandQueue.ReadFromImage(this.ComputeImage3D, ptr, true, null);
            Marshal.Copy(ptr, data, 0, data.Length);
            Marshal.FreeHGlobal(ptr);

            return data.Cast<ushort>().ToArray();
        }
    }

    public class Image3DSignedInt16 : Image3D<short>
    {
        public Image3DSignedInt16(string name, int channelAmount, int width, int height, int depth)
            : base(name, channelAmount, width, height, depth) { }

        public override void UploadData(short[] data, IDevice device, bool blocking)
        {
            if (data == null)
                data = new short[this.width * this.height * this.depth];

            IntPtr ptr = Marshal.AllocHGlobal(data.Length * sizeof(short));
            Marshal.Copy(data, 0, ptr, data.Length);
            this.ContextInternal.CommandQueues.Where(cq => cq.Device == device).First().ComputeCommandQueue.WriteToImage(ptr, this.ComputeImage3D, true, null);
            Marshal.FreeHGlobal(ptr);
        }

        public override short[] DownloadData(IDevice device, bool blocking)
        {
            short[] data = new short[this.width * this.height * this.depth];

            IntPtr ptr = Marshal.AllocHGlobal(data.Length * sizeof(short));
            this.ContextInternal.CommandQueues.Where(cq => cq.Device == device).First().ComputeCommandQueue.ReadFromImage(this.ComputeImage3D, ptr, true, null);
            Marshal.Copy(ptr, data, 0, data.Length);
            Marshal.FreeHGlobal(ptr);

            return data;
        }
    }

    public class Image3DUnsignedInt32 : Image3D<uint>
    {
        public Image3DUnsignedInt32(string name, int channelAmount, int width, int height, int depth)
            : base(name, channelAmount, width, height, depth) { }

        public override void UploadData(uint[] data, IDevice device, bool blocking)
        {
            if (data == null)
                data = new uint[this.width * this.height * this.depth];

            IntPtr ptr = Marshal.AllocHGlobal(data.Length * sizeof(uint));
            Marshal.Copy(data.Cast<int>().ToArray(), 0, ptr, data.Length);
            this.ContextInternal.CommandQueues.Where(cq => cq.Device == device).First().ComputeCommandQueue.WriteToImage(ptr, this.ComputeImage3D, true, null);
            Marshal.FreeHGlobal(ptr);
        }

        public override uint[] DownloadData(IDevice device, bool blocking)
        {
            int[] data = new int[this.width * this.height * this.depth];

            IntPtr ptr = Marshal.AllocHGlobal(data.Length * sizeof(uint));
            this.ContextInternal.CommandQueues.Where(cq => cq.Device == device).First().ComputeCommandQueue.ReadFromImage(this.ComputeImage3D, ptr, true, null);
            Marshal.Copy(ptr, data, 0, data.Length);
            Marshal.FreeHGlobal(ptr);

            return data.Cast<uint>().ToArray();
        }
    }

    public class Image3DSignedInt32 : Image3D<int>
    {
        public Image3DSignedInt32(string name, int channelAmount, int width, int height, int depth)
            : base(name, channelAmount, width, height, depth) { }

        public override void UploadData(int[] data, IDevice device, bool blocking)
        {
            if (data == null)
                data = new int[this.width * this.height * this.depth];

            IntPtr ptr = Marshal.AllocHGlobal(data.Length * sizeof(int));
            Marshal.Copy(data, 0, ptr, data.Length);
            this.ContextInternal.CommandQueues.Where(cq => cq.Device == device).First().ComputeCommandQueue.WriteToImage(ptr, this.ComputeImage3D, true, null);
            Marshal.FreeHGlobal(ptr);
        }

        public override int[] DownloadData(IDevice device, bool blocking)
        {
            int[] data = new int[this.width * this.height * this.depth];

            IntPtr ptr = Marshal.AllocHGlobal(data.Length * sizeof(int));
            this.ContextInternal.CommandQueues.Where(cq => cq.Device == device).First().ComputeCommandQueue.ReadFromImage(this.ComputeImage3D, ptr, true, null);
            Marshal.Copy(ptr, data, 0, data.Length);
            Marshal.FreeHGlobal(ptr);

            return data;
        }
    }

    public class Image3DFloat : Image3D<float>
    {
        public Image3DFloat(string name, int channelAmount, int width, int height, int depth)
            : base(name, channelAmount, width, height, depth) { }

        public override void UploadData(float[] data, IDevice device, bool blocking)
        {
            if (data == null)
                data = new float[this.width * this.height * this.depth];

            IntPtr ptr = Marshal.AllocHGlobal(data.Length * sizeof(float));
            Marshal.Copy(data, 0, ptr, data.Length);
            this.ContextInternal.CommandQueues.Where(cq => cq.Device == device).First().ComputeCommandQueue.WriteToImage(ptr, this.ComputeImage3D, true, null);
            Marshal.FreeHGlobal(ptr);
        }

        public override float[] DownloadData(IDevice device, bool blocking)
        {
            float[] data = new float[this.width * this.height * this.depth];

            IntPtr ptr = Marshal.AllocHGlobal(data.Length * sizeof(float));
            this.ContextInternal.CommandQueues.Where(cq => cq.Device == device).First().ComputeCommandQueue.ReadFromImage(this.ComputeImage3D, ptr, true, null);
            Marshal.Copy(ptr, data, 0, data.Length);
            Marshal.FreeHGlobal(ptr);

            return data;
        }
    }
}
