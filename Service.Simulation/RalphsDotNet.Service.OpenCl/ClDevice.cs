﻿using System.Linq;
using Cloo;
using RalphsDotNet.Service.OpenCl.Interface;
using System;

namespace RalphsDotNet.Service.OpenCl
{
    public class ClDevice : IClDevice
    {
        internal ComputeDevice ComputeDevice { get; private set; }

        public string Name { get { return this.ComputeDevice.Name; } }
        public string Vendor { get { return this.ComputeDevice.Vendor; } }

        public ClDevice(ComputeDevice computeDevice)
        {
            this.ComputeDevice = computeDevice;
        }

        public string[] GetProperties()
        {
            return this.ComputeDevice.GetType().GetProperties().Select(p => p.Name).ToArray();
        }

        public object GetProperty(string propertyName)
        {
            var pi = this.ComputeDevice.GetType().GetProperty(propertyName);
            if (pi == null)
                throw (new ArgumentException(string.Format("device property named {0} not found", pi.Name)));
            return pi.GetValue(this.ComputeDevice, null);
        }

        public void Dispose()
        {
            
        }
    }
}
