﻿using Cloo;
using RalphsDotNet.Service.OpenCl.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RalphsDotNet.Service.OpenCl
{
    internal class CommandQueue : IDisposable
    {
        internal Context Context { get; private set; }
        internal IDevice Device { get; private set; }
        internal ComputeCommandQueue ComputeCommandQueue { get; private set; }

        internal CommandQueue(Context context, IDevice device)
        {
            this.Context = context;
            this.Device = device;
            this.ComputeCommandQueue = new ComputeCommandQueue(context.ComputeContext, ((Device)device).ComputeDevice, ComputeCommandQueueFlags.None);
        }

        public void Dispose()
        {
            this.ComputeCommandQueue.Dispose();
        }
    }
}
