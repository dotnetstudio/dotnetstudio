﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using Cloo;
using RalphsDotNet.Service.OpenCl.Interface;

namespace RalphsDotNet.Service.OpenCl
{
    public class ClKernel : IClKernel
    {
        internal ClKernel(ClContext context, ComputeKernel computeKernel)
        {
            this.ContextInternal = context;
            this.ComputeKernel = computeKernel;
            this.Arguments = new ObservableDictionary<int, IClMemoryObject>();
            this.Arguments.CollectionChanged += this.Arguments_CollectionChanged;
        }

        internal ComputeKernel ComputeKernel { get; set; }

        internal ObservableDictionary<int, IClMemoryObject> Arguments { get; private set; }

        internal ClContext ContextInternal { get; private set; }

        public IClContext Context
        {
            get { return this.ContextInternal; }
        }

        public string Name
        {
            get { return this.ComputeKernel.FunctionName; }
        }

        public int[] GetArgumentIndices()
        {
            return this.Arguments.Keys.ToArray();
        }

        public void AddArgument(int index, IClMemoryObject obj)
        {
            this.Arguments.Add(index, obj);
        }

        public void RemoveArgument(int index)
        {
            this.Arguments.Remove(index);
        }

        public void ClearArguments()
        {
            foreach (var argIndex in this.GetArgumentIndices())
            {
                this.RemoveArgument(argIndex);
            }
        }

        public void ExecuteSingle(IClDevice device, bool blocking)
        {
            var queue = this.ContextInternal.CommandQueues.First(cq => cq.Device == device);
            queue.ComputeCommandQueue.ExecuteTask(this.ComputeKernel, null);
            if (blocking) queue.ComputeCommandQueue.Finish();
        }

        public void Execute(IClDevice device, long[] globalWorkSize, bool blocking)
        {
            this.Execute(device, null, globalWorkSize, null, blocking);
        }

        public void Execute(IClDevice device, long[] globalWorkOffset, long[] globalWorkSize, long[] localWorkSize,
            bool blocking)
        {
            var queue = this.ContextInternal.CommandQueues.First(cq => cq.Device == device);
            queue.ComputeCommandQueue.Execute(this.ComputeKernel, globalWorkOffset, globalWorkSize, localWorkSize,
                null);
            if (blocking) queue.ComputeCommandQueue.Finish();
        }

        public void Dispose()
        {
            this.ComputeKernel.Dispose();
        }

        private void Arguments_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.OldItems != null)
                throw (new NotSupportedException("assigned arguments cannot be removed anymore, only replaced"));

            foreach (KeyValuePair<int, IClMemoryObject> item in e.NewItems)
            {
                this.ComputeKernel.SetMemoryArgument(item.Key, ((ClMemoryObject) item.Value).ComputeMemory);
            }
        }
    }
}