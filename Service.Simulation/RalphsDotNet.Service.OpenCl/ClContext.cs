﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Cloo;
using RalphsDotNet.Service.OpenCl.Interface;

namespace RalphsDotNet.Service.OpenCl
{
    public class ClContext : IClContext
    {
        internal const ComputeDeviceTypes DefaultDeviceType = ComputeDeviceTypes.Cpu;

        private readonly List<IClMemoryObject> _memoryObjects = new List<IClMemoryObject>();

        private string _error = string.Empty;

        internal ClContext(OpenClService service, IEnumerable<string> sources, string platformSearchPattern,
            string[] requiredExtensions,
            ClDeviceType deviceType)
        {
            this.Service = service;

            var clooDeviceType =
                TranslateDeviceType(deviceType == ClDeviceType.GpuPreferred ? ClDeviceType.Gpu : deviceType);
            this.ComputePlatform = GetPlatform(platformSearchPattern, requiredExtensions, clooDeviceType);

            if (this.ComputePlatform == null && deviceType == ClDeviceType.GpuPreferred)
            {
                clooDeviceType =
                    TranslateDeviceType(deviceType == ClDeviceType.GpuPreferred ? ClDeviceType.All : deviceType);
                this.ComputePlatform = GetPlatform(platformSearchPattern, requiredExtensions, clooDeviceType);
            }

            if (this.ComputePlatform == null)
                throw (new ArgumentException("no matching opencl platform for given arguments found"));

            this.Devices =
                this.ComputePlatform.Devices.Where(d => MatchDeviceType(d, clooDeviceType, requiredExtensions))
                    .Select(cd => new ClDevice(cd))
                    .Cast<IClDevice>()
                    .ToArray();

            this.ComputeContext =
                new ComputeContext(clooDeviceType != ComputeDeviceTypes.Default ? clooDeviceType : DefaultDeviceType,
                    new ComputeContextPropertyList(this.ComputePlatform), this.Notifier, IntPtr.Zero);

            this.CommandQueues = this.Devices.Select(d => new ClCommandQueue(this, d)).ToArray();

            this.InitializeKernels(sources);
        }

        internal ClContext(OpenClService service, ClContextConfiguration configuration)
        {
            this.Service = service;

            var clooDeviceType = TranslateDeviceType(configuration.DeviceType);
            this.ComputePlatform = GetPlatform(configuration.PlatformSearchPattern, configuration.RequiredExtensions,
                clooDeviceType);

            if (this.ComputePlatform == null)
                throw (new ArgumentException("no matching opencl platform for given arguments found"));

            this.Devices =
                this.ComputePlatform.Devices.Where(
                    d => MatchDeviceType(d, clooDeviceType, configuration.RequiredExtensions))
                    .Select(cd => new ClDevice(cd))
                    .Cast<IClDevice>()
                    .ToArray();

            this.ComputeContext =
                new ComputeContext(clooDeviceType != ComputeDeviceTypes.Default ? clooDeviceType : DefaultDeviceType,
                    new ComputeContextPropertyList(this.ComputePlatform), this.Notifier, IntPtr.Zero);

            this.CommandQueues = this.Devices.Select(d => new ClCommandQueue(this, d)).ToArray();

            this.InitializeKernels(configuration.SourceFiles.Select(File.ReadAllText));

            foreach (var moConf in configuration.MemoryObjects)
            {
                this.AddMemoryObject(moConf);
            }

            foreach (var kConf in configuration.Kernels)
            {
                var kernel = this.Kernels.FirstOrDefault(k => k.Name == kConf.Name);
                if (kernel != null)
                {
                    var idx = 0;
                    foreach (var argument in kConf.Arguments)
                    {
                        var mo = this.MemoryObjects.FirstOrDefault(m => m.Name == argument);
                        if (mo != null)
                        {
                            kernel.AddArgument(idx++, mo);
                        }
                        else
                        {
                            throw (new ArgumentException(
                                string.Format(
                                    "configured argument <{0}> for kernel <{1}> not found in memory object configuration",
                                    argument, kConf.Name)));
                        }
                    }
                }
                else
                {
                    throw (new ArgumentException(string.Format("configured kernel <{0}> not found in sources",
                        kConf.Name)));
                }
            }
        }

        internal OpenClService Service { get; private set; }
        internal ComputePlatform ComputePlatform { get; private set; }
        internal ComputeContext ComputeContext { get; private set; }
        internal ClCommandQueue[] CommandQueues { get; private set; }

        internal IClDevice[] Devices { get; private set; }

        internal IClKernel[] Kernels { get; private set; }

        internal IClMemoryObject[] MemoryObjects
        {
            get { return this._memoryObjects.ToArray(); }
        }

        public string Name
        {
            get { return this.ComputePlatform.Name; }
        }

        public string[] GetKernelNames()
        {
            return this.Kernels.Select(k => k.Name).ToArray();
        }

        public IClKernel GetKernel(string name)
        {
            return this.Kernels.FirstOrDefault(k => k.Name == name);
        }

        public string[] GetDeviceNames()
        {
            return this.Devices.Select(d => d.Name).ToArray();
        }

        public IClDevice GetDevice(string name)
        {
            return this.Devices.FirstOrDefault(d => d.Name == name);
        }

        public string[] GetMemoryObjectNames()
        {
            return this.MemoryObjects.Select(m => m.Name).ToArray();
        }

        public IClMemoryObject AddMemoryObject(ClMemoryObjectConfiguration moConf)
        {
            try
            {
                ClMemoryObject mo = null;

                if (moConf is ClBufferConfiguration)
                {
                    var bufConf = moConf as ClBufferConfiguration;
                    var bufferType = typeof (ClBuffer<>).MakeGenericType(bufConf.DataType);
                    mo = Activator.CreateInstance(bufferType, bufConf.Name, bufConf.Size) as ClMemoryObject;
                    mo.ContextInternal = this;
                }
                else if (moConf is ClImage2DConfiguration)
                {
                    var i2DConf = moConf as ClImage2DConfiguration;
                    var i2DType = this.Resolve2DImageType(i2DConf.DataType);

                    mo =
                        Activator.CreateInstance(i2DType, i2DConf.Name, i2DConf.ChannelAmount, i2DConf.Width,
                            i2DConf.Height)
                            as ClMemoryObject;
                    mo.ContextInternal = this;
                }
                else if (moConf is ClImage3DConfiguration)
                {
                    var i3DConf = moConf as ClImage3DConfiguration;
                    var i3DType = this.Resolve3DImageType(i3DConf.DataType);
                    mo =
                        Activator.CreateInstance(i3DType, i3DConf.Name, i3DConf.ChannelAmount, i3DConf.Width,
                            i3DConf.Height,
                            i3DConf.Depth) as ClMemoryObject;
                    mo.ContextInternal = this;
                }

                return mo;
            }
            catch (Exception ex)
            {
                var error = this._error;
                this._error = string.Empty;
                throw new OpenClException(error, ex);
            }
        }

        public IClMemoryObject GetMemoryObject(string name)
        {
            return this.MemoryObjects.FirstOrDefault(m => m.Name == name);
        }

        public void RemoveMemoryObject(string name)
        {
            try
            {
                var mo = this.GetMemoryObject(name);
                ((ClMemoryObject) mo).ContextInternal = null;
                ((ClMemoryObject) mo).Dispose();
            }
            catch (Exception ex)
            {
                var error = this._error;
                this._error = string.Empty;
                throw new OpenClException(error, ex);
            }
        }

        public void ClearMemoryObjects()
        {
            foreach (var moName in this.GetMemoryObjectNames())
            {
                this.RemoveMemoryObject(moName);
            }
        }

        public void Dispose()
        {
            if (this.Service != null)
                this.Service.RemoveContext(this);

            this._memoryObjects.ForEach(mo => mo.Dispose());
            this.CommandQueues.ToList().ForEach(q => q.Dispose());
            this.Kernels.ToList().ForEach(k => k.Dispose());
            this.ComputeContext.Dispose();
        }

        public void WaitForQueueToFinish(IClDevice device)
        {
            try
            {
                var queue = this.CommandQueues.First(cq => cq.Device == device);
                queue.ComputeCommandQueue.Finish();
            }
            catch (Exception ex)
            {
                var error = this._error;
                this._error = string.Empty;
                throw new OpenClException(error, ex);
            }
        }

        private void Notifier(string errorInfo, IntPtr clDataPtr, IntPtr clDataSize, IntPtr userDataPtr)
        {
            this._error += string.Concat(errorInfo, Environment.NewLine, Environment.NewLine);
        }

        private Type Resolve2DImageType(Type dataType)
        {
            Type imageType;

            if (dataType == typeof (byte)) imageType = typeof (ClImage2DUnsignedInt8);
            else if (dataType == typeof (sbyte)) imageType = typeof (ClImage2DSignedInt8);
            else if (dataType == typeof (ushort)) imageType = typeof (ClImage2DUnsignedInt16);
            else if (dataType == typeof (short)) imageType = typeof (ClImage2DSignedInt16);
            else if (dataType == typeof (uint)) imageType = typeof (ClImage2DUnsignedInt32);
            else if (dataType == typeof (int)) imageType = typeof (ClImage2DSignedInt32);
            else if (dataType == typeof (float)) imageType = typeof (ClImage2DFloat);
            else if (dataType == typeof (double)) imageType = typeof (ClImage2DDouble);
            else throw new ApplicationException(string.Format("datatype \"{0}\" not supported for 2D image", dataType));

            return imageType;
        }

        private Type Resolve3DImageType(Type dataType)
        {
            Type imageType;

            if (dataType == typeof (byte)) imageType = typeof (ClImage3DUnsignedInt8);
            else if (dataType == typeof (sbyte)) imageType = typeof (ClImage3DSignedInt8);
            else if (dataType == typeof (ushort)) imageType = typeof (ClImage3DUnsignedInt16);
            else if (dataType == typeof (short)) imageType = typeof (ClImage3DSignedInt16);
            else if (dataType == typeof (uint)) imageType = typeof (ClImage3DUnsignedInt32);
            else if (dataType == typeof (int)) imageType = typeof (ClImage3DSignedInt32);
            else if (dataType == typeof (float)) imageType = typeof (ClImage3DFloat);
            else if (dataType == typeof (double)) imageType = typeof (ClImage3DDouble);
            else throw new ApplicationException(string.Format("datatype \"{0}\" not supported for 3D image", dataType));

            return imageType;
        }

        private static ComputePlatform GetPlatform(string platformSearchPattern, string[] requiredExtensions,
            ComputeDeviceTypes deviceType)
        {
            if (!string.IsNullOrEmpty(platformSearchPattern))
            {
                foreach (var platformSearchWord in platformSearchPattern.Split(';'))
                {
                    var fcp = ComputePlatform.Platforms.FirstOrDefault(
                        p =>
                            (platformSearchWord == null || p.Name.ToLower().Contains(platformSearchWord.ToLower())) &&
                            p.Devices.Any(d => MatchDeviceType(d, deviceType, requiredExtensions)));

                    if (fcp != null)
                        return fcp;
                }
            }

            return
                ComputePlatform.Platforms.FirstOrDefault(
                    p => p.Devices.Any(d => MatchDeviceType(d, deviceType, requiredExtensions)));
        }

        private static ComputeDeviceTypes TranslateDeviceType(ClDeviceType deviceType)
        {
            ComputeDeviceTypes clooDeviceType;
            switch (deviceType)
            {
                case ClDeviceType.All:
                    clooDeviceType = ComputeDeviceTypes.All;
                    break;
                case ClDeviceType.Gpu:
                    clooDeviceType = ComputeDeviceTypes.Gpu;
                    break;
                case ClDeviceType.Cpu:
                    clooDeviceType = ComputeDeviceTypes.Cpu;
                    break;
                case ClDeviceType.Accelerator:
                    clooDeviceType = ComputeDeviceTypes.Accelerator;
                    break;
                default:
                    clooDeviceType = ComputeDeviceTypes.Default;
                    break;
            }

            return clooDeviceType;
        }

        private static bool MatchDeviceType(ComputeDevice device, ComputeDeviceTypes clooDeviceType,
            string[] requiredExtensions)
        {
            return (requiredExtensions == null || requiredExtensions.All(e => device.Extensions.Contains(e))) &&
                   (clooDeviceType == ComputeDeviceTypes.All
                    || (clooDeviceType == ComputeDeviceTypes.Default && device.Type == DefaultDeviceType)
                    || device.Type == clooDeviceType);
        }

        private void InitializeKernels(IEnumerable<string> sources)
        {
            var kernels = new List<ComputeKernel>();
            foreach (var program in sources.Select(source => new ComputeProgram(this.ComputeContext, source)))
            {
                try
                {
#if DEBUG
                    if (Directory.Exists("C:\\Temp"))
                        File.WriteAllText("C:\\Temp\\Debug.cl", program.Source.First());
                    var platform = this.ComputePlatform.Name;
                    program.Build(this.Devices.Select(d => ((ClDevice) d).ComputeDevice).ToArray(),
                        platform.Contains("Intel") ? "-g -s \"C:\\Temp\\Debug.cl\"" : string.Empty, null,
                        IntPtr.Zero);
#else
                    program.Build(this.Devices.Select(d => ((ClDevice) d).ComputeDevice).ToArray(), string.Empty, null,
                        IntPtr.Zero);
#endif
                }
                catch (Exception)
                {
                    var message = string.Concat("opencl build error(s):", Environment.NewLine, Environment.NewLine);
                    message = this.Devices.Aggregate(message,
                        (current, dev) =>
                            current +
                            string.Concat("error on device <", dev.Name, ">", Environment.NewLine,
                                "---------------------------", program.GetBuildLog(((ClDevice) dev).ComputeDevice),
                                "---------------------------", Environment.NewLine, Environment.NewLine));

                    throw (new ArgumentException(message));
                }

                kernels.AddRange(program.CreateAllKernels());
            }

            this.Kernels = kernels.Select(k => new ClKernel(this, k)).Cast<IClKernel>().ToArray();
        }

        internal void AddMemoryObjectInternal(IClMemoryObject mo)
        {
            this._memoryObjects.Add(mo);
        }

        internal void RemoveMemoryObjectInternal(IClMemoryObject mo)
        {
            this._memoryObjects.Remove(mo);
        }
    }
}