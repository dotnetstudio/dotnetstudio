﻿using Cloo;
using RalphsDotNet.Service.OpenCl.Interface;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace RalphsDotNet.Service.OpenCl
{
    public class Context : IContext
    {
        internal const ComputeDeviceTypes DefaultDeviceType = ComputeDeviceTypes.Cpu;

        private static ComputePlatform GetPlatform(string platformSearchWord, ComputeDeviceTypes deviceType)
        {
            return ComputePlatform.Platforms
                .Where(p => (platformSearchWord != null ? p.Name.ToLower().Contains(platformSearchWord.ToLower()) : true) && p.Devices.Where(d => MatchDeviceType(d, deviceType)).Any())
                .FirstOrDefault();
        }

        private static ComputeDeviceTypes TranslateDeviceType(DeviceType deviceType)
        {
            ComputeDeviceTypes clooDeviceType = ComputeDeviceTypes.Default;
            switch (deviceType)
            {
                case DeviceType.All:
                    clooDeviceType = ComputeDeviceTypes.All;
                    break;
                case DeviceType.Gpu:
                    clooDeviceType = ComputeDeviceTypes.Gpu;
                    break;
                case DeviceType.Cpu:
                    clooDeviceType = ComputeDeviceTypes.Cpu;
                    break;
                case DeviceType.Accelerator:
                    clooDeviceType = ComputeDeviceTypes.Accelerator;
                    break;
                default:
                    clooDeviceType = ComputeDeviceTypes.Default;
                    break;
            }

            return clooDeviceType;
        }

        private static bool MatchDeviceType(ComputeDevice device, ComputeDeviceTypes clooDeviceType)
        {
            return clooDeviceType == ComputeDeviceTypes.All
                || (clooDeviceType == ComputeDeviceTypes.Default && device.Type == DefaultDeviceType)
                || device.Type == clooDeviceType;
        }

        private List<IMemoryObject> memoryObjects = new List<IMemoryObject>();

        public IMemoryObject[] MemoryObjects { get { return this.memoryObjects.ToArray(); } }

        internal OpenClService Service { get; private set; }
        internal ComputePlatform ComputePlatform { get; private set; }
        public IDevice[] Devices { get; private set; }
        internal ComputeContext ComputeContext { get; private set; }
        internal CommandQueue[] CommandQueues { get; private set; }

        public IKernel[] Kernels { get; private set; }

        internal Context(OpenClService service, IEnumerable<string> sources, string platformSearchWord, DeviceType deviceType)
        {
            this.Service = service;

            ComputeDeviceTypes clooDeviceType = TranslateDeviceType(deviceType);
            this.ComputePlatform = GetPlatform(platformSearchWord, clooDeviceType);

            if (this.ComputePlatform == null)
                throw (new ArgumentException("no matching opencl platform for given arguments found"));

            this.Devices = this.ComputePlatform.Devices.Where(d => MatchDeviceType(d, clooDeviceType)).Select(cd => new Device(cd)).ToArray();

            this.ComputeContext = new ComputeContext(clooDeviceType != ComputeDeviceTypes.Default ? clooDeviceType : DefaultDeviceType, new ComputeContextPropertyList(this.ComputePlatform), null, IntPtr.Zero);

            this.CommandQueues = this.Devices.Select(d => new CommandQueue(this, d)).ToArray();

            this.InitializeKernels(sources);
        }

        internal Context(OpenClService service, ContextConfiguration configuration)
        {
            this.Service = service;

            ComputeDeviceTypes clooDeviceType = TranslateDeviceType(configuration.DeviceType);
            this.ComputePlatform = GetPlatform(configuration.PlatformSearchWord, clooDeviceType);

            if (this.ComputePlatform == null)
                throw (new ArgumentException("no matching opencl platform for given arguments found"));

            this.Devices = this.ComputePlatform.Devices.Where(d => MatchDeviceType(d, clooDeviceType)).Select(cd => new Device(cd)).ToArray();

            this.ComputeContext = new ComputeContext(clooDeviceType != ComputeDeviceTypes.Default ? clooDeviceType : DefaultDeviceType, new ComputeContextPropertyList(this.ComputePlatform), null, IntPtr.Zero);

            this.CommandQueues = this.Devices.Select(d => new CommandQueue(this, d)).ToArray();

            this.InitializeKernels(configuration.SourceFiles.Select(sf => File.ReadAllText(sf)));

            foreach (MemoryObjectConfiguration moConf in configuration.MemoryObjects)
            {
                if (moConf is BufferConfiguration)
                {
                    BufferConfiguration bufConf = moConf as BufferConfiguration;
                    Type bufferType = typeof(Buffer<>).MakeGenericType(bufConf.DataType);
                    this.AddMemoryObject(Activator.CreateInstance(bufferType, bufConf.Name, bufConf.Size) as MemoryObject);
                }
                else if (moConf is Image2DConfiguration)
                {
                    Image2DConfiguration i2dConf = moConf as Image2DConfiguration;
                    Type i2dType = typeof(Image2D<>).MakeGenericType(i2dConf.DataType);
                    this.AddMemoryObject(Activator.CreateInstance(i2dType, i2dConf.Name, i2dConf.ChannelAmount, i2dConf.Width, i2dConf.Height) as MemoryObject);
                }
                else if (moConf is Image3DConfiguration)
                {
                    Image3DConfiguration i3dConf = moConf as Image3DConfiguration;
                    Type i3dType = typeof(Image3D<>).MakeGenericType(i3dConf.DataType);
                    this.AddMemoryObject(Activator.CreateInstance(i3dType, i3dConf.Name, i3dConf.ChannelAmount, i3dConf.Width, i3dConf.Height, i3dConf.Depth) as MemoryObject);
                }
            }

            foreach (KernelConfiguration kConf in configuration.Kernels)
            {
                IKernel kernel = this.Kernels.Where(k => k.Name == kConf.Name).FirstOrDefault();
                if (kernel != null)
                {
                    int idx = 0;
                    foreach (string argument in kConf.Arguments)
                    {
                        IMemoryObject mo = this.MemoryObjects.Where(m => m.Name == argument).FirstOrDefault();
                        if (mo != null)
                        {
                            kernel.Arguments.Add(idx++, mo);
                        }
                        else
                        {
                            throw (new ArgumentException(string.Format("configured argument <{0}> for kernel <{1}> not found in memory object configuration", argument, kConf.Name)));
                        }
                    }
                }
                else
                {
                    throw (new ArgumentException(string.Format("configured kernel <{0}> not found in sources", kConf.Name)));
                }
            }
        }

        private void InitializeKernels(IEnumerable<string> sources)
        {
            List<ComputeKernel> kernels = new List<ComputeKernel>();
            foreach (string source in sources)
            {
                ComputeProgram program = new ComputeProgram(this.ComputeContext, source);
                try
                {
                    program.Build(this.Devices.Select(d => ((Device)d).ComputeDevice).ToArray(), string.Empty, null, IntPtr.Zero);
                }
                catch (Exception ex)
                {
                    string message = string.Concat("opencl build error(s):", Environment.NewLine, Environment.NewLine);
                    foreach (IDevice dev in this.Devices)
                    {
                        message += string.Concat("error on device <", dev.Name, ">", Environment.NewLine, "---------------------------", program.GetBuildLog(((Device)dev).ComputeDevice), "---------------------------", Environment.NewLine, Environment.NewLine);
                    }

                    throw (new ArgumentException(message));
                }

                kernels.AddRange(program.CreateAllKernels());
            }

            this.Kernels = kernels.Select(k => new Kernel(this, k)).ToArray();
        }

        public void AddMemoryObject(IMemoryObject mo)
        {
            ((MemoryObject)mo).ContextInternal = this;
        }

        internal void AddMemoryObjectInternal(IMemoryObject mo)
        {
            this.memoryObjects.Add(mo);
        }

        public void RemoveMemoryObject(IMemoryObject mo)
        {
            ((MemoryObject)mo).ContextInternal = null;
        }

        internal void RemoveMemoryObjectInternal(IMemoryObject mo)
        {
            this.memoryObjects.Remove(mo);
        }

        public void Dispose()
        {
            if (this.Service != null)
                this.Service.RemoveContext(this);

            this.memoryObjects.ForEach(mo => mo.Dispose());
            this.CommandQueues.ToList().ForEach(q => q.Dispose());
            this.Kernels.ToList().ForEach(k => k.Dispose());
            this.ComputeContext.Dispose();
        }
    }
}
