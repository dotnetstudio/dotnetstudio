﻿using Cloo;
using RalphsDotNet.Service.OpenCl.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RalphsDotNet.Service.OpenCl
{
    public class Device : IDevice
    {
        internal ComputeDevice ComputeDevice { get; private set; }

        public string Name { get { return this.ComputeDevice.Name; } }
        public string Vendor { get { return this.ComputeDevice.Vendor; } }

        public Device(ComputeDevice computeDevice)
        {
            this.ComputeDevice = computeDevice;
        }
    }
}
