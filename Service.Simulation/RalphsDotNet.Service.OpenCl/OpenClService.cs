﻿using System;
using System.Collections.Generic;
using RalphsDotNet.Service.OpenCl.Interface;

namespace RalphsDotNet.Service.OpenCl
{
    public class OpenClService : IOpenClService
    {
        private readonly List<IClContext> _contexts = new List<IClContext>();

        public OpenClService(IOpenClService proxy, ServiceConfiguration configuration)
        {
            this.ServiceConfiguration = configuration;
        }

        public string Id
        {
            get { return this.ServiceConfiguration.Id; }
        }

        public ServiceConfiguration ServiceConfiguration { get; set; }

        public IClContext CreateContext(IEnumerable<string> sources)
        {
            return this.CreateContext(sources, ClDeviceType.All);
        }

        public IClContext CreateContext(IEnumerable<string> sources, ClDeviceType deviceType)
        {
            return this.CreateContext(sources, null, null, deviceType);
        }

        public IClContext CreateContext(IEnumerable<string> sources, string platformSearchWord, string[] requiredExtensions, ClDeviceType deviceType)
        {
                IClContext context = new ClContext(this, sources, platformSearchWord, requiredExtensions, deviceType);

                this._contexts.Add(context);

                return context;
        }

        public IClContext CreateContext(ClContextConfiguration configuration)
        {
            IClContext context = new ClContext(this, configuration);

            this._contexts.Add(context);

            return context;
        }

        public void Dispose()
        {
            foreach (var context in this._contexts.ToArray())
            {
                context.Dispose();
            }
        }

        internal void RemoveContext(IClContext context)
        {
            this._contexts.Remove(context);
        }
    }

    public class OpenClException : Exception
    {
        public OpenClException(string message) : base(message)
        {
        }

        public OpenClException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}