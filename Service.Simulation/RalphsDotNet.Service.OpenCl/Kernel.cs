﻿using Cloo;
using RalphsDotNet.Service.OpenCl.Interface;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Text;

namespace RalphsDotNet.Service.OpenCl
{
    public class Kernel : IKernel
    {
        internal ComputeKernel ComputeKernel { get; set; }

        public ObservableDictionary<int, IMemoryObject> Arguments { get; private set; }

        public IContext Context { get { return this.ContextInternal; } }

        internal Context ContextInternal { get; private set; }

        public string Name { get { return this.ComputeKernel.FunctionName; } }

        internal Kernel(Context context, ComputeKernel computeKernel)
        {
            this.ContextInternal = context;
            this.ComputeKernel = computeKernel;
            this.Arguments = new ObservableDictionary<int, IMemoryObject>();
            this.Arguments.CollectionChanged += Arguments_CollectionChanged;
        }

        void Arguments_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.OldItems != null)
                throw (new NotSupportedException("assigned arguments cannot be removed anymore, only replaced"));

            foreach (KeyValuePair<int, IMemoryObject> item in e.NewItems)
            {
                this.ComputeKernel.SetMemoryArgument(item.Key, ((MemoryObject)item.Value).ComputeMemory);
            }
        }

        public void ExecuteSingle(IDevice device)
        {
            this.ContextInternal.CommandQueues.Where(cq => cq.Device == device).First().ComputeCommandQueue.ExecuteTask(this.ComputeKernel, null);
        }

        public void Execute(IDevice device, long[] globalWorkSize)
        {
            this.Execute(device, null, globalWorkSize, null);
        }

        public void Execute(IDevice device, long[] globalWorkOffset, long[] globalWorkSize, long[] localWorkSize)
        {
            this.ContextInternal.CommandQueues.Where(cq => cq.Device == device).First().ComputeCommandQueue.Execute(this.ComputeKernel, globalWorkOffset, globalWorkSize, localWorkSize, null);
        }

        public void Dispose()
        {
            this.ComputeKernel.Dispose();
        }
    }
}
