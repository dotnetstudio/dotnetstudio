﻿using System;
using System.Linq;
using System.Runtime.InteropServices;
using Cloo;
using RalphsDotNet.Service.OpenCl.Interface;

namespace RalphsDotNet.Service.OpenCl
{
    public abstract class ClMemoryObject : IClMemoryObject
    {
        private ClContext _context;

        protected ClMemoryObject(string name)
        {
            this.Name = name;
        }

        internal ComputeMemory ComputeMemory { get; set; }

        internal ClContext ContextInternal
        {
            get { return this._context; }
            set
            {
                if (this._context != value)
                {
                    if (this._context != null)
                    {
                        this._context.RemoveMemoryObjectInternal(this);
                        this.ComputeMemory.Dispose();
                    }

                    this._context = value;

                    if (this._context != null)
                    {
                        this._context.AddMemoryObjectInternal(this);
                        this.CreateComputeMemory();
                    }
                }
            }
        }

        public abstract long Size { get; }

        public abstract MemoryType Type { get; }
        public string Name { get; private set; }

        public IClContext Context
        {
            get { return this.ContextInternal; }
        }

        public abstract void Dispose();

        protected static ComputeImageChannelType GetChannelType(Type type)
        {
            if (type == typeof (byte))
                return ComputeImageChannelType.UnsignedInt8;
            if (type == typeof (sbyte))
                return ComputeImageChannelType.SignedInt8;
            if (type == typeof (ushort))
                return ComputeImageChannelType.UnsignedInt16;
            if (type == typeof (short))
                return ComputeImageChannelType.SignedInt16;
            if (type == typeof (uint))
                return ComputeImageChannelType.UnsignedInt32;
            if (type == typeof (int))
                return ComputeImageChannelType.SignedInt32;
            if (type == typeof (float))
                return ComputeImageChannelType.Float;
            throw (new ArgumentException(string.Format("{0} is not supported", type.Name)));
        }

        protected static ComputeImageChannelOrder GetChannelOrder(int channelAmount)
        {
            switch (channelAmount)
            {
                case 1:
                    return ComputeImageChannelOrder.R;
                case 2:
                    return ComputeImageChannelOrder.RG;
                case 3:
                    return ComputeImageChannelOrder.Rgb;
                case 4:
                    return ComputeImageChannelOrder.Rgba;
                default:
                    throw (new ArgumentException(
                        "an image can have only between 1 and 4 channels (red, green, blue, alpha)"));
            }
        }

        protected abstract void CreateComputeMemory();
    }

    public abstract class ClMemoryObject<T> : ClMemoryObject, IClMemoryObject<T> where T : struct
    {
        protected ClMemoryObject(string name) : base(name)
        {
        }

        public abstract void UploadData(T[] data, IClDevice device, bool blocking);

        public abstract T[] DownloadData(IClDevice device, bool blocking);
    }

    public class ClBuffer<T> : ClMemoryObject<T> where T : struct
    {
        private readonly long _size;

        public ClBuffer(string name, long size)
            : base(name)
        {
            this._size = size;
        }

        public override long Size
        {
            get { return this._size; }
        }

        internal ComputeBuffer<T> ComputeBuffer
        {
            get { return this.ComputeMemory as ComputeBuffer<T>; }
        }

        public override MemoryType Type
        {
            get { return MemoryType.Buffer; }
        }

        public override void UploadData(T[] data, IClDevice device, bool blocking)
        {
            if (data == null)
                data = new T[this._size];

            this.ContextInternal.CommandQueues.First(cq => cq.Device == device)
                .ComputeCommandQueue.WriteToBuffer(data, this.ComputeBuffer, blocking, null);
        }

        public override T[] DownloadData(IClDevice device, bool blocking)
        {
            var data = new T[this._size];
            this.ContextInternal.CommandQueues.First(cq => cq.Device == device)
                .ComputeCommandQueue.ReadFromBuffer(this.ComputeBuffer, ref data, blocking, null);

            return data;
        }

        protected override void CreateComputeMemory()
        {
            this.ComputeMemory = new ComputeBuffer<T>(this.ContextInternal.ComputeContext, ComputeMemoryFlags.None,
                this._size);
        }

        public override void Dispose()
        {
            if (this.ComputeBuffer != null)
                this.ComputeBuffer.Dispose();
        }
    }

    public abstract class ClImage2D<T> : ClMemoryObject<T> where T : struct
    {
        protected int ChannelAmount;
        protected int Height;
        protected int Width;

        protected ClImage2D(string name, int channelAmount, int width, int height)
            : base(name)
        {
            this.Width = width;
            this.Height = height;
            this.ChannelAmount = channelAmount;
        }

        public override long Size
        {
            get { return this.Width*this.Height; }
        }

        internal ComputeImage2D ComputeImage2D
        {
            get { return this.ComputeMemory as ComputeImage2D; }
        }

        public override MemoryType Type
        {
            get { return MemoryType.Image; }
        }

        protected override void CreateComputeMemory()
        {
            this.ComputeMemory = new ComputeImage2D(this.ContextInternal.ComputeContext, ComputeMemoryFlags.ReadWrite,
                new ComputeImageFormat(GetChannelOrder(this.ChannelAmount), GetChannelType(typeof (T))), this.Width,
                this.Height, 0, IntPtr.Zero);
        }

        public override void Dispose()
        {
            this.ComputeImage2D.Dispose();
        }
    }

    public abstract class ClImage3D<T> : ClMemoryObject<T> where T : struct
    {
        protected int ChannelAmount;
        protected int Depth;
        protected int Height;
        protected int Width;

        protected ClImage3D(string name, int channelAmount, int width, int height, int depth)
            : base(name)
        {
            this.Width = width;
            this.Height = height;
            this.Depth = depth;
            this.ChannelAmount = channelAmount;
        }

        public override long Size
        {
            get { return this.Width*this.Height*this.Depth; }
        }

        internal ComputeImage3D ComputeImage3D
        {
            get { return this.ComputeMemory as ComputeImage3D; }
        }

        public override MemoryType Type
        {
            get { return MemoryType.Image; }
        }

        protected override void CreateComputeMemory()
        {
            this.ComputeMemory = new ComputeImage3D(this.ContextInternal.ComputeContext, ComputeMemoryFlags.ReadWrite,
                new ComputeImageFormat(GetChannelOrder(this.ChannelAmount), GetChannelType(typeof (T))), this.Width,
                this.Height, this.Depth, 0, 0, IntPtr.Zero);
        }

        public override void Dispose()
        {
            this.ComputeImage3D.Dispose();
        }
    }

    public class ClImage2DUnsignedInt8 : ClImage2D<byte>
    {
        public ClImage2DUnsignedInt8(string name, int channelAmount, int width, int height)
            : base(name, channelAmount, width, height)
        {
        }

        public override void UploadData(byte[] data, IClDevice device, bool blocking)
        {
            if (data == null)
                data = new byte[this.Width*this.Height];

            var ptr = Marshal.AllocHGlobal(data.Length*sizeof (byte));
            Marshal.Copy(data, 0, ptr, data.Length);
            this.ContextInternal.CommandQueues.First(cq => cq.Device == device)
                .ComputeCommandQueue.WriteToImage(ptr, this.ComputeImage2D, true, null);
            Marshal.FreeHGlobal(ptr);
        }

        public override byte[] DownloadData(IClDevice device, bool blocking)
        {
            var data = new byte[this.Width*this.Height];

            var ptr = Marshal.AllocHGlobal(data.Length*sizeof (byte));
            this.ContextInternal.CommandQueues.First(cq => cq.Device == device)
                .ComputeCommandQueue.ReadFromImage(this.ComputeImage2D, ptr, true, null);
            Marshal.Copy(ptr, data, 0, data.Length);
            Marshal.FreeHGlobal(ptr);

            return data;
        }
    }

    public class ClImage2DSignedInt8 : ClImage2D<sbyte>
    {
        public ClImage2DSignedInt8(string name, int channelAmount, int width, int height)
            : base(name, channelAmount, width, height)
        {
        }

        public override void UploadData(sbyte[] data, IClDevice device, bool blocking)
        {
            if (data == null)
                data = new sbyte[this.Width*this.Height];

            var ptr = Marshal.AllocHGlobal(data.Length*sizeof (sbyte));
            Marshal.Copy(data.Cast<byte>().ToArray(), 0, ptr, data.Length);
            this.ContextInternal.CommandQueues.First(cq => cq.Device == device)
                .ComputeCommandQueue.WriteToImage(ptr, this.ComputeImage2D, true, null);
            Marshal.FreeHGlobal(ptr);
        }

        public override sbyte[] DownloadData(IClDevice device, bool blocking)
        {
            var data = new byte[this.Width*this.Height];

            var ptr = Marshal.AllocHGlobal(data.Length*sizeof (sbyte));
            this.ContextInternal.CommandQueues.First(cq => cq.Device == device)
                .ComputeCommandQueue.ReadFromImage(this.ComputeImage2D, ptr, true, null);
            Marshal.Copy(ptr, data, 0, data.Length);
            Marshal.FreeHGlobal(ptr);

            return data.Cast<sbyte>().ToArray();
        }
    }

    public class ClImage2DUnsignedInt16 : ClImage2D<ushort>
    {
        public ClImage2DUnsignedInt16(string name, int channelAmount, int width, int height)
            : base(name, channelAmount, width, height)
        {
        }

        public override void UploadData(ushort[] data, IClDevice device, bool blocking)
        {
            if (data == null)
                data = new ushort[this.Width*this.Height];

            var ptr = Marshal.AllocHGlobal(data.Length*sizeof (ushort));
            Marshal.Copy(data.Cast<short>().ToArray(), 0, ptr, data.Length);
            this.ContextInternal.CommandQueues.First(cq => cq.Device == device)
                .ComputeCommandQueue.WriteToImage(ptr, this.ComputeImage2D, true, null);
            Marshal.FreeHGlobal(ptr);
        }

        public override ushort[] DownloadData(IClDevice device, bool blocking)
        {
            var data = new short[this.Width*this.Height];

            var ptr = Marshal.AllocHGlobal(data.Length*sizeof (ushort));
            this.ContextInternal.CommandQueues.First(cq => cq.Device == device)
                .ComputeCommandQueue.ReadFromImage(this.ComputeImage2D, ptr, true, null);
            Marshal.Copy(ptr, data, 0, data.Length);
            Marshal.FreeHGlobal(ptr);

            return data.Cast<ushort>().ToArray();
        }
    }

    public class ClImage2DSignedInt16 : ClImage2D<short>
    {
        public ClImage2DSignedInt16(string name, int channelAmount, int width, int height)
            : base(name, channelAmount, width, height)
        {
        }

        public override void UploadData(short[] data, IClDevice device, bool blocking)
        {
            if (data == null)
                data = new short[this.Width*this.Height];

            var ptr = Marshal.AllocHGlobal(data.Length*sizeof (short));
            Marshal.Copy(data, 0, ptr, data.Length);
            this.ContextInternal.CommandQueues.First(cq => cq.Device == device)
                .ComputeCommandQueue.WriteToImage(ptr, this.ComputeImage2D, true, null);
            Marshal.FreeHGlobal(ptr);
        }

        public override short[] DownloadData(IClDevice device, bool blocking)
        {
            var data = new short[this.Width*this.Height];

            var ptr = Marshal.AllocHGlobal(data.Length*sizeof (short));
            this.ContextInternal.CommandQueues.First(cq => cq.Device == device)
                .ComputeCommandQueue.ReadFromImage(this.ComputeImage2D, ptr, true, null);
            Marshal.Copy(ptr, data, 0, data.Length);
            Marshal.FreeHGlobal(ptr);

            return data;
        }
    }

    public class ClImage2DUnsignedInt32 : ClImage2D<uint>
    {
        public ClImage2DUnsignedInt32(string name, int channelAmount, int width, int height)
            : base(name, channelAmount, width, height)
        {
        }

        public override void UploadData(uint[] data, IClDevice device, bool blocking)
        {
            if (data == null)
                data = new uint[this.Width*this.Height];

            var ptr = Marshal.AllocHGlobal(data.Length*sizeof (uint));
            Marshal.Copy(data.Cast<int>().ToArray(), 0, ptr, data.Length);
            this.ContextInternal.CommandQueues.First(cq => cq.Device == device)
                .ComputeCommandQueue.WriteToImage(ptr, this.ComputeImage2D, true, null);
            Marshal.FreeHGlobal(ptr);
        }

        public override uint[] DownloadData(IClDevice device, bool blocking)
        {
            var data = new int[this.Width*this.Height];

            var ptr = Marshal.AllocHGlobal(data.Length*sizeof (uint));
            this.ContextInternal.CommandQueues.First(cq => cq.Device == device)
                .ComputeCommandQueue.ReadFromImage(this.ComputeImage2D, ptr, true, null);
            Marshal.Copy(ptr, data, 0, data.Length);
            Marshal.FreeHGlobal(ptr);

            return data.Cast<uint>().ToArray();
        }
    }

    public class ClImage2DSignedInt32 : ClImage2D<int>
    {
        public ClImage2DSignedInt32(string name, int channelAmount, int width, int height)
            : base(name, channelAmount, width, height)
        {
        }

        public override void UploadData(int[] data, IClDevice device, bool blocking)
        {
            if (data == null)
                data = new int[this.Width*this.Height];

            var ptr = Marshal.AllocHGlobal(data.Length*sizeof (int));
            Marshal.Copy(data, 0, ptr, data.Length);
            this.ContextInternal.CommandQueues.First(cq => cq.Device == device)
                .ComputeCommandQueue.WriteToImage(ptr, this.ComputeImage2D, true, null);
            Marshal.FreeHGlobal(ptr);
        }

        public override int[] DownloadData(IClDevice device, bool blocking)
        {
            var data = new int[this.Width*this.Height];

            var ptr = Marshal.AllocHGlobal(data.Length*sizeof (int));
            this.ContextInternal.CommandQueues.First(cq => cq.Device == device)
                .ComputeCommandQueue.ReadFromImage(this.ComputeImage2D, ptr, true, null);
            Marshal.Copy(ptr, data, 0, data.Length);
            Marshal.FreeHGlobal(ptr);

            return data;
        }
    }

    public class ClImage2DFloat : ClImage2D<float>
    {
        public ClImage2DFloat(string name, int channelAmount, int width, int height)
            : base(name, channelAmount, width, height)
        {
        }

        public override void UploadData(float[] data, IClDevice device, bool blocking)
        {
            if (data == null)
                data = new float[this.Width*this.Height];

            var ptr = Marshal.AllocHGlobal(data.Length*sizeof (float));
            Marshal.Copy(data, 0, ptr, data.Length);
            this.ContextInternal.CommandQueues.First(cq => cq.Device == device)
                .ComputeCommandQueue.WriteToImage(ptr, this.ComputeImage2D, true, null);
            Marshal.FreeHGlobal(ptr);
        }

        public override float[] DownloadData(IClDevice device, bool blocking)
        {
            var data = new float[this.Width*this.Height];

            var ptr = Marshal.AllocHGlobal(data.Length*sizeof (float));
            this.ContextInternal.CommandQueues.First(cq => cq.Device == device)
                .ComputeCommandQueue.ReadFromImage(this.ComputeImage2D, ptr, true, null);
            Marshal.Copy(ptr, data, 0, data.Length);
            Marshal.FreeHGlobal(ptr);

            return data;
        }
    }

    public class ClImage2DDouble : ClImage2D<double>
    {
        public ClImage2DDouble(string name, int channelAmount, int width, int height)
            : base(name, channelAmount, width, height)
        {
        }

        public override void UploadData(double[] data, IClDevice device, bool blocking)
        {
            if (data == null)
                data = new double[this.Width*this.Height];

            var ptr = Marshal.AllocHGlobal(data.Length*sizeof (double));
            Marshal.Copy(data, 0, ptr, data.Length);
            this.ContextInternal.CommandQueues.First(cq => cq.Device == device)
                .ComputeCommandQueue.WriteToImage(ptr, this.ComputeImage2D, true, null);
            Marshal.FreeHGlobal(ptr);
        }

        public override double[] DownloadData(IClDevice device, bool blocking)
        {
            var data = new double[this.Width*this.Height];

            var ptr = Marshal.AllocHGlobal(data.Length*sizeof (double));
            this.ContextInternal.CommandQueues.First(cq => cq.Device == device)
                .ComputeCommandQueue.ReadFromImage(this.ComputeImage2D, ptr, true, null);
            Marshal.Copy(ptr, data, 0, data.Length);
            Marshal.FreeHGlobal(ptr);

            return data;
        }

        protected override void CreateComputeMemory()
        {
            this.ComputeMemory = new ComputeImage2D(this.ContextInternal.ComputeContext, ComputeMemoryFlags.ReadWrite,
                new ComputeImageFormat(GetChannelOrder(this.ChannelAmount*2), GetChannelType(typeof (int))), this.Width,
                this.Height, 0, IntPtr.Zero);
        }
    }

    public class ClImage3DUnsignedInt8 : ClImage3D<byte>
    {
        public ClImage3DUnsignedInt8(string name, int channelAmount, int width, int height, int depth)
            : base(name, channelAmount, width, height, depth)
        {
        }

        public override void UploadData(byte[] data, IClDevice device, bool blocking)
        {
            if (data == null)
                data = new byte[this.Width*this.Height*this.Depth];

            var ptr = Marshal.AllocHGlobal(data.Length*sizeof (byte));
            Marshal.Copy(data, 0, ptr, data.Length);
            this.ContextInternal.CommandQueues.First(cq => cq.Device == device)
                .ComputeCommandQueue.WriteToImage(ptr, this.ComputeImage3D, true, null);
            Marshal.FreeHGlobal(ptr);
        }

        public override byte[] DownloadData(IClDevice device, bool blocking)
        {
            var data = new byte[this.Width*this.Height*this.Depth];

            var ptr = Marshal.AllocHGlobal(data.Length*sizeof (byte));
            this.ContextInternal.CommandQueues.First(cq => cq.Device == device)
                .ComputeCommandQueue.ReadFromImage(this.ComputeImage3D, ptr, true, null);
            Marshal.Copy(ptr, data, 0, data.Length);
            Marshal.FreeHGlobal(ptr);

            return data;
        }
    }

    public class ClImage3DSignedInt8 : ClImage3D<sbyte>
    {
        public ClImage3DSignedInt8(string name, int channelAmount, int width, int height, int depth)
            : base(name, channelAmount, width, height, depth)
        {
        }

        public override void UploadData(sbyte[] data, IClDevice device, bool blocking)
        {
            if (data == null)
                data = new sbyte[this.Width*this.Height*this.Depth];

            var ptr = Marshal.AllocHGlobal(data.Length*sizeof (sbyte));
            Marshal.Copy(data.Cast<byte>().ToArray(), 0, ptr, data.Length);
            this.ContextInternal.CommandQueues.First(cq => cq.Device == device)
                .ComputeCommandQueue.WriteToImage(ptr, this.ComputeImage3D, true, null);
            Marshal.FreeHGlobal(ptr);
        }

        public override sbyte[] DownloadData(IClDevice device, bool blocking)
        {
            var data = new byte[this.Width*this.Height*this.Depth];

            var ptr = Marshal.AllocHGlobal(data.Length*sizeof (sbyte));
            this.ContextInternal.CommandQueues.First(cq => cq.Device == device)
                .ComputeCommandQueue.ReadFromImage(this.ComputeImage3D, ptr, true, null);
            Marshal.Copy(ptr, data, 0, data.Length);
            Marshal.FreeHGlobal(ptr);

            return data.Cast<sbyte>().ToArray();
        }
    }

    public class ClImage3DUnsignedInt16 : ClImage3D<ushort>
    {
        public ClImage3DUnsignedInt16(string name, int channelAmount, int width, int height, int depth)
            : base(name, channelAmount, width, height, depth)
        {
        }

        public override void UploadData(ushort[] data, IClDevice device, bool blocking)
        {
            if (data == null)
                data = new ushort[this.Width*this.Height*this.Depth];

            var ptr = Marshal.AllocHGlobal(data.Length*sizeof (ushort));
            Marshal.Copy(data.Cast<short>().ToArray(), 0, ptr, data.Length);
            this.ContextInternal.CommandQueues.First(cq => cq.Device == device)
                .ComputeCommandQueue.WriteToImage(ptr, this.ComputeImage3D, true, null);
            Marshal.FreeHGlobal(ptr);
        }

        public override ushort[] DownloadData(IClDevice device, bool blocking)
        {
            var data = new short[this.Width*this.Height*this.Depth];

            var ptr = Marshal.AllocHGlobal(data.Length*sizeof (ushort));
            this.ContextInternal.CommandQueues.First(cq => cq.Device == device)
                .ComputeCommandQueue.ReadFromImage(this.ComputeImage3D, ptr, true, null);
            Marshal.Copy(ptr, data, 0, data.Length);
            Marshal.FreeHGlobal(ptr);

            return data.Cast<ushort>().ToArray();
        }
    }

    public class ClImage3DSignedInt16 : ClImage3D<short>
    {
        public ClImage3DSignedInt16(string name, int channelAmount, int width, int height, int depth)
            : base(name, channelAmount, width, height, depth)
        {
        }

        public override void UploadData(short[] data, IClDevice device, bool blocking)
        {
            if (data == null)
                data = new short[this.Width*this.Height*this.Depth];

            var ptr = Marshal.AllocHGlobal(data.Length*sizeof (short));
            Marshal.Copy(data, 0, ptr, data.Length);
            this.ContextInternal.CommandQueues.First(cq => cq.Device == device)
                .ComputeCommandQueue.WriteToImage(ptr, this.ComputeImage3D, true, null);
            Marshal.FreeHGlobal(ptr);
        }

        public override short[] DownloadData(IClDevice device, bool blocking)
        {
            var data = new short[this.Width*this.Height*this.Depth];

            var ptr = Marshal.AllocHGlobal(data.Length*sizeof (short));
            this.ContextInternal.CommandQueues.First(cq => cq.Device == device)
                .ComputeCommandQueue.ReadFromImage(this.ComputeImage3D, ptr, true, null);
            Marshal.Copy(ptr, data, 0, data.Length);
            Marshal.FreeHGlobal(ptr);

            return data;
        }
    }

    public class ClImage3DUnsignedInt32 : ClImage3D<uint>
    {
        public ClImage3DUnsignedInt32(string name, int channelAmount, int width, int height, int depth)
            : base(name, channelAmount, width, height, depth)
        {
        }

        public override void UploadData(uint[] data, IClDevice device, bool blocking)
        {
            if (data == null)
                data = new uint[this.Width*this.Height*this.Depth];

            var ptr = Marshal.AllocHGlobal(data.Length*sizeof (uint));
            Marshal.Copy(data.Cast<int>().ToArray(), 0, ptr, data.Length);
            this.ContextInternal.CommandQueues.First(cq => cq.Device == device)
                .ComputeCommandQueue.WriteToImage(ptr, this.ComputeImage3D, true, null);
            Marshal.FreeHGlobal(ptr);
        }

        public override uint[] DownloadData(IClDevice device, bool blocking)
        {
            var data = new int[this.Width*this.Height*this.Depth];

            var ptr = Marshal.AllocHGlobal(data.Length*sizeof (uint));
            this.ContextInternal.CommandQueues.First(cq => cq.Device == device)
                .ComputeCommandQueue.ReadFromImage(this.ComputeImage3D, ptr, true, null);
            Marshal.Copy(ptr, data, 0, data.Length);
            Marshal.FreeHGlobal(ptr);

            return data.Cast<uint>().ToArray();
        }
    }

    public class ClImage3DSignedInt32 : ClImage3D<int>
    {
        public ClImage3DSignedInt32(string name, int channelAmount, int width, int height, int depth)
            : base(name, channelAmount, width, height, depth)
        {
        }

        public override void UploadData(int[] data, IClDevice device, bool blocking)
        {
            if (data == null)
                data = new int[this.Width*this.Height*this.Depth];

            var ptr = Marshal.AllocHGlobal(data.Length*sizeof (int));
            Marshal.Copy(data, 0, ptr, data.Length);
            this.ContextInternal.CommandQueues.First(cq => cq.Device == device)
                .ComputeCommandQueue.WriteToImage(ptr, this.ComputeImage3D, true, null);
            Marshal.FreeHGlobal(ptr);
        }

        public override int[] DownloadData(IClDevice device, bool blocking)
        {
            var data = new int[this.Width*this.Height*this.Depth];

            var ptr = Marshal.AllocHGlobal(data.Length*sizeof (int));
            this.ContextInternal.CommandQueues.First(cq => cq.Device == device)
                .ComputeCommandQueue.ReadFromImage(this.ComputeImage3D, ptr, true, null);
            Marshal.Copy(ptr, data, 0, data.Length);
            Marshal.FreeHGlobal(ptr);

            return data;
        }
    }

    public class ClImage3DFloat : ClImage3D<float>
    {
        public ClImage3DFloat(string name, int channelAmount, int width, int height, int depth)
            : base(name, channelAmount, width, height, depth)
        {
        }

        public override void UploadData(float[] data, IClDevice device, bool blocking)
        {
            if (data == null)
                data = new float[this.Width*this.Height*this.Depth];

            var ptr = Marshal.AllocHGlobal(data.Length*sizeof (float));
            Marshal.Copy(data, 0, ptr, data.Length);
            this.ContextInternal.CommandQueues.First(cq => cq.Device == device)
                .ComputeCommandQueue.WriteToImage(ptr, this.ComputeImage3D, true, null);
            Marshal.FreeHGlobal(ptr);
        }

        public override float[] DownloadData(IClDevice device, bool blocking)
        {
            var data = new float[this.Width*this.Height*this.Depth];

            var ptr = Marshal.AllocHGlobal(data.Length*sizeof (float));
            this.ContextInternal.CommandQueues.First(cq => cq.Device == device)
                .ComputeCommandQueue.ReadFromImage(this.ComputeImage3D, ptr, true, null);
            Marshal.Copy(ptr, data, 0, data.Length);
            Marshal.FreeHGlobal(ptr);

            return data;
        }
    }

    public class ClImage3DDouble : ClImage3D<double>
    {
        public ClImage3DDouble(string name, int channelAmount, int width, int height, int depth)
            : base(name, channelAmount, width, height, depth)
        {
        }

        public override void UploadData(double[] data, IClDevice device, bool blocking)
        {
            if (data == null)
                data = new double[this.Width*this.Height*this.Depth];

            var ptr = Marshal.AllocHGlobal(data.Length*sizeof (double));
            Marshal.Copy(data, 0, ptr, data.Length);
            this.ContextInternal.CommandQueues.First(cq => cq.Device == device)
                .ComputeCommandQueue.WriteToImage(ptr, this.ComputeImage3D, true, null);
            Marshal.FreeHGlobal(ptr);
        }

        public override double[] DownloadData(IClDevice device, bool blocking)
        {
            var data = new double[this.Width*this.Height*this.Depth];

            var ptr = Marshal.AllocHGlobal(data.Length*sizeof (double));
            this.ContextInternal.CommandQueues.First(cq => cq.Device == device)
                .ComputeCommandQueue.ReadFromImage(this.ComputeImage3D, ptr, true, null);
            Marshal.Copy(ptr, data, 0, data.Length);
            Marshal.FreeHGlobal(ptr);

            return data;
        }

        protected override void CreateComputeMemory()
        {
            this.ComputeMemory = new ComputeImage3D(this.ContextInternal.ComputeContext, ComputeMemoryFlags.ReadWrite,
                new ComputeImageFormat(GetChannelOrder(this.ChannelAmount*2), GetChannelType(typeof (int))), this.Width,
                this.Height, this.Depth, 0, 0, IntPtr.Zero);
        }
    }
}