﻿using Cloo;
using RalphsDotNet.Service.OpenCl.Interface;
using System;

namespace RalphsDotNet.Service.OpenCl
{
    internal class ClCommandQueue : IDisposable
    {
        internal ClContext Context { get; private set; }
        internal IClDevice Device { get; private set; }
        internal ComputeCommandQueue ComputeCommandQueue { get; private set; }

        internal ClCommandQueue(ClContext context, IClDevice device)
        {
            this.Context = context;
            this.Device = device;
            this.ComputeCommandQueue = new ComputeCommandQueue(context.ComputeContext, ((ClDevice)device).ComputeDevice, ComputeCommandQueueFlags.None);
        }

        public void Dispose()
        {
            this.ComputeCommandQueue.Dispose();
        }
    }
}
