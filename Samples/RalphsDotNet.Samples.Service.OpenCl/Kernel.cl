﻿__kernel void sort(__global int* numbers, __global int* sortedNumbers)
{
	int actPos = get_global_id(0);
	int size = get_global_size(0);

	int newPos = actPos;

	for(int i = 0; i < size; i++)
	{
		if(i < actPos && numbers[i] > numbers[actPos])
		{
			newPos--;
		}
		else if(i > actPos && numbers[i] < numbers[actPos])
		{
			newPos++;
		}
	}
	//printf("%i\r\n", numbers[actPos]);
	sortedNumbers[newPos] = numbers[actPos];
}