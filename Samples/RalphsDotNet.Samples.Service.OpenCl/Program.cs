﻿using System;
using System.Linq;
using RalphsDotNet.Service;
using RalphsDotNet.Service.OpenCl;
using RalphsDotNet.Service.OpenCl.Interface;

namespace RalphsDotNet.Samples.Service.OpenCl
{
    internal class Program
    {
        private const int Size = 100000;

        private static readonly OpenClServiceConfiguration Configuration = new OpenClServiceConfiguration
        {
            Id = "OpenCl",
            ServiceAssembly = typeof (OpenClService).Assembly
        };

        private static IOpenClService _service;
        private static IClContext _context;
        private static readonly int[] Numbers = new int[Size];
        private static readonly int[] CpuSortedNumbers = new int[Size];
        private static IClMemoryObject<int> _numbersBuffer;
        private static IClMemoryObject<int> _sortedNumbersBuffer;
        private static IClKernel _sortKernel;

// ReSharper disable once UnusedParameter.Local
        private static void Main(string[] args)
        {
            #region save a context configuration

            //// the context configuration is fully data contract serializable, you can define it in xml
            //ClContextConfiguration contextConf = new ClContextConfiguration();
            //contextConf.DeviceType = ClDeviceType.Gpu;
            //contextConf.SourceFiles = new string[] { "Kernel.cl" };
            //contextConf.MemoryObjects = new ClMemoryObjectConfiguration[]{
            //    new ClBufferConfiguration() { DataType = typeof(int), Name = "numbers", Size = size },
            //    new ClBufferConfiguration() { DataType = typeof(int), Name = "sortedNumbers", Size = size },
            //    new ClBufferConfiguration() { DataType = typeof(int), Name = "size", Size = 1 }
            //};
            //contextConf.Kernels = new ClKernelConfiguration[] { new ClKernelConfiguration() { Name = "sort", Arguments = new string[] { "numbers", "sortedNumbers", "size" } } };

            //Configuration.Save<ClContextConfiguration>(contextConf, "Configuration2");

            #endregion

            #region load an existing context configuration

            // this is a datacontract serialization, take care of the order if you create the xml manualy,
            // the way it should be done is to create a tool for that, maybe I'll do that once
            var contextConf = RalphsDotNet.Service.Configuration.Load<ClContextConfiguration>("Configuration");

            #endregion

            _service = ServiceProvider.GetService<IOpenClService>(Configuration);

            #region way with context configuration

            _context = _service.CreateContext(contextConf);
            _numbersBuffer = _context.GetMemoryObject("numbers") as IClMemoryObject<int>;
            _sortedNumbersBuffer =
                _context.GetMemoryObject("sortedNumbers") as IClMemoryObject<int>;
            _sortKernel = _context.GetKernel("sort");

            #endregion

            #region way without context configuration

            //context = service.CreateContext(new string[] { File.ReadAllText("Kernel.cl") }, ClDeviceType.Gpu);
            //numbersBuffer = context.AddMemoryObject(new ClBufferConfiguration() { DataType = typeof(int), Name = "numbers", Size = size }) as IClMemoryObject<int>;
            //sortedNumbersBuffer = context.AddMemoryObject(new ClBufferConfiguration() { DataType = typeof(int), Name = "sortedNumbers", Size = size }) as IClMemoryObject<int>;
            //sizeBuffer = context.AddMemoryObject(new ClBufferConfiguration() { DataType = typeof(int), Name = "size", Size = 1 }) as IClMemoryObject<int>;

            //sortKernel = context.GetKernel("sort");
            //sortKernel.AddArgument(0, numbersBuffer);
            //sortKernel.AddArgument(1, sortedNumbersBuffer);

            #endregion

            var rnd = new Random();
            for (var i = 0; i < Numbers.Length; i++)
            {
                Numbers[i] = rnd.Next();
            }

            Console.WriteLine("opencl context:\t{0}", _context.Name);

            foreach (var deviceName in _context.GetDeviceNames())
            {
                var device = _context.GetDevice(deviceName);
                Console.WriteLine("\tVendor <{0}> Device <{1}>", device.Vendor, device.Name);
                foreach (var property in device.GetProperties())
                    Console.WriteLine("\t\t{0}: {1}", property, device.GetProperty(property));
            }

            Console.WriteLine();
            Console.WriteLine("sorting {0} random integers", Numbers.Length);
            Console.WriteLine();

            var firstDevice = _context.GetDevice(_context.GetDeviceNames().First());

            DateTime time;
            using (_context)
            {
                time = DateTime.UtcNow;
                Console.WriteLine("uploading data to cl device");
                _numbersBuffer.UploadData(Numbers, firstDevice, true);
                Console.WriteLine("uploaded in {0}", DateTime.UtcNow - time);

                time = DateTime.UtcNow;
                Console.WriteLine("sorting by cl device");
                _sortKernel.Execute(firstDevice, new long[] { Size }, true);
                Console.WriteLine("sorted in {0}", DateTime.UtcNow - time);

                time = DateTime.UtcNow;
                Console.WriteLine("download data from cl device");
                _sortedNumbersBuffer.DownloadData(firstDevice, true);
                Console.WriteLine("downloaded in {0}", DateTime.UtcNow - time);
            }

            Console.WriteLine();

            time = DateTime.UtcNow;
            Console.WriteLine("sorting by cpu");
            for (var i = 0; i < Size; i++)
            {
                var newPos = i;

                for (var j = 0; j < Size; j++)
                {
                    if (j < i && Numbers[j] > Numbers[i])
                        newPos--;
                    else if (j > i && Numbers[j] < Numbers[i])
                        newPos++;
                }

                CpuSortedNumbers[newPos] = Numbers[i];
            }
            Console.WriteLine("sorted in {0}", DateTime.UtcNow - time);

            Console.ReadKey();
        }
    }
}