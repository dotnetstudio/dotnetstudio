﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using RalphsDotNet.Data;
using RalphsDotNet.Data.Base;
using RalphsDotNet.Data.Web;
using RalphsDotNet.Service;
using RalphsDotNet.Service.Data;
using RalphsDotNet.Service.Data.Interface;

namespace RalphsDotNet.Samples.Service.Data
{
    internal class MainClass
    {
        private static IDataService _service;

        public static void Main(string[] args)
        {
            var quit = false;
            while (!quit)
            {
                Console.Clear();

                if (_service != null)
                    Console.Write("data service is running");
                else
                    Console.WriteLine("data service is not running");

                Console.WriteLine();

                if (_service == null)
                    Console.WriteLine("e\tedit configuration");

                if (_service == null)
                    Console.WriteLine("c\tcreate data service");

                if (_service != null)
                    Console.WriteLine("t\ttest database connection");

                if (_service != null)
                    Console.WriteLine("p\ttryout performance");

                if (_service != null)
                    Console.WriteLine("s\tstop data service");

                Console.WriteLine("q\tquit");
                Console.WriteLine();

                var key = Console.ReadKey().KeyChar;

                Console.WriteLine();

                try
                {
                    switch (key)
                    {
                        case 'e':
                            Edit();
                            break;
                        case 'c':
                            Create();
                            break;
                        case 't':
                            Test();
                            break;
                        case 'p':
                            Preformance();
                            break;
                        case 's':
                            Stop();
                            break;
                        case 'q':
                            if (_service != null)
                                Stop();
                            quit = true;
                            break;
                    }
                }
                catch (Exception ex)
                {
                    Console.Clear();
                    Console.WriteLine("FAILED");
                    Console.WriteLine(ex);
                    Console.WriteLine("PRESS ANY KEY TO CONTINUE");
                    Console.ReadKey();
                }
            }
        }

        private static void Edit()
        {
            var quit = false;

            if (!Configuration.Exists<ServicesConfiguration>())
            {
                Configuration.Save(new ServicesConfiguration {Services = new ServiceConfiguration[] {}});
            }

            // check if there is an instance configured named test
            if (ServiceProvider.ServiceConfigurations["test"] == null)
            {
                // add test instance to configuration and fill it with default values
                ServiceProvider.ServiceConfigurations.AddOrReplace(new DataServiceConfiguration
                {
                    Id = "test",
                    ServiceAssemblyName = "RalphsDotNet.Service.Data",
                    DataManagerType = typeof(NHibernateManager),
                    ObjectAssemblies =
                        new[]
                        {
                            Assembly.Load("RalphsDotNet.Data.Base"),
                            Assembly.Load("RalphsDotNet.Data.Web")
                        },
                    RepositoryAssemblies =
                        new[]
                        {
                            Assembly.Load("RalphsDotNet.Data.Base.Repositories"),
                            Assembly.Load("RalphsDotNet.Data.Web.Repositories")
                        },
                    Values = new[]
                    {
                        // first entries:  Sqlite harddisk              (on SSD performs better than Sqlite in-memory)
                        // second entries: MySQL                        (performs really bad here. 1500% slower than Sqlite harddisk on a SSD)
                        // third entries:  MsSql 2012                   (performs well, just 150% slower than Sqlite harddisk on a SSD)
                        new ConfigurationValue
                        {
                            Key = NHibernateManager.ConfigDbconfig,
                            Value = "FluentNHibernate.Cfg.Db.SQLiteConfiguration, FluentNHibernate"
                            //"FluentNHibernate.Cfg.Db.MySQLConfiguration, FluentNHibernate"
                            //"FluentNHibernate.Cfg.Db.MsSqlConfiguration, FluentNHibernate"
                        },
                        new ConfigurationValue
                        {
                            Key = NHibernateManager.ConfigDbvariant,
                            Value = "Standard"
                            //"Standard"
                            //"MsSql2012"
                        },
                        new ConfigurationValue
                        {
                            Key = NHibernateManager.ConfigConnectionstring,
                            Value = "FullUri=file:test.db?cache=shared"
                            //"Server=localhost;Database=test;"
                            //"Server=localhost;Database=test;Trusted_Connection=True;"
                        },
                        /* only needed for batch mysql since there is no included batcher in nhibernate
                         * there are also a few high recommended performance tweaks you should consider for mysql in my.ini configuration file in [mysqld] section
                         * 1. "innodb_buffer_pool_size" default value 16M
                         *    "innodb_additional_mem_pool_size" default value 2M
                         *    "innodb_log_file_size" default value 5M
                         *    "innodb_log_buffer_size" default value 8M
                         *    "max_allowed_packet" default value 1M
                         *    you sould increase them to sizes able to buffer you whole bulks
                         *    (since I've 8GB RAM on a client system, I set them to 512M, 32M, 128M, 192M, 64M, on a serversystem hosting only the db you can go up to 50 - 80% of your RAM)
                         * 2. put "innodb_flush_log_at_trx_commit=2" this will avoid a permanant sync which makes sense if your disk is the bottleneck.
                         *    You can also use 0 but this implies that you have a 100% stable system or you may loose data on a crash.
                         *    but you only should use this if you have a emergency powered system, or you simply don't mind if you loose data in such a case
                         * 3. "sync_binlog=0" you need to consider the same hints as above
                         * with these tweaks I got mysql to almost the same high data insert performance(windows system) as mssql but sqlite + batch + SSD is still the best*/
                        //new ConfigurationValue  
                        //{
                        //    Key = NHibernateManager.ConfigDbBatcherFactory,
                        //    Value = string.Concat(typeof(NHibernate.MySQLBatcher.MySqlClientBatchingBatcherFactory).Namespace, ".", typeof(NHibernate.MySQLBatcher.MySqlClientBatchingBatcherFactory).Name, ", ", typeof(NHibernate.MySQLBatcher.MySqlClientBatchingBatcherFactory).Assembly.GetName().Name)
                        //},
                        new ConfigurationValue
                        {
                            Key = NHibernateManager.ConfigDbBatchSize,
                            Value = 1000.ToString()
                        }
                    }
                });

                Configuration.Save(ServiceProvider.ServiceConfigurations);
            }

            while (!quit)
            {
                Console.Clear();
                ConsoleWriteConfigValue(NHibernateManager.ConfigDbconfig);
                ConsoleWriteConfigValue(NHibernateManager.ConfigDbvariant);
                ConsoleWriteConfigValue(NHibernateManager.ConfigConnectionstring);
                Console.WriteLine();

                Console.WriteLine("1\tedit " + NHibernateManager.ConfigDbconfig);
                Console.WriteLine("2\tedit " + NHibernateManager.ConfigDbvariant);
                Console.WriteLine("3\tedit " + NHibernateManager.ConfigConnectionstring);

                Console.WriteLine("s\tsave");
                Console.WriteLine("b\tback");
                Console.WriteLine();

                var key = Console.ReadKey().KeyChar;

                Console.WriteLine();

                switch (key)
                {
                    case '1':
                        Console.WriteLine("for NHibernate configurer names see NHibernate documentation");
                        ConsoleReadConfigValue(NHibernateManager.ConfigDbconfig);
                        break;
                    case '2':
                        Console.WriteLine("for NHibernate configurer variants see NHibernate documentation");
                        ConsoleReadConfigValue(NHibernateManager.ConfigDbvariant);
                        break;
                    case '3':
                        Console.WriteLine(
                            "for a valid connection string to your database try http://http://www.connectionstrings.com/ or search for");
                        ConsoleReadConfigValue(NHibernateManager.ConfigConnectionstring);
                        break;
                    case 's':
                        Configuration.Save(ServiceProvider.ServiceConfigurations);
                        Console.WriteLine();
                        Console.WriteLine("saved");
                        Thread.Sleep(1000);
                        break;
                    case 'b':
                        quit = true;
                        break;
                }
            }

            if (_service != null)
            {
                Console.WriteLine("reinitializing data service...");
                _service = null;
                ServiceProvider.Initialize();
                Thread.Sleep(2000);
            }
        }

        private static void ConsoleWriteConfigValue(string key)
        {
            Console.WriteLine("{0}\t{1}",
                key,
                ServiceProvider.ServiceConfigurations.Services.Where(c => c.Id == "test")
                    .Cast<DataServiceConfiguration>()
                    .First()
                    .Values.First(v => v.Key == key).Value);
        }

        private static void ConsoleReadConfigValue(string key)
        {
            Console.WriteLine("type new value followed by enter (empty = no change)");
            var newValue = Console.ReadLine().Trim();

            if (!string.IsNullOrEmpty(newValue))
            {
                ServiceProvider.ServiceConfigurations.Services.Where(c => c.Id == "test")
                    .Cast<DataServiceConfiguration>()
                    .First()
                    .Values.First(v => v.Key == key).Value = newValue;
            }

            Console.WriteLine();
            Console.WriteLine("value changed");
            Thread.Sleep(1000);
        }

        private static void Stop()
        {
            ServiceProvider.DisposeService(_service.Id);
            _service = null;

            Console.WriteLine();
            Console.WriteLine("service disposed");
            Thread.Sleep(1000);
        }

        private static void Create()
        {
            _service = ServiceProvider.GetService<IDataService>("test");

            Console.WriteLine();
            Console.WriteLine("service created");
            Thread.Sleep(1000);
        }

        private static void Test()
        {
            var userRepo = _service.GetRepository<IUser, IUserRepository>();
            var langRepo = _service.GetRepository<ILanguage, ILanguageRepository>();

            var lang = langRepo.GetOrCreate("en-US");

            userRepo.SaveOrUpdate(userRepo.Create("test@test.te", "test", "user", "test123", lang));

            Console.WriteLine();
            Console.WriteLine("service tested, search for a language and a user in given database");
            Thread.Sleep(1000);
        }

        private static void Preformance()
        {
            try
            {
                Console.Clear();
                Console.WriteLine("running insert test");
                Console.WriteLine("how much email contents should be generated");
                var count = Convert.ToInt32(Console.ReadLine());

                var beginTime = DateTime.UtcNow;

                // since this whole spins around one service and we want to avoid the performance eaten by marshalling, we invoke the whole thing in the service thread
                // sure this works only when service is running local, otherwise code gets invoked into the thread of the serviceclient)
                // be careful, everything running in the servicethread blocks the service so it cannot be responsive for other threads
                // there is also the possibility given to invoke async, do this only if synchronization means anything to you
                // nontheless you should not run nhibernate operations outside of its session thread, but you have not to worry about as long as you use the repositories or the service itself
                // worrying about thread safety is only needed when you use lazyloading features or simmilar
                ServiceBase.Invoke(_service, () =>
                {
                    var langRepo = _service.GetRepository<ILanguage, ILanguageRepository>();
                    var ecRepo = _service.GetRepository<IEmailContent, IEmailContentRepository>();
                    var attRepo = _service.GetRepository<IAttachment, IAttachmentRepository>();
                    var lang = langRepo.GetOrCreate("en-US");

                    var ecList = new List<IEmailContent>();
                    _service.BeginBatchTransaction();

                    try
                    {
                        for (var i = 0; i < count; i++)
                        {
                            var emailAtt = attRepo.Create("bla file", "bla.txt");
                            emailAtt.FileContent = Encoding.Default.GetBytes("test blabla");

                            ecList.Add(ecRepo.Create("text/plain", lang, "alkdshfkjashdfkahsdflkjhsadlkjfhasd",
                                "halksdfhlkjsadhflkjashdf lkasdfha lksdfhkajs dhflkjashdf lkahsdf lkahsdlkf jhsa lkjfdhalskjdhf lksahfd lkasjhdflkjashdkjfhaslkjdfhaslkjdhflkjashfdlkjsahdf ksahd fklsadhf kashdkf hsadkf haskdj faksjdhf ash dfkahsdf lkasdh fakshd",
                                new[] {emailAtt}));
                        }

                        ecRepo.BatchSaveOrUpdate(ecList);

                        _service.CommitBatchTransaction();
                    }
                    catch (Exception)
                    {
                        _service.RollbackBatchTransaction();
                        throw;
                    }
                });

                var endTime = DateTime.UtcNow;

                Console.WriteLine("operation took {0} ms", (endTime - beginTime).TotalMilliseconds);
                Console.WriteLine("press any key to continue");
                Console.ReadKey();
            }
            catch (Exception)
            {
                try
                {
                    _service.RollbackTransaction();
                }
                catch
                {
                }

                throw;
            }
        }
    }
}