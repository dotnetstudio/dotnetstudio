using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using RalphsDotNet.Data;
using RalphsDotNet.Extension;

namespace RalphsDotNet.Service.Data.Interface
{
    [DataContract]
    public class DataServiceConfiguration : ServiceConfiguration
    {
        [DataMember(Order = 0)]
        public string DataManagerTypeName { get; set; }

        [DataMember(Order = 1)]
        public string ObjectAssemblyNames { get; set; }

        [DataMember(Order = 2)]
        public string RepositoryAssemblyNames { get; set; }

        [DataMember(Order = 3)]
        public ConfigurationValue[] Values { get; set; }

        public Type DataManagerType
        {
            get { return this.DataManagerTypeName.FindType(); }
            set { this.DataManagerTypeName = string.Format("{0}, {1}", value.FullName, value.Assembly.GetName().Name); }
        }

        public Assembly[] ObjectAssemblies
        {
            get
            {
                var assemblyList = new List<Assembly>();

                foreach (var assemblyName in this.ObjectAssemblyNames.Split(';').Where(x => !string.IsNullOrEmpty(x)))
                {
                    var repositoryAssembly = assemblyName.FindAssembly();

                    if (repositoryAssembly == null)
                        throw (new ArgumentException(string.Concat("object assembly <", assemblyName, "> not found")));
                    assemblyList.Add(repositoryAssembly);
                }

                return assemblyList.ToArray();
            }
            set { this.ObjectAssemblyNames = string.Join(";", value.Select(x => x.GetName().Name)); }
        }

        public Assembly[] RepositoryAssemblies
        {
            get
            {
                var assemblyList = new List<Assembly>();

                foreach (
                    var assemblyName in this.RepositoryAssemblyNames.Split(';').Where(x => !string.IsNullOrEmpty(x)))
                {
                    var repositoryAssembly = assemblyName.FindAssembly();

                    if (repositoryAssembly == null)
                        throw (new ArgumentException(string.Concat("repository assembly <", assemblyName, "> not found")));
                    assemblyList.Add(repositoryAssembly);
                }

                return assemblyList.ToArray();
            }
            set { this.RepositoryAssemblyNames = string.Join(";", value.Select(x => x.GetName().Name)); }
        }

        public override Type ServiceType
        {
            get { return typeof (IDataService); }
        }
    }

    /// <summary>
    ///     Service for persisting data objects.
    /// </summary>
    public interface IDataService : IService, IDataManager
    {
        /// <summary>
        ///     Gets the configuration.
        /// </summary>
        /// <value>
        ///     The configuration.
        /// </value>
        DataServiceConfiguration DataServiceConfiguration { get; }
    }
}