﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using RalphsDotNet.Data;
using RalphsDotNet.Service.DataStream.Interface;

namespace RalphsDotNet.Service.DataStream
{
    public class DataStreamService : IDataStreamService
    {
        private readonly List<DataStreamBase> _openStreams = new List<DataStreamBase>();

        public DataStreamService(IDataStreamService proxy, ServiceConfiguration configuration)
        {
            this.ServiceConfiguration = configuration;

            this.InitModel();
        }

        public DataStreamServiceConfiguration DataStreamServiceConfiguration
        {
            get { return this.ServiceConfiguration as DataStreamServiceConfiguration; }
        }

        public string Id
        {
            get { return this.ServiceConfiguration.Id; }
        }

        public ServiceConfiguration ServiceConfiguration { get; private set; }

        public void Dispose()
        {
            foreach (var stream in this._openStreams.ToArray())
            {
                this.CloseStream(stream);
            }
        }

        public DataStreamBase GetStream(string id)
        {
            return this._openStreams.FirstOrDefault(s => s.Id == id);
        }

        public DataStreamBase CreateStream(string id, Stream baseStream, int maxBufferSize)
        {
            if (this.GetStream(id) != null)
                throw new ArgumentException(string.Format("a stream with id \"{0}\" already exists", id));

            if (baseStream != null)
                this._openStreams.Add(new DataStream(id, this, baseStream, maxBufferSize));
            else
                this._openStreams.Add(new LoopbackStream(id, this));

            return this.GetStream(id);
        }

        public DataStreamBase CreateStream(string id, Stream baseStream)
        {
            return this.CreateStream(id, baseStream, 100);
        }

        public DataStreamBase CreateStream(string id)
        {
            return this.CreateStream(id, null, 0);
        }

        private void InitModel()
        {
            if (this.DataStreamServiceConfiguration.ObjectAssemblies != null)
            {
                foreach (
                    var dataAssembly in
                        this.DataStreamServiceConfiguration.ObjectAssemblies.Where(
                            dataAssembly => ModelEmitter.ResolveAssembly(dataAssembly) == null))
                {
                    ModelEmitter.EmitModelFromDataAssembly(dataAssembly);
                }
            }
        }

        internal void CloseStream(DataStreamBase stream)
        {
            this._openStreams.Remove(stream);

            stream.Dispose();
        }
    }
}