﻿using System;
using System.IO;
using RalphsDotNet.Data;

namespace RalphsDotNet.Service.DataStream
{
    internal class LoopbackStream : DataStreamBase
    {
        private readonly DataStreamService _service;
        private bool _isDisposed;

        internal LoopbackStream(string id, DataStreamService service)
            : base(id)
        {
            this._service = service;

            this.InitializeStream();
        }

        public override bool CanRead
        {
            get { return true; }
        }

        public override bool CanSeek
        {
            get { return false; }
        }

        public override bool CanWrite
        {
            get { return true; }
        }

        public override long Length
        {
            get { return 1; }
        }

        public override long Position
        {
            get { return 0; }
            set { throw new NotSupportedException(); }
        }

        private void InitializeStream()
        {
            if (this.CanSeek && this.CanRead)
            {
                // TODO create streamindex
            }
        }

        public override void Write<T>(T obj)
        {
            this.Last = obj;
        }

        public override IDataObject Read()
        {
            return this.Last;
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            throw new NotSupportedException();
        }

        public override void SetLength(long value)
        {
            throw new NotSupportedException();
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            throw new NotSupportedException();
        }

        public override IAsyncResult BeginRead(byte[] buffer, int offset, int count, AsyncCallback callback,
            object state)
        {
            throw new NotSupportedException();
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            throw new NotSupportedException();
        }

        public override IAsyncResult BeginWrite(byte[] buffer, int offset, int count, AsyncCallback callback,
            object state)
        {
            throw new NotSupportedException();
        }

        public override int ReadByte()
        {
            throw new NotSupportedException();
        }

        public override void WriteByte(byte value)
        {
            throw new NotSupportedException();
        }

        public override void Flush()
        {
        }

        protected override void Dispose(bool disposing)
        {
            if (!this._isDisposed)
            {
                lock (this)
                {
                    this._isDisposed = true;
                    base.Dispose(disposing);
                }

                this._service.CloseStream(this);
            }
        }
    }
}