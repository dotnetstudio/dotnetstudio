﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using RalphsDotNet.Data;
using RalphsDotNet.Extension;
using RalphsDotNet.Tool.System;

namespace RalphsDotNet.Service.DataStream
{
    internal class DataStream : DataStreamBase
    {
        public const string IndexUuid = "6f3b4150-cae6-11e3-9c1a-0800200c9a66";
        public const string MarkUuid = "076fd708-a061-4e0a-8c35-e4a16af3276c";
        private static byte[] _indexUuidBytes;
        private static byte[] _markUuidBytes;
        private readonly Stream _baseStream;
        private readonly Queue<IDataObject> _buffer = new Queue<IDataObject>();
        private readonly Dictionary<Type, bool> _isEnumerableBuffer = new Dictionary<Type, bool>();
        private readonly int _maxBufferSize;
        private readonly Dictionary<Type, PropertyInfo[]> _piCache = new Dictionary<Type, PropertyInfo[]>();
        private readonly DataStreamService _service;
        private readonly List<Index> _streamIndex = new List<Index>();
        private readonly Thread _thread;
        private bool _isDisposed;
        private long _position;
        private Exception _writingException;
        private readonly Synchronizer _writeSynchronizer = new Synchronizer();
        private readonly Synchronizer _bufferSynchronizer = new Synchronizer();

        internal DataStream(string id, DataStreamService service, Stream baseStream, int maxBufferSize)
            : base(id)
        {
            this._service = service;
            this._baseStream = baseStream;
            this._maxBufferSize = maxBufferSize;

            this.InitializeStream();

            this._thread = new Thread(this.RunInternal);
            this._thread.Start();
        }

        public static byte[] IndexUuidBytes
        {
            get { return _indexUuidBytes ?? (_indexUuidBytes = Encoding.ASCII.GetBytes(IndexUuid)); }
        }

        public static byte[] MarkUuidBytes
        {
            get { return _markUuidBytes ?? (_markUuidBytes = Encoding.ASCII.GetBytes(MarkUuid)); }
        }

        public override bool CanRead
        {
            get
            {
                this.ThrowException();
                return this._baseStream.CanRead;
            }
        }

        public override bool CanSeek
        {
            get
            {
                this.ThrowException();
                return this._baseStream.CanSeek;
            }
        }

        public override bool CanWrite
        {
            get
            {
                this.ThrowException();
                return this._baseStream.CanWrite;
            }
        }

        public override long Length
        {
            get
            {
                this.ThrowException();
                return this._streamIndex.Count;
            }
        }

        public override long Position
        {
            get
            {
                this.ThrowException();
                return this._position;
            }
            set
            {
                this.ThrowException();
                this.Seek(value - this._position, SeekOrigin.Current);
            }
        }

        private void InitializeStream()
        {
            if (this.CanSeek && this.CanRead)
            {
                // TODO create streamindex
            }
        }

        private void ThrowException()
        {
            if (this._writingException != null)
            {
                lock (this)
                {
                    var ex = this._writingException;
                    this._writingException = null;
                    throw ex;
                }
            }
        }

        private void RunInternal()
        {
            try
            {
                while (!this._isDisposed)
                {
                    IDataObject obj = null;
                    try
                    {
                        if (this._buffer.Count > 0)
                            this.RemoveIndices();

                        while (this._buffer.Count > 0)
                        {
                            obj = this._buffer.Dequeue();

                            this.WriteObject(obj);

                            obj = null;

                            if(this._buffer.Count < this._maxBufferSize)
                                this._bufferSynchronizer.Release();
                        }

                        this.WriteIndices();

                        this._writingException = null;
                    }
                    catch (Exception ex)
                    {
                        this._writingException = ex;

                        lock (this)
                        {
                            var newBuffer = new Queue<IDataObject>();

                            if (obj != null)
                                newBuffer.Enqueue(obj);

                            while (this._buffer.Count > 0)
                                newBuffer.Enqueue(this._buffer.Dequeue());
                        }
                    }

                    if (!this._isDisposed)
                        this._writeSynchronizer.Wait();
                }
            }
            finally
            {
                this.Close();
            }
        }

        private void RemoveIndices()
        {
            // TODO check existance of indices
            // TODO remmove old indices if existing
        }

        private void WriteIndices()
        {
            // TODO write indexmarks on the end of stream
        }

        private void WriteObject(IDataObject obj)
        {
            lock (this)
            {
                if (this.CanSeek)
                {
                    var initialPosition = this._baseStream.Position;
                    this._baseStream.Seek(0, SeekOrigin.End);
                    var containerPosition = this._baseStream.Position;
                    this.WriteContainer(obj);
                    this._streamIndex.Add(new Index(containerPosition));

                    if (initialPosition != containerPosition)
                        this._baseStream.Seek(initialPosition, SeekOrigin.Begin);
                }
                else
                {
                    this.WriteContainer(obj);
                }
            }
        }

        private void WriteContainer(IDataObject obj)
        {
            var types = new Dictionary<Type, int>();
            var b = this.SerializeObject(obj, types);
            var t = this.SerializeTypes(types);

            if (b.LongLength > int.MaxValue)
                throw new ArgumentException(
                    "your container is > 2GB, really bad idea. rethink what you are doing, handling such amounts of data will screew up systems running your software");

            // if index at the end of stream is damaged, reconstruct the index by seeking for marks
            this._baseStream.Write(MarkUuidBytes, 0, MarkUuidBytes.Length);
            // need greater than 2GB container? two possible reasons, we count the year 2025 or you've done something wrong, rethink it.
            this._baseStream.Write(BitConverter.GetBytes(t.Length), 0, sizeof(int));
            this._baseStream.Write(t, 0, t.Length);
            this._baseStream.Write(BitConverter.GetBytes(b.Length), 0, sizeof(int));
            this._baseStream.Write(b, 0, b.Length);
            this._baseStream.Flush();

            // has to be last, we do not want someone to edit data before we wrote it
            this.Last = obj;
        }

        private byte[] SerializeTypes(Dictionary<Type, int> types)
        {
            using (var ms = new MemoryStream())
            {
                foreach (var t in types)
                {
                    var iFaceTypeBytes = Encoding.ASCII.GetBytes(t.Key.GetResolvingName());
                    ms.Write(BitConverter.GetBytes(t.Value), 0, sizeof(int));
                    ms.Write(BitConverter.GetBytes(iFaceTypeBytes.Length), 0, sizeof(int));
                    ms.Write(iFaceTypeBytes, 0, iFaceTypeBytes.Length);
                }

                return ms.ToArray();
            }
        }

        private byte[] SerializeObject(IDataObject obj, Dictionary<Type, int> types)
        {
            if (obj != null)
            {
                var iFace = this.ReverseResolve(obj);

                using (var memStream = new MemoryStream())
                {
                    PropertyInfo[] pis;
                    if (!this._piCache.TryGetValue(iFace, out pis))
                    {
                        pis =
                            iFace.GetProperties()
                                .Where(
                                    p => !p.GetCustomAttributes<IgnorePersistanceAttribute>().Any())
                                .ToArray();
                        this._piCache.Add(iFace, pis);
                    }

                    foreach (var pi in pis)
                    {
                        this.WriteProperty(memStream, pi.PropertyType, pi.GetValue(obj, null), types);
                    }

                    memStream.Seek(0, SeekOrigin.Begin);

                    int tIndex;
                    if (!types.TryGetValue(iFace, out tIndex))
                    {
                        tIndex = types.Count > 0 ? types.Values.Max() + 1 : 0;

                        types.Add(iFace, tIndex);
                    }

                    var iFaceTypeBytes = BitConverter.GetBytes(tIndex);

                    var bytes = new byte[1 + iFaceTypeBytes.Length + sizeof (long) + memStream.Length];

                    var pos = 0;
                    bytes[pos] = 1; // obj is not null

                    pos += 1;
                    iFaceTypeBytes.CopyTo(bytes, pos);

                    pos += iFaceTypeBytes.Length;
                    BitConverter.GetBytes(memStream.Length).CopyTo(bytes, pos);

                    pos += sizeof (long);
                    memStream.ToArray().CopyTo(bytes, pos);

                    return bytes;
                }
            }

            return new byte[] {0};
        }

        public void WriteProperty(MemoryStream memStream, Type type, object value, Dictionary<Type, int> types)
        {
            bool isEnumerable;
            if (!this._isEnumerableBuffer.TryGetValue(type, out isEnumerable))
            {
                isEnumerable = type.GetInterfaces().Any(i => i == typeof (IEnumerable));
                this._isEnumerableBuffer.Add(type, isEnumerable);
            }

            if (isEnumerable && type.IsGenericType)
            {
                if (value as IEnumerable != null)
                {
                    memStream.WriteByte(1);
                    var e = (value as IEnumerable).OfType<object>();
                    memStream.Write(BitConverter.GetBytes(e.Count()), 0, sizeof (int));
                    foreach (var obj in e)
                        this.WriteProperty(memStream, type.GetGenericArguments()[0], obj, types);
                }
                else memStream.WriteByte(0);
            }
            else if (type == typeof (string))
            {
                if (value as string != null)
                {
                    memStream.WriteByte(1);
                    var b = Encoding.ASCII.GetBytes(value as string);
                    memStream.Write(BitConverter.GetBytes(b.Length), 0, sizeof (int));
                    memStream.Write(b, 0, b.Length);
                }
                else memStream.WriteByte(0);
            }
            else if (type == typeof (byte))
                memStream.WriteByte((byte) value);
            else if (type == typeof (Guid))
            {
                var b = Encoding.ASCII.GetBytes(((Guid) value).ToString());
                memStream.Write(BitConverter.GetBytes(b.Length), 0, sizeof (int));
                memStream.Write(b, 0, b.Length);
            }
            else if (type == typeof (DateTime))
            {
                var b = Encoding.ASCII.GetBytes(((DateTime) value).ToString());
                memStream.Write(BitConverter.GetBytes(b.Length), 0, sizeof (int));
                memStream.Write(b, 0, b.Length);
            }
            else if (type == typeof (bool))
                memStream.Write(BitConverter.GetBytes((bool) value), 0, sizeof (bool));
            else if (type == typeof (char))
                memStream.Write(BitConverter.GetBytes((char) value), 0, sizeof (char));
            else if (type == typeof (double))
                memStream.Write(BitConverter.GetBytes((double) value), 0, sizeof (double));
            else if (type == typeof (float))
                memStream.Write(BitConverter.GetBytes((float) value), 0, sizeof (float));
            else if (type == typeof (int))
                memStream.Write(BitConverter.GetBytes((int) value), 0, sizeof (int));
            else if (type == typeof (long))
                memStream.Write(BitConverter.GetBytes((long) value), 0, sizeof (long));
            else if (type == typeof (short))
                memStream.Write(BitConverter.GetBytes((short) value), 0, sizeof (short));
            else if (type == typeof (uint))
                memStream.Write(BitConverter.GetBytes((uint) value), 0, sizeof (uint));
            else if (type == typeof (ulong))
                memStream.Write(BitConverter.GetBytes((ulong) value), 0, sizeof (ulong));
            else if (type == typeof (ushort))
                memStream.Write(BitConverter.GetBytes((ushort) value), 0, sizeof (ushort));
            else if (type.GetInterfaces().Any(i => i == typeof (IDataObject)))
            {
                var b = this.SerializeObject(value as IDataObject, types);
                memStream.Write(b, 0, b.Length);
            }
        }

        public override void Write<T>(T obj)
        {
            this.ThrowException();
            if (this.CanWrite)
            {
                lock (this)
                {
                    if (this._buffer.Count >= this._maxBufferSize)
                        this._bufferSynchronizer.Wait();

                    this._buffer.Enqueue(obj);
                    this._writeSynchronizer.Release();
                }
            }
            else
            {
                throw new NotSupportedException();
            }
        }

        public override IDataObject Read()
        {
            this.ThrowException();
            if (this.CanRead)
            {
                lock (this)
                {
                    // TODO read object
                }
            }
            else
            {
                throw new NotSupportedException();
            }

            return null;
        }

        public override void Flush()
        {
            this.ThrowException();
            while (this._buffer.Count > 0)
                new ManualResetEvent(false).WaitOne(TimeSpan.FromMilliseconds(0.1));
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            this.ThrowException();
            if (this.CanSeek)
            {
                lock (this)
                {
                    switch (origin)
                    {
                        case SeekOrigin.Begin:
                            this._position = offset;
                            break;
                        case SeekOrigin.Current:
                            this._position += offset;
                            break;
                        case SeekOrigin.End:
                            this._position = this.Length - offset;
                            break;
                    }

                    this._baseStream.Seek(this._streamIndex[(int) this.Position].Position, SeekOrigin.Begin);

                    return this.Position;
                }
            }
            throw new NotSupportedException();
        }

        public override void SetLength(long value)
        {
            this.ThrowException();
            throw new NotSupportedException();
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            this.ThrowException();
            throw new NotSupportedException();
        }

        public override IAsyncResult BeginRead(byte[] buffer, int offset, int count, AsyncCallback callback,
            object state)
        {
            this.ThrowException();
            throw new NotSupportedException();
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            this.ThrowException();
            throw new NotSupportedException();
        }

        public override IAsyncResult BeginWrite(byte[] buffer, int offset, int count, AsyncCallback callback,
            object state)
        {
            this.ThrowException();
            throw new NotSupportedException();
        }

        public override int ReadByte()
        {
            this.ThrowException();
            throw new NotSupportedException();
        }

        public override void WriteByte(byte value)
        {
            this.ThrowException();
            throw new NotSupportedException();
        }

        public override void WaitForQueueToFinish()
        {
            while (this._buffer.Count > 0)
                new ManualResetEvent(false).WaitOne(TimeSpan.FromMilliseconds(0.1));
        }

        protected override void Dispose(bool disposing)
        {
            if (!this._isDisposed)
            {
                lock (this)
                {
                    this._isDisposed = true;
                    this._writeSynchronizer.Release();
                    this._baseStream.Dispose();
                    base.Dispose(disposing);
                }

                this._service.CloseStream(this);
            }
        }

        private Type Resolve<T>() where T : IDataObject
        {
            return ModelEmitter.ResolveType(typeof (T));
        }

        private Type ReverseResolve(IDataObject obj)
        {
            return this.ReverseResolve(obj.GetType());
        }

        private Type ReverseResolve(Type type)
        {
            return ModelEmitter.ReverseResolveType(type);
        }
    }

    internal class Index
    {
        internal Index(long position)
        {
            this.Position = position;
        }

        public long Position { get; private set; }
    }
}