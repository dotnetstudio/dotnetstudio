﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using RalphsDotNet.Data;
using RalphsDotNet.Extension;

namespace RalphsDotNet.Service.DataStream.Interface
{
    [DataContract]
    public class DataStreamServiceConfiguration : ServiceConfiguration
    {
        [DataMember(Order = 0)]
        public string ObjectAssembliesNames { get; set; }

        public Assembly[] ObjectAssemblies
        {
            get
            {
                var assemblyList = new List<Assembly>();

                foreach (var assemblyName in this.ObjectAssembliesNames.Split(';').Where(x => !string.IsNullOrEmpty(x)))
                {
                    var repositoryAssembly = assemblyName.FindAssembly();

                    if (repositoryAssembly == null)
                        throw (new ArgumentException(string.Concat("repository assembly <", assemblyName, "> not found")));
                    assemblyList.Add(repositoryAssembly);
                }

                return assemblyList.ToArray();
            }
            set { this.ObjectAssembliesNames = string.Join(";", value.Select(x => x.GetName().Name)); }
        }

        public override Type ServiceType
        {
            get { return typeof (IDataStreamService); }
        }
    }

    public interface IDataStreamService : IService
    {
        DataStreamBase GetStream(string id);
        DataStreamBase CreateStream(string id);
        DataStreamBase CreateStream(string id, Stream baseStream);
        DataStreamBase CreateStream(string id, Stream baseStream, int maxBufferSize);
    }
}