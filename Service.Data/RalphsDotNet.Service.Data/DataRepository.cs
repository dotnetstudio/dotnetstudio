using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using RalphsDotNet.Data;
using RalphsDotNet.Types;

namespace RalphsDotNet.Service.Data
{
    public abstract class DataRepository : NotifyPropertyChangingPropertyChangedBase, IDataRepository
    {
        private IDataManager _manager;

        protected DataRepository()
        {
            this.PropertyChanging += this.DataRepository_PropertyChanging;
            this.PropertyChanged += this.DataRepository_PropertyChanged;
        }

        public abstract Type DataType { get; }

        public IDataManager Manager
        {
            get { return this._manager; }
            set
            {
                if (!Equals(this._manager, value))
                {
                    this.OnPropertyChanging("Manager");

                    this._manager = value;

                    this.OnPropertyChanged("Manager");
                }
            }
        }

        public void Dispose()
        {
            this.PropertyChanging -= this.DataRepository_PropertyChanging;
            this.PropertyChanged -= this.DataRepository_PropertyChanged;
        }

        private void DataRepository_PropertyChanging(object sender, PropertyChangingEventArgs e)
        {
            if (e.PropertyName == "Manager")
                this.OnManagerChanging();
        }

        private void DataRepository_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Manager")
                this.OnManagerChanged();
        }

        protected virtual void OnManagerChanging()
        {
        }

        protected virtual void OnManagerChanged()
        {
        }
    }

    public abstract class DataRepository<T> : DataRepository, IDataRepository<T> where T : IDataObject
    {
        public override Type DataType
        {
            get { return typeof (T); }
        }

        public IQueryable<T> All
        {
            get { return this.Manager.Get<T>(); }
        }

        public IQueryable<T> BatchAll
        {
            get { return this.Manager.BatchGet<T>(); }
        }

        public void SaveOrUpdate(T obj)
        {
            this.Manager.SaveOrUpdate(obj);
        }

        public void BatchSaveOrUpdate(T obj)
        {
            this.Manager.BatchSaveOrUpdate(obj);
        }

        public void SaveOrUpdate(IEnumerable<T> objs)
        {
            this.Manager.SaveOrUpdate(objs.Cast<IDataObject>());
        }

        public void BatchSaveOrUpdate(IEnumerable<T> objs)
        {
            this.Manager.BatchSaveOrUpdate(objs.Cast<IDataObject>());
        }

        public void Delete(T obj)
        {
            this.Manager.Delete(obj);
        }

        public void BatchDelete(T obj)
        {
            this.Manager.BatchDelete(obj);
        }

        public void Delete(IEnumerable<T> objs)
        {
            this.Manager.Delete(objs.Cast<IDataObject>());
        }

        public void BatchDelete(IEnumerable<T> objs)
        {
            this.Manager.BatchDelete(objs.Cast<IDataObject>());
        }

        public void Reload(T obj)
        {
            this.Manager.Reload(obj);
        }

        public void Reload(IEnumerable<T> objs)
        {
            this.Manager.Reload(objs.Cast<IDataObject>());
        }

        protected virtual T CreateInternal()
        {
            var instance = DataObject.Create<T>();

            return instance;
        }
    }
}