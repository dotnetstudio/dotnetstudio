using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using RalphsDotNet.Data;
using RalphsDotNet.Service.Data.Interface;

namespace RalphsDotNet.Service.Data
{
    public abstract class DataManager : IDataManager
    {
        private readonly Dictionary<Type, IDataRepository> _repositories = new Dictionary<Type, IDataRepository>();
        private IDataService _service;
        public List<Assembly> DataTypeAssemblies { get; private set; }
        public abstract bool ActiveTransaction { get; }
        public abstract bool ActiveBatchTransaction { get; }

        public TR GetRepository<TO, TR>()
            where TO : IDataObject
            where TR : IDataRepository<TO>
        {
            if (this._repositories.ContainsKey(typeof (TO)))
                return (TR) this._repositories[typeof (TO)];

            throw (new ArgumentException(string.Concat("no repository for <", typeof (TO).FullName, "> found")));
        }

        public abstract void BeginTransaction();

        public abstract void BeginBatchTransaction();

        public abstract void CommitTransaction();

        public abstract void CommitBatchTransaction();

        public abstract void RollbackTransaction();

        public abstract void RollbackBatchTransaction();

        public abstract void SaveOrUpdate(IDataObject obj);

        public abstract void BatchSaveOrUpdate(IDataObject obj);

        public abstract void SaveOrUpdate(IEnumerable<IDataObject> objs);

        public abstract void BatchSaveOrUpdate(IEnumerable<IDataObject> objs);

        public abstract void Delete(IDataObject obj);

        public abstract void BatchDelete(IDataObject obj);

        public abstract void Delete(IEnumerable<IDataObject> objs);

        public abstract void BatchDelete(IEnumerable<IDataObject> objs);

        public abstract void Reload(IDataObject obj);

        public abstract void Reload(IEnumerable<IDataObject> objs);

        public abstract IQueryable<T> Get<T>() where T : IDataObject;

        public abstract IQueryable<T> BatchGet<T>() where T : IDataObject;

        public abstract void Dispose();

        internal void Initialize(IDataService service)
        {
            this.DataTypeAssemblies = new List<Assembly>();
            this._service = service;

            this.InitModel();

            this.OnInitialize();
        }

        /// <summary>
        ///     Called at the initialization of the data manager.
        /// </summary>
        protected virtual void OnInitialize()
        {
            this.InitRepositories();
        }

        /// <summary>
        ///     Gets a config value.
        /// </summary>
        /// <returns>
        ///     The config value.
        /// </returns>
        /// <param name='key'>
        ///     The config value key.
        /// </param>
        protected string GetConfigValue(string key)
        {
            return
                this._service.DataServiceConfiguration.Values.Where(cv => cv.Key == key)
                    .Select(cv => cv.Value)
                    .FirstOrDefault();
        }

        private void InitModel()
        {
            if (this._service.DataServiceConfiguration.ObjectAssemblies != null)
            {
                foreach (var dataAssembly in this._service.DataServiceConfiguration.ObjectAssemblies)
                {
                    if (ModelEmitter.ResolveAssembly(dataAssembly) == null)
                    {
                        ModelEmitter.EmitModelFromDataAssembly(dataAssembly);
                    }

                    this.DataTypeAssemblies.Add(ModelEmitter.ResolveAssembly(dataAssembly));
                }
            }
        }

        private void InitRepositories()
        {
            if (this._service.DataServiceConfiguration.RepositoryAssemblies != null)
            {
                foreach (var respositoryAssembly in this._service.DataServiceConfiguration.RepositoryAssemblies)
                {
                    foreach (
                        var type in
                            respositoryAssembly.GetTypes()
                                .Where(x => !x.IsAbstract && x.IsSubclassOf(typeof (DataRepository))))
                    {
                        var repository = (DataRepository) Activator.CreateInstance(type);
                        repository.Manager = this._service;
                        if (this.OnCreateRepository(repository))
                            this._repositories.Add(type.BaseType.GetGenericArguments()[0], repository);
                    }
                }
            }
        }

        /// <summary>
        ///     Called at the creation of a data repository.
        /// </summary>
        /// <returns>
        ///     <c>false</c> if the creation of the repository should be interrupted, otherwise <c>true</c>.
        /// </returns>
        /// <param name='repository'>
        ///     The repository gets created.
        /// </param>
        protected virtual bool OnCreateRepository(DataRepository repository)
        {
            return true;
        }

        public Type Resolve<T>() where T : IDataObject
        {
            return this.Resolve(typeof (T));
        }

        public Type Resolve(Type iType)
        {
            if (iType.GetInterfaces().All(i => i != typeof (IDataObject)))
                throw (new ArgumentException("given type has to implement IDataObject"));

            return ModelEmitter.ResolveType(iType);
        }
    }
}