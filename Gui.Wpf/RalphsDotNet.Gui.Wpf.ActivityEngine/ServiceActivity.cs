﻿using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using RalphsDotNet.Gui.ActivityModel;

namespace RalphsDotNet.Gui.Wpf.ActivityEngine
{
    public class ServiceActivity : UserControl, IServiceActivity
    {
        public static readonly DependencyProperty HostProperty =
               DependencyProperty.Register("Host", typeof(IActivityHost), typeof(ServiceActivity), new UIPropertyMetadata(null, (o, args) => ((ServiceActivity)o).OnPropertyChanged("Host")));

        public IActivityHost Host
        {
            get { return GetValue(HostProperty) as IActivityHost; }
            set { SetValue(HostProperty, value); }
        }

        protected void OnPropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
