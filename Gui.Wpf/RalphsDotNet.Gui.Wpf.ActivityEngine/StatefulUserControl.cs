﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using RalphsDotNet.Gui.ActivityModel;

namespace RalphsDotNet.Gui.Wpf.ActivityEngine
{
    public abstract class StatefulUserControl : UserControl
    {
        public static readonly DependencyProperty StatesProperty =
            DependencyProperty.Register("States", typeof(ControlStateCollection), typeof(StatefulUserControl),
                new UIPropertyMetadata(new ControlStateCollection()));

        public ControlStateCollection States
        {
            get { return GetValue(StatesProperty) as ControlStateCollection; }
            set { SetValue(StatesProperty, value); }
        }

        public static readonly DependencyProperty ActiveStateProperty =
            DependencyProperty.Register("ActiveState", typeof(ControlState), typeof(StatefulUserControl));

        public ControlState ActiveState
        {
            get { return GetValue(ActiveStateProperty) as ControlState; }
            set { SetValue(ActiveStateProperty, value); }
        }

        protected override void OnInitialized(EventArgs e)
        {
            base.OnInitialized(e);

            this.UpdateState();
        }

        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            base.OnPropertyChanged(e);

            if (e.Property == ActiveStateProperty)
            {
                if (e.OldValue as ControlState != null)
                    ((ControlState) e.OldValue).IsActive = false;

                if (this.ActiveState != null)
                    this.ActiveState.IsActive = true;
            }
            else if (e.Property != StatesProperty && ((!typeof (StatefulUserControl).IsSubclassOf(e.Property.OwnerType) &&
                 this.GetType().IsSubclassOf(e.Property.OwnerType)) || e.Property.OwnerType == this.GetType()))
            {
                this.UpdateState();
            }
        }

        protected void UpdateState()
        {
            var usingState = this.States.FirstOrDefault(activityState => activityState.CheckHandler == null);
            foreach (var activityState in this.States.Where(activityState => activityState.CheckHandler != null && activityState.CheckHandler(activityState)))
            {
                usingState = activityState;
            }

            this.ActiveState = usingState ?? this.ActiveState;
        }
    }
}
