﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;
using RalphsDotNet.Gui.ActivityModel;
using Dispatcher = System.Windows.Threading.Dispatcher;
using DispatcherPriority = System.Windows.Threading.DispatcherPriority;

namespace RalphsDotNet.Gui.Wpf.ActivityEngine
{
    /// <summary>
    ///     Interaction logic for ActivityHostWindow.xaml
    /// </summary>
    public partial class ActivityHostWindow : IActivityHost
    {
        public static readonly DependencyProperty CanBackProperty =
            DependencyProperty.Register("CanBack", typeof (bool), typeof (ActivityHostWindow),
                new UIPropertyMetadata(false));

        public static readonly DependencyProperty HeaderImageProperty =
            DependencyProperty.Register("HeaderImage", typeof (string), typeof (ActivityHostWindow));

        public static readonly DependencyProperty HeaderTextProperty =
            DependencyProperty.Register("HeaderText", typeof (string), typeof (ActivityHostWindow));

        public static readonly DependencyProperty SubHeaderTextProperty =
            DependencyProperty.Register("SubHeaderText", typeof (string), typeof (ActivityHostWindow));

        public static readonly DependencyProperty FooterTextProperty =
            DependencyProperty.Register("FooterText", typeof (string), typeof (ActivityHostWindow));

        public static readonly DependencyProperty ProgressHandlerProperty =
            DependencyProperty.Register("ProgressHandler", typeof (ProgressHandler), typeof (ActivityHostWindow));

        private readonly Stack<IActivity> _activityStack = new Stack<IActivity>();
        private readonly Type _startActivityType;
        private bool _initialized;

        public ApplicationController Controller { get; private set; }

        public ActivityHostWindow(ApplicationController controller)
        {
            this.Controller = controller;
            this.InitializeComponent();
        }

        public ActivityHostWindow(ApplicationController controller, Type startActivityType)
            : this(controller)
        {
            if (startActivityType.GetInterface(typeof (IActivity).Name) == null)
                throw (new ArgumentException(string.Format("start activity type must implement <{0}>",
                    typeof (IActivity).Name)));

            this._startActivityType = startActivityType;
        }

        public bool CanBack
        {
            get { return (bool) this.GetValue(CanBackProperty); }
            set { this.SetValue(CanBackProperty, value); }
        }

        public ProgressHandler ProgressHandler
        {
            get { return this.GetValue(ProgressHandlerProperty) as ProgressHandler; }
            set { this.SetValue(ProgressHandlerProperty, value); }
        }

        public string HeaderImage
        {
            get { return this.GetValue(HeaderImageProperty) as string; }
            set { this.SetValue(HeaderImageProperty, value); }
        }

        public string HeaderText
        {
            get { return this.GetValue(HeaderTextProperty) as string; }
            set { this.SetValue(HeaderTextProperty, value); }
        }

        public string SubHeaderText
        {
            get { return this.GetValue(SubHeaderTextProperty) as string; }
            set { this.SetValue(SubHeaderTextProperty, value); }
        }

        public string FooterText
        {
            get { return this.GetValue(FooterTextProperty) as string; }
            set { this.SetValue(FooterTextProperty, value); }
        }

        public object LastResult { get; private set; }

        public void ClearActivities()
        {
            IActivity shownActivity = null;
            while (this._activityStack.Count > 0)
            {
                var oldActivity = this._activityStack.Pop();

                if (shownActivity == null)
                    shownActivity = oldActivity;

                oldActivity.Pause();
                oldActivity.IsActive = false;
                oldActivity.PropertyChanged -= this.activity_PropertyChanged;
                oldActivity.Host = null;
                oldActivity.DataContext = null;
                oldActivity.Dispose();
            }

            this.ReplaceActivity(shownActivity, null);

            this.RefreshReferences(null);
            this.LastResult = null;

            this._activityStack.Clear();
        }

        public IActivity LoadActivity(Type activityType)
        {
            return this.LoadActivity(activityType, null);
        }

        public IActivity LoadActivity(Type activityType, object dataContext)
        {
            if (this._startActivityType != null && this._startActivityType.GetInterface(typeof (IActivity).Name) == null)
                throw (new ArgumentException(string.Format("start activity type must implement <{0}>",
                    typeof (IActivity).Name)));

            var activity = Activator.CreateInstance(activityType) as IActivity;
            this.LoadActivity(activity, dataContext);

            return activity;
        }

        public void LoadActivity(IActivity activity)
        {
            this.LoadActivity(activity, null);
        }

        public void LoadActivity(IActivity activity, object dataContext)
        {
            var hasOwnProgress = false;

            if (this.ProgressHandler == null)
            {
                this.ProgressHandler = new ProgressHandler {Maximum = 3, Text = "LoadingActivityProgressString"};
                hasOwnProgress = true;
            }

            this.Dispatch();

            try
            {
                if (activity is FrameworkElement)
                {
                    var oldActivity = this._activityStack.Count > 0 ? this._activityStack.Peek() : null;
                    if (oldActivity != null)
                    {
                        if (oldActivity.CanPause)
                        {
                            oldActivity.Pause();
                            oldActivity.IsActive = false;
                        }
                        else
                        {
                            this._activityStack.Pop();
                            oldActivity.IsActive = false;
                            oldActivity.PropertyChanged -= this.activity_PropertyChanged;
                            oldActivity.Host = null;
                            oldActivity.DataContext = null;
                            oldActivity.Dispose();
                        }
                    }

                    this.LastResult = null;

                    this.ProgressHandler.Value++;
                    this.Dispatch();

                    activity.Host = this;
                    this.RefreshReferences(activity);
                    activity.PropertyChanged += this.activity_PropertyChanged;
                    this._activityStack.Push(activity);

                    this.ProgressHandler.Value++;
                    this.Dispatch();

                    this.ReplaceActivity(oldActivity, activity);

                    activity.IsActive = true;
                    if (dataContext != null)
                        activity.DataContext = dataContext;

                    this.CanBack = this._activityStack.Count > 1;

                    this.ProgressHandler.Value++;
                }
                else
                    throw (new ArgumentException("Activity has to be an FrameworkElement"));
            }
            finally
            {
                if (hasOwnProgress)
                    this.ProgressHandler = null;
            }
        }

        public IActivity LoadActivity(ActivityReferenceBase reference)
        {
            object parameter = null;
            var execute = true;
            if (reference.PreAction != null)
            {
                var hasOwnProgress = false;

                if (this.ProgressHandler == null)
                {
                    this.Dispatch();

                    this.ProgressHandler = new ProgressHandler { IsIndeterminate = true, Text = "LoadingActivityProgressString" };
                    hasOwnProgress = true;

                    this.Dispatch();
                }

                try
                {
                    execute = reference.PreAction.Invoke(reference, out parameter);
                }
                finally
                {
                    if (hasOwnProgress)
                        this.ProgressHandler = null;
                }
            }

            if (reference as ActivityReference != null)
            {
                var activityRef = (ActivityReference)reference;
                if (execute && activityRef.Activity != null)
                {
                    var newActivity = Activator.CreateInstance(activityRef.Activity) as IActivity;

                    if (newActivity != null)
                    {
                        this.LoadActivity(newActivity, parameter);
                    }

                    return newActivity;
                }
            }
            else if (reference as ProgressActivityReference != null &&
                     ((ProgressActivityReference)reference).ProgressAction != null)
            {
                this.ProgressHandler = new ProgressHandler();
                try
                {
                    ((ProgressActivityReference)reference).ProgressAction.Invoke(
                        (ProgressActivityReference)reference, this.ProgressHandler);
                }
                finally
                {
                    this.ProgressHandler = null;
                }
            }

            return null;
        }

        private void Dispatch()
        {
            var frame = new DispatcherFrame {Continue = true};
            Dispatcher.CurrentDispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
            {
                frame.Continue = false;
            }));
            Dispatcher.PushFrame(frame);
        }

        public void Back()
        {
            if (this._activityStack.Count > 1)
            {
                var hasOwnProgress = false;

                if (this.ProgressHandler == null)
                {
                    this.ProgressHandler = new ProgressHandler {Maximum = 5, Text = "GoingBackProgressString"};
                    hasOwnProgress = true;
                }

                this.Dispatch();

                try
                {
                    this.UnfocusActual();

                    this.CanBack = this._activityStack.Count > 2;

                    this.ProgressHandler.Value++;
                    this.Dispatch();

                    var oldActivity = this._activityStack.Pop();
                    this.LastResult = oldActivity.Result;
                    if (oldActivity.CanPause) oldActivity.Pause();
                    oldActivity.IsActive = false;
                    oldActivity.PropertyChanged -= this.activity_PropertyChanged;
                    this.RefreshReferences(null);
                    oldActivity.Host = null;
                    oldActivity.DataContext = null;
                    oldActivity.Dispose();

                    this.ProgressHandler.Value++;
                    this.Dispatch();

                    var newActivity = this._activityStack.Peek();
                    this.RefreshReferences(newActivity);

                    this.ProgressHandler.Value++;
                    this.Dispatch();

                    this.ReplaceActivity(oldActivity, newActivity);

                    this.ProgressHandler.Value++;
                    this.Dispatch();

                    newActivity.Resume();
                    newActivity.IsActive = true;

                    this.ProgressHandler.Value++;
                }
                finally
                {
                    if (hasOwnProgress)
                        this.ProgressHandler = null;
                }
            }
        }

        protected override void OnInitialized(EventArgs e)
        {
            base.OnInitialized(e);

            // search for a configurationattribute in entry assembly and load it
            var attrib =
                Assembly.GetEntryAssembly()
                    .GetCustomAttributes(typeof (ActivityHostConfigurationAttribute), false)
                    .FirstOrDefault() as ActivityHostConfigurationAttribute;

            if (attrib != null)
            {
                this.HeaderImage = attrib.HeaderImage;
                this.HeaderText = attrib.HeaderText;
                this.SubHeaderText = attrib.SubHeaderText;
                this.FooterText = attrib.FooterText;
                this.LoadActivity(attrib.EntryActivity);
            }
        }

        protected override void OnRender(DrawingContext drawingContext)
        {
            base.OnRender(drawingContext);

            if (!this._initialized && this._startActivityType != null)
            {
                this.LoadActivity(this._startActivityType);
            }

            this._initialized = true;
        }

        private void Grid_Drag(object sender, MouseButtonEventArgs e)
        {
            if (Mouse.LeftButton == MouseButtonState.Pressed)
                this.DragMove();
        }

        private void closeButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void activity_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "References" && sender is IActivity)
                this.RefreshReferences(sender as IActivity);
        }

        private void RefreshReferences(IActivity activity)
        {
            this.Buttons.ItemsSource = null;

            if (activity != null)
            {
                var container = new List<ActivityReferenceContainer>();
                if (activity.References != null)
                {
                    foreach (var ar in activity.References)
                    {
                        PropertyInfo symbolPi = null;
                        if (ar as ActivityReference != null && ((ActivityReference) ar).Activity != null)
                        {
                            symbolPi = ((ActivityReference) ar).Activity
                                .GetProperty("Symbol", BindingFlags.Static | BindingFlags.Public);
                        }

                        var symbolName = symbolPi != null ? symbolPi.GetValue(null, null) as string : null;
                        container.Add(new ActivityReferenceContainer(ar, symbolName));
                        // TODO replace local resources against global resources
                    }
                }

                if (container.Count > 0)
                {
                    this.Buttons.ItemsSource = container;
                }
            }
        }

        private void UnfocusActual()
        {
            FocusManager.SetFocusedElement(FocusManager.GetFocusScope(this), this);
        }

        private void ReplaceActivity(IActivity oldActivity, IActivity newActivity)
        {
            if (this.ResizeMode == ResizeMode.NoResize || this.ResizeMode == ResizeMode.CanMinimize)
            {
                var newH = this.Height;
                var newW = this.Width;
                var newT = this.Top;
                var newL = this.Left;

                if (oldActivity as FrameworkElement != null)
                {
                    var diffH = ((FrameworkElement) oldActivity).MinHeight - 150;
                    var diffW = ((FrameworkElement) oldActivity).MinWidth - 370;

                    newH -= diffH;
                    newW -= diffW;
                    newT += diffH/2;
                    newL += diffW/2;
                }

                if (newActivity as FrameworkElement != null)
                {
                    var diffH = ((FrameworkElement) newActivity).MinHeight - 150;
                    var diffW = ((FrameworkElement) newActivity).MinWidth - 370;

                    newL -= diffW/2;
                    newT -= diffH/2;
                    newH += diffH;
                    newW += diffW;

                    ((FrameworkElement) newActivity).Opacity = 0;
                    this.ContentGrid.Children.Add(newActivity as FrameworkElement);
                }

                var resize = newH != this.Height || newW != this.Width || newT != this.Top || newL != this.Left;

                if (oldActivity as FrameworkElement != null)
                    ((FrameworkElement) oldActivity).Opacity = 0;

                if (resize && this.WindowState != WindowState.Maximized)
                {
                    this.Width = newW;
                    this.Left = newL;
                    this.Height = newH;
                    this.Top = newT;
                }

                if (newActivity as FrameworkElement != null)
                    ((FrameworkElement) newActivity).Opacity = 100;
            }
            else if (newActivity as FrameworkElement != null)
            {
                this.ContentGrid.Children.Add(newActivity as FrameworkElement);
            }

            if (oldActivity as FrameworkElement != null)
                this.ContentGrid.Children.Remove(oldActivity as FrameworkElement);
        }

        private void backButton_Click(object sender, RoutedEventArgs e)
        {
            this.Back();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var activityRefCont = ((FrameworkElement) sender).DataContext as ActivityReferenceContainer;
            if (activityRefCont != null)
            {
                var reference = activityRefCont.Reference;
                this.LoadActivity(reference);
            }
        }

        private void Window_OnKeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Space && Keyboard.IsKeyDown(Key.LeftCtrl))
                this.Back();
        }

        private void Window_OnClosed(object sender, EventArgs e)
        {
            this.ClearActivities();
        }

        private void ActivityHostWindow_OnClosing(object sender, CancelEventArgs e)
        {
            e.Cancel = this.ProgressHandler != null;
        }
    }
}