﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace RalphsDotNet.Gui.Wpf.ActivityEngine
{
	public static class OutputProvider
	{
		public static readonly OutputWriter Instance = new OutputWriter();

		public static void Write(char value)
		{
			Instance.Write(value);
		}

		public static void Write(string format, params object[] param)
		{
			Write(string.Format(format, param));
		}

		public static void Write(string value)
		{
			Instance.Write(value);
		}

		public static void WriteLine(string line)
		{
			Instance.WriteLine(line);
		}

		public static void WriteLine(string format, params object[] param)
		{
			WriteLine(string.Format(format, param));
		}

		public static void WriteSeparator()
		{
			WriteLine("------------------------------");
		}

		public static void Clear()
		{
		}

		public static void WriteException(Exception ex)
		{
			WriteSeparator();
			WriteLine("ERROR: {0}", ex.GetType().Name);
			WriteSeparator();
			WriteLine(ex.Message);
			WriteSeparator();

			if (ex.InnerException != null)
				WriteException(ex.InnerException);
		}
	}

	public class Tracer : TraceListener
	{
		public override void Write(string message)
		{
			//OutputProvider.Write(message);
		}

		public override void WriteLine(string message)
		{
			//OutputProvider.WriteLine(message);
		}
	}

	public class OutputWriter : TextWriter, INotifyPropertyChanged
	{
		private string _output;

		public string Output
		{
			get { return this._output; }
			private set
			{
				if (!Equals(this._output, value))
				{
					this._output = value;

					this.OnPropertyChanged("Output");
				}
			}
		}

		public override Encoding Encoding
		{
			get { return Encoding.Default; }
		}

		public event PropertyChangedEventHandler PropertyChanged;

		public void Clear()
		{
			this.Output = string.Empty;
		}

		public override void Write(char value)
		{
			this.Output += value;
		}

		public override void Write(string value)
		{
			this.Output += value;
		}

		public override void WriteLine(string value)
		{
			this.Output += string.Concat(value, Environment.NewLine);
		}

		protected virtual void OnPropertyChanged(string propertyName)
		{
			var handler = this.PropertyChanged;
			if (handler != null)
				handler(this, new PropertyChangedEventArgs(propertyName));
		}
	}
}