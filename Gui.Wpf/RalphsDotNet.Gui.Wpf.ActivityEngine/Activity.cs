﻿using System.ComponentModel;
using System.Windows;
using RalphsDotNet.Gui.ActivityModel;

namespace RalphsDotNet.Gui.Wpf.ActivityEngine
{
    public class Activity : StatefulUserControl, IActivity
    {
        public static readonly DependencyProperty ReferencesProperty =
            DependencyProperty.Register("References", typeof (ActivityReferenceCollection), typeof (Activity),
                new UIPropertyMetadata(null,
                    (o, args) => ((Activity) o).OnPropertyChanged("References")));

        public static readonly DependencyProperty IsActiveProperty =
            DependencyProperty.Register("IsActive", typeof (bool), typeof (Activity),
                new UIPropertyMetadata(false, (o, args) => ((Activity) o).OnPropertyChanged("IsActive")));

        public static readonly DependencyProperty CanPauseProperty =
            DependencyProperty.Register("CanPause", typeof (bool), typeof (Activity),
                new UIPropertyMetadata(false, (o, args) => ((Activity) o).OnPropertyChanged("CanPause")));

        public static readonly DependencyProperty HostProperty =
            DependencyProperty.Register("Host", typeof (IActivityHost), typeof (Activity),
                new UIPropertyMetadata(null, (o, args) => ((Activity) o).OnPropertyChanged("Host")));

        public static readonly DependencyProperty ResultProperty =
            DependencyProperty.Register("Result", typeof (object), typeof (Activity),
                new UIPropertyMetadata(null, (o, args) => ((Activity) o).OnPropertyChanged("Result")));

        public Activity()
        {
            this.References = new ActivityReferenceCollection();
        }

        public bool CanPause
        {
            get { return (bool) this.GetValue(CanPauseProperty); }
            set { this.SetValue(CanPauseProperty, value); }
        }

        public ActivityReferenceCollection References
        {
            get { return this.GetValue(ReferencesProperty) as ActivityReferenceCollection; }
            set { this.SetValue(ReferencesProperty, value); }
        }

        public bool IsActive
        {
            get { return (bool) this.GetValue(IsActiveProperty); }
            set { this.SetValue(IsActiveProperty, value); }
        }

        public IActivityHost Host
        {
            get { return this.GetValue(HostProperty) as IActivityHost; }
            set { this.SetValue(HostProperty, value); }
        }

        public object Result
        {
            get { return this.GetValue(ResultProperty); }
            protected set { this.SetValue(ResultProperty, value); }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private bool _isInitialized;
        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            base.OnPropertyChanged(e);

            if (!this._isInitialized && e.Property == HostProperty)
            {
                this.OnActivityInitialize();
            }
            else if (e.Property == IsActiveProperty)
            {
                if (this.IsActive)
                    this.OnActivityActivated();
                else
                    this.OnActivityDeactivated();
            }
        }

        protected virtual void OnActivityDeactivated()
        {
        }

        protected virtual void OnActivityActivated()
        {
        }

        protected virtual void OnActivityInitialize()
        {
            this._isInitialized = true;
        }

        public virtual void Dispose()
        {
        }

        public virtual void Pause()
        {
        }

        public virtual void Resume()
        {
        }

        protected void OnPropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}