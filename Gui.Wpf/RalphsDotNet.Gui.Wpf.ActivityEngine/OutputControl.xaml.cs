﻿using System;
using System.Windows.Controls;
using System.Windows.Threading;

namespace RalphsDotNet.Gui.Wpf.ActivityEngine
{
    /// <summary>
    ///     Interaction logic for OutputControl.xaml
    /// </summary>
    public partial class OutputControl
    {
        public readonly static TimeSpan BackToBottomIntervall = new TimeSpan(0, 0, 0, 30);

        private DispatcherTimer _backToBottomTimer;

        public OutputControl()
        {
            this.InitializeComponent();
        }

        protected override void OnInitialized(EventArgs e)
        {
            base.OnInitialized(e);

            this._backToBottomTimer = new DispatcherTimer(BackToBottomIntervall, DispatcherPriority.Background, (sender, args) => this._ScrollViewer.ScrollToBottom(), Dispatcher.CurrentDispatcher);
        }

        private void TextBoxBase_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            this._ScrollViewer.ScrollToBottom();
        }

        private void ScrollViewer_OnScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            this._backToBottomTimer.Stop();
            this._backToBottomTimer.Start();
        }
    }
}