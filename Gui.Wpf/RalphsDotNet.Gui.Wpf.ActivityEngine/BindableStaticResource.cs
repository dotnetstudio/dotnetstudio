﻿using System;
using System.Windows;
using System.Windows.Data;
using System.Windows.Markup;

namespace RalphsDotNet.Gui.Wpf.ActivityEngine
{
    public class BindableStaticResource : StaticResourceExtension
    {
        private static readonly DependencyProperty DummyProperty;

        static BindableStaticResource()
        {
            DummyProperty = DependencyProperty.RegisterAttached("Dummy",
                typeof (Object),
                typeof (DependencyObject),
                new UIPropertyMetadata(null));
        }

        public BindableStaticResource()
        {
        }

        public BindableStaticResource(Binding binding)
        {
            this.MyBinding = binding;
        }

        public Binding MyBinding { get; set; }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            var target = (IProvideValueTarget) serviceProvider.GetService(typeof (IProvideValueTarget));
            var targetObject = (FrameworkElement) target.TargetObject;

            this.MyBinding.Source = targetObject.DataContext;
            var dummyDo = new DependencyObject();
            BindingOperations.SetBinding(dummyDo, DummyProperty, this.MyBinding);

            this.ResourceKey = dummyDo.GetValue(DummyProperty);

            return base.ProvideValue(serviceProvider);
        }
    }
}