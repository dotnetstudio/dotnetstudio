﻿using System;
using System.ComponentModel;
using System.Threading;
using System.Windows;
using System.Windows.Threading;
using RalphsDotNet.Gui.ActivityModel;
using RalphsDotNet.Service;
using RalphsDotNet.Tool.System;
using Dispatcher = RalphsDotNet.Tool.System.Dispatcher;
using DispatcherPriority = System.Windows.Threading.DispatcherPriority;

namespace RalphsDotNet.Gui.Wpf.ActivityEngine
{
    public interface IApplicationController
    {
        void Invoke(ApplicationInvoke action);
        void ProgressInvoke(ApplicationInvoke action);
        void ProgressInvoke(ApplicationInvoke action, string text);
    }

    public abstract class ApplicationController : Application, INotifyPropertyChanged
    {
        private readonly Synchronizer _initSynchronizer = new Synchronizer();
        private Exception _workingException;
        private Thread _workingThread;

        public IActivityHost ActivityHost { get; internal set; }

        public Dispatcher WorkingDispatcher
        {
            get { return Tool.System.Dispatcher.ForThread(this._workingThread); }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            this.MainWindow = new ActivityHostWindow(this);
            this.ActivityHost = (IActivityHost) this.MainWindow;
            this.MainWindow.Closing += this.MainWindow_Closing;
            this.DispatcherUnhandledException += this.OnDispatcherUnhandledException;

            this.InitializeWorkingDispatcher();

            this.Dispatcher.BeginInvoke(new Action(() => this.MainWindow.Show()));
        }

        private void InitializeWorkingDispatcher()
        {
            this._workingThread = new Thread(() =>
            {
                this._initSynchronizer.Release();
                this.RunDispatcher();
            });
            this._workingThread.Start();

            this._initSynchronizer.Wait();

            while (this.WorkingDispatcher == null)
                new ManualResetEvent(false).WaitOne(TimeSpan.FromTicks(10));
        }

        private void RunDispatcher()
        {
            try
            {
                Tool.System.Dispatcher.Run();
            }
            catch (ThreadAbortException)
            {
                throw;
            }
            catch (Exception ex)
            {
                this._workingException = ex;
                this.RunDispatcher();
            }
        }

        private void MainWindow_Closing(object sender, CancelEventArgs e)
        {
            e.Cancel = this.ActivityHost.ProgressHandler != null || !this.OnClosing();
        }

        protected virtual bool OnClosing()
        {
            return true;
        }

        protected override void OnExit(ExitEventArgs e)
        {
            base.OnExit(e);

            if (this._workingThread.IsAlive)
            {
                this.WorkingDispatcher.Stop();
            }
        }

        private void OnDispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            OutputProvider.WriteException(e.Exception);
            e.Handled = true;
        }

        protected void ServiceInvoke(IService service, Action action)
        {
            var s = (ServiceBase) service;

            var dO = ServiceBase.BeginInvoke(s, action);

            var frame = new DispatcherFrame();
            var isFinished = false;

            dO.Finished += (sender, args) => isFinished = true;

            while (!isFinished)
            {
                frame.Continue = true;
                System.Windows.Threading.Dispatcher.CurrentDispatcher.BeginInvoke(DispatcherPriority.Background,
                    new Action(() => { frame.Continue = false; }));
                System.Windows.Threading.Dispatcher.PushFrame(frame);
            }

            s.CheckForError();
        }

        public void Invoke(ApplicationInvoke action)
        {
            action.Invoke();
        }

        public void ProgressInvoke(ApplicationInvoke action)
        {
            this.ProgressInvoke(action, null);
        }

        public void ProgressInvoke(ApplicationInvoke action, string text)
        {
            this.ActivityHost.LoadActivity(new ProgressActivityReference
            {
                ProgressAction = (@ref, handler) =>
                {
                    handler.IsIndeterminate = true;
                    handler.Text = text;

                    this.Invoke(action);
                }
            });
        }

        private void CheckForError()
        {
            if (this._workingException != null)
            {
                var ex = this._workingException;
                this._workingException = null;
                throw ex;
            }
        }

        protected virtual void OnPropertyChanged(string propertyName)
        {
            var handler = this.PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public delegate void ApplicationInvoke();
}