﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace RalphsDotNet.Gui.Wpf.ActivityEngine.Converter
{
    public class StringToStaticResourceConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value as string != null && Application.Current.Resources.Contains(value))
            {
                return Application.Current.Resources[value];
            }

            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}