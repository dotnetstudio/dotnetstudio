﻿using System.Media;
using System.Text.RegularExpressions;
using System.Windows;
using ICSharpCode.AvalonEdit;

namespace RalphsDotNet.Gui.Wpf.Controls
{
    /// <summary>
    ///     Interaction logic for FindReplaceDialog.xaml
    /// </summary>
    public partial class FindAndReplaceDialog
    {
        private readonly TextEditor _editor;

        public FindAndReplaceDialog(TextEditor editor)
        {
            this.InitializeComponent();

            this._editor = editor;
        }

        private void FindNextClick(object sender, RoutedEventArgs e)
        {
            if (!this.FindNext(this._TxtFind.Text))
                SystemSounds.Beep.Play();
        }

        private void FindNext2Click(object sender, RoutedEventArgs e)
        {
            if (!this.FindNext(this._TxtFind2.Text))
                SystemSounds.Beep.Play();
        }

        private void ReplaceClick(object sender, RoutedEventArgs e)
        {
            var regex = this.GetRegEx(this._TxtFind2.Text);
            var input = this._editor.Text.Substring(this._editor.SelectionStart, this._editor.SelectionLength);
            var match = regex.Match(input);
            var replaced = false;
            if (match.Success && match.Index == 0 && match.Length == input.Length)
            {
                this._editor.Document.Replace(this._editor.SelectionStart, this._editor.SelectionLength,
                    this._TxtReplace.Text);
                replaced = true;
            }

            if (!this.FindNext(this._TxtFind2.Text) && !replaced)
                SystemSounds.Beep.Play();
        }

        private void ReplaceAllClick(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Are you sure you want to Replace All occurences of \"" +
                                this._TxtFind2.Text + "\" with \"" + this._TxtReplace.Text + "\"?",
                "Replace All", MessageBoxButton.OKCancel, MessageBoxImage.Question) == MessageBoxResult.OK)
            {
                var regex = this.GetRegEx(this._TxtFind2.Text, true);
                var offset = 0;
                this._editor.BeginChange();
                foreach (Match match in regex.Matches(this._editor.Text))
                {
                    this._editor.Document.Replace(offset + match.Index, match.Length, this._TxtReplace.Text);
                    offset += this._TxtReplace.Text.Length - match.Length;
                }
                this._editor.EndChange();
            }
        }

        private bool FindNext(string textToFind)
        {
            var regex = this.GetRegEx(textToFind);
            var start = regex.Options.HasFlag(RegexOptions.RightToLeft)
                ? this._editor.SelectionStart
                : this._editor.SelectionStart + this._editor.SelectionLength;
            var match = regex.Match(this._editor.Text, start);

            if (!match.Success) // start again from beginning or end
            {
                match = regex.Match(this._editor.Text, regex.Options.HasFlag(RegexOptions.RightToLeft) ? this._editor.Text.Length : 0);
            }

            if (match.Success)
            {
                this._editor.Select(match.Index, match.Length);
                var loc = this._editor.Document.GetLocation(match.Index);
                this._editor.ScrollTo(loc.Line, loc.Column);
            }

            return match.Success;
        }

        private Regex GetRegEx(string textToFind, bool leftToRight = false)
        {
            var options = RegexOptions.None;
            if (this._CbSearchUp.IsChecked == true && !leftToRight)
                options |= RegexOptions.RightToLeft;
            if (this._CbCaseSensitive.IsChecked == false)
                options |= RegexOptions.IgnoreCase;

            if (this._CbRegex.IsChecked == true)
            {
                return new Regex(textToFind, options);
            }
            var pattern = Regex.Escape(textToFind);
            if (this._CbWildcards.IsChecked == true)
                pattern = pattern.Replace("\\*", ".*").Replace("\\?", ".");
            if (this._CbWholeWord.IsChecked == true)
                pattern = "\\b" + pattern + "\\b";
            return new Regex(pattern, options);
        }

        public void ShowFind()
        {
            this._TabMain.SelectedIndex = 0;
        }

        public void ShowReplace()
        {
            this._TabMain.SelectedIndex = 1;
        }

        public void TakeSelection()
        {
            if (!this._editor.TextArea.Selection.IsMultiline)
            {
                this._TxtFind.Text = this._TxtFind2.Text = this._editor.TextArea.Selection.GetText();
                this._TxtFind.SelectAll();
                this._TxtFind2.SelectAll();
                this._TxtFind2.Focus();
            }
        }
    }
}