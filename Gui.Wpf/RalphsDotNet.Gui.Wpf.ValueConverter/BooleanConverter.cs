﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Data;

namespace RalphsDotNet.Gui.Wpf.ValueConverter
{
    public abstract class BooleanConverter<T> : IValueConverter
    {
        public T TrueValue { get; set; }
        public T FalseValue { get; set; }

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is bool && (bool)value)
                return this.TrueValue;
            if (value is bool && !(bool) value)
                return this.FalseValue;
            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter,
            System.Globalization.CultureInfo culture)
        {
            if (Equals(this.TrueValue, value))
                return true;
            if (Equals(this.FalseValue, value))
                return false;
            return null;
        }
    }

    public class BooleanToVisibilityConverter : BooleanConverter<Visibility>
    {
        public BooleanToVisibilityConverter()
        {
            this.TrueValue = Visibility.Visible;
            this.FalseValue = Visibility.Collapsed;
        }
    }

    public class NotBooleanToVisibilityConverter : BooleanConverter<Visibility>
    {
        public NotBooleanToVisibilityConverter()
        {
            this.TrueValue = Visibility.Collapsed;
            this.FalseValue = Visibility.Visible;
        }
    }

    public class InverseBooleanConverter : BooleanConverter<bool>
    {
        public InverseBooleanConverter()
        {
            this.TrueValue = false;
            this.FalseValue = true;
        }
    }
}
