﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyCompany("RalphsTech")]
[assembly: AssemblyCopyright("Copyright © Ralph Alexander Bariz 2014")]
[assembly: ComVisible(false)]