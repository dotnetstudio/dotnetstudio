﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RalphsDotNet.Service
{
    [AttributeUsage(AttributeTargets.Method)]
    public class NonBlockingAttribute : Attribute
    {
    }

    [AttributeUsage(AttributeTargets.Method)]
    public class NonInvokingAttribute : Attribute
    {
    }
}
