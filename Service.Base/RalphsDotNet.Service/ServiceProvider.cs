using System;
using System.Collections.Generic;
using System.Linq;
using Castle.DynamicProxy;
using RalphsDotNet.Extension;

namespace RalphsDotNet.Service
{
    public static class ServiceProvider
    {
        private static readonly Random Rnd = new Random();
        private static readonly ProxyGenerator ProxyGenerator = new ProxyGenerator();

        private static readonly Dictionary<string, IService> Services = new Dictionary<string, IService>();

        static ServiceProvider()
        {
            Initialize();
        }

        public static ServicesConfiguration ServiceConfigurations { get; private set; }

        public static T GetService<T>(string id) where T : IService
        {
            if (Services.ContainsKey(id))
            {
                return (T) Services[id];
            }

            ServiceConfiguration configuration = ServiceConfigurations.Services.FirstOrDefault(c => c.Id == id);

            if (configuration != null)
            {
                System.Type foundType =
                    configuration.ServiceAssemblyName.FindAssembly()
                        .GetTypes()
                        .First(t => t.GetInterfaces().Any(i => i == configuration.ServiceType));

                var serviceProxy = (T) GetServiceProxy(foundType, configuration);

                Services.Add(id, serviceProxy);

                return serviceProxy;
            }

            throw (new ConfigurationException("no service with id <" + id + "> found in configuration"));
        }

        public static T GetService<T>(ServiceConfiguration configuration) where T : IService
        {
            if (string.IsNullOrEmpty(configuration.Id) || !Services.ContainsKey(configuration.Id))
            {
                if (string.IsNullOrEmpty(configuration.Id))
                    configuration.Id = (Rnd).Next().ToString();

                System.Type foundType =
                    configuration.ServiceAssembly.GetTypes()
                        .First(t => t.GetInterfaces().Any(i => i == configuration.ServiceType));

                var serviceProxy = (T) GetServiceProxy(foundType, configuration);
                Services.Add(configuration.Id, serviceProxy);

                return serviceProxy;
            }

            return (T) Services[configuration.Id];
        }

        public static bool CheckConfigurationValidity(ServiceConfiguration configuration)
        {
            // TODO check recursive for validation attributes and validate them
            return true;
        }

        private static IService GetServiceProxy(System.Type serviceType, ServiceConfiguration configuration)
        {
            var serviceProxy = (ServiceBase) ProxyGenerator.CreateClassProxy(typeof (ServiceBase), new Type[] {serviceType},
                new ProxyGenerationOptions(), new ServiceInterceptor());

            serviceProxy.CreateService(serviceType, configuration);

            return serviceProxy;
        }

        public static void Initialize()
        {
            foreach (IService service in Services.Values)
                DisposeService(service.Id);

            ServiceConfigurations = Configuration.Load<ServicesConfiguration>();

            if (ServiceConfigurations == null)
                ServiceConfigurations = new ServicesConfiguration();
        }

        public static void DisposeService(string id)
        {
            if (Services.ContainsKey(id))
            {
                Services[id].Dispose();
            }
        }

        internal static void ServiceDisposing(string id)
        {
            Services.Remove(id);
        }
    }
}