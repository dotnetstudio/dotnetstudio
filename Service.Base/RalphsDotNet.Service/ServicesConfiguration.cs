﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace RalphsDotNet.Service
{
    [DataContract]
    public class ServicesConfiguration
    {
        [DataMember(Order = 0)]
        public ServiceConfiguration[] Services { get; set; }

        public ServiceConfiguration this[string id]
        {
            get
            {
                return this.Services != null ? this.Services.FirstOrDefault(c => c.Id == id) : null;
            }
        }

        public void AddOrReplace(ServiceConfiguration serviceConfiguration)
        {
            if (this[serviceConfiguration.Id] != null)
                this.Remove(serviceConfiguration.Id);

            var list = this.Services != null ? this.Services.ToList() : new List<ServiceConfiguration>();
            list.Add(serviceConfiguration);
            this.Services = list.ToArray();
        }

        public void Remove(string id)
        {
            if (this.Services == null || this.Services.All(c => c.Id != id))
                throw new ArgumentException(string.Format("there's no configuration with id \"{0}\" existing", id));

            this.Services = this.Services.ToList().Where(c => c.Id != id).ToArray();
        }
    }
}
