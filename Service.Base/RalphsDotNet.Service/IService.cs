﻿using System;
using System.ComponentModel;
using System.Reflection;
using System.Runtime.Serialization;
using RalphsDotNet.Extension;
using RalphsDotNet.Types;

namespace RalphsDotNet.Service
{
    [DataContract]
    public abstract class ServiceConfiguration : NotifyPropertyChangingPropertyChangedBase
    {
        private string _id;

        private string _serviceAssemblyName;

        [DataMember(Order = 0), Browsable(false)]
        public string Id
        {
            get { return this._id; }
            set
            {
                if (this._id != value)
                {
                    this.OnPropertyChanging("Id");
                    this._id = value;
                    this.OnPropertyChanged("Id");
                }
            }
        }

        [DataMember(Order = 1), Browsable(false)]
        public string ServiceAssemblyName
        {
            get { return this._serviceAssemblyName; }
            set
            {
                if (this._serviceAssemblyName != value)
                {
                    this.OnPropertyChanging("ServiceAssemblyName");
                    this.OnPropertyChanging("ServiceAssembly");
                    this._serviceAssemblyName = value;
                    this.OnPropertyChanged("ServiceAssemblyName");
                    this.OnPropertyChanged("ServiceAssembly");
                }
            }
        }

        [Browsable(false)]
        public Assembly ServiceAssembly
        {
            get { return this.ServiceAssemblyName.FindAssembly(); }
            set { this.ServiceAssemblyName = value.GetName().Name; }
        }

        [Browsable(false)]
        public abstract System.Type ServiceType { get; }
    }

    public interface IService : IServiceObject
    {
        /// <summary>
        ///     Gets the service instance configuration identifier.
        /// </summary>
        /// <value>
        ///     The identifier.
        /// </value>
        string Id { get; }

        /// <summary>
        ///     Gets or sets the configuration.
        /// </summary>
        /// <value>
        ///     The configuration.
        /// </value>
        ServiceConfiguration ServiceConfiguration { get; }
    }

    public interface IServiceObject : IDisposable
    {
    }
}