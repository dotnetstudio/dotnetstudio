using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Castle.DynamicProxy;
using RalphsDotNet.Extension;

namespace RalphsDotNet.Service
{
    public class ServiceInterceptor : IInterceptor
    {
        private static readonly Dictionary<MethodInfo, bool> ShouldInvokeBuffer = new Dictionary<MethodInfo, bool>();
        private static readonly Dictionary<MethodInfo, bool> AsyncBuffer = new Dictionary<MethodInfo, bool>();

        public void Intercept(IInvocation invocation)
        {
            if (invocation.Method.IsPublic)
            {
                bool shouldInvoke;
                lock (ShouldInvokeBuffer)
                    if (!ShouldInvokeBuffer.TryGetValue(invocation.Method, out shouldInvoke))
                    {
                        shouldInvoke =
                            !invocation.Method.GetCustomAttributes<NonInvokingAttribute>().Any();
                        ShouldInvokeBuffer.Add(invocation.Method, shouldInvoke);
                    }

                bool async;
                lock (AsyncBuffer)
                    if (!AsyncBuffer.TryGetValue(invocation.Method, out async))
                    {
                        async =
                            invocation.Method.GetCustomAttributes<NonBlockingAttribute>().Any();
                        AsyncBuffer.Add(invocation.Method, async);
                    }

                var serviceBase = invocation.Proxy is ServiceBase
                    ? (ServiceBase) invocation.Proxy
                    : ((ServiceObject) invocation.Proxy).Base;

                if (AsyncBuffer[invocation.Method])
                    serviceBase.BeginInvoke(invocation.Method, (ServiceObject) invocation.Proxy,
                        invocation.Arguments);
                else
                    invocation.ReturnValue = serviceBase.Invoke(invocation.Method, (ServiceObject) invocation.Proxy,
                        invocation.Arguments, ShouldInvokeBuffer[invocation.Method]);
            }
            else
            {
                invocation.Proceed();
            }
        }
    }
}