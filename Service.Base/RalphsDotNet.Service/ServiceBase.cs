using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices.ComTypes;
using System.Threading;
using Castle.DynamicProxy;
using RalphsDotNet.Tool.System;

namespace RalphsDotNet.Service
{
    public class ServiceObject : IServiceObject
    {
        internal readonly Dictionary<MethodInfo, Delegate> MethodLookupDictionary =
            new Dictionary<MethodInfo, Delegate>();

        public ServiceBase Base { get; internal set; }
        public IServiceObject Object { get; internal set; }

        public void Dispose()
        {
            if (this.Base != null)
            {
                this.Base.Invoke(typeof (IServiceObject).GetMethod("Dispose"), this, new object[] {});
            }
            this.OnDispose();
        }

        protected virtual void OnDispose()
        {
        }
    }

    public class ServiceBase : ServiceObject, IService
    {
        private static readonly ProxyGenerator ProxyGenerator = new ProxyGenerator();
        private readonly Synchronizer _initSynchronizer = new Synchronizer();
        private List<DispatcherOperation> _operations = new List<DispatcherOperation>();
        private Thread _thread;

        protected ServiceBase()
        {
        }

        public Exception RuntimeError { get; set; }

        public Dispatcher Dispatcher
        {
            get { return Dispatcher.ForThread(this._thread); }
        }

        public string Id
        {
            get { return this.ServiceConfiguration.Id; }
        }

        public ServiceConfiguration ServiceConfiguration
        {
            get { return ((IService) this.Object).ServiceConfiguration; }
        }

        private void InitializeDispatcher()
        {
            this._thread = new Thread(() =>
            {
                this._initSynchronizer.Release();
                Dispatcher.Run();
            });

            this._thread.Start();

            this._initSynchronizer.Wait();

            while (this.Dispatcher == null)
                new ManualResetEvent(false).WaitOne(TimeSpan.FromTicks(10));
        }

        private Delegate GetInvokingAction(MethodInfo invocationMethodInfo, ServiceObject serviceObject)
        {
            lock (serviceObject.MethodLookupDictionary)
            {
                Delegate del;
                if (!serviceObject.MethodLookupDictionary.TryGetValue(invocationMethodInfo, out del))
                {
                    var parameters =
                        invocationMethodInfo.GetParameters().Select(p => p.ParameterType).ToList();

                    Type delegateType;
                    if (invocationMethodInfo.ReturnType != null && invocationMethodInfo.ReturnType != typeof (void))
                    {
                        parameters.Add(invocationMethodInfo.ReturnType);
                        delegateType =
                            typeof (object).Assembly.GetTypes()
                                .First(
                                    t =>
                                        t.Name.StartsWith("Func`") &&
                                        t.GetGenericArguments().Count() == parameters.Count())
                                .MakeGenericType(parameters.ToArray());
                    }
                    else if (parameters.Count > 0)
                    {
                        delegateType =
                            typeof (object).Assembly.GetTypes()
                                .First(
                                    t =>
                                        t.Name.StartsWith("Action`") &&
                                        t.GetGenericArguments().Count() == parameters.Count())
                                .MakeGenericType(parameters.ToArray());
                    }
                    else
                    {
                        delegateType = typeof (Action);
                    }

                    del = Delegate.CreateDelegate(delegateType, serviceObject.Object, invocationMethodInfo);
                    serviceObject.MethodLookupDictionary.Add(invocationMethodInfo, del);
                }

                return del;
            }
        }

        internal object Invoke(MethodInfo invocationMethodInfo, ServiceObject serviceObject, object[] args,
            bool shouldInvoke = true)
        {
            return this.InvokeInternal(invocationMethodInfo, serviceObject, args, false, shouldInvoke);
        }

        internal void BeginInvoke(MethodInfo invocationMethodInfo, ServiceObject serviceObject, object[] args)
        {
            this.InvokeInternal(invocationMethodInfo, serviceObject, args, true);
        }

        private object InvokeInternal(MethodInfo invocationMethodInfo, ServiceObject serviceObject, object[] args,
            bool async, bool shouldInvoke = true)
        {
            object internalRet;
            var del = this.GetInvokingAction(invocationMethodInfo, serviceObject);
            if (this._thread != Thread.CurrentThread && this._thread.IsAlive)
            {
                lock (this._thread)
                {
                    var invokerArgs = new List<object> {del, args.Any() ? this.DeProxyArgs(args) : null};

                    if (async)
                    {
                        internalRet =
                            this.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                                new Func<Delegate, object[], object>(this.InvokeInDispatcher), invokerArgs.ToArray());

                        lock (this._operations)
                        {
                            this._operations =
                                this._operations.Where(
                                    o =>
                                        o.Status == DispatcherOperationStatus.Executing ||
                                        o.Status == DispatcherOperationStatus.Pending).ToList();

                            if (((internalRet as DispatcherOperation).Status == DispatcherOperationStatus.Pending ||
                                 (internalRet as DispatcherOperation).Status == DispatcherOperationStatus.Executing))
                                this._operations.Add(internalRet as DispatcherOperation);
                        }
                    }
                    else if (shouldInvoke)
                    {
                        var dO = this.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                            new Func<Delegate, object[], object>(this.InvokeInDispatcher),
                            invokerArgs.ToArray());

                        this.WaitFordO(dO);

                        internalRet = dO.Result;

                        this.CheckForError();
                    }
                    else
                    {
                        internalRet = del.Method.Invoke(del.Target, this.DeProxyArgs(args));
                    }
                }
            }
            else
            {
                internalRet = args.Any() ? del.DynamicInvoke(this.DeProxyArgs(args)) : del.DynamicInvoke();
            }

            if (invocationMethodInfo.Name == "Dispose")
                this.Dispose();

            var ret = internalRet != null && internalRet as DispatcherOperation == null &&
                      !internalRet.GetType().IsValueType
                ? this.ProxyRet(internalRet)
                : internalRet;

            return ret;
        }

        private void WaitFordO(DispatcherOperation dO)
        {
            dO.Wait();

            if (dO.Status != DispatcherOperationStatus.Completed)
            {
                throw (new ApplicationException("service call died", dO.Error));
            }
        }

        private object InvokeInDispatcher(Delegate del, object[] args)
        {
            if (this.RuntimeError == null)
            {
                try
                {
                    return del.Method.Invoke(del.Target, args ?? new object[] {});
                }
                catch (ThreadAbortException)
                {
                    throw;
                }
                catch (Exception ex)
                {
                    this.RuntimeError = ex;
                }
            }

            return null;
        }

        private object ProxyRet(object internalRet)
        {
            if (internalRet as IServiceObject != null && internalRet as IService == null &&
                internalRet as IDataObject != null)
            {
                var ret = ProxyGenerator.CreateClassProxy(typeof (ServiceObject), new[] {internalRet.GetType()},
                    new ProxyGenerationOptions(), new ServiceInterceptor());

                ((ServiceObject) ret).Object = (IServiceObject) internalRet;
                ((ServiceObject) ret).Base = this;

                return ret;
            }

            return internalRet;
        }

        private object[] DeProxyArgs(object[] args)
        {
            return args.Select(a => a as ServiceObject != null ? ((ServiceObject) a).Object : a).ToArray();
        }

        internal void CreateService(Type serviceType, ServiceConfiguration configuration)
        {
            this.InitializeDispatcher();

            lock (this._thread)
            {
                this.Dispatcher.Invoke(DispatcherPriority.Normal,
                    new Action(
                        () => { this.Object = (IService) Activator.CreateInstance(serviceType, this, configuration); }));

                this.CheckForError();
            }
        }

        /// <summary>
        ///     invoke an action in service context
        /// </summary>
        /// <param name="service"></param>
        /// <param name="action"></param>
        public static void Invoke(IService service, Action action)
        {
            var s = (ServiceBase) service;
            if (s._thread != Thread.CurrentThread)
            {
                lock (s.Dispatcher)
                {
                    var dO = s.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                        new Action(() => s.InvokeInDispatcher(action, null)));

                    s.WaitFordO(dO);

                    s.CheckForError();
                }
            }
            else
            {
                action.DynamicInvoke();
            }
        }

        public void CheckForError()
        {
            if (this.RuntimeError != null)
            {
                var ex = this.RuntimeError;
                this.RuntimeError = null;
                throw ex;
            }
        }

        /// <summary>
        ///     invoke an action in service context
        /// </summary>
        /// <param name="service"></param>
        /// <param name="action"></param>
        public static DispatcherOperation BeginInvoke(IService service, Action action)
        {
            var s = (ServiceBase) service;
            if (s._thread != Thread.CurrentThread)
            {
                lock (s.Dispatcher)
                {
                    return s.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                        new Action(() => s.InvokeInDispatcher(action, null)));
                }
            }
            else
            {
                action.DynamicInvoke();
            }

            return null;
        }

        ~ServiceBase()
        {
            this.Dispose();
        }

        protected override void OnDispose()
        {
            GC.SuppressFinalize(this);

            lock (this._thread)
            {
                this._operations.Where(
                    o =>
                        o.Status == DispatcherOperationStatus.Pending || o.Status == DispatcherOperationStatus.Executing)
                    .ToList()
                    .ForEach(o => o.Abort());

                if (this._thread.IsAlive)
                {
                    this.Dispatcher.Stop();

                    this.CheckForError();
                }
            }

            ServiceProvider.ServiceDisposing(this.Id);
        }
    }

    internal class ServiceInvocationException : Exception
    {
        public ServiceInvocationException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}