using System;
using System.Linq;
using System.Runtime.Serialization;
using System.Reflection;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using RalphsDotNet.Extension;

namespace RalphsDotNet.Service
{
    /// <summary>
    /// Configuration service.
    /// </summary>
    public static class Configuration
    {
        private static string _configDir = Directory.GetCurrentDirectory();

        private static string _resourcePrefix = string.Empty;

        private static Assembly _resourceAssembly;

        /// <summary>
        /// Sets the directory containing the configuration files.
        /// </summary>
        /// <param name='dir'>
        /// The directory path.
        /// </param>
        public static void SetDirectory(string dir)
        {
            _configDir = dir;
        }

        public static void SetResourcePrefix(string prefix)
        {
            _resourcePrefix = prefix;
        }

        /// <summary>
        /// Sets the assembly containing the configuration files as embedded ressources.
        /// This assembly resources are used as fallback configuration files if a file is not found in the set directory.
        /// </summary>
        /// <param name='assembly'>
        /// The resource assembly.
        /// </param>
        public static void SetResourceAssembly(Assembly assembly)
        {
            _resourceAssembly = assembly;
        }

        public static bool Exists<T>(string configName = null)
        {
            configName = configName ?? typeof(T).FullName;
            var name = configName + ".config.xml";
            var path = Path.Combine(_configDir, name);
            var resourceName = _resourceAssembly != null
                                      ? string.Concat(_resourceAssembly.GetName().Name, ".", _resourcePrefix, name)
                                      : null;

            return File.Exists(path)
                   || (!string.IsNullOrEmpty(resourceName)
                       && _resourceAssembly.GetManifestResourceInfo(resourceName) != null);
        }

        /// <summary>
        /// Loads a configuration object.
        /// </summary>
        /// <returns>
        /// The configuration object.
        /// </returns>
        /// <typeparam name='T'>
        /// Type of the configuration object.
        /// </typeparam>
        public static T Load<T>(string configName = null) where T : class
        {
            configName = configName ?? typeof(T).FullName;
            var name = configName + ".config.xml";
            var tname = configName + ".types.xml";
            var path = Path.Combine(_configDir, name);
            var tpath = Path.Combine(_configDir, tname);
            var resourceName = _resourceAssembly != null
                                   ? string.Concat(_resourceAssembly.GetName().Name, ".", _resourcePrefix, name)
                                   : null;
            var tresourceName = _resourceAssembly != null
                                    ? string.Concat(_resourceAssembly.GetName().Name, ".", _resourcePrefix, tname)
                                    : null;

            string config;
            string types;

            if (File.Exists(tpath))
            {
                types = File.ReadAllText(tpath);
            }
            else if (!string.IsNullOrEmpty(tresourceName)
                     && _resourceAssembly.GetManifestResourceInfo(tresourceName) != null)
            {
                using (var resourceStream = _resourceAssembly.GetManifestResourceStream(resourceName))
                {
                    Debug.Assert(resourceStream != null, "resourceStream != null");
                    var sr = new StreamReader(resourceStream);
                    types = sr.ReadToEnd();
                }
            }
            else
            {
                return null;
            }

            if (File.Exists(path))
            {
                config = File.ReadAllText(path);
            }
            else if (!string.IsNullOrEmpty(resourceName)
                     && _resourceAssembly.GetManifestResourceInfo(resourceName) != null)
            {
                using (var resourceStream = _resourceAssembly.GetManifestResourceStream(resourceName))
                {
                    Debug.Assert(resourceStream != null, "resourceStream != null");
                    var sr = new StreamReader(resourceStream);
                    config = sr.ReadToEnd();
                }
            }
            else
            {
                return null;
            }

            return Load<T>(config, types);
        }

        public static T Load<T>(string config, string types) where T : class
        {
            IEnumerable<System.Type> knownTypes;
            T retVal;

            var typesByteArray = Encoding.Default.GetBytes(types);
            using (var typesStream = new MemoryStream(typesByteArray)) knownTypes = typesStream.DeserializeDataContract<KnownTypesRegister>(null).ResolvedKnownTypes;

            var configByteArray = Encoding.Default.GetBytes(config);
            using (var configStream = new MemoryStream(configByteArray)) retVal = configStream.DeserializeDataContract<T>(knownTypes);

            return retVal;
        }

        /// <summary>
        /// Saves the specified configuration object.
        /// </summary>
        /// <param name='config'>
        /// The configuration object to save.
        /// </param>
        /// <param name="configName">different name of the configuration file, null = default</param>
        /// <typeparam name='T'>
        /// Type of the configuration object.
        /// </typeparam>
        public static void Save<T>(T config, string configName = null) where T : class
        {
            configName = configName ?? typeof(T).FullName;
            var name = configName + ".config.xml";
            var tname = configName + ".types.xml";
            var path = Path.Combine(_configDir, name);
            var tpath = Path.Combine(_configDir, tname);

            var knownTypes = config.GetKnownTypes().ToArray();

            if (File.Exists(tpath)) File.Delete(tpath);
            using (var fs = File.OpenWrite(tpath))
            {
                (new KnownTypesRegister(knownTypes)).SerializeDataContract(fs, null);
            }

            if (File.Exists(tpath)) File.Delete(path);
            using (var fs = File.OpenWrite(path))
            {
                config.SerializeDataContract(fs, knownTypes);
            }
        }
    }

    [DataContract]
    internal class KnownTypesRegister
    {
        public IEnumerable<System.Type> ResolvedKnownTypes
        {
            get
            {
                return this.KnownTypes.Select(knownType => knownType.FindType());
            }
        }

        [DataMember]
        public string[] KnownTypes { get; private set; }

        internal KnownTypesRegister(IEnumerable<System.Type> knownTypes)
        {
            this.KnownTypes = knownTypes.Select(t => t.GetResolvingName()).ToArray();
        }
    }

    /// <summary>
    /// Object to store dynamic configuration values.
    /// </summary>
    [DataContract]
    public class ConfigurationValue
    {
        [DataMember]
        public string Key { get; set; }

        [DataMember]
        public string Value { get; set; }
    }

    public class ConfigurationException : Exception
    {
        public ConfigurationException(string message)
            : base(message)
        {
        }
    }
}