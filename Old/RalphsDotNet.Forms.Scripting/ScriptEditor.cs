﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using RalphsDotNet.Data.Scripting;

namespace RalphsDotNet.Forms.Scripting
{
    public partial class ScriptEditor : EditorControl
    {
        protected static Property ScriptProperty = new Property("Script", typeof(IScript), null);

        public IScript Script { get { return this.GetValue(ScriptProperty) as IScript; } set { this.SetValue(ScriptProperty, value); } }
        public string ScriptCode { get { return this.textEditorControl.Text; } }

        public ScriptEditor()
        {
            InitializeComponent();
        }

        protected override void OnPropertyChanged(Property property, object newValue, object oldValue)
        {
            base.OnPropertyChanged(property, newValue, oldValue);

            if (property == ScriptProperty)
            {
                if (oldValue != null)
                {
                    ((IScript)oldValue).PropertyChanged -= script_PropertyChanged;
                }

                if (newValue != null)
                {
                    ((IScript)newValue).PropertyChanged += script_PropertyChanged;
                    this.textEditorControl.Text = this.Script.Code;
                    this.textEditorControl.SetHighlighting(this.Script.Language.ToString());
                    this.textEditorControl.Enabled = true;
                }
                else
                {
                    this.textEditorControl.Enabled = false;
                }
            }
        }

        void script_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Code" && !this._editing)
            {
                if (this.IsReloadAllowed)
                {
                    this.textEditorControl.Text = this.Script.Code;
                }
            }
        }

        public void Cut()
        {
            var selectedText = this.textEditorControl.ActiveTextAreaControl.SelectionManager.SelectedText;

            if (!string.IsNullOrEmpty(selectedText))
            {
                Clipboard.SetText(selectedText);
                this.textEditorControl.ActiveTextAreaControl.SelectionManager.RemoveSelectedText();
            }
            else
            {
                Clipboard.Clear();
            }
        }

        public void Copy()
        {
            var selectedText = this.textEditorControl.ActiveTextAreaControl.SelectionManager.SelectedText;

            if (!string.IsNullOrEmpty(selectedText))
            {
                Clipboard.SetText(selectedText);
            }
            else
            {
                Clipboard.Clear();
            }
        }

        public void Paste()
        {
            this.textEditorControl.ActiveTextAreaControl.SelectionManager.RemoveSelectedText();
            this.textEditorControl.ActiveTextAreaControl.Document.Insert(this.textEditorControl.ActiveTextAreaControl.Caret.Offset, Clipboard.GetText());
        }

        public void Undo()
        {
            this.textEditorControl.Undo();
        }

        public void Redo()
        {
            this.textEditorControl.Redo();
        }

        public void JumpTo(int lineIndex, int columnIndex)
        {
            this.textEditorControl.ActiveTextAreaControl.JumpTo(lineIndex, columnIndex);
        }

        private bool _editing;
        private void textEditorControl_TextChanged(object sender, EventArgs e)
        {
            this._editing = true;
            this.Script.Code = this.textEditorControl.Text;
            this._editing = false;
            this.SendSaveRequired();
        }
    }
}
