﻿using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace RalphsDotNet.Forms.Editors
{
    public partial class PropertyEditor : UserControl
    {
        private Data.IDataObject _entity;
        public Data.IDataObject Entity
        {
            get { return this._entity; }
            set
            {
                if (this._entity != value)
                {
                    this.OnPropertyChanging("Entity");

                    if (this._entity != null)
                        this._entity.PropertyChanged -= entity_PropertyChanged;

                    this._entity = value;

                    if (this._entity != null)
                    {
                        this._entity.PropertyChanged += entity_PropertyChanged;

                        this.propertyGrid.SelectedObject = this.Entity;

                        this.propertyGrid.Enabled = true;

                        this.Text = this.Entity.GetType().Name;
                    }
                    else
                    {
                        this.propertyGrid.Enabled = false;
                        this.propertyGrid.SelectedObject = null;
                    }

                    this.OnPropertyChanged("Entity");
                }
            }
        }

        void entity_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            throw new NotImplementedException();
        }

        public PropertyEditor()
        {
            InitializeComponent();
        }

        private void propertyGrid_PropertyValueChanged(object s, PropertyValueChangedEventArgs e)
        {
            this._entity = this.propertyGrid.SelectedObject as Data.IDataObject;
        }
    }
}
