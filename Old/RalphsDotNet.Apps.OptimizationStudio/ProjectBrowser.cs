﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using DockDotNET;
using RalphsDotNet.Apps.OptimizationStudio.BusinessLogic;

namespace RalphsDotNet.Apps.OptimizationStudio
{
    public partial class ProjectBrowser : DockWindow
    {
        private readonly MainWindow _mainWindow;

        public OptimizationProject Project { get { return this._mainWindow != null ? this._mainWindow.Project : null; } }

        public string SelectedScript {
            get { return this.treeView.SelectedNode != null && this.treeView.SelectedNode.Parent != null && this.treeView.SelectedNode.Parent.Text == OptimizationProject.ScriptsDirectory ? this.treeView.SelectedNode.Text : null; }
            set
            {
                this.treeView.SelectedNode = this.treeView.Nodes.Cast<TreeNode>().First(tn => tn.Text == OptimizationProject.ScriptsDirectory).Nodes.Cast<TreeNode>().First(tn => tn.Text == value);
            }
        }

        public ProjectBrowser(MainWindow mainWindow)
        {
            this._mainWindow = mainWindow;
            this._mainWindow.PropertyChanging += mainWindow_PropertyChanging;
            this._mainWindow.PropertyChanged += mainWindow_PropertyChanged;

            InitializeComponent();
        }

        void mainWindow_PropertyChanging(object sender, PropertyChangingEventArgs e)
        {
            if (e.PropertyName == "Project")
            {
                if (this.Project != null)
                {
                    this.Project.PropertyChanged -= Project_PropertyChanged;
                    this.Project.ScriptNames.CollectionChanged -= ScriptNames_CollectionChanged;
                }
            }
        }

        private void mainWindow_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Project")
            {
                if (this.Project != null)
                {
                    this.Project.PropertyChanged += Project_PropertyChanged;
                    this.Project.ScriptNames.CollectionChanged += ScriptNames_CollectionChanged;
                    this.FillTree();
                }
                else
                {
                    this.ClearTree();
                }
            }
        }

        void Project_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
        }

        void ScriptNames_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            this.ClearTree();
            this.FillTree();
        }

        private void FillTree()
        {
            var scripts = this.treeView.Nodes.Add(OptimizationProject.ScriptsDirectory, OptimizationProject.ScriptsDirectory, 0, 0);

            foreach (var script in this.Project.ScriptNames.OrderBy(x => x))
            {
                scripts.Nodes.Add(script, script, 1, 1);
            }
            
            scripts.Expand();

            this.treeView.Enabled = true;
        }

        private void ClearTree()
        {
            this.treeView.Enabled = false;

            this.treeView.Nodes.Clear();
        }

        private void treeView_DoubleClick(object sender, EventArgs e)
        {
            this._mainWindow.OpenScriptEditor(this.SelectedScript);
        }
    }
}
