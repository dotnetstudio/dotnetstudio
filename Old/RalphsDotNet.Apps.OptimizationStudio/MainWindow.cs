﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using System.IO;

using RalphsDotNet.Apps.OptimizationStudio.BusinessLogic;

namespace RalphsDotNet.Apps.OptimizationStudio
{
    using System.Globalization;

    using Properties;

    public partial class MainWindow : Form, INotifyPropertyChanging, INotifyPropertyChanged
    {
        private readonly ProjectBrowser _projectBrowser;
        internal Dictionary<string, ScriptEditor> ScriptEditors;

        private readonly OptimizationConfigurationEditor _optimizationConfigurationEditor;

        private readonly ScriptOutputView _scriptOutputView;

        private readonly SearchPanel _searchPanel;

        private PopulationPlotViewer _populationPlotViewer;
        private CurvePlotViewer _curvePlotViewer;

        private ScriptEditor _activeScriptEditor;
        public ScriptEditor ActiveScriptEditor
        {
            get { return this._activeScriptEditor; }
            internal set
            {
                if (this._activeScriptEditor != value)
                {
                    this.OnPropertyChanging("ActiveScriptEditor");
                    this._activeScriptEditor = value;
                    this.SwitchEditElementsState(this._activeScriptEditor != null);
                    this.OnPropertyChanged("ActiveScriptEditor");
                }
            }
        }

        private OptimizationProject _project;

        public OptimizationProject Project
        {
            get { return this._project; }
            private set
            {
                if (this._project != value)
                {
                    this.OnPropertyChanging("Project");

                    if (this.Project != null)
                        this.Project.PropertyChanged -= this.ProjectPropertyChanged;

                    this._project = value;
                    this.SwitchScriptsElementsState(this.Project != null);

                    if (this.Project != null)
                    {
                        this.Project.PropertyChanged += this.ProjectPropertyChanged;

                        this.OpenPlotViewer();
                        this.OpenScriptEditor(string.Concat(OptimizationProject.MainScriptName, OptimizationProject.ScriptExtension));
                    }

                    this.OnPropertyChanged("Project");
                }
            }
        }

        delegate void Changed();
        void ProjectPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "IsRunning")
            {
                this.Invoke(new Changed(this.ProjectRunningChanged));
            }
        }

        private DateTime? _optimizationStartTime;
        void ProjectRunningChanged()
        {
            this.ToolStripMenuItem_File_Quit.Enabled = !this.Project.IsRunning;
            this.ScriptEditors.Values.ToList().ForEach(se => se.Enabled = !this.Project.IsRunning);
            this.toolStripMenuItem_Optimization_Run.Enabled = this.toolStripButton_RunOptimization.Enabled = !this.Project.IsRunning;
            this.toolStripMenuItem_Optimization_Stop.Enabled = this.toolStripButton_StopOptimization.Enabled = this.Project.IsRunning;

            if (!this.Project.IsRunning && this.Project.RuntimeError != null)
            {
#if DEBUG
                MessageBox.Show(this.Project.RuntimeError.ToString(), Resources.MainWindow_OptimizationError_Title, MessageBoxButtons.OK, MessageBoxIcon.Error);
#else
                    MessageBox.Show(this.Project.RuntimeError.Message, "An optimization error occured!", MessageBoxButtons.OK, MessageBoxIcon.Error);
#endif
            }

            if (this._project.IsRunning)
            {
                this._optimizationStartTime = DateTime.UtcNow;
                
                lock(this.timer)
                    this.timer.Start();
            }
            else
            {
                this._optimizationStartTime = null;
            }
        }

        public MainWindow()
        {
            this._projectBrowser = new ProjectBrowser(this);
            this._optimizationConfigurationEditor = new OptimizationConfigurationEditor(this);
            this.ScriptEditors = new Dictionary<string, ScriptEditor>();

            InitializeComponent();

            this.dockManager.DockWindow(this._projectBrowser, DockStyle.Right);
            this._projectBrowser.HostContainer.DockWindow(this._optimizationConfigurationEditor, DockStyle.Bottom);

            this._scriptOutputView = new ScriptOutputView();
            this._searchPanel = new SearchPanel(this);
            this.dockManager.DockWindow(this._scriptOutputView, DockStyle.Bottom);
            this._scriptOutputView.HostContainer.DockWindow(this._searchPanel, DockStyle.Fill);

            this.PropertyChanged += this.MainWindowPropertyChanged;
        }

        private void SwitchScriptsElementsState(bool enabled)
        {
            this.toolStripMenuItem_File_NewScript.Enabled = this.toolStripButton_NewScript.Enabled = enabled;
            this.toolStripMenuItem_File_RenameScript.Enabled = this.toolStripButton_RenameScript.Enabled = enabled;
            this.toolStripMenuItem_File_DeleteScript.Enabled = this.toolStripButton_DeleteScript.Enabled = enabled;
            this.toolStripMenuItem_Optimization_Debug.Enabled = this.toolStripButton_DebugOptimization.Enabled = enabled;
            this.toolStripMenuItem_Optimization_Run.Enabled = this.toolStripButton_RunOptimization.Enabled = enabled;
            this.toolStripMenuItem_Edit_Search.Enabled = this.toolStripButton_Search.Enabled = enabled;
        }

        private void SwitchEditElementsState(bool enabled)
        {
            this.toolStripMenuItem_File_CloseScript.Enabled = this.toolStripButton_CloseScript.Enabled = enabled;
            this.toolStripMenuItem_Edit_Undo.Enabled = this.toolStripButton_Undo.Enabled = enabled;
            this.toolStripMenuItem_Edit_Redo.Enabled = this.toolStripButton_Redo.Enabled = enabled;
            this.toolStripMenuItem_Edit_Cut.Enabled = this.toolStripButton_Cut.Enabled = enabled;
            this.toolStripMenuItem_Edit_Copy.Enabled = this.toolStripButton_Copy.Enabled = enabled;
            this.toolStripMenuItem_Edit_Paste.Enabled = this.toolStripButton_Paste.Enabled = enabled;
        }

        public void NewProject()
        {
            this.CloseProject();

            if (this.Project == null && this.saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                if (File.Exists(this.saveFileDialog.FileName))
                    File.Delete(this.saveFileDialog.FileName);

                this._populationPlotViewer = new PopulationPlotViewer();
                this._curvePlotViewer = new CurvePlotViewer();
                this.Project = new OptimizationProject(this.saveFileDialog.FileName, this._scriptOutputView, this._populationPlotViewer, this._curvePlotViewer);
            }
        }

        public void OpenProject()
        {
            this.CloseProject();

            if (this.Project == null && this.openFileDialog.ShowDialog() == DialogResult.OK)
            {
                this._populationPlotViewer = new PopulationPlotViewer();
                this._curvePlotViewer = new CurvePlotViewer();
                this.Project = new OptimizationProject(this.openFileDialog.FileName, this._scriptOutputView, this._populationPlotViewer, this._curvePlotViewer);
            }
        }

        public void CloseProject()
        {
            if (this.Project != null)
            {
                var result = DialogResult.Cancel;

                if (this.HasUnsaved())
                {
                    result = MessageBox.Show(Resources.MainWindow_UnsavedChanges_Text, Resources.MainWindow_UnsavedChanges_Title, MessageBoxButtons.YesNoCancel);
                }

                if (!this.HasUnsaved() || result != DialogResult.Cancel)
                {
                    if (result == DialogResult.Yes)
                        this.SaveProject();

                    this.ScriptEditors.Keys.ToList().ForEach(this.CloseScriptEditor);
                    this.ClosePlotViewer();

                    this.Project = null;
                }
            }
        }

        public void SaveProject()
        {
            this.ScriptEditors.Values.ToList().ForEach(se => se.Save());
        }

        private bool HasUnsaved()
        {
            return this.ScriptEditors.Values.Any(se => se.HasChanges);
        }

        public void CreateScript()
        {
            var name = "Untitled";

            while ((this.Project.ScriptNames.Contains(name) || this.Project.ScriptNames.Contains(string.Concat(name, OptimizationProject.ScriptExtension)) || string.IsNullOrEmpty(name)))
            {
                name = RenameBox.ShowDialog(string.IsNullOrEmpty(name) ? "New Script" : name, "Name of new Script?");

                if (this.Project.ScriptNames.Contains(name) || this.Project.ScriptNames.Contains(string.Concat(name, OptimizationProject.ScriptExtension)))
                    MessageBox.Show(Resources.MainWindow_ScriptNameAlreadyExists_Text, Resources.MainWindow_ScriptNameAlreadyExists_Title);

                if (name == null)
                {
                    break;
                }
            }

            if (name != null)
            {
                if (!name.EndsWith(OptimizationProject.ScriptExtension))
                    name = string.Concat(name, OptimizationProject.ScriptExtension);

                this.Project.WriteScript(name, string.Empty);
            }
        }

        public void RenameScript()
        {
            var selectedScript = this._projectBrowser.SelectedScript;
            var name = "Untitled";

            if (!string.IsNullOrEmpty(selectedScript) && selectedScript != OptimizationProject.MainScriptName && selectedScript != string.Concat(OptimizationProject.MainScriptName, OptimizationProject.ScriptExtension))
            {
                while ((this.Project.ScriptNames.Contains(name) || this.Project.ScriptNames.Contains(string.Concat(name, OptimizationProject.ScriptExtension)) || string.IsNullOrEmpty(name)) && name != selectedScript)
                {
                    name = RenameBox.ShowDialog(string.IsNullOrEmpty(name) ? selectedScript : name, "Name of new Script?");

                    if (this.Project.ScriptNames.Contains(name) || this.Project.ScriptNames.Contains(string.Concat(name, OptimizationProject.ScriptExtension)))
                        MessageBox.Show(Resources.MainWindow_ScriptNameAlreadyExists_Text, Resources.MainWindow_ScriptNameAlreadyExists_Title);

                    if (name == null)
                    {
                        break;
                    }
                }

                if (name != null)
                {
                    if (!name.EndsWith(OptimizationProject.ScriptExtension))
                        name = string.Concat(name, OptimizationProject.ScriptExtension);

                    var text = this.Project.ReadScript(selectedScript);

                    var isOpen = this.ScriptEditors.ContainsKey(selectedScript);
                    if (isOpen)
                    {
                        text = this.ScriptEditors[selectedScript].Script;
                        this.CloseScriptEditor(selectedScript);
                    }

                    this.Project.WriteScript(name, text);

                    this.Project.RemoveScript(selectedScript);

                    this._projectBrowser.SelectedScript = name;

                    if (isOpen)
                    {
                        this.OpenScriptEditor(name);
                    }
                }
            }
        }

        public void RemoveScript()
        {
            var selectedScript = this._projectBrowser.SelectedScript;

            if (!string.IsNullOrEmpty(selectedScript) && selectedScript != OptimizationProject.MainScriptName && selectedScript != string.Concat(OptimizationProject.MainScriptName, OptimizationProject.ScriptExtension))
            {
                var isOpen = this.ScriptEditors.ContainsKey(selectedScript);
                if (isOpen)
                {
                    this.CloseScriptEditor(selectedScript);
                }

                this.Project.RemoveScript(selectedScript);
            }
        }

        public void OpenPlotViewer()
        {
            this.dockManager.DockWindow(this._populationPlotViewer, DockStyle.Fill);
            this.dockManager.DockWindow(this._curvePlotViewer, DockStyle.Fill);
        }

        public void OpenScriptEditor(string scriptName)
        {
            if (!this.ScriptEditors.ContainsKey(scriptName))
            {
                var scriptEditor = new ScriptEditor(this, scriptName);
                this.ScriptEditors.Add(scriptName, scriptEditor);
                this.dockManager.DockWindow(scriptEditor, DockStyle.Fill);
            }
            else
            {
                this.ScriptEditors[scriptName].HostContainer.SelectTab(this.ScriptEditors[scriptName].ControlContainer);
            }
        }

        internal void UnregisterScriptEditor(string scriptName)
        {
            if (this.ScriptEditors.ContainsKey(scriptName))
            {
                this.ScriptEditors.Remove(scriptName);
            }
        }

        public void CloseScriptEditor(string scriptName)
        {
            if (this.ScriptEditors.ContainsKey(scriptName))
            {
                var editor = this.ScriptEditors[scriptName];
                editor.Close();
                editor.Dispose();
            }
        }

        public void ClosePlotViewer()
        {
            this._populationPlotViewer.Close();
            this._populationPlotViewer.Dispose();
            this._populationPlotViewer = null;

            this._curvePlotViewer.Close();
            this._curvePlotViewer.Dispose();
            this._curvePlotViewer = null;
        }

        public void DebugOptimization()
        {
            this.SaveProject();
            this._scriptOutputView.HostContainer.SelectTab(this._scriptOutputView.ControlContainer);
            this.Project.Debug();
        }

        public void RunOptimization()
        {
            this.SaveProject();
            this._scriptOutputView.HostContainer.SelectTab(this._scriptOutputView.ControlContainer);
            this._populationPlotViewer.HostContainer.SelectTab(this._populationPlotViewer.ControlContainer);
            this.Project.Start();

            this.toolStripMenuItem_Optimization_Pause.Visible = this.toolStripButton_PauseOptimization.Visible = true;
            this.toolStripMenuItem_Optimization_Pause.Enabled = this.toolStripButton_PauseOptimization.Enabled = true;
            this.toolStripMenuItem_Optimization_Run.Visible = this.toolStripButton_RunOptimization.Visible = false;
            this.toolStripMenuItem_Optimization_Run.Enabled = this.toolStripButton_RunOptimization.Enabled = false;
        }

        public void PauseOptimization()
        {
            this.Project.Pause();

            this.toolStripMenuItem_Optimization_Pause.Visible = this.toolStripButton_PauseOptimization.Visible = false;
            this.toolStripMenuItem_Optimization_Pause.Enabled = this.toolStripButton_PauseOptimization.Enabled = false;
            this.toolStripMenuItem_Optimization_Run.Visible = this.toolStripButton_RunOptimization.Visible = true;
            this.toolStripMenuItem_Optimization_Run.Enabled = this.toolStripButton_RunOptimization.Enabled = true;
        }

        public void StopOptimization()
        {
            this.Project.Stop();
            
            this.toolStripMenuItem_Optimization_Pause.Visible = this.toolStripButton_PauseOptimization.Visible = false;
            this.toolStripMenuItem_Optimization_Pause.Enabled = this.toolStripButton_PauseOptimization.Enabled = false;
            this.toolStripMenuItem_Optimization_Run.Visible = this.toolStripButton_RunOptimization.Visible = true;
            this.toolStripMenuItem_Optimization_Run.Enabled = this.toolStripButton_RunOptimization.Enabled = true;
        }

        void MainWindowPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Project")
            {
                this.toolStripMenuItem_File_SaveProject.Enabled = 
                this.toolStripMenuItem_File_CloseProject.Enabled =
                this.toolStripButton_SaveProject.Enabled = 
                this.toolStripButton_CloseProject.Enabled = this.Project != null;
            }
        }

        private bool _closingPending;
        protected override void OnClosing(CancelEventArgs e)
        {
            base.OnClosing(e);

            if (!this._closingPending)
            {
                if (this.Project == null || !this.Project.IsRunning)
                {
                    var result = DialogResult.No;
                    if (this.HasUnsaved())
                    {
                        result = MessageBox.Show(Resources.MainWindow_UnsavedChanges_Text, Resources.MainWindow_UnsavedChanges_Title, MessageBoxButtons.YesNoCancel);
                    }

                    if (!this.HasUnsaved() || result != DialogResult.Cancel)
                    {
                        if (result == DialogResult.Yes)
                            this.SaveProject();
                    }

                    if (result != DialogResult.Cancel)
                    {
                        this._closingPending = true;
                    }
                    else
                    {
                        e.Cancel = true;
                    }
                }
                else
                {
                    MessageBox.Show(Resources.MainWindow_CannotCloseWhileProjectRunning_Text, Resources.MainWindow_CannotCloseWhileProjectRunning_Title);
                    e.Cancel = true;
                }
            }
        }

        private void ToolStripMenuItemFileNewProjectClick(object sender, EventArgs e)
        {
            this.NewProject();
        }

        private void ToolStripMenuItemFileOpenProjectClick(object sender, EventArgs e)
        {
            this.OpenProject();
        }

        private void ToolStripMenuItemFileCloseProjectClick(object sender, EventArgs e)
        {
            this.CloseProject();
        }

        private void ToolStripMenuItemFileSaveProjectClick(object sender, EventArgs e)
        {
            this.SaveProject();
        }

        private void ToolStripMenuItemNewScriptClick(object sender, EventArgs e)
        {
            this.CreateScript();
        }

        private void ToolStripMenuItemRenameScriptClick(object sender, EventArgs e)
        {
            this.RenameScript();
        }

        private void ToolStripMenuItemDeleteScriptClick(object sender, EventArgs e)
        {
            this.RemoveScript();
        }

        private void ToolStripMenuItemEditUndoClick(object sender, EventArgs e)
        {
            this.ActiveScriptEditor.Undo();
        }

        private void ToolStripMenuItemEditRedoClick(object sender, EventArgs e)
        {
            this.ActiveScriptEditor.Redo();
        }

        private void ToolStripMenuItemEditCutClick(object sender, EventArgs e)
        {
            this.ActiveScriptEditor.Cut();
        }

        private void ToolStripMenuItemEditCopyClick(object sender, EventArgs e)
        {
            this.ActiveScriptEditor.Copy();
        }

        private void ToolStripMenuItemEditPasteClick(object sender, EventArgs e)
        {
            this.ActiveScriptEditor.Paste();
        }

        private void ToolStripMenuItemFileCloseScriptClick(object sender, EventArgs e)
        {
            this.CloseScriptEditor(this.ActiveScriptEditor.Text);
        }

        private void ToolStripMenuItemOptimizationRunClick(object sender, EventArgs e)
        {
            this.RunOptimization();
        }

        private void ToolStripMenuItemOptimizationStopClick(object sender, EventArgs e)
        {
            this.StopOptimization();
        }

        private void ToolStripMenuItemFileQuitClick(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ToolStripMenuItemOptimizationDebugClick(object sender, EventArgs e)
        {
            this.DebugOptimization();
        }

        private void TimerTick(object sender, EventArgs e)
        {
            lock (this.timer)
            {
                this.timer.Stop();
                if (this._optimizationStartTime.HasValue)
                {
                    if(this.Project.Service != null)
                        this.toolStripStatusLabel_Round.Text = this.Project.Service.Round.ToString(CultureInfo.CurrentCulture);
                    this.toolStripStatusLabel_Time.Text = (DateTime.UtcNow - this._optimizationStartTime.Value).ToString();
                    this.timer.Start();
                }
                else
                {
                    this.toolStripStatusLabel_Round.Text = string.Empty;
                    this.toolStripStatusLabel_Time.Text = string.Empty;
                }
            }
        }

        private void ToolStripMenuItemOptimizationPauseClick(object sender, EventArgs e)
        {
            this.PauseOptimization();
        }

        private void ToolStripMenuItemEditSearchClick(object sender, EventArgs e)
        {
            this._scriptOutputView.HostContainer.SelectTab(this._searchPanel.ControlContainer);
            this._searchPanel.ActivateTextBox();
        }

        protected virtual void OnPropertyChanging(string propertyName)
        {
            if (this.PropertyChanging != null)
                this.PropertyChanging(this, new PropertyChangingEventArgs(propertyName));
        }
        public event PropertyChangingEventHandler PropertyChanging;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
