﻿using System;
using DockDotNET;
using RalphsDotNet.Apps.OptimizationStudio.BusinessLogic;

namespace RalphsDotNet.Apps.OptimizationStudio
{
    public partial class ScriptOutputView : DockWindow, IScriptOutputProvider
    {
        private string _buffer = string.Empty;

        public ScriptOutputView()
        {
            InitializeComponent();
        }

        public void Write(object output)
        {
            lock(this._buffer)
                this._buffer += output.ToString();
        }

        public void WriteLine(object output)
        {
            lock (this._buffer)
            {
                this._buffer += output.ToString();
                this._buffer += Environment.NewLine;
            }
        }

        public void NewLine()
        {
            lock (this._buffer)
                this._buffer += Environment.NewLine;
        }

        public void Clear()
        {
            lock (this._buffer)
                this._buffer = string.Empty;
        }

        private void Sync()
        {
            lock (this._buffer)
            {
                if (this.textBox.Text != this._buffer)
                {
                    this.textBox.Text = this._buffer;
                    this.ScrollToBottom();
                }
            }

        }

        private void ScrollToBottom()
        {
            this.textBox.SelectionStart = this.textBox.Text.Length;
            this.textBox.ScrollToCaret();
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            lock (this.timer)
            {
                this.timer.Stop();
                this.Sync();
                this.timer.Start();
            }
        }
    }
}
