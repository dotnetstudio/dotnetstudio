﻿using System;
using System.Windows.Forms;
using DockDotNET;
using System.Text.RegularExpressions;

namespace RalphsDotNet.Apps.OptimizationStudio
{
    public partial class SearchPanel : DockWindow
    {
        private readonly MainWindow _mainWindow;

        public SearchPanel(MainWindow mainWindow)
        {
            this._mainWindow = mainWindow;

            InitializeComponent();
        }        

        private void Search()
        {
            this.listBox_Results.Items.Clear();
            var searchPattern = this.textBox_Pattern.Text;

            if (this._mainWindow.Project != null && !string.IsNullOrEmpty(searchPattern))
            {
                foreach (var scriptName in this._mainWindow.Project.ScriptNames)
                {
                    var script = this._mainWindow.ScriptEditors.ContainsKey(scriptName) ? this._mainWindow.ScriptEditors[scriptName].Script : this._mainWindow.Project.ReadScript(scriptName);

                    var lines = script.Split('\n');

                    for (var i = 0; i < lines.Length; i++)
                    {
                        var match = Regex.Match(lines[i], searchPattern, RegexOptions.Singleline);
                        if (match.Success)
                        {
                            this.listBox_Results.Items.Add(new Finding { ScriptName = scriptName, LineIndex = i, ColumnIndex = match.Index, Line = lines[i] });
                        }
                    }
                }
            }
        }

        private void button_Search_Click(object sender, EventArgs e)
        {
            this.Search();
        }

        private void listBox_Results_DoubleClick(object sender, EventArgs e)
        {
            if (this._mainWindow.Project != null && this.listBox_Results.SelectedItem is Finding)
            {
                var finding = (Finding)this.listBox_Results.SelectedItem;
                this._mainWindow.OpenScriptEditor(finding.ScriptName);
                this._mainWindow.ActiveScriptEditor.JumpTo(finding.LineIndex, finding.ColumnIndex);
            }
        }

        private void textBox_Pattern_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.Search();
                e.Handled = true;

                this.listBox_Results.Focus();
            }
        }

        public void ActivateTextBox()
        {
            this.textBox_Pattern.Focus();
            this.textBox_Pattern.SelectAll();
        }
    }

    internal class Finding
    {
        internal string ScriptName { get; set; }
        internal int LineIndex { get; set; }
        internal int ColumnIndex { get; set; }
        internal string Line { get; set; }

        public override string ToString()
        {
            return string.Format("{0}, {1}, {2}:\t{3}", this.ScriptName, this.LineIndex + 1, this.ColumnIndex + 1, this.Line);
        }
    }
}
