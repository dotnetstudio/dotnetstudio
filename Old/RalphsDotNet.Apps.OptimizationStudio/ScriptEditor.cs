﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using DockDotNET;
using RalphsDotNet.Apps.OptimizationStudio.BusinessLogic;
using RalphsDotNet.Apps.OptimizationStudio.Properties;

namespace RalphsDotNet.Apps.OptimizationStudio
{
    public partial class ScriptEditor : DockWindow, INotifyPropertyChanged
    {
        private readonly MainWindow _mainWindow;

        public OptimizationProject Project { get { return this._mainWindow != null ? this._mainWindow.Project : null; } }

        public string Script { get { return this.textEditorControl.Text; } }

        private readonly string _scriptName;

        private bool _hasChanges;
        public bool HasChanges
        {
            get { return this._hasChanges; }
            private set
            {
                if (this._hasChanges != value)
                {
                    this._hasChanges = value;
                    this.OnPropertyChanged("HasChanges");
                }
            }
        }

        public ScriptEditor(MainWindow mainWindow, string scriptName)
        {
            this._mainWindow = mainWindow;
            this._scriptName = scriptName;

            InitializeComponent();

            this.Text = this._scriptName;
            this.textEditorControl.SetHighlighting(OptimizationProject.ScriptType);
            
            this.Reload();
        }

        public void Reload()
        {
            this.textEditorControl.Text = this.Project.ReadScript(this._scriptName);
            this.textEditorControl.Refresh();

            this.HasChanges = false;
        }

        public void Save()
        {
            this.Project.WriteScript(_scriptName, this.textEditorControl.Text);

            this.HasChanges = false;
        }

        public void Cut()
        {
            var selectedText = this.textEditorControl.ActiveTextAreaControl.SelectionManager.SelectedText;

            if (!string.IsNullOrEmpty(selectedText))
            {
                Clipboard.SetText(selectedText);
                this.textEditorControl.ActiveTextAreaControl.SelectionManager.RemoveSelectedText();
            }
            else
            {
                Clipboard.Clear();
            }
        }

        public void Copy()
        {
            var selectedText = this.textEditorControl.ActiveTextAreaControl.SelectionManager.SelectedText;

            if (!string.IsNullOrEmpty(selectedText))
            {
                Clipboard.SetText(selectedText);
            }
            else
            {
                Clipboard.Clear();
            }
        }

        public void Paste()
        {
            this.textEditorControl.ActiveTextAreaControl.SelectionManager.RemoveSelectedText();
            this.textEditorControl.ActiveTextAreaControl.Document.Insert(this.textEditorControl.ActiveTextAreaControl.Caret.Offset, Clipboard.GetText());
        }

        public void Undo()
        {
            this.textEditorControl.Undo();
        }

        public void Redo()
        {
            this.textEditorControl.Redo();
        }

        public void JumpTo(int lineIndex, int columnIndex)
        {
            this.textEditorControl.ActiveTextAreaControl.JumpTo(lineIndex, columnIndex);
        }

        private void textEditorControl_TextChanged(object sender, EventArgs e)
        {
            this.HasChanges = true;
        }

        private bool _closingPending;
        protected override void OnClosing(CancelEventArgs e)
        {
            base.OnClosing(e);

            if (!this._closingPending)
            {
                var result = DialogResult.No;
                if (this.HasChanges)
                {
                    result = MessageBox.Show(Resources.ScriptEditor_OnClosing_UnsavedChanges_Text, Resources.ScriptEditor_OnClosing_UnsavedChanges_Title, MessageBoxButtons.YesNoCancel);
                }

                if (!this.HasChanges || result != DialogResult.Cancel)
                {
                    if (result == DialogResult.Yes)
                        this._mainWindow.SaveProject();
                }

                if (result != DialogResult.Cancel)
                {
                    if (this._mainWindow.ActiveScriptEditor == this)
                        this._mainWindow.ActiveScriptEditor = null;
                    this._mainWindow.UnregisterScriptEditor(this._scriptName);

                    this._closingPending = true;
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }

        protected override void OnActivated(EventArgs e)
        {
            base.OnActivated(e);

            this._mainWindow.ActiveScriptEditor = this;
        }

        protected virtual void OnPropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
