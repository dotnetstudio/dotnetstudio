﻿using DockDotNET;
using System;
using System.Linq;
using OxyPlot;
using RalphsDotNet.Apps.OptimizationStudio.BusinessLogic;
using OxyPlot.Axes;
using OxyPlot.Series;

namespace RalphsDotNet.Apps.OptimizationStudio
{
    public partial class CurvePlotViewer : DockWindow, ICurvePlottingProvider
    {
        public CurvePlotViewer()
        {
            InitializeComponent();

            this.plot.Model = new PlotModel("Curve Plot");
            this.plot.Model.Axes.Add(new LinearAxis(AxisPosition.Left, "Y") { Minimum = 0 });
            this.plot.Model.Axes.Add(new LinearAxis(AxisPosition.Bottom,"X") { Minimum = 0 });
            this.plot.Model.PlotType = PlotType.XY;

            this.InitPlot();
        }

        private void InitPlot()
        {
            this.plot.Model.Series.Add(new LineSeries(OxyColors.Red, 1, "Calculated"));
            this.plot.Model.Series.Add(new LineSeries(OxyColors.Blue, 1, "Expected"));
        }

        public void Clear()
        {
            this.Invoke(new ClearHandler(this.ClearInternal));
        }

        private delegate void ClearHandler();
        private void ClearInternal()
        {
            ((LineSeries)this.plot.Model.Series.First()).Points.Clear();
            ((LineSeries)this.plot.Model.Series.Last()).Points.Clear();
        }

        public void PlotCalculated(double x, double y)
        {
            this.BeginInvoke(new PlotHandler(this.PlotCalculatedInternal), x, y);
        }

        public void PlotExpected(double x, double y)
        {
            this.BeginInvoke(new PlotHandler(this.PlotExpectedInternal), x, y);
        }

        private delegate void PlotHandler(double x, double y);

        private void PlotCalculatedInternal(double x, double y)
        {
            ((LineSeries)this.plot.Model.Series.First()).Points.Add(new DataPoint(x, y));
        }

        private void PlotExpectedInternal(double x, double y)
        {
            ((LineSeries)this.plot.Model.Series.Last()).Points.Add(new DataPoint(x, y));
        }

        private void TimerTick(object sender, EventArgs e)
        {
            this.plot.RefreshPlot(true);
        }
    }
}
