﻿namespace RalphsDotNet.Apps.OptimizationStudio
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindow));
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel_Round = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel_Time = new System.Windows.Forms.ToolStripStatusLabel();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.toolStripContainer = new System.Windows.Forms.ToolStripContainer();
            this.dockManager = new DockDotNET.DockManager(this.components);
            this.toolStrip = new System.Windows.Forms.ToolStrip();
            this.toolStripButton_NewProject = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton_OpenProject = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton_SaveProject = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton_CloseProject = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton_NewScript = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton_RenameScript = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton_DeleteScript = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton_CloseScript = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton_Undo = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton_Redo = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton_Cut = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton_Copy = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton_Paste = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton_Search = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton_DebugOptimization = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton_RunOptimization = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton_PauseOptimization = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton_StopOptimization = new System.Windows.Forms.ToolStripButton();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.toolStripMenuItem_File = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem_File_NewProject = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem_File_OpenProject = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem_File_SaveProject = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem_File_CloseProject = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItem_File_NewScript = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem_File_RenameScript = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem_File_DeleteScript = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem_File_CloseScript = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.ToolStripMenuItem_File_Quit = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem_Edit = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem_Edit_Undo = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem_Edit_Redo = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItem_Edit_Cut = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem_Edit_Copy = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem_Edit_Paste = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItem_Edit_Search = new System.Windows.Forms.ToolStripMenuItem();
            this.optimizationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem_Optimization_Debug = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem_Optimization_Run = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem_Optimization_Pause = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem_Optimization_Stop = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem_Help = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem_Help_Info = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.statusStrip.SuspendLayout();
            this.toolStripContainer.ContentPanel.SuspendLayout();
            this.toolStripContainer.TopToolStripPanel.SuspendLayout();
            this.toolStripContainer.SuspendLayout();
            this.toolStrip.SuspendLayout();
            this.menuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip
            // 
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.toolStripStatusLabel_Round,
            this.toolStripStatusLabel2,
            this.toolStripStatusLabel_Time});
            this.statusStrip.Location = new System.Drawing.Point(0, 409);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(1058, 22);
            this.statusStrip.TabIndex = 0;
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(45, 17);
            this.toolStripStatusLabel1.Text = "Round:";
            // 
            // toolStripStatusLabel_Round
            // 
            this.toolStripStatusLabel_Round.AutoSize = false;
            this.toolStripStatusLabel_Round.Margin = new System.Windows.Forms.Padding(0, 3, 20, 2);
            this.toolStripStatusLabel_Round.Name = "toolStripStatusLabel_Round";
            this.toolStripStatusLabel_Round.Size = new System.Drawing.Size(50, 17);
            this.toolStripStatusLabel_Round.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(37, 17);
            this.toolStripStatusLabel2.Text = "Time:";
            // 
            // toolStripStatusLabel_Time
            // 
            this.toolStripStatusLabel_Time.Margin = new System.Windows.Forms.Padding(0, 3, 20, 2);
            this.toolStripStatusLabel_Time.Name = "toolStripStatusLabel_Time";
            this.toolStripStatusLabel_Time.Size = new System.Drawing.Size(0, 17);
            this.toolStripStatusLabel_Time.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // openFileDialog
            // 
            this.openFileDialog.DefaultExt = "py";
            this.openFileDialog.Filter = "Optimization Project|*.oProj";
            // 
            // saveFileDialog
            // 
            this.saveFileDialog.DefaultExt = "py";
            this.saveFileDialog.Filter = "Optimization Project|*.oProj";
            // 
            // toolStripContainer
            // 
            // 
            // toolStripContainer.ContentPanel
            // 
            this.toolStripContainer.ContentPanel.Controls.Add(this.dockManager);
            this.toolStripContainer.ContentPanel.Size = new System.Drawing.Size(1058, 360);
            this.toolStripContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toolStripContainer.Location = new System.Drawing.Point(0, 24);
            this.toolStripContainer.Name = "toolStripContainer";
            this.toolStripContainer.Size = new System.Drawing.Size(1058, 385);
            this.toolStripContainer.TabIndex = 3;
            this.toolStripContainer.Text = "toolStripContainer1";
            // 
            // toolStripContainer.TopToolStripPanel
            // 
            this.toolStripContainer.TopToolStripPanel.Controls.Add(this.toolStrip);
            // 
            // dockManager
            // 
            this.dockManager.BackColor = System.Drawing.Color.Transparent;
            this.dockManager.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dockManager.DockBorder = 20;
            this.dockManager.DockType = DockDotNET.DockContainerType.Document;
            this.dockManager.FastDrawing = false;
            this.dockManager.Location = new System.Drawing.Point(0, 0);
            this.dockManager.Name = "dockManager";
            this.dockManager.Padding = new System.Windows.Forms.Padding(2, 23, 2, 2);
            this.dockManager.ShowIcons = true;
            this.dockManager.Size = new System.Drawing.Size(1058, 360);
            this.dockManager.SplitterWidth = 5;
            this.dockManager.TabIndex = 0;
            this.dockManager.VisualStyle = DockDotNET.DockVisualStyle.VS2005;
            // 
            // toolStrip
            // 
            this.toolStrip.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton_NewProject,
            this.toolStripButton_OpenProject,
            this.toolStripButton_SaveProject,
            this.toolStripButton_CloseProject,
            this.toolStripSeparator1,
            this.toolStripButton_NewScript,
            this.toolStripButton_RenameScript,
            this.toolStripButton_DeleteScript,
            this.toolStripButton_CloseScript,
            this.toolStripSeparator3,
            this.toolStripButton_Undo,
            this.toolStripButton_Redo,
            this.toolStripSeparator2,
            this.toolStripButton_Cut,
            this.toolStripButton_Copy,
            this.toolStripButton_Paste,
            this.toolStripSeparator6,
            this.toolStripButton_Search,
            this.toolStripSeparator9,
            this.toolStripButton_DebugOptimization,
            this.toolStripButton_RunOptimization,
            this.toolStripButton_PauseOptimization,
            this.toolStripButton_StopOptimization});
            this.toolStrip.Location = new System.Drawing.Point(3, 0);
            this.toolStrip.Name = "toolStrip";
            this.toolStrip.Size = new System.Drawing.Size(487, 25);
            this.toolStrip.TabIndex = 1;
            this.toolStrip.Text = "toolStrip1";
            // 
            // toolStripButton_NewProject
            // 
            this.toolStripButton_NewProject.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton_NewProject.Image = global::RalphsDotNet.Apps.OptimizationStudio.Properties.Resources.page_add;
            this.toolStripButton_NewProject.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_NewProject.Name = "toolStripButton_NewProject";
            this.toolStripButton_NewProject.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton_NewProject.Text = "New Project";
            this.toolStripButton_NewProject.Click += new System.EventHandler(this.ToolStripMenuItemFileNewProjectClick);
            // 
            // toolStripButton_OpenProject
            // 
            this.toolStripButton_OpenProject.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton_OpenProject.Image = global::RalphsDotNet.Apps.OptimizationStudio.Properties.Resources.page_edit;
            this.toolStripButton_OpenProject.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_OpenProject.Name = "toolStripButton_OpenProject";
            this.toolStripButton_OpenProject.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton_OpenProject.Text = "Open Project";
            this.toolStripButton_OpenProject.Click += new System.EventHandler(this.ToolStripMenuItemFileOpenProjectClick);
            // 
            // toolStripButton_SaveProject
            // 
            this.toolStripButton_SaveProject.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton_SaveProject.Enabled = false;
            this.toolStripButton_SaveProject.Image = global::RalphsDotNet.Apps.OptimizationStudio.Properties.Resources.page_save;
            this.toolStripButton_SaveProject.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_SaveProject.Name = "toolStripButton_SaveProject";
            this.toolStripButton_SaveProject.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton_SaveProject.Text = "Save Project";
            this.toolStripButton_SaveProject.Click += new System.EventHandler(this.ToolStripMenuItemFileSaveProjectClick);
            // 
            // toolStripButton_CloseProject
            // 
            this.toolStripButton_CloseProject.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton_CloseProject.Enabled = false;
            this.toolStripButton_CloseProject.Image = global::RalphsDotNet.Apps.OptimizationStudio.Properties.Resources.page_delete;
            this.toolStripButton_CloseProject.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_CloseProject.Name = "toolStripButton_CloseProject";
            this.toolStripButton_CloseProject.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton_CloseProject.Text = "Close Project";
            this.toolStripButton_CloseProject.Click += new System.EventHandler(this.ToolStripMenuItemFileCloseProjectClick);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButton_NewScript
            // 
            this.toolStripButton_NewScript.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton_NewScript.Enabled = false;
            this.toolStripButton_NewScript.Image = global::RalphsDotNet.Apps.OptimizationStudio.Properties.Resources.page_white_add;
            this.toolStripButton_NewScript.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_NewScript.Name = "toolStripButton_NewScript";
            this.toolStripButton_NewScript.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton_NewScript.Text = "New Script";
            this.toolStripButton_NewScript.Click += new System.EventHandler(this.ToolStripMenuItemNewScriptClick);
            // 
            // toolStripButton_RenameScript
            // 
            this.toolStripButton_RenameScript.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton_RenameScript.Enabled = false;
            this.toolStripButton_RenameScript.Image = global::RalphsDotNet.Apps.OptimizationStudio.Properties.Resources.textfield_rename;
            this.toolStripButton_RenameScript.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_RenameScript.Name = "toolStripButton_RenameScript";
            this.toolStripButton_RenameScript.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton_RenameScript.Text = "Rename Script";
            this.toolStripButton_RenameScript.Click += new System.EventHandler(this.ToolStripMenuItemRenameScriptClick);
            // 
            // toolStripButton_DeleteScript
            // 
            this.toolStripButton_DeleteScript.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton_DeleteScript.Enabled = false;
            this.toolStripButton_DeleteScript.Image = global::RalphsDotNet.Apps.OptimizationStudio.Properties.Resources.page_white_cross;
            this.toolStripButton_DeleteScript.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_DeleteScript.Name = "toolStripButton_DeleteScript";
            this.toolStripButton_DeleteScript.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton_DeleteScript.Text = "Remove Script";
            this.toolStripButton_DeleteScript.Click += new System.EventHandler(this.ToolStripMenuItemDeleteScriptClick);
            // 
            // toolStripButton_CloseScript
            // 
            this.toolStripButton_CloseScript.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton_CloseScript.Enabled = false;
            this.toolStripButton_CloseScript.Image = global::RalphsDotNet.Apps.OptimizationStudio.Properties.Resources.page_white_delete;
            this.toolStripButton_CloseScript.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_CloseScript.Name = "toolStripButton_CloseScript";
            this.toolStripButton_CloseScript.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton_CloseScript.Text = "Close Script";
            this.toolStripButton_CloseScript.Click += new System.EventHandler(this.ToolStripMenuItemFileCloseScriptClick);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButton_Undo
            // 
            this.toolStripButton_Undo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton_Undo.Enabled = false;
            this.toolStripButton_Undo.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton_Undo.Image")));
            this.toolStripButton_Undo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_Undo.Name = "toolStripButton_Undo";
            this.toolStripButton_Undo.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton_Undo.Text = "Undo";
            this.toolStripButton_Undo.Click += new System.EventHandler(this.ToolStripMenuItemEditUndoClick);
            // 
            // toolStripButton_Redo
            // 
            this.toolStripButton_Redo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton_Redo.Enabled = false;
            this.toolStripButton_Redo.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton_Redo.Image")));
            this.toolStripButton_Redo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_Redo.Name = "toolStripButton_Redo";
            this.toolStripButton_Redo.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton_Redo.Text = "Redo";
            this.toolStripButton_Redo.Click += new System.EventHandler(this.ToolStripMenuItemEditRedoClick);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButton_Cut
            // 
            this.toolStripButton_Cut.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton_Cut.Enabled = false;
            this.toolStripButton_Cut.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton_Cut.Image")));
            this.toolStripButton_Cut.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_Cut.Name = "toolStripButton_Cut";
            this.toolStripButton_Cut.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton_Cut.Text = "Cut";
            this.toolStripButton_Cut.Click += new System.EventHandler(this.ToolStripMenuItemEditCutClick);
            // 
            // toolStripButton_Copy
            // 
            this.toolStripButton_Copy.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton_Copy.Enabled = false;
            this.toolStripButton_Copy.Image = global::RalphsDotNet.Apps.OptimizationStudio.Properties.Resources.page_white_copy;
            this.toolStripButton_Copy.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_Copy.Name = "toolStripButton_Copy";
            this.toolStripButton_Copy.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton_Copy.Text = "Copy";
            this.toolStripButton_Copy.Click += new System.EventHandler(this.ToolStripMenuItemEditCopyClick);
            // 
            // toolStripButton_Paste
            // 
            this.toolStripButton_Paste.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton_Paste.Enabled = false;
            this.toolStripButton_Paste.Image = global::RalphsDotNet.Apps.OptimizationStudio.Properties.Resources.page_white_paste;
            this.toolStripButton_Paste.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_Paste.Name = "toolStripButton_Paste";
            this.toolStripButton_Paste.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton_Paste.Text = "Paste";
            this.toolStripButton_Paste.Click += new System.EventHandler(this.ToolStripMenuItemEditPasteClick);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButton_Search
            // 
            this.toolStripButton_Search.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton_Search.Enabled = false;
            this.toolStripButton_Search.Image = global::RalphsDotNet.Apps.OptimizationStudio.Properties.Resources.magnifier;
            this.toolStripButton_Search.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_Search.Name = "toolStripButton_Search";
            this.toolStripButton_Search.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton_Search.Text = "Search in scripts";
            this.toolStripButton_Search.Click += new System.EventHandler(this.ToolStripMenuItemEditSearchClick);
            // 
            // toolStripSeparator9
            // 
            this.toolStripSeparator9.Name = "toolStripSeparator9";
            this.toolStripSeparator9.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButton_DebugOptimization
            // 
            this.toolStripButton_DebugOptimization.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton_DebugOptimization.Enabled = false;
            this.toolStripButton_DebugOptimization.Image = global::RalphsDotNet.Apps.OptimizationStudio.Properties.Resources.control_end;
            this.toolStripButton_DebugOptimization.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_DebugOptimization.Name = "toolStripButton_DebugOptimization";
            this.toolStripButton_DebugOptimization.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton_DebugOptimization.Text = "Debug Optimization Script";
            this.toolStripButton_DebugOptimization.Click += new System.EventHandler(this.ToolStripMenuItemOptimizationDebugClick);
            // 
            // toolStripButton_RunOptimization
            // 
            this.toolStripButton_RunOptimization.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton_RunOptimization.Enabled = false;
            this.toolStripButton_RunOptimization.Image = global::RalphsDotNet.Apps.OptimizationStudio.Properties.Resources.control_play;
            this.toolStripButton_RunOptimization.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_RunOptimization.Name = "toolStripButton_RunOptimization";
            this.toolStripButton_RunOptimization.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton_RunOptimization.Text = "Run Optimization";
            this.toolStripButton_RunOptimization.Click += new System.EventHandler(this.ToolStripMenuItemOptimizationRunClick);
            // 
            // toolStripButton_PauseOptimization
            // 
            this.toolStripButton_PauseOptimization.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton_PauseOptimization.Enabled = false;
            this.toolStripButton_PauseOptimization.Image = global::RalphsDotNet.Apps.OptimizationStudio.Properties.Resources.control_pause;
            this.toolStripButton_PauseOptimization.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_PauseOptimization.Name = "toolStripButton_PauseOptimization";
            this.toolStripButton_PauseOptimization.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton_PauseOptimization.Text = "Pause Optimization";
            this.toolStripButton_PauseOptimization.Visible = false;
            this.toolStripButton_PauseOptimization.Click += new System.EventHandler(this.ToolStripMenuItemOptimizationPauseClick);
            // 
            // toolStripButton_StopOptimization
            // 
            this.toolStripButton_StopOptimization.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton_StopOptimization.Enabled = false;
            this.toolStripButton_StopOptimization.Image = global::RalphsDotNet.Apps.OptimizationStudio.Properties.Resources.control_stop;
            this.toolStripButton_StopOptimization.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_StopOptimization.Name = "toolStripButton_StopOptimization";
            this.toolStripButton_StopOptimization.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton_StopOptimization.Text = "Stop Optimization";
            this.toolStripButton_StopOptimization.Click += new System.EventHandler(this.ToolStripMenuItemOptimizationStopClick);
            // 
            // timer
            // 
            this.timer.Tick += new System.EventHandler(this.TimerTick);
            // 
            // toolStripMenuItem_File
            // 
            this.toolStripMenuItem_File.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem_File_NewProject,
            this.toolStripMenuItem_File_OpenProject,
            this.toolStripMenuItem_File_SaveProject,
            this.toolStripMenuItem_File_CloseProject,
            this.toolStripSeparator4,
            this.toolStripMenuItem_File_NewScript,
            this.toolStripMenuItem_File_RenameScript,
            this.toolStripMenuItem_File_DeleteScript,
            this.toolStripMenuItem_File_CloseScript,
            this.toolStripSeparator5,
            this.ToolStripMenuItem_File_Quit});
            this.toolStripMenuItem_File.Name = "toolStripMenuItem_File";
            this.toolStripMenuItem_File.Size = new System.Drawing.Size(37, 20);
            this.toolStripMenuItem_File.Text = "&File";
            // 
            // toolStripMenuItem_File_NewProject
            // 
            this.toolStripMenuItem_File_NewProject.Image = global::RalphsDotNet.Apps.OptimizationStudio.Properties.Resources.page_add;
            this.toolStripMenuItem_File_NewProject.Name = "toolStripMenuItem_File_NewProject";
            this.toolStripMenuItem_File_NewProject.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.toolStripMenuItem_File_NewProject.Size = new System.Drawing.Size(186, 22);
            this.toolStripMenuItem_File_NewProject.Text = "&New Project";
            this.toolStripMenuItem_File_NewProject.Click += new System.EventHandler(this.ToolStripMenuItemFileNewProjectClick);
            // 
            // toolStripMenuItem_File_OpenProject
            // 
            this.toolStripMenuItem_File_OpenProject.Image = global::RalphsDotNet.Apps.OptimizationStudio.Properties.Resources.page_edit;
            this.toolStripMenuItem_File_OpenProject.Name = "toolStripMenuItem_File_OpenProject";
            this.toolStripMenuItem_File_OpenProject.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.toolStripMenuItem_File_OpenProject.Size = new System.Drawing.Size(186, 22);
            this.toolStripMenuItem_File_OpenProject.Text = "&Open Project";
            this.toolStripMenuItem_File_OpenProject.Click += new System.EventHandler(this.ToolStripMenuItemFileOpenProjectClick);
            // 
            // toolStripMenuItem_File_SaveProject
            // 
            this.toolStripMenuItem_File_SaveProject.Enabled = false;
            this.toolStripMenuItem_File_SaveProject.Image = global::RalphsDotNet.Apps.OptimizationStudio.Properties.Resources.page_save;
            this.toolStripMenuItem_File_SaveProject.Name = "toolStripMenuItem_File_SaveProject";
            this.toolStripMenuItem_File_SaveProject.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.toolStripMenuItem_File_SaveProject.Size = new System.Drawing.Size(186, 22);
            this.toolStripMenuItem_File_SaveProject.Text = "&Save Project";
            this.toolStripMenuItem_File_SaveProject.Click += new System.EventHandler(this.ToolStripMenuItemFileSaveProjectClick);
            // 
            // toolStripMenuItem_File_CloseProject
            // 
            this.toolStripMenuItem_File_CloseProject.Enabled = false;
            this.toolStripMenuItem_File_CloseProject.Image = global::RalphsDotNet.Apps.OptimizationStudio.Properties.Resources.page_delete;
            this.toolStripMenuItem_File_CloseProject.Name = "toolStripMenuItem_File_CloseProject";
            this.toolStripMenuItem_File_CloseProject.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.E)));
            this.toolStripMenuItem_File_CloseProject.Size = new System.Drawing.Size(186, 22);
            this.toolStripMenuItem_File_CloseProject.Text = "&Close Project";
            this.toolStripMenuItem_File_CloseProject.Click += new System.EventHandler(this.ToolStripMenuItemFileCloseProjectClick);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(183, 6);
            // 
            // toolStripMenuItem_File_NewScript
            // 
            this.toolStripMenuItem_File_NewScript.Enabled = false;
            this.toolStripMenuItem_File_NewScript.Image = global::RalphsDotNet.Apps.OptimizationStudio.Properties.Resources.page_white_add;
            this.toolStripMenuItem_File_NewScript.Name = "toolStripMenuItem_File_NewScript";
            this.toolStripMenuItem_File_NewScript.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.T)));
            this.toolStripMenuItem_File_NewScript.Size = new System.Drawing.Size(186, 22);
            this.toolStripMenuItem_File_NewScript.Text = "N&ew Script";
            this.toolStripMenuItem_File_NewScript.Click += new System.EventHandler(this.ToolStripMenuItemNewScriptClick);
            // 
            // toolStripMenuItem_File_RenameScript
            // 
            this.toolStripMenuItem_File_RenameScript.Enabled = false;
            this.toolStripMenuItem_File_RenameScript.Image = global::RalphsDotNet.Apps.OptimizationStudio.Properties.Resources.textfield_rename;
            this.toolStripMenuItem_File_RenameScript.Name = "toolStripMenuItem_File_RenameScript";
            this.toolStripMenuItem_File_RenameScript.ShortcutKeys = System.Windows.Forms.Keys.F2;
            this.toolStripMenuItem_File_RenameScript.Size = new System.Drawing.Size(186, 22);
            this.toolStripMenuItem_File_RenameScript.Text = "&Rename Script";
            this.toolStripMenuItem_File_RenameScript.Click += new System.EventHandler(this.ToolStripMenuItemRenameScriptClick);
            // 
            // toolStripMenuItem_File_DeleteScript
            // 
            this.toolStripMenuItem_File_DeleteScript.Enabled = false;
            this.toolStripMenuItem_File_DeleteScript.Image = global::RalphsDotNet.Apps.OptimizationStudio.Properties.Resources.page_white_cross;
            this.toolStripMenuItem_File_DeleteScript.Name = "toolStripMenuItem_File_DeleteScript";
            this.toolStripMenuItem_File_DeleteScript.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D)));
            this.toolStripMenuItem_File_DeleteScript.Size = new System.Drawing.Size(186, 22);
            this.toolStripMenuItem_File_DeleteScript.Text = "&Delete Script";
            this.toolStripMenuItem_File_DeleteScript.Click += new System.EventHandler(this.ToolStripMenuItemDeleteScriptClick);
            // 
            // toolStripMenuItem_File_CloseScript
            // 
            this.toolStripMenuItem_File_CloseScript.Enabled = false;
            this.toolStripMenuItem_File_CloseScript.Image = global::RalphsDotNet.Apps.OptimizationStudio.Properties.Resources.page_white_delete;
            this.toolStripMenuItem_File_CloseScript.Name = "toolStripMenuItem_File_CloseScript";
            this.toolStripMenuItem_File_CloseScript.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.W)));
            this.toolStripMenuItem_File_CloseScript.Size = new System.Drawing.Size(186, 22);
            this.toolStripMenuItem_File_CloseScript.Text = "C&lose Script";
            this.toolStripMenuItem_File_CloseScript.Click += new System.EventHandler(this.ToolStripMenuItemFileCloseScriptClick);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(183, 6);
            // 
            // ToolStripMenuItem_File_Quit
            // 
            this.ToolStripMenuItem_File_Quit.Image = global::RalphsDotNet.Apps.OptimizationStudio.Properties.Resources.cross;
            this.ToolStripMenuItem_File_Quit.Name = "ToolStripMenuItem_File_Quit";
            this.ToolStripMenuItem_File_Quit.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Q)));
            this.ToolStripMenuItem_File_Quit.Size = new System.Drawing.Size(186, 22);
            this.ToolStripMenuItem_File_Quit.Text = "&Quit";
            this.ToolStripMenuItem_File_Quit.Click += new System.EventHandler(this.ToolStripMenuItemFileQuitClick);
            // 
            // toolStripMenuItem_Edit
            // 
            this.toolStripMenuItem_Edit.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem_Edit_Undo,
            this.toolStripMenuItem_Edit_Redo,
            this.toolStripSeparator8,
            this.toolStripMenuItem_Edit_Cut,
            this.toolStripMenuItem_Edit_Copy,
            this.toolStripMenuItem_Edit_Paste,
            this.toolStripSeparator7,
            this.toolStripMenuItem_Edit_Search});
            this.toolStripMenuItem_Edit.Name = "toolStripMenuItem_Edit";
            this.toolStripMenuItem_Edit.Size = new System.Drawing.Size(39, 20);
            this.toolStripMenuItem_Edit.Text = "&Edit";
            // 
            // toolStripMenuItem_Edit_Undo
            // 
            this.toolStripMenuItem_Edit_Undo.Enabled = false;
            this.toolStripMenuItem_Edit_Undo.Image = global::RalphsDotNet.Apps.OptimizationStudio.Properties.Resources.arrow_undo;
            this.toolStripMenuItem_Edit_Undo.Name = "toolStripMenuItem_Edit_Undo";
            this.toolStripMenuItem_Edit_Undo.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Z)));
            this.toolStripMenuItem_Edit_Undo.Size = new System.Drawing.Size(149, 22);
            this.toolStripMenuItem_Edit_Undo.Text = "&Undo";
            this.toolStripMenuItem_Edit_Undo.Click += new System.EventHandler(this.ToolStripMenuItemEditUndoClick);
            // 
            // toolStripMenuItem_Edit_Redo
            // 
            this.toolStripMenuItem_Edit_Redo.Enabled = false;
            this.toolStripMenuItem_Edit_Redo.Image = global::RalphsDotNet.Apps.OptimizationStudio.Properties.Resources.arrow_redo;
            this.toolStripMenuItem_Edit_Redo.Name = "toolStripMenuItem_Edit_Redo";
            this.toolStripMenuItem_Edit_Redo.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Y)));
            this.toolStripMenuItem_Edit_Redo.Size = new System.Drawing.Size(149, 22);
            this.toolStripMenuItem_Edit_Redo.Text = "&Redo";
            this.toolStripMenuItem_Edit_Redo.Click += new System.EventHandler(this.ToolStripMenuItemEditRedoClick);
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(146, 6);
            // 
            // toolStripMenuItem_Edit_Cut
            // 
            this.toolStripMenuItem_Edit_Cut.Enabled = false;
            this.toolStripMenuItem_Edit_Cut.Image = global::RalphsDotNet.Apps.OptimizationStudio.Properties.Resources.cut;
            this.toolStripMenuItem_Edit_Cut.Name = "toolStripMenuItem_Edit_Cut";
            this.toolStripMenuItem_Edit_Cut.Size = new System.Drawing.Size(149, 22);
            this.toolStripMenuItem_Edit_Cut.Text = "&Cut";
            this.toolStripMenuItem_Edit_Cut.Click += new System.EventHandler(this.ToolStripMenuItemEditCutClick);
            // 
            // toolStripMenuItem_Edit_Copy
            // 
            this.toolStripMenuItem_Edit_Copy.Enabled = false;
            this.toolStripMenuItem_Edit_Copy.Image = global::RalphsDotNet.Apps.OptimizationStudio.Properties.Resources.page_white_copy;
            this.toolStripMenuItem_Edit_Copy.Name = "toolStripMenuItem_Edit_Copy";
            this.toolStripMenuItem_Edit_Copy.Size = new System.Drawing.Size(149, 22);
            this.toolStripMenuItem_Edit_Copy.Text = "C&opy";
            this.toolStripMenuItem_Edit_Copy.Click += new System.EventHandler(this.ToolStripMenuItemEditCopyClick);
            // 
            // toolStripMenuItem_Edit_Paste
            // 
            this.toolStripMenuItem_Edit_Paste.Enabled = false;
            this.toolStripMenuItem_Edit_Paste.Image = global::RalphsDotNet.Apps.OptimizationStudio.Properties.Resources.page_white_paste;
            this.toolStripMenuItem_Edit_Paste.Name = "toolStripMenuItem_Edit_Paste";
            this.toolStripMenuItem_Edit_Paste.Size = new System.Drawing.Size(149, 22);
            this.toolStripMenuItem_Edit_Paste.Text = "&Paste";
            this.toolStripMenuItem_Edit_Paste.Click += new System.EventHandler(this.ToolStripMenuItemEditPasteClick);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(146, 6);
            // 
            // toolStripMenuItem_Edit_Search
            // 
            this.toolStripMenuItem_Edit_Search.Enabled = false;
            this.toolStripMenuItem_Edit_Search.Image = global::RalphsDotNet.Apps.OptimizationStudio.Properties.Resources.magnifier;
            this.toolStripMenuItem_Edit_Search.Name = "toolStripMenuItem_Edit_Search";
            this.toolStripMenuItem_Edit_Search.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F)));
            this.toolStripMenuItem_Edit_Search.Size = new System.Drawing.Size(149, 22);
            this.toolStripMenuItem_Edit_Search.Text = "&Search";
            this.toolStripMenuItem_Edit_Search.Click += new System.EventHandler(this.ToolStripMenuItemEditSearchClick);
            // 
            // optimizationToolStripMenuItem
            // 
            this.optimizationToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem_Optimization_Debug,
            this.toolStripMenuItem_Optimization_Run,
            this.toolStripMenuItem_Optimization_Pause,
            this.toolStripMenuItem_Optimization_Stop});
            this.optimizationToolStripMenuItem.Name = "optimizationToolStripMenuItem";
            this.optimizationToolStripMenuItem.Size = new System.Drawing.Size(88, 20);
            this.optimizationToolStripMenuItem.Text = "&Optimization";
            // 
            // toolStripMenuItem_Optimization_Debug
            // 
            this.toolStripMenuItem_Optimization_Debug.Enabled = false;
            this.toolStripMenuItem_Optimization_Debug.Image = global::RalphsDotNet.Apps.OptimizationStudio.Properties.Resources.control_end;
            this.toolStripMenuItem_Optimization_Debug.Name = "toolStripMenuItem_Optimization_Debug";
            this.toolStripMenuItem_Optimization_Debug.ShortcutKeys = System.Windows.Forms.Keys.F5;
            this.toolStripMenuItem_Optimization_Debug.Size = new System.Drawing.Size(151, 22);
            this.toolStripMenuItem_Optimization_Debug.Text = "Debug";
            this.toolStripMenuItem_Optimization_Debug.Click += new System.EventHandler(this.ToolStripMenuItemOptimizationDebugClick);
            // 
            // toolStripMenuItem_Optimization_Run
            // 
            this.toolStripMenuItem_Optimization_Run.Enabled = false;
            this.toolStripMenuItem_Optimization_Run.Image = global::RalphsDotNet.Apps.OptimizationStudio.Properties.Resources.control_play;
            this.toolStripMenuItem_Optimization_Run.Name = "toolStripMenuItem_Optimization_Run";
            this.toolStripMenuItem_Optimization_Run.ShortcutKeys = System.Windows.Forms.Keys.F6;
            this.toolStripMenuItem_Optimization_Run.Size = new System.Drawing.Size(151, 22);
            this.toolStripMenuItem_Optimization_Run.Text = "&Run";
            this.toolStripMenuItem_Optimization_Run.Click += new System.EventHandler(this.ToolStripMenuItemOptimizationRunClick);
            // 
            // toolStripMenuItem_Optimization_Pause
            // 
            this.toolStripMenuItem_Optimization_Pause.Enabled = false;
            this.toolStripMenuItem_Optimization_Pause.Image = global::RalphsDotNet.Apps.OptimizationStudio.Properties.Resources.control_pause;
            this.toolStripMenuItem_Optimization_Pause.Name = "toolStripMenuItem_Optimization_Pause";
            this.toolStripMenuItem_Optimization_Pause.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F6)));
            this.toolStripMenuItem_Optimization_Pause.Size = new System.Drawing.Size(151, 22);
            this.toolStripMenuItem_Optimization_Pause.Text = "Pause";
            this.toolStripMenuItem_Optimization_Pause.Visible = false;
            this.toolStripMenuItem_Optimization_Pause.Click += new System.EventHandler(this.ToolStripMenuItemOptimizationPauseClick);
            // 
            // toolStripMenuItem_Optimization_Stop
            // 
            this.toolStripMenuItem_Optimization_Stop.Enabled = false;
            this.toolStripMenuItem_Optimization_Stop.Image = global::RalphsDotNet.Apps.OptimizationStudio.Properties.Resources.control_stop;
            this.toolStripMenuItem_Optimization_Stop.Name = "toolStripMenuItem_Optimization_Stop";
            this.toolStripMenuItem_Optimization_Stop.ShortcutKeys = System.Windows.Forms.Keys.F7;
            this.toolStripMenuItem_Optimization_Stop.Size = new System.Drawing.Size(151, 22);
            this.toolStripMenuItem_Optimization_Stop.Text = "&Stop";
            this.toolStripMenuItem_Optimization_Stop.Click += new System.EventHandler(this.ToolStripMenuItemOptimizationStopClick);
            // 
            // toolStripMenuItem_Help
            // 
            this.toolStripMenuItem_Help.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem_Help_Info});
            this.toolStripMenuItem_Help.Name = "toolStripMenuItem_Help";
            this.toolStripMenuItem_Help.Size = new System.Drawing.Size(44, 20);
            this.toolStripMenuItem_Help.Text = "&Help";
            // 
            // toolStripMenuItem_Help_Info
            // 
            this.toolStripMenuItem_Help_Info.Image = global::RalphsDotNet.Apps.OptimizationStudio.Properties.Resources.information;
            this.toolStripMenuItem_Help_Info.Name = "toolStripMenuItem_Help_Info";
            this.toolStripMenuItem_Help_Info.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.I)));
            this.toolStripMenuItem_Help_Info.Size = new System.Drawing.Size(132, 22);
            this.toolStripMenuItem_Help_Info.Text = "&Info";
            // 
            // menuStrip
            // 
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem_File,
            this.toolStripMenuItem_Edit,
            this.optimizationToolStripMenuItem,
            this.toolStripMenuItem_Help});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(1058, 24);
            this.menuStrip.TabIndex = 2;
            this.menuStrip.Text = "menuStrip1";
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1058, 431);
            this.Controls.Add(this.toolStripContainer);
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.menuStrip);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip;
            this.Name = "MainWindow";
            this.Text = "Ralphs Optimization Studio";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.toolStripContainer.ContentPanel.ResumeLayout(false);
            this.toolStripContainer.TopToolStripPanel.ResumeLayout(false);
            this.toolStripContainer.TopToolStripPanel.PerformLayout();
            this.toolStripContainer.ResumeLayout(false);
            this.toolStripContainer.PerformLayout();
            this.toolStrip.ResumeLayout(false);
            this.toolStrip.PerformLayout();
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private System.Windows.Forms.ToolStripContainer toolStripContainer;
        private DockDotNET.DockManager dockManager;
        private System.Windows.Forms.ToolStrip toolStrip;
        private System.Windows.Forms.ToolStripButton toolStripButton_NewProject;
        private System.Windows.Forms.ToolStripButton toolStripButton_OpenProject;
        private System.Windows.Forms.ToolStripButton toolStripButton_SaveProject;
        private System.Windows.Forms.ToolStripButton toolStripButton_CloseProject;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton toolStripButton_NewScript;
        private System.Windows.Forms.ToolStripButton toolStripButton_RenameScript;
        private System.Windows.Forms.ToolStripButton toolStripButton_DeleteScript;
        private System.Windows.Forms.ToolStripButton toolStripButton_CloseScript;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton toolStripButton_Undo;
        private System.Windows.Forms.ToolStripButton toolStripButton_Redo;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton toolStripButton_Cut;
        private System.Windows.Forms.ToolStripButton toolStripButton_Copy;
        private System.Windows.Forms.ToolStripButton toolStripButton_Paste;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
        private System.Windows.Forms.ToolStripButton toolStripButton_DebugOptimization;
        private System.Windows.Forms.ToolStripButton toolStripButton_RunOptimization;
        private System.Windows.Forms.ToolStripButton toolStripButton_StopOptimization;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel_Round;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel_Time;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripButton toolStripButton_Search;
        private System.Windows.Forms.ToolStripButton toolStripButton_PauseOptimization;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem_File;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem_File_NewProject;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem_File_OpenProject;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem_File_SaveProject;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem_File_CloseProject;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem_File_NewScript;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem_File_RenameScript;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem_File_DeleteScript;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem_File_CloseScript;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItem_File_Quit;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem_Edit;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem_Edit_Undo;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem_Edit_Redo;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem_Edit_Cut;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem_Edit_Copy;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem_Edit_Paste;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem_Edit_Search;
        private System.Windows.Forms.ToolStripMenuItem optimizationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem_Optimization_Debug;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem_Optimization_Run;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem_Optimization_Pause;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem_Optimization_Stop;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem_Help;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem_Help_Info;
        private System.Windows.Forms.MenuStrip menuStrip;
    }
}

