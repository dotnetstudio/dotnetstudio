﻿using DockDotNET;
using System;
using System.Collections.Generic;
using System.Linq;

using OxyPlot;
using RalphsDotNet.Apps.OptimizationStudio.BusinessLogic;
using OxyPlot.Axes;
using OxyPlot.Series;

namespace RalphsDotNet.Apps.OptimizationStudio
{
    public partial class PopulationPlotViewer : DockWindow, IPopulationPlottingProvider
    {
        private readonly List<ScatterSeries> _scatterSeriesCollection = new List<ScatterSeries>();

        public PopulationPlotViewer()
        {
            InitializeComponent();

            this.plot.Model = new PlotModel("Optimization Plot");
            this.plot.Model.Axes.Add(new LinearAxis(AxisPosition.Left, "Quality") { Minimum = 0 });
            this.plot.Model.Axes.Add(new LinearAxis(AxisPosition.Bottom, "Lifetime") { Minimum = 0 });
            this.plot.Model.PlotType = PlotType.XY;

            this.InitPlot();
        }

        private void InitPlot()
        {
            this._scatterSeriesCollection.Add(new ScatterSeries(string.Empty, OxyColors.Black, 1));
            this.plot.Model.Series.Add(this._scatterSeriesCollection.First());
        }

        public void Clear()
        {
            this.Invoke(new ClearHandler(this.ClearInternal));
        }

        private delegate void ClearHandler();
        private void ClearInternal()
        {
            this.plot.Model.Series.Clear();
            this._scatterSeriesCollection.Clear();

            this.InitPlot();
        }

        public void NewRound()
        {
            this.Invoke(new NewRoundHandler(this.NewRoundInternal));
        }

        private delegate void NewRoundHandler();
        private void NewRoundInternal()
        {
            var toDelete = new List<ScatterSeries>();
            foreach (var scatterSeries in this._scatterSeriesCollection)
            {
                if (scatterSeries.MarkerFill.A > 20)
                    scatterSeries.MarkerFill.A -= 20;
                else
                    toDelete.Add(scatterSeries);
            }

            foreach (var scatterSeries in toDelete)
            {
                this.plot.Model.Series.Remove(scatterSeries);
                this._scatterSeriesCollection.Remove(scatterSeries);
            }

            this._scatterSeriesCollection.Add(new ScatterSeries(string.Empty, OxyColors.Black, 1));
            this.plot.Model.Series.Add(this._scatterSeriesCollection.Last());
        }

        public void Plot(int lifeTime, double quality)
        {
            this.BeginInvoke(new PlotHandler(this.PlotInternal), lifeTime, quality);
        }

        private delegate void PlotHandler(int lifeTime, double quality);
        private void PlotInternal(int lifeTime, double quality)
        {
            if (!this._scatterSeriesCollection.Last().Points.Any(p => p.X == lifeTime && p.Y == quality)) this._scatterSeriesCollection.Last().Points.Add(new DataPoint(lifeTime, quality));
        }

        private void TimerTick(object sender, EventArgs e)
        {
            this.plot.RefreshPlot(true);
        }
    }
}
