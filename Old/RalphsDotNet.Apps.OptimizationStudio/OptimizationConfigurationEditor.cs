﻿using System.ComponentModel;
using System.Windows.Forms;
using DockDotNET;
using RalphsDotNet.Apps.OptimizationStudio.BusinessLogic;
using RalphsDotNet.Service.Optimization.Interface;

namespace RalphsDotNet.Apps.OptimizationStudio
{
    public partial class OptimizationConfigurationEditor : DockWindow
    {
        private readonly MainWindow _mainWindow;

        public OptimizationProject Project { get { return this._mainWindow != null ? this._mainWindow.Project : null; } }

        public OptimizationConfigurationEditor(MainWindow mainWindow)
        {
            this._mainWindow = mainWindow;

            InitializeComponent();

            this._mainWindow.PropertyChanged += mainWindow_PropertyChanged;
        }

        void mainWindow_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Project")
            {
                if (this._mainWindow.Project != null)
                {
                    this.propertyGrid.SelectedObject = this.Project.OptimizerConfiguration;
                    this.propertyGrid.Enabled = true;
                }
                else
                {
                    this.propertyGrid.Enabled = false;
                    this.propertyGrid.SelectedObject = null;
                }
            }
        }

        private void propertyGrid_PropertyValueChanged(object s, PropertyValueChangedEventArgs e)
        {
            this.Project.OptimizerConfiguration = this.propertyGrid.SelectedObject as OptimizerConfiguration;
        }
    }
}
