﻿using System;
using System.Windows.Forms;

namespace RalphsDotNet.Apps.OptimizationStudio
{
    public partial class RenameBox : Form
    {
        public static string ShowDialog(string name, string title)
        {
            var box = new RenameBox {Name = name, Text = title};
            var result = box.ShowDialog();

            return result == DialogResult.OK ? box.Name : null;
        }

        private string _name;
        public new string Name
        {
            get { return this._name; }
            set
            {
                this._name = value;
                this.textBox_Name.Text = value;
                base.Name = value;
            }
        }

        private RenameBox()
        {
            InitializeComponent();
        }

        private void textBox_Name_TextChanged(object sender, EventArgs e)
        {
            this._name = this.textBox_Name.Text;
        }

        private void button_OK_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void button_Cancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }
    }
}
