﻿namespace RalphsDotNet.Apps.OptimizationStudio.BusinessLogic
{
    public interface ICurvePlottingProvider
    {
        void Clear();
        void PlotCalculated(double x, double y);
        void PlotExpected(double x, double y);
    }
}
