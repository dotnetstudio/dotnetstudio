﻿namespace RalphsDotNet.Apps.OptimizationStudio.BusinessLogic
{
    public interface IPopulationPlottingProvider
    {
        void Clear();
        void NewRound();
        void Plot(int lifeTime, double quality);
    }
}
