﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using Ionic.Zip;
using IronPython.Hosting;
using Microsoft.Scripting.Hosting;
using RalphsDotNet.Extension;
using RalphsDotNet.Service;
using RalphsDotNet.Service.Optimization.Genetic;
using RalphsDotNet.Service.Optimization.Interface;
using RalphsDotNet.Types;

namespace RalphsDotNet.Apps.OptimizationStudio.BusinessLogic
{
    public class OptimizationProject : NotifyPropertyChangedBase
    {
        public const string OptimizerConfig = "OptimizerConfig";
        public const string ScriptsDirectory = "Scripts";
        public const string MainScriptName = "Main";
        public const string ScriptType = "Python";
        public const string ScriptExtension = ".py";
        private readonly Queue<ScriptEngine> _scriptEngines = new Queue<ScriptEngine>();
        public ICurvePlottingProvider CurvePlottingProvider;
        public IPopulationPlottingProvider PopulationPlottingProvider;
        public IScriptOutputProvider ScriptOutputProvider;
        private Thread _debugThread;

        private string _filePath;
        private OptimizerConfiguration _optimizerConfiguration;
        private bool _running;
        private Exception _runtimeError;
        private Hashtable _scriptGlobals;

        private IOptimizerService _service;

        public OptimizationProject(string filePath, IScriptOutputProvider scriptOutputProvider,
            IPopulationPlottingProvider populationPlottingProvider, ICurvePlottingProvider curvePlottingProvider)
        {
            this.ScriptNames = new ObservableCollection<string>();

            this.FilePath = filePath;

            this.PopulationPlottingProvider = populationPlottingProvider;
            this.CurvePlottingProvider = curvePlottingProvider;
            this.ScriptOutputProvider = scriptOutputProvider;

            if (!File.Exists(this.FilePath))
            {
                this.Create();
            }

            this.ReadScriptNames();
        }

        public string FilePath
        {
            get { return this._filePath; }
            private set
            {
                if (this._filePath != value)
                {
                    this._filePath = value;
                    this.OnPropertyChanged("FilePath");
                }
            }
        }

        public IOptimizerService Service
        {
            get { return this._service; }
            private set
            {
                if (this._service != value)
                {
                    this._service = value;
                    this.OnPropertyChanged("Service");
                }
            }
        }

        public ObservableCollection<string> ScriptNames { get; private set; }

        public OptimizerConfiguration OptimizerConfiguration
        {
            get
            {
                if (this._optimizerConfiguration == null)
                {
                    using (var projFile = ZipFile.Read(this.FilePath))
                    {
                        using (var ms = new MemoryStream())
                        {
                            projFile.Entries.First(e => e.FileName == OptimizerConfig)
                                .Extract(ms);

                            var buf = new byte[ms.Length];
                            ms.Seek(0, SeekOrigin.Begin);
                            ms.Read(buf, 0, Convert.ToInt32(ms.Length));

                            this._optimizerConfiguration =
                                ms.GetBuffer().DeserializeDataContract<OptimizerConfiguration>();
                            this._optimizerConfiguration.PropertyChanged += this.OptimizerConfigurationPropertyChanged;
                        }
                    }
                }

                return this._optimizerConfiguration;
            }
            set
            {
                if (this._optimizerConfiguration != value)
                {
                    this._optimizerConfiguration.PropertyChanged -= this.OptimizerConfigurationPropertyChanged;

                    this._optimizerConfiguration = value;

                    this.SaveOptimizerConfiguration();

                    this._optimizerConfiguration.PropertyChanged += this.OptimizerConfigurationPropertyChanged;

                    this.OnPropertyChanged("OptimizerConfiguration");
                }
            }
        }

        public bool IsRunning
        {
            get { return this._running; }
            private set
            {
                if (this._running != value)
                {
                    this._running = value;
                    this.OnPropertyChanged("IsRunning");
                }
            }
        }

        public Exception RuntimeError
        {
            get { return this._runtimeError; }
            private set
            {
                if (this._runtimeError != value)
                {
                    this._runtimeError = value;

                    if (this.RuntimeError != null)
                        this.Stop();

                    this.OnPropertyChanged("RuntimeError");
                }
            }
        }

        private void SaveOptimizerConfiguration()
        {
            using (var projFile = ZipFile.Read(this.FilePath))
            {
                projFile.RemoveEntry(projFile.Entries.First(e => e.FileName == OptimizerConfig));

                projFile.AddEntry(OptimizerConfig, this._optimizerConfiguration.SerializeDataContract());

                projFile.Save();
            }
        }

        private void OptimizerConfigurationPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            this.SaveOptimizerConfiguration();
        }

        private void Create()
        {
            using (var projFile = new ZipFile())
            {
                var config = new OptimizerConfiguration {ServiceAssembly = typeof (OptimizerService).Assembly};
                projFile.AddEntry(OptimizerConfig, config.SerializeDataContract());

                projFile.AddDirectoryByName(ScriptsDirectory);


                projFile.AddEntry(Path.Combine(ScriptsDirectory, string.Concat(MainScriptName, ScriptExtension)),
                    new byte[] {});

                projFile.Save(this.FilePath);
            }
        }

        private void ReadScriptNames()
        {
            this.ScriptNames.Clear();

            using (var projFile = ZipFile.Read(this.FilePath))
            {
                projFile.EntryFileNames
                    .Where(e => e.StartsWith(ScriptsDirectory) && e.EndsWith(ScriptExtension)).ToList()
                    .ForEach(x => this.ScriptNames.Add(Path.GetFileName(x)));
            }
        }

        public string ReadScript(string scriptName)
        {
            using (var projFile = ZipFile.Read(this.FilePath))
            {
                using (var ms = new MemoryStream())
                {
                    projFile.Entries.First(
                        e => e.FileName.StartsWith(ScriptsDirectory) && e.FileName.EndsWith(scriptName))
                        .Extract(ms);

                    var buf = new byte[ms.Length];
                    ms.Seek(0, SeekOrigin.Begin);
                    ms.Read(buf, 0, Convert.ToInt32(ms.Length));

                    return Encoding.Default.GetString(buf);
                }
            }
        }

        public void WriteScript(string scriptName, string script)
        {
            using (var projFile = ZipFile.Read(this.FilePath))
            {
                if (this.ScriptNames.Contains(scriptName)) // overwrite
                {
                    projFile.RemoveEntry(
                        projFile.Entries.First(
                            e => e.FileName.StartsWith(ScriptsDirectory) && e.FileName.EndsWith(scriptName)));
                }

                projFile.AddEntry(Path.Combine(ScriptsDirectory, scriptName), Encoding.Default.GetBytes(script));

                if (!this.ScriptNames.Contains(scriptName))
                    this.ScriptNames.Add(scriptName);

                projFile.Save();
            }
        }

        public void RemoveScript(string scriptName)
        {
            using (var projFile = ZipFile.Read(this.FilePath))
            {
                if (this.ScriptNames.Contains(scriptName)) // overwrite
                {
                    projFile.RemoveEntry(
                        projFile.Entries.First(
                            e => e.FileName.StartsWith(ScriptsDirectory) && e.FileName.EndsWith(scriptName)));
                }

                if (this.ScriptNames.Contains(scriptName))
                    this.ScriptNames.Remove(scriptName);

                projFile.Save();
            }
        }

        public void Debug()
        {
            this.ScriptOutputProvider.Clear();
            this.IsRunning = true;
            this.RuntimeError = null;

            try
            {
                this._debugThread = new Thread(this.DebugInternal);
                this._debugThread.Start();
            }
            catch (Exception ex)
            {
                this.RuntimeError = ex;
            }
        }

        private void DebugInternal()
        {
            this.ScriptOutputProvider.WriteLine("-----\tDEBUG BEGIN\t-----");
            this.InitScripting();

            var debugBytes =
                new byte[
                    this.OptimizerConfiguration.GeneAmount*this.OptimizerConfiguration.GeneSize -
                    this.OptimizerConfiguration.DeadEndSize];
            (new Random()).NextBytes(debugBytes);

            var retVal = this.EvalIndividual(debugBytes, 0, true);
            this.ScriptOutputProvider.WriteLine("-----\tDEBUG END\t-----");
            this.ScriptOutputProvider.WriteLine(string.Format("Return Value: <{0}>", retVal));

            this._debugThread = null;
            this.IsRunning = false;
        }

        public void Start()
        {
            if (this.Service == null || !this.Service.IsPaused)
            {
                this.ScriptOutputProvider.Clear();
                this.RuntimeError = null;
                this.IsRunning = true;

                try
                {
                    this.InitScripting();

                    this.PopulationPlottingProvider.Clear();

                    this.OptimizerConfiguration.CalculateFunction = this.EvalIndividual;
                    this.Service = ServiceProvider.GetService<IOptimizerService>(this.OptimizerConfiguration);

                    this.Service.RoundBegun += this.ServiceRoundBegun;
                    this.Service.RoundFinished += this.ServiceRoundFinished;

                    this.Service.Run();
                }
                catch (Exception ex)
                {
                    this.RuntimeError = ex;
                }
            }
            else
            {
                this.Service.Run();
            }
        }

        private void ServiceRoundFinished(int roundNum, IEnumerable<Tuple<double, byte[]>> successful)
        {
            this.EvalRound(roundNum, false);
        }

        private void ServiceRoundBegun(int rountNum, IEnumerable<Tuple<double, byte[]>> successful)
        {
            this.PopulationPlottingProvider.Clear();
        }

        private void InitScripting()
        {
            this._scriptEngines.Clear();

            GC.Collect();

            this.CurvePlottingProvider.Clear();
            this._scriptGlobals = new Hashtable();
        }

        public void Pause()
        {
            this.Service.Pause();
        }

        public void Stop()
        {
            if (this._debugThread != null)
            {
                var lDebugThread = this._debugThread;
                this._debugThread = null;
                this.IsRunning = false;
                lDebugThread.Abort();
            }
            else
            {
                this.Service.RoundFinished -= this.ServiceRoundFinished;
                this.Service.RoundBegun -= this.ServiceRoundBegun;
                this.Service.Stop();
                this.Service.Dispose();
                this.IsRunning = false;
            }
        }

        public void EvalIndividual(byte[] bytes, int round, FunctionCallbackHandler callback)
        {
            callback(this.EvalIndividual(bytes, round, false));
        }

        // TODO Ralph's Optimization Studio - REQ: run optimizer and scripts in an own appdomain
        public double EvalIndividual(byte[] bytes, int round, bool debug)
        {
            if (debug)
                this.PopulationPlottingProvider.Clear();

            var retVal = Double.MaxValue;

            if (this.RuntimeError == null)
            {
                ScriptEngine scriptEngine;

                lock (this._scriptEngines)
                {
                    scriptEngine = this._scriptEngines.Count > 1 ? this._scriptEngines.Dequeue() : Python.CreateEngine();
                }

                var script = this.GetScript(scriptEngine);
                var scope = scriptEngine.CreateScope();
                Thread execThread = null;

                try
                {
                    scope.SetVariable("RunAsIndividual", true);
                    scope.SetVariable("Debug", debug);

                    scope.SetVariable("Bytes", bytes);

                    scope.SetVariable("Quality", retVal);

                    var execStartTime = DateTime.UtcNow;
                    execThread = new Thread(this.ExecuteEval);
                    execThread.Start(new object[] {scope, script});

                    while ((this.Service == null || this.Service.OptimizerConfiguration.EvalTimeout == 0 ||
                            (DateTime.UtcNow - execStartTime).TotalMilliseconds <
                            this.Service.OptimizerConfiguration.EvalTimeout) &&
                           execThread.ThreadState == ThreadState.Running)
                    {
                        Thread.Sleep(10);
                    }

                    if (execThread.ThreadState == ThreadState.Running)
                        execThread.Abort();

                    retVal = scope.GetVariable("Quality");

                    this.PopulationPlottingProvider.Plot(round + 1, retVal);
                }
                catch (ThreadAbortException)
                {
                    if (execThread != null && execThread.ThreadState == ThreadState.Running)
                        execThread.Abort();

                    throw;
                }
                catch (Exception ex)
                {
                    this.OutputException(ex, script.Engine);
                }

                lock (this._scriptEngines)
                {
                    this._scriptEngines.Enqueue(scriptEngine);
                }
            }

            return retVal;
        }

        public void EvalRound(int round, bool debug)
        {
            if (this.RuntimeError == null)
            {
                ScriptEngine scriptEngine;

                lock (this._scriptEngines)
                {
                    scriptEngine = this._scriptEngines.Count > 1 ? this._scriptEngines.Dequeue() : Python.CreateEngine();
                }

                var script = this.GetScript(scriptEngine);
                var scope = scriptEngine.CreateScope();
                Thread execThread = null;

                try
                {
                    scope.SetVariable("RunAsIndividual", false);
                    scope.SetVariable("Debug", debug);
                    scope.SetVariable("Output", this.ScriptOutputProvider);
                    scope.SetVariable("Plot", this.CurvePlottingProvider);

                    scope.SetVariable(
                        "SuccessfulBytes",
                        this.Service != null && this.Service.LastSuccessful != null && this.Service.LastSuccessful.Any()
                            ? this.Service.LastSuccessful.First().Item2
                            : new byte[] {});
                    scope.SetVariable(
                        "SuccessfulQuality",
                        this.Service != null && this.Service.LastSuccessful != null && this.Service.LastSuccessful.Any()
                            ? this.Service.LastSuccessful.First().Item1
                            : Double.MaxValue);

                    scope.SetVariable("Globals", this._scriptGlobals);

                    scope.SetVariable("Round", this.Service != null ? this.Service.Round : 0);

                    var execStartTime = DateTime.UtcNow;
                    execThread = new Thread(this.ExecuteEval);
                    execThread.Start(new object[] {scope, script});

                    while ((this.Service == null || this.Service.OptimizerConfiguration.EvalTimeout == 0 ||
                            (DateTime.UtcNow - execStartTime).TotalMilliseconds <
                            this.Service.OptimizerConfiguration.EvalTimeout) &&
                           execThread.ThreadState == ThreadState.Running)
                    {
                        Thread.Sleep(10);
                    }

                    if (execThread.ThreadState == ThreadState.Running)
                        execThread.Abort();
                }
                catch (ThreadAbortException)
                {
                    if (execThread != null && execThread.ThreadState == ThreadState.Running)
                        execThread.Abort();

                    throw;
                }
                catch (Exception ex)
                {
                    this.OutputException(ex, script.Engine);
                }

                lock (this._scriptEngines)
                {
                    this._scriptEngines.Enqueue(scriptEngine);
                }
            }
        }

        private ScriptSource GetScript(ScriptEngine scriptEngine)
        {
            var runtimeDir = Path.Combine(Path.GetTempPath(), Guid.NewGuid().ToString());
            if (!Directory.Exists(runtimeDir))
                Directory.CreateDirectory(runtimeDir);

            var searchPaths = scriptEngine.GetSearchPaths();
            searchPaths.Add(runtimeDir);
// ReSharper disable AssignNullToNotNullAttribute
            searchPaths.Add(Path.Combine(Path.GetDirectoryName(this.GetType().Assembly.Location), "Resources", "PyLib"));
// ReSharper restore AssignNullToNotNullAttribute
            scriptEngine.SetSearchPaths(searchPaths);

            foreach (var scriptName in this.ScriptNames)
            {
                File.WriteAllText(Path.Combine(runtimeDir, scriptName), this.ReadScript(scriptName));
            }

            var scriptSource =
                scriptEngine.CreateScriptSourceFromFile(Path.Combine(runtimeDir,
                    string.Concat(MainScriptName, ScriptExtension)));

            return scriptSource;
        }

        private void ExecuteEval(object param)
        {
            var scope = (ScriptScope) ((object[]) param)[0];
            var script = (ScriptSource) ((object[]) param)[1];

            try
            {
                script.Execute(scope);
            }
            catch (ThreadAbortException)
            {
                throw;
            }
            catch (Exception ex)
            {
                this.OutputException(ex, script.Engine);
            }
        }

        private void OutputException(Exception ex, ScriptEngine scriptEngine)
        {
            this.ScriptOutputProvider.NewLine();
#if DEBUG
            this.ScriptOutputProvider.WriteLine(ex);
#else
                    this.ScriptOutputProvider.WriteLine(ex.Message);
#endif
            var eo = scriptEngine.GetService<ExceptionOperations>();
            this.ScriptOutputProvider.WriteLine(eo.FormatException(ex));

            this.RuntimeError = ex;
        }
    }
}