﻿namespace RalphsDotNet.Apps.OptimizationStudio.BusinessLogic
{
    public interface IScriptOutputProvider
    {
        void Write(object output);
        void WriteLine(object output);
        void NewLine();
        void Clear();
    }
}
