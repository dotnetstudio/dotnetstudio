﻿namespace RalphsDotNet.Service.Optimization.Interface
{
    public enum ModelFormat
    {
        CSharp,
        Python,
        Excel
    }
}
