﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using RalphsDotNet.Extension;

namespace RalphsDotNet.Service.Optimization.Interface
{
    [DataContract]
    public class OptimizerConfiguration : ServiceConfiguration
    {
        #region Runtime

        private int _evalTimeout;
        private bool _multithreading;

        /// <summary>
        ///     Enable/Disable Multithreading
        /// </summary>
        [DataMember(Order = 100)]
        [DisplayName("1. Multithreading")]
        [Category("1. Runtime")]
        [Description("Enable/Disable Multithreading.")]
        public bool Multithreading
        {
            get { return this._multithreading; }
            set
            {
                if (this._multithreading != value)
                {
                    this._multithreading = value;
                    this.OnPropertyChanged("Multithreading");
                }
            }
        }

        /// <summary>
        ///     Defines a timeout for evaluation in miliseconds, 0 = disabled
        /// </summary>
        [DataMember(Order = 101)]
        [DisplayName("2. Evaluation Timeout")]
        [Category("1. Runtime")]
        [Description("Defines a timeout for evaluation in miliseconds, 0 = disabled")]
        public int EvalTimeout
        {
            get { return this._evalTimeout; }
            set
            {
                if (this._evalTimeout != value)
                {
                    this._evalTimeout = value;
                    this.OnPropertyChanged("EvalTimeout");
                }
            }
        }

        #endregion

        #region General

        private double _clusterDifference;
        private int _initialPopulationSize;

        private int _maxLifeSpan;

        private bool _replaceDead;

        /// <summary>
        ///     Initial population size (&lt;2 is interpreted as 2)
        /// </summary>
        [DataMember(Order = 102)]
        [DisplayName("1. Initial Population Size")]
        [Category("2. General")]
        [Description("Initial population size (<2 is interpreted as 2)")]
        public int InitialPopulationSize
        {
            get { return this._initialPopulationSize; }
            set
            {
                if (this._initialPopulationSize != value)
                {
                    var val = value;
                    if (val < 2) val = 2;

                    this._initialPopulationSize = val;
                    this.OnPropertyChanged("InitialPopulationSize");
                }
            }
        }

        /// <summary>
        ///     Max life span of a successful individual (&lt;=0 means immortal)
        /// </summary>
        [DataMember(Order = 103)]
        [DisplayName("2. Max Life Span")]
        [Category("2. General")]
        [Description("Max life span of a successful individual (<= means immortal)")]
        public int MaxLifeSpan
        {
            get { return this._maxLifeSpan; }
            set
            {
                if (this._maxLifeSpan != value)
                {
                    this._maxLifeSpan = value;
                    this.OnPropertyChanged("MaxLifeSpan");
                }
            }
        }

        /// <summary>
        ///     Replace dead individuals with new ones to reach a population size which equals the initial population size
        /// </summary>
        [DataMember(Order = 104)]
        [DisplayName("3. Replace dead")]
        [Category("2. General")]
        [Description(
            "Replace dead individuals with new ones to reach a population size which equals the initial population size"
            )]
        public bool ReplaceDead
        {
            get { return this._replaceDead; }
            set
            {
                if (this._replaceDead != value)
                {
                    this._replaceDead = value;
                    this.OnPropertyChanged("ReplaceDead");
                }
            }
        }

        /// <summary>
        ///     Defines the minimum difference needed to count as separate cluster
        /// </summary>
        [DataMember(Order = 105)]
        [DisplayName("4. Cluster Difference")]
        [Category("2. General")]
        [Description("Defines the minimum difference needed to count as separate cluster")]
        public double ClusterDifference
        {
            get { return this._clusterDifference; }
            set
            {
                if (this._clusterDifference != value)
                {
                    this._clusterDifference = value;
                    this.OnPropertyChanged("ClusterDifference");
                }
            }
        }

        #endregion

        #region Genetic

        private bool _automaticGeneticGrowth;
        private int _automaticGeneticGrowthStepsSize;
        private int _deadEndSize;
        private int _geneAmount;
        private int _geneMaxValue;
        private int _geneMinValue;
        private int _geneSize;
        private bool _uniqueGenes;

        /// <summary>
        ///     Size of a gene (&lt;1 is interpreted as 1)
        /// </summary>
        [DataMember(Order = 106)]
        [DisplayName("1. Gene Size")]
        [Category("3. Genetic")]
        [Description("Size of a gene (<1 is interpreted as 1)")]
        public int GeneSize
        {
            get { return this._geneSize; }
            set
            {
                if (this._geneSize != value)
                {
                    this._geneSize = value;
                    this.OnPropertyChanged("GeneSize");
                }
            }
        }

        /// <summary>
        ///     Amount of genes (&lt;1 is interpreted as 1)
        /// </summary>
        [DataMember(Order = 107)]
        [DisplayName("2. Gene Amount")]
        [Category("3. Genetic")]
        [Description("Amount of genes (<1 is interpreted as 1)")]
        public int GeneAmount
        {
            get { return this._geneAmount; }
            set
            {
                if (this._geneAmount != value)
                {
                    this._geneAmount = value;
                    this.OnPropertyChanged("GeneAmount");
                }
            }
        }

        /// <summary>
        ///     Minimum value a gene can have (&lt;0 is interpreted as 0)
        /// </summary>
        [DataMember(Order = 120)]
        [DisplayName("3. Gene Min Value")]
        [Category("3. Genetic")]
        [Description("Minimum value a gene can have (<0 is interpreted as 0)")]
        public int GeneMinValue
        {
            get { return this._geneMinValue; }
            set
            {
                if (this._geneMinValue != value)
                {
                    var val = value;
                    if (val < 0) val = 0;

                    this._geneMinValue = val;
                    this.OnPropertyChanged("GeneMinValue");
                }
            }
        }

        /// <summary>
        ///     Maximum value a gene can have (&gt;255 is interpreted as 255)
        /// </summary>
        [DataMember(Order = 121)]
        [DisplayName("4. Gene Max Value")]
        [Category("3. Genetic")]
        [Description("Maximum value a gene can have (>255 is interpreted as 255)")]
        public int GeneMaxValue
        {
            get { return this._geneMaxValue; }
            set
            {
                if (this._geneMaxValue != value)
                {
                    var val = value;
                    if (val > 255) val = 255;

                    this._geneMaxValue = val;
                    this.OnPropertyChanged("GeneMaxValue");
                }
            }
        }

        /// <summary>
        ///     Should each gene be unique in the whole sequence?
        /// </summary>
        [DataMember(Order = 122)]
        [DisplayName("5. Unique Genes")]
        [Category("3. Genetic")]
        [Description("Should each gene be unique in the whole sequence?")]
        public bool UniqueGenes
        {
            get { return this._uniqueGenes; }
            set
            {
                if (this._uniqueGenes != value)
                {
                    this._uniqueGenes = value;
                    this.OnPropertyChanged("UniqueGenes");
                }
            }
        }


        /// <summary>
        ///     Size of the unused area in the last gene (if bigger than gene size a value of gene size - (gene size - 1) is used)
        /// </summary>
        [DataMember(Order = 108)]
        [Browsable(true)]
        [DisplayName("6. Dead End Size")]
        [Category("3. Genetic")]
        [Description(
            "Size of the unused area in the last gene (if bigger than gene size a value of gene size - (gene size - 1) is used)"
            )]
        public int DeadEndSize
        {
            get { return this._deadEndSize; }
            set
            {
                if (this._deadEndSize != value)
                {
                    this._deadEndSize = value;
                    this.OnPropertyChanged("DeadEndSize");
                }
            }
        }

        // TODO Optimizer - AutomaticGeneticGrowth
        /// <summary>
        ///     True if the genes should grow (gene amount, gene size and dead end size gets ignored)
        /// </summary>
        [DataMember(Order = 109)]
        [Browsable(false)]
        [DisplayName("7. Automatic Genetic Growth")]
        [Category("3. Genetic")]
        [Description("True if the genes should grow (gene amount, gene size and dead end size gets ignored)")]
        public bool AutomaticGeneticGrowth
        {
            get { return this._automaticGeneticGrowth; }
            set
            {
                if (this._automaticGeneticGrowth != value)
                {
                    this._automaticGeneticGrowth = value;
                    this.OnPropertyChanged("AutomaticGeneticGrowth");
                }
            }
        }

        // TODO Optimizer - AutomaticGeneticGrowthStepsSize
        /// <summary>
        ///     If AutomaticGeneticGrowth is True, which size have the growing steps?
        /// </summary>
        [DataMember(Order = 110)]
        [Browsable(false)]
        [DisplayName("8. Automatic Genetic Growth Steps Size")]
        [Category("3. Genetic")]
        [Description("If AutomaticGeneticGrowth is True, which size have the growing steps?")]
        public int AutomaticGeneticGrowthStepsSize
        {
            get { return this._automaticGeneticGrowthStepsSize; }
            set
            {
                if (this._automaticGeneticGrowthStepsSize != value)
                {
                    this._automaticGeneticGrowthStepsSize = value;
                    this.OnPropertyChanged("AutomaticGeneticGrowthStepsSize");
                }
            }
        }

        #endregion

        #region Proliferate

        private int _proliferateAmount;
        private int _topProliferateAmount;
        private int _transcriptionErrorMaxAmount;
        private int _transcriptionErrorMinAmount;
        private double _transcriptionErrorSize;

        /// <summary>
        ///     The most X successful are allowed to reproduce (&lt;10 is interpreted as 10)
        /// </summary>
        [DataMember(Order = 111)]
        [DisplayName("1. Top Amount for Proliferate")]
        [Category("4. Proliferate")]
        [Description("The most X successful are allowed to reproduce (<10 is interpreted as 10)")]
        public int TopProliferateAmount
        {
            get { return this._topProliferateAmount; }
            set
            {
                if (this._topProliferateAmount != value)
                {
                    this._topProliferateAmount = value;
                    this.OnPropertyChanged("TopProliferateAmount");
                }
            }
        }

        /// <summary>
        ///     The most X successful are allowed to reproduce Y times (&lt;1 is interpreted as 1)
        /// </summary>
        [DataMember(Order = 112)]
        [DisplayName("2. Amount of Proliferate")]
        [Category("4. Proliferate")]
        [Description("The most X successful are allowed to reproduce Y times (<1 is interpreted as 1)")]
        public int ProliferateAmount
        {
            get { return this._proliferateAmount; }
            set
            {
                if (this._proliferateAmount != value)
                {
                    this._proliferateAmount = value;
                    this.OnPropertyChanged("ProliferateAmount");
                }
            }
        }

        /// <summary>
        ///     Minimum amount of transcription errors on proliferate (&lt;2 is interpreted as 2)
        /// </summary>
        [DataMember(Order = 113)]
        [DisplayName("3. Min Amount of Transcription Error")]
        [Category("4. Proliferate")]
        [Description("Minimum amount of transcription errors on proliferate (<2 is interpreted as 2)")]
        public int TranscriptionErrorMinAmount
        {
            get { return this._transcriptionErrorMinAmount; }
            set
            {
                if (this._transcriptionErrorMinAmount != value)
                {
                    var val = value;
                    val = val >= 2 ? val : 2;

                    this._transcriptionErrorMinAmount = val;
                    this.OnPropertyChanged("TranscriptionErrorMinAmount");
                }
            }
        }

        /// <summary>
        ///     Maximum amount of transcription errors on proliferate (&lt;1 is interpreted as 1)
        /// </summary>
        [DataMember(Order = 114)]
        [DisplayName("4. Max Amount of Transcription Error")]
        [Category("4. Proliferate")]
        [Description("Maximum amount of transcription errors on proliferate (<1 is interpreted as 1)")]
        public int TranscriptionErrorMaxAmount
        {
            get { return this._transcriptionErrorMaxAmount; }
            set
            {
                if (this._transcriptionErrorMaxAmount != value)
                {
                    this._transcriptionErrorMaxAmount = value;
                    this.OnPropertyChanged("TranscriptionErrorMaxAmount");
                }
            }
        }

        // TODO Optimizer - TranscriptionErrorSize
        /// <summary>
        ///     Max possible error size at transcriptions
        /// </summary>
        [DataMember(Order = 115)]
        [Browsable(false)]
        [DisplayName("5. Transcription Error Size")]
        [Category("4. Proliferate")]
        [Description("Max possible error size at transcriptions")]
        public double TranscriptionErrorSize
        {
            get { return this._transcriptionErrorSize; }
            set
            {
                if (this._transcriptionErrorSize != value)
                {
                    this._transcriptionErrorSize = value;
                    this.OnPropertyChanged("TranscriptionErrorSize");
                }
            }
        }

        #endregion

        #region Survival

        private int _surviveWithoutBeeingTopRoundAmount;
        private int _topSurviveAmount;

        /// <summary>
        ///     The most X successful are allowed to survive (&lt;10 is interpreted as 10)
        /// </summary>
        [DataMember(Order = 116)]
        [DisplayName("1. Top Amount for Survive")]
        [Category("5. Survival")]
        [Description("The most X successful are allowed to survive (<10 is interpreted as 10)")]
        public int TopSurviveAmount
        {
            get { return this._topSurviveAmount; }
            set
            {
                if (this._topSurviveAmount != value)
                {
                    this._topSurviveAmount = value;
                    this.OnPropertyChanged("TopSurviveAmount");
                }
            }
        }

        /// <summary>
        ///     How long they can survive without being in the top survive amount
        /// </summary>
        [DataMember(Order = 117)]
        [DisplayName("2. Amount of Rounds to Survive Without Beeing Top")]
        [Category("5. Survival")]
        [Description("How long they can survive without being in the top survive amount")]
        public int SurviveWithoutBeeingTopRoundAmount
        {
            get { return this._surviveWithoutBeeingTopRoundAmount; }
            set
            {
                if (this._surviveWithoutBeeingTopRoundAmount != value)
                {
                    this._surviveWithoutBeeingTopRoundAmount = value;
                    this.OnPropertyChanged("SurviveWithoutBeeingTopRoundAmount");
                }
            }
        }

        #endregion

        #region Gene Transfer

        private int _geneTransferRange;
        private int _successfulRoundsUntilGeneTransferAmount;

        /// <summary>
        ///     How many rounds has to pass until the last modified gene of a successful indiviual is transferred
        /// </summary>
        [DataMember(Order = 118)]
        [DisplayName("1. Amount of Successful Rounds Until Gene Transfer")]
        [Category("6. Gene Transfer")]
        [Description("How many rounds has to pass until the last modified gene of a successful indiviual is transferred"
            )]
        public int SuccessfulRoundsUntilGeneTransferAmount
        {
            get { return this._successfulRoundsUntilGeneTransferAmount; }
            set
            {
                if (this._successfulRoundsUntilGeneTransferAmount != value)
                {
                    this._successfulRoundsUntilGeneTransferAmount = value;
                    this.OnPropertyChanged("SuccessfulRoundsUntilGeneTransferAmount");
                }
            }
        }

        /// <summary>
        ///     The range where successful genes should be transfered (0 means disable gene transfer)
        /// </summary>
        [DataMember(Order = 119)]
        [DisplayName("2. Gene Transfer Range")]
        [Category("6. Gene Transfer")]
        [Description("6. The range where successful genes should be transfered (0 means disable gene transfer)")]
        public int GeneTransferRange
        {
            get { return this._geneTransferRange; }
            set
            {
                if (this._geneTransferRange != value)
                {
                    this._geneTransferRange = value;
                    this.OnPropertyChanged("GeneTransferRange");
                }
            }
        }

        #endregion

        private FunctionHandler _calculateFunction;
        private string _calculateFunctionDefinition;

        /// <summary>
        ///     Function to be called on calculating a constant set result
        /// </summary>
        [DataMember(Order = 10)]
        [Browsable(false)]
        public string CalculateFunctionDefinition
        {
            get { return this._calculateFunctionDefinition; }
            set
            {
                if (this._calculateFunctionDefinition != value)
                {
                    this._calculateFunctionDefinition = value;
                    this.OnPropertyChanged("CalculateFunctionDefinition");
                }
            }
        }

        [IgnoreDataMember]
        [Browsable(false)]
        public FunctionHandler CalculateFunction
        {
            get
            {
                if (this._calculateFunction == null && !string.IsNullOrEmpty(this.CalculateFunctionDefinition))
                {
                    var parts = this.CalculateFunctionDefinition.Split(';');
                    var type = parts[0].Trim().FindType();
                    if (type != null)
                    {
                        var method =
                            type.GetMethods()
                                .FirstOrDefault(
                                    m =>
                                        m.Name == parts[1].Trim() && m.GetParameters().Length == 1
                                        && m.GetParameters()[0].ParameterType == typeof (byte[]));

                        if (method != null)
                            this._calculateFunction = (FunctionHandler) Delegate.CreateDelegate(type, method);
                    }
                }

                return this._calculateFunction;
            }
            set
            {
                this._calculateFunction = value;
                // ReSharper disable PossibleNullReferenceException
                this.CalculateFunctionDefinition = string.Format(
                    "{0}, {1}; {2}",
                    value.Method.DeclaringType.FullName,
                    value.Method.DeclaringType.Assembly.GetName().FullName,
                    value.Method.Name);
                // ReSharper restore PossibleNullReferenceException
            }
        }

        [Browsable(false)]
        public override Type ServiceType
        {
            get { return typeof (IOptimizerService); }
        }
    }

    public interface IOptimizerService : IService, INotifyPropertyChanged
    {
        OptimizerConfiguration OptimizerConfiguration { get; }

        /// <summary>
        ///     Gets the actual round
        /// </summary>
        int Round { get; }

        /// <summary>
        ///     Gets the best result sets of finished round, enumerable of tuples where the first double is the quality and the
        ///     second T[] the constant set
        /// </summary>
        Tuple<double, byte[]>[] LastSuccessful { get; }

        /// <summary>
        ///     Gets the quality indicator for the most successful constants
        /// </summary>
        double MostSuccessfulQuality { get; }

        /// <summary>
        ///     Gets the most successful constant set
        /// </summary>
        byte[] MostSuccessful { get; }

        /// <summary>
        ///     Gets the average quality of a round's successful individuals
        /// </summary>
        double AverageSuccessfulQuality { get; }

        /// <summary>
        ///     Gets if the optimizer is actually running
        /// </summary>
        bool IsRunning { get; }

        /// <summary>
        ///     Gets if the optimizer is actually paused
        /// </summary>
        bool IsPaused { get; }

        /// <summary>
        ///     Runs optimization
        /// </summary>
        [NonBlocking]
        void Run();

        /// <summary>
        ///     Stops optimization
        /// </summary>
        void Stop();

        /// <summary>
        ///     Pauses the optimization
        /// </summary>
        void Pause();

        event RoundHandler RoundBegun;

        event RoundHandler RoundFinished;
    }

    /// <summary>
    ///     Optimization calls this delegate for tryout
    /// </summary>
    /// <param name="constants">an enumerable of the calculated constants to use for the formula</param>
    /// <param name="lifeTime"></param>
    /// <param name="callback"></param>
    /// <returns>a double value which indicates the quality of the results. the lower the better, 0 is top</returns>
    public delegate void FunctionHandler(byte[] constants, int lifeTime, FunctionCallbackHandler callback);

    public delegate void FunctionCallbackHandler(double quality);

    /// <summary>
    ///     When a round finishes, this function will be called.
    /// </summary>
    /// <param name="roundNum">number of finished round</param>
    /// <param name="successful">
    ///     best result sets of finished round, enumerable of tuples where the first double is the quality
    ///     and the second T[] the constant set
    /// </param>
    public delegate void RoundHandler(int roundNum, IEnumerable<Tuple<double, byte[]>> successful);
}