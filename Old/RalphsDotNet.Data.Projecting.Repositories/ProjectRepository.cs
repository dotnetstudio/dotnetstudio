﻿using RalphsDotNet.Service.Data;
using System;
using System.Linq;

namespace RalphsDotNet.Data.Projecting.Repositories
{
    public class ProjectRepository : DataRepository<IProject>, IProjectRepository
    {
        protected override IProject CreateInternal()
        {
            var project = base.CreateInternal();
            this.UpdateAccess(project);
            this.UpdateModification(project);

            return project;
        }

        public IProject Create()
        {
            return this.CreateInternal();
        }

        public void UpdateAccess(IProject project)
        {
            project.LastAccess = DateTime.UtcNow;
        }

        public void UpdateModification(IProject project)
        {
            project.LastModification = DateTime.UtcNow;
        }

        public IQueryable<IProject> GetOfType(Guid type)
        {
            return this.All.Where(p => p.Type == type);
        }

        public IQueryable<IProject> GetOfTypeOrderedByName(Guid type)
        {
            return this.GetOfType(type).OrderBy(p => p.Name);
        }

        public IQueryable<IProject> GetOfTypeOrderedByLastAccess(Guid type)
        {
            return this.GetOfType(type).OrderBy(p => p.LastAccess);
        }
    }
}
