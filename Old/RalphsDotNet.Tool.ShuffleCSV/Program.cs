﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace RalphsDotNet.Tool.ShuffleCSV
{
    class Program
    {
        static void Main(string[] args)
        {
            var csvfile = args[0];
            var shufflesize = Convert.ToInt32(args[1]);

            var csvDirectory = Path.GetDirectoryName(csvfile) ?? Path.GetDirectoryName(typeof(Program).Assembly.Location);
            Debug.Assert(csvDirectory != null, "csvDirectory != null");
            var outfile = Path.Combine(csvDirectory, string.Concat(Path.GetFileNameWithoutExtension(csvfile), "_shuffle", Path.GetExtension(csvfile)));

            var lines = File.ReadLines(csvfile).ToArray();

            var linecount = lines.Count();

            if (linecount < shufflesize)
                throw (new ArgumentException("shufflesize cannot be greater than the amount of lines in the csv file"));

            var rnd = new Random();

            Console.WriteLine("Roll the dices");

            var linesToUse = new List<int>();
            while(linesToUse.Count < shufflesize)
            {
                var linenum = rnd.Next(linecount - 1);

                if (!linesToUse.Contains(linenum))
                    linesToUse.Add(linenum);
            }

            Console.WriteLine("Copy data");

            var linecnt = 0;
            foreach (var line in lines)
            {
                if (linesToUse.Contains(linecnt))
                    File.AppendAllText(outfile, string.Concat(line, Environment.NewLine));
                
                linecnt++;
            }

            Console.WriteLine(string.Concat("Finished, look at ", outfile));
            Console.WriteLine("Press a key to exit");
            Console.ReadKey();
        }
    }
}
