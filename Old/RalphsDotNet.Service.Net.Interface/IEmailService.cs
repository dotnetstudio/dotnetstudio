namespace RalphsDotNet.Service.Net.Interface
{
    public interface IEmailService : IService
    {
        void Send(string to, string subject, string body);
    }
}