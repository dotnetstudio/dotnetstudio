﻿using System;
using System.Runtime.Serialization;
using RalphsDotNet.Validation;

namespace RalphsDotNet.Service.Net.Interface
{
    [DataContract]
    public class EmailServiceConfiguration : ServiceConfiguration
    {
        private string _senderAdress;
        private int _smtpPort;
        private string _smtpHost;
        private string _smtpPassword;
        private string _smtpUsername;
        private bool _smtpUsesSSL;

        [EmailAdress]
        [DataMember(Order = 100)]
        public string SenderAdress
        {
            get { return this._senderAdress; }
            set
            {
                if (!Equals(this._senderAdress, value))
                {
                    this.OnPropertyChanging("SenderAdress");

                    this._senderAdress = value;

                    this.OnPropertyChanged("SenderAdress");
                }
            }
        }

        [NotNullOrEmpty]
        [DataMember(Order = 101)]
        public string SmtpHost
        {
            get { return this._smtpHost; }
            set
            {
                if (!Equals(this._smtpHost, value))
                {
                    this.OnPropertyChanging("SmtpHost");

                    this._smtpHost = value;

                    this.OnPropertyChanged("SmtpHost");
                }
            }
        }

        [MinValue(1)]
        [MaxValue(65535)]
        [DataMember(Order = 102)]
        public int SmtpPort
        {
            get { return this._smtpPort; }
            set
            {
                if (this._smtpPort != value)
                {
                    this.OnPropertyChanging("SmtpPort");

                    this._smtpPort = value;

                    this.OnPropertyChanged("SmtpPort");
                }
            }
        }

        [DataMember(Order = 103)]
        public bool SmtpUsesSSL
        {
            get { return this._smtpUsesSSL; }
            set
            {
                if (this._smtpUsesSSL != value)
                {
                    this.OnPropertyChanging("SmtpUsesSSL");

                    this._smtpUsesSSL = value;

                    this.OnPropertyChanged("SmtpUsesSSL");
                }
            }
        }

        [DataMember(Order = 104)]
        public string SmtpUsername
        {
            get { return this._smtpUsername; }
            set
            {
                if (!Equals(this._smtpUsername, value))
                {
                    this.OnPropertyChanging("SmtpUsername");

                    this._smtpUsername = value;

                    this.OnPropertyChanged("SmtpUsername");
                }
            }
        }

        [DataMember(Order = 105)]
        public string SmtpPassword
        {
            get { return this._smtpPassword; }
            set
            {
                if (!Equals(this._smtpPassword, value))
                {
                    this.OnPropertyChanging("SmtpPassword");

                    this._smtpPassword = value;

                    this.OnPropertyChanged("SmtpPassword");
                }
            }
        }

        public override Type ServiceType
        {
            get { return typeof (IEmailService); }
        }
    }
}