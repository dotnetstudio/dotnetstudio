﻿using RalphsDotNet.Data;
using RalphsDotNet.Data.Projecting;
using RalphsDotNet.Data.Scripting;
using System.Linq;

namespace RalphsDotNet.Forms.Studio.Scripting.Data
{
    public interface IScriptProjectMapping : IDataObject
    {
        [Index]
        IProject Project { get; set; }

        [CascadeAll]
        [Unique]
        IScript Script { get; set; }
    }

    public interface IScriptProjectMappingRepository : IDataRepository<IScriptProjectMapping>
    {
        IScriptProjectMapping Create(IScript script, IProject project);
        IQueryable<IScript> GetScriptsOfProject(IProject project);
    }
}
