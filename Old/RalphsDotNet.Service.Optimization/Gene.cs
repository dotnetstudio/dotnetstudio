namespace RalphsDotNet.Service.Optimization
{
    using System.Linq;

    public struct Gene
    {
        public int LastMutationRound {get; set;}

        public byte[] Code {get; set;}

        public override bool Equals(object obj)
        {
            if (obj is Gene)
            {
                var o = (Gene)obj;

                if (this.Code.Length != o.Code.Length) return false;

                return !this.Code.Where((t, i) => t != o.Code[i]).Any();
            }

            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return this.Code.GetHashCode();
        }
    }
}

