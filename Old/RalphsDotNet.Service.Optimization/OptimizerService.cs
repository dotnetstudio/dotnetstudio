using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using RalphsDotNet.Extension;
using RalphsDotNet.Service.Optimization.Interface;
using RalphsDotNet.Tool.System;

namespace RalphsDotNet.Service.Optimization.Genetic
{
    public class OptimizerService : IOptimizerService
    {
        private static readonly Random RandomGenerator = new Random();
        private readonly Synchronizer _initSynchronizer = new Synchronizer();
        private Individual[] _individuals;
        private bool _stopped;
        private bool _stopping;
        private Thread _thread;

        public OptimizerService(IOptimizerService proxy, ServiceConfiguration configuration)
        {
            this.ServiceConfiguration = configuration;

            this.MostSuccessfulQuality = Double.PositiveInfinity;
            this.InitializeDispatcher();
        }

        public Dispatcher Dispatcher
        {
            get { return Dispatcher.ForThread(this._thread); }
        }

        public string Id
        {
            get { return this.ServiceConfiguration.Id; }
        }

        public ServiceConfiguration ServiceConfiguration { get; set; }

        public OptimizerConfiguration OptimizerConfiguration
        {
            get { return (OptimizerConfiguration) this.ServiceConfiguration; }
        }

        /// <summary>
        ///     Gets the actual round
        /// </summary>
        public int Round { get; private set; }

        /// <summary>
        ///     Gets the best result sets of finished round, enumerable of tuples where the first double is the quality and the
        ///     second T[] the constant set
        /// </summary>
        public Tuple<double, byte[]>[] LastSuccessful { get; private set; }

        /// <summary>
        ///     Gets the quality indicator for the most successful constants
        /// </summary>
        public double MostSuccessfulQuality { get; private set; }

        /// <summary>
        ///     Gets the most successful constant set
        /// </summary>
        public byte[] MostSuccessful { get; private set; }

        /// <summary>
        ///     Gets the average quality of a round's successful individuals
        /// </summary>
        public double AverageSuccessfulQuality { get; private set; }

        /// <summary>
        ///     Gets if the optimizer is actually running
        /// </summary>
        public bool IsRunning { get; private set; }

        /// <summary>
        ///     Gets if the optimizer is actually paused
        /// </summary>
        public bool IsPaused { get; private set; }

        public void Run()
        {
            if (!this.IsPaused)
            {
                if (this.IsRunning) throw (new OptimizerAlreadyRunningException());

                this.IsRunning = true;
                this.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action(() =>
                {
                    this.RunInternal();
                    this.IsRunning = false;
                }));
            }
            else
            {
                this.IsPaused = false;
            }
        }

        public void Stop()
        {
            this.IsPaused = false;
            this._stopping = true;

            var cnt = 0;
            while (!this._stopped && cnt < 10)
            {
                Thread.Sleep(10);
                cnt++;
            }

            this._individuals = null;
            this.Round = 0;
        }

        public void Pause()
        {
            this.IsPaused = true;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public event RoundHandler RoundBegun;

        public event RoundHandler RoundFinished;

        public void Dispose()
        {
            this.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(this.Stop));

            GC.SuppressFinalize(this);

            this.Dispatcher.Stop();
        }

        private void InitializeDispatcher()
        {
            this._thread = new Thread(() =>
            {
                try
                {
                    this._initSynchronizer.Release();
                    Dispatcher.Run();
                }
                catch (ThreadAbortException)
                {
                }
            });
            //this._thread.SetApartmentState(ApartmentState.STA);
            this._thread.Start();
            this._initSynchronizer.Wait();

            while (this.Dispatcher == null)
                new ManualResetEvent(false).WaitOne(TimeSpan.FromTicks(10));
        }

        private void RunInternal()
        {
            this._stopping = this._stopped = false;

            this.LastSuccessful = null;

            this.InitializeIndividuals();

            this.CalculateRounds();
        }

        private void InitializeIndividuals()
        {
            var individualsAmount = this.OptimizerConfiguration.InitialPopulationSize;

            this._individuals = new Individual[individualsAmount];

            for (var i = 0; i < individualsAmount; i++)
            {
                this._individuals[i] = new Individual(this);
                this._individuals[i].Initialize();
            }
        }


        private void CalculateRounds()
        {
            while (!this._stopping)
            {
                if (this.RoundBegun != null) this.RoundBegun(this.Round, this.LastSuccessful);

                this._individuals.Action(i => i.Calculate(), this.OptimizerConfiguration.Multithreading);

                var now = DateTime.UtcNow;
                while ((DateTime.UtcNow - now).TotalMilliseconds < this.OptimizerConfiguration.EvalTimeout
                       && this._individuals.Any(i => i.WaitForCalculate)) Thread.Sleep(10);

                this.ProcessIndividuals();

                if (this.RoundFinished != null) this.RoundFinished(this.Round, this.LastSuccessful);

                this.Round++;
                this.OnPropertyChanged("Round");

                while (this.IsPaused) Thread.Sleep(100);
            }

            this._stopped = true;
        }

        private void ProcessIndividuals()
        {
            var topProliferateAmount = this.OptimizerConfiguration.TopProliferateAmount > 9
                ? this.OptimizerConfiguration.TopProliferateAmount
                : 10;
            var topSurviveAmount = this.OptimizerConfiguration.TopSurviveAmount > 9
                ? this.OptimizerConfiguration.TopSurviveAmount
                : 10;
            var surviveWithoutBeeingTopRoundAmount = this.OptimizerConfiguration.SurviveWithoutBeeingTopRoundAmount >= 0
                ? this.OptimizerConfiguration
                    .SurviveWithoutBeeingTopRoundAmount
                : 0;

            // sorting the individuals
            IEnumerable<Individual> sortedIndividuals = this.SortIndividuals();
            IEnumerable<Individual> successful = sortedIndividuals.Take(topSurviveAmount).ToArray();
            IEnumerable<Individual> proliferate = sortedIndividuals.Take(topProliferateAmount).ToArray();
            IEnumerable<Individual> unsuccessful = sortedIndividuals.Where(i => !successful.Contains(i)).ToArray();
            IEnumerable<Individual> unsuccessfulSurvivers =
                unsuccessful.Where(i => i.LastSuccessfulRound + surviveWithoutBeeingTopRoundAmount > this.Round)
                    .ToArray();

            this.LastSuccessful = successful.Select(i => new Tuple<double, byte[]>(i.LastQuality, i.Code)).ToArray();
            this.OnPropertyChanged("LastSuccessful");

            this.AverageSuccessfulQuality = successful.Select(i => i.LastQuality).Average();
            this.OnPropertyChanged("AverageSuccessfulQuality");

            if (this.MostSuccessfulQuality < successful.First().LastQuality)
                // TODO Optimizer - BUG: MostSuccessful is not working
            {
                this.MostSuccessfulQuality = successful.First().LastQuality;
                this.OnPropertyChanged("MostSuccessfulQuality");
                this.MostSuccessful = successful.First().Code;
                this.OnPropertyChanged("MostSuccessful");
            }

            // marking successful individuals
            successful.Action(i => i.SetSuccessful(), this.OptimizerConfiguration.Multithreading);

            // marking unsuccessful surviver individuals
            unsuccessfulSurvivers.Action(i => i.SetUnsuccessful(), this.OptimizerConfiguration.Multithreading);

            // collect the new individuals
            var newIndividuals = new List<Individual>();

            newIndividuals.AddRange(
                successful.Where(
                    i =>
                        this.OptimizerConfiguration.MaxLifeSpan <= 0
                        || i.BirthRound + this.OptimizerConfiguration.MaxLifeSpan > this.Round));
            newIndividuals.AddRange(
                unsuccessfulSurvivers.Where(
                    i =>
                        this.OptimizerConfiguration.MaxLifeSpan <= 0
                        || i.BirthRound + this.OptimizerConfiguration.MaxLifeSpan > this.Round));

            //execute horizontal gene transfer
            var successfulRoundsUntilGeneTransferAmount =
                this.OptimizerConfiguration.SuccessfulRoundsUntilGeneTransferAmount >= 0
                    ? this.OptimizerConfiguration.SuccessfulRoundsUntilGeneTransferAmount
                    : 0;
            var newIndividualsGeneTransfer =
                newIndividuals.Where(i => i.SuccessfulRounds >= successfulRoundsUntilGeneTransferAmount);

            newIndividualsGeneTransfer.Action(
                i => this.CalculateGeneTransfer(i, unsuccessfulSurvivers.ToList()),
                this.OptimizerConfiguration.Multithreading);

            // proliferate
            proliferate.Action(
                i => this.ProliferateIndividual(i, newIndividuals), this.OptimizerConfiguration.Multithreading);

            //replace dead
            if (this.OptimizerConfiguration.ReplaceDead)
            {
                for (var i = 0; i < this.OptimizerConfiguration.InitialPopulationSize - newIndividuals.Count; i++)
                {
                    var individual = new Individual(this);
                    individual.Initialize();
                    newIndividuals.Add(individual);
                }
            }

            this._individuals = newIndividuals.AsParallel().OrderBy(i => i.RandomOrder).ToArray();
            this.OnPropertyChanged("Individuals");
        }

        private void ProliferateIndividual(Individual proliferateIndividual, List<Individual> newIndividuals)
        {
            var proliferateAmount = this.OptimizerConfiguration.ProliferateAmount > 9
                ? this.OptimizerConfiguration.ProliferateAmount
                : 10;

            for (var i = 0; i < proliferateAmount; i++)
            {
                lock (newIndividuals) newIndividuals.Add(proliferateIndividual.Proliferate());
            }
        }

        private void CalculateGeneTransfer(Individual geneExportIndividual, List<Individual> importingIndividuals)
        {
            var geneTransferRange = this.OptimizerConfiguration.GeneTransferRange > 0
                ? this.OptimizerConfiguration.GeneTransferRange
                : 0;

            if (geneTransferRange > 0)
            {
                var exportingGene = geneExportIndividual.GetExportGene();

                int individualIndex;
                lock (RandomGenerator) individualIndex = RandomGenerator.Next(0, importingIndividuals.Count);
                var rangeBegin = individualIndex - geneTransferRange;
                rangeBegin = rangeBegin >= 0 ? rangeBegin : 0;
                var rangeEnd = individualIndex + geneTransferRange;
                rangeEnd = rangeEnd < importingIndividuals.Count ? rangeEnd : (importingIndividuals.Count - 1);

                foreach (var importingIndividual in
                    importingIndividuals.GetRange(rangeBegin, rangeEnd - rangeBegin + 1)
                        .Where(
                            importingIndividual =>
                                importingIndividual.LastQuality > geneExportIndividual.LastQuality))
                {
                    importingIndividual.ImportGene(exportingGene);
                }
            }
        }

        private Individual[] SortIndividuals()
        {
            var topProliferateAmount = this.OptimizerConfiguration.TopProliferateAmount > 9
                ? this.OptimizerConfiguration.TopProliferateAmount
                : 10;

            var sorted = this._individuals.AsParallel().OrderBy(i => i.LastQuality).ToArray();

            var last = sorted.Last();
            var previous = new List<Individual>();
            foreach (var individual in sorted)
            {
                if (previous.Count <= topProliferateAmount)
                {
                    if (previous.Count < 1)
                    {
                        previous.Add(individual);
                    }
                    else
                    {
                        var ownCluster = true;
                        foreach (var prev in previous)
                        {
                            double diff = 0;
                            for (var i = 0; i < individual.Code.Length; i++)
                            {
                                double localDiff = prev.Code[i] - individual.Code[i];

                                diff += localDiff >= 0 ? localDiff : localDiff*-1;
                                diff = diff/2;

                                if (diff >= this.OptimizerConfiguration.ClusterDifference) break;
                            }

                            if (diff < this.OptimizerConfiguration.ClusterDifference)
                            {
                                individual.LastQuality = last.LastQuality;
                                ownCluster = false;
                                break;
                            }
                        }

                        if (ownCluster)
                        {
                            previous.Add(individual);
                        }
                    }
                }
                else
                {
                    break;
                }
            }

            return this._individuals.AsParallel().OrderBy(i => i.LastQuality).ToArray();
        }

        private void OnPropertyChanged(params string[] properties)
        {
            if (this.PropertyChanged != null)
            {
                foreach (var property in properties) this.PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }

        ~OptimizerService()
        {
            this.Dispose();
        }
    }

    public class OptimizerAlreadyRunningException : Exception
    {
    }
}