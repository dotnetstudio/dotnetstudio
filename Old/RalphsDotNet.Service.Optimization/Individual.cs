using System;
using System.Linq;
using System.Collections.Generic;

namespace RalphsDotNet.Service.Optimization.Genetic
{
    public class Individual
    {
        public Gene[] GeneticCode { get; private set; }

        public OptimizerService OptimizerService { get; private set; }

        public double LastQuality { get; set; }

        public int SuccessfulRounds { get; set; }

        public int LastSuccessfulRound { get; set; }

        public int BirthRound { get; private set; }

        public int ProliferateCount { get; set; }

        public int RandomOrder
        {
            get
            {
                return GetRandomInt(int.MinValue, int.MaxValue);
            }
        }

        private int _lastCodeCollectionRound = -1;

        private byte[] _code;

        public byte[] Code
        {
            get
            {
                if (this.OptimizerService.Round > this._lastCodeCollectionRound)
                {
                    var lCode = new List<byte>();

                    foreach (var t in this.GeneticCode)
                    {
                        lCode.AddRange(t.Code);
                    }

                    this._code = lCode.ToArray();

                    this._lastCodeCollectionRound = this.OptimizerService.Round;
                }

                return this._code;
            }
        }

        public Individual(OptimizerService optimizerServiceService)
        {
            this.OptimizerService = optimizerServiceService;
            this.SuccessfulRounds = 0;
            this.ProliferateCount = 0;
            this.BirthRound = this.OptimizerService.Round;
        }

        public void Initialize()
        {
            var geneAmount = this.OptimizerService.OptimizerConfiguration.GeneAmount > 0
                ? this.OptimizerService.OptimizerConfiguration.GeneAmount
                : 1;
            var geneSize = this.OptimizerService.OptimizerConfiguration.GeneSize > 0
                ? this.OptimizerService.OptimizerConfiguration.GeneSize
                : 1;

            this.GeneticCode = new Gene[geneAmount];

            for (var i = 0; i < geneAmount; i++)
            {
                for (var j = 0; j < geneSize; j++)
                {
                    if (this.GeneticCode[i].Code == null) this.GeneticCode[i].Code = new byte[geneSize];

                    this.GeneticCode[i].Code[j] = GetRandomByte(
                        this.OptimizerService.OptimizerConfiguration.GeneMinValue,
                        this.OptimizerService.OptimizerConfiguration.GeneMaxValue);
                    this.GeneticCode[i].LastMutationRound = 0;
                }

                var repeat = false;
                if (this.OptimizerService.OptimizerConfiguration.UniqueGenes)
                {
                    for (var k = 0; k < i; k++)
                    {
                        if (this.GeneticCode[k].Equals(this.GeneticCode[i]))
                        {
                            repeat = true;
                            break;
                        }
                    }
                }

                if (repeat) i--;
            }
        }

        public void ImportGene(Tuple<int, Gene> gene)
        {
            this.ImportGene(gene.Item1, gene.Item2);
        }

        public void ImportGene(int geneNumber, Gene gene)
        {
            this.GeneticCode[geneNumber - 1] = gene;
        }

        public Tuple<int, Gene> GetExportGene()
        {
            var transferGene = this.GeneticCode.AsParallel().OrderByDescending(g => g.LastMutationRound).First();
            return new Tuple<int, Gene>(this.GeneticCode.ToList().IndexOf(transferGene) + 1, transferGene);
        }

        public Individual Proliferate()
        {
            bool hasDoubles;
            Individual child;
            do
            {
                hasDoubles = false;

                var geneSize = this.OptimizerService.OptimizerConfiguration.GeneSize > 0
                    ? this.OptimizerService.OptimizerConfiguration.GeneSize
                    : 1;
                var transcriptionErrorMinAmount = this.OptimizerService.OptimizerConfiguration.TranscriptionErrorMinAmount >= 0
                    ? this.OptimizerService.OptimizerConfiguration
                        .TranscriptionErrorMinAmount
                    : 0;
                var transcriptionErrorMaxAmount = this.OptimizerService.OptimizerConfiguration.TranscriptionErrorMaxAmount > 0
                    ? this.OptimizerService.OptimizerConfiguration
                        .TranscriptionErrorMaxAmount
                    : 1;

                // prepare transcription errors
                var transcriptionErrorAmount = GetRandomInt(transcriptionErrorMinAmount, transcriptionErrorMaxAmount);

                child = new Individual(this.OptimizerService) {GeneticCode = new Gene[this.GeneticCode.Length]};

                var transcriptioErrors = new List<Tuple<int, int, byte>>();
                for (var a = 0; a < transcriptionErrorAmount; a++)
                {
                    var geneNumber = GetRandomInt(1, this.GeneticCode.Length + 1);
                    var codeNumber = GetRandomInt(1, geneSize + 1);

                    var newValue = GetRandomByte(
                        this.OptimizerService.OptimizerConfiguration.GeneMinValue,
                        this.OptimizerService.OptimizerConfiguration.GeneMaxValue);

                    transcriptioErrors.Add(new Tuple<int, int, byte>(geneNumber, codeNumber, newValue));
                }

                // transcribe
                for (var i = 0; i < this.GeneticCode.Length; i++)
                {
                    child.GeneticCode[i].Code = new byte[geneSize];
                    this.GeneticCode[i].Code.CopyTo(child.GeneticCode[i].Code, 0);

                    var geneNum = i;
                    var actualErrors = transcriptioErrors.Where(e => e.Item1 == geneNum + 1);
                    foreach (var error in actualErrors)
                    {
                        child.GeneticCode[i].Code[error.Item2 - 1] = error.Item3;
                    }

                    child.GeneticCode[i].LastMutationRound = this.OptimizerService.Round;
                }

                if (this.OptimizerService.OptimizerConfiguration.UniqueGenes)
                {
                    if (
                        child.GeneticCode.Where(
                            (t1, i) => child.GeneticCode.Where((t, j) => i != j && t1.Equals(t)).Any()).Any())
                    {
                        hasDoubles = true;
                    }
                }
            } while (this.OptimizerService.OptimizerConfiguration.UniqueGenes && hasDoubles);

            this.ProliferateCount++;

            return child;
        }

        internal bool WaitForCalculate { get; private set; }

        public void Calculate()
        {
            this.WaitForCalculate = true;
            this.OptimizerService.OptimizerConfiguration.CalculateFunction(
                this.Code, this.OptimizerService.Round - this.BirthRound, this.CalculateCallback);
        }

        private void CalculateCallback(double quality)
        {
            this.LastQuality = quality;
            this.WaitForCalculate = false;
        }

        public void SetSuccessful()
        {
            this.LastSuccessfulRound = this.OptimizerService.Round;
            this.SuccessfulRounds++;
        }

        public void SetUnsuccessful()
        {
            this.SuccessfulRounds = 0;
        }

        protected static Random RandomGenerator = new Random();

        protected static int GetRandomInt(int min, int max)
        {
            lock (RandomGenerator)
            {
                return RandomGenerator.Next(min, max);
            }
        }

        protected static byte GetRandomByte(int min, int max)
        {
            return (byte) GetRandomInt(min >= 0 ? min : 0, max <= 255 ? max : 255);
        }
    }
}