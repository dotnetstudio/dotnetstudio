﻿using RalphsDotNet.Data;
using RalphsDotNet.Data.Projecting;
using RalphsDotNet.Data.Scripting;
using RalphsDotNet.Service.Data;
using System.Linq;

namespace RalphsDotNet.Forms.Studio.Scripting.Data.Repositories
{
    public class ScriptProjectMappingRepository : DataRepository<IScriptProjectMapping>, IScriptProjectMappingRepository
    {
        public IScriptProjectMapping Create(IScript script, IProject project)
        {
            var mapping = base.CreateInternal();
            mapping.Script = script;
            mapping.Project = project;

            return mapping;
        }

        public IQueryable<IScript> GetScriptsOfProject(IProject project)
        {
            return this.All.Where(m => m.Project == project).Select(m => m.Script);
        }
    }
}
