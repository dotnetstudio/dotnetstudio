using System.Collections.Generic;
using System.Collections.ObjectModel;
using RalphsDotNet.Data.Base;
using RalphsDotNet.Service.Data;

namespace RalphsDotNet.Data.Web
{
    public class ArticleRepository : DataRepository<IArticle>, IArticleRepository
	{
        protected override IArticle CreateInternal()
        {
            var obj = base.CreateInternal();
            obj.Attachments = new ObservableCollection<IAttachment>();
            return obj;
        }

        public IArticle Create(string type, ILanguage language, string title, string text, IEnumerable<IAttachment> attachments)
        {
            var article = this.CreateInternal();
            article.Type = type;
            article.Language = language;
            article.Title = title;
            article.Text = text;

            if (attachments != null)
            {
                foreach (var attachment in attachments)
                    article.Attachments.Add(attachment);
            }

            return article;
        }
	}
}

