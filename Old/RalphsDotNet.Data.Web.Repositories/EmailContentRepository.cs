using System.Collections.Generic;
using System.Collections.ObjectModel;
using RalphsDotNet.Data.Base;
using RalphsDotNet.Service.Data;

namespace RalphsDotNet.Data.Web
{
    public class EmailContentRepository : DataRepository<IEmailContent>, IEmailContentRepository
	{
        protected override IEmailContent CreateInternal()
        {
            var obj = base.CreateInternal();
            obj.Attachments = new ObservableCollection<IAttachment>();
            return obj;
        }

        public IEmailContent Create(string type, ILanguage language, string title, string text, IEnumerable<IAttachment> attachments)
        {
            var emailContent = this.CreateInternal();
            emailContent.Type = type;
            emailContent.Language = language;
            emailContent.Title = title;
            emailContent.Text = text;

            if (attachments != null)
            {
                foreach (var attachment in attachments)
                    emailContent.Attachments.Add(attachment);
            }

            return emailContent;
        }
	}
}

