﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using RalphsDotNet.Data.Projecting;
using RalphsDotNet.Extension;
using RalphsDotNet.Forms.Studio.Controls;
using RalphsDotNet.Forms.Studio.Docks;
using RalphsDotNet.Forms.Studio.Properties;
using RalphsDotNet.Service;
using RalphsDotNet.Service.Data.Interface;
using WeifenLuo.WinFormsUI.Docking;

namespace RalphsDotNet.Forms.Studio
{
    [DefaultEvent("Load")]
    [Designer(
        "System.Windows.Forms.Design.FormDocumentDesigner, System.Design, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a",
        typeof (IRootDesigner))]
    [DesignerCategory(@"Form")]
    [DesignTimeVisible(false)]
    [InitializationEvent("Load")]
    [ToolboxItem(false)]
    [ToolboxItemFilter("System.Windows.Forms.Control.TopLevel")]
    public partial class Window : Form
    {
        protected static Property ToolStripContainerProperty = new Property("ToolStripContainer",
            typeof (ToolStripContainer), null);

        protected static Property DockPanelContainerProperty = new Property("DockPanelContainer", typeof (Control), null);

        protected static Property RepositoriesProperty = new Property("Repositories", typeof (string), string.Empty);
        protected static Property ProjectTypeProperty = new Property("ProjectType", typeof (Guid), Guid.Empty);
        protected static Property WorkspaceProperty = new Property("Workspace", typeof (IDataService));
        protected static Property ProjectProperty = new Property("Project", typeof (IProject));
        protected static Property IsProjectDirtyProperty = new Property("IsProjectDirty", typeof (bool), false);
        private readonly Dictionary<Dock, List<ToolStrip>> _bottomToolStrips = new Dictionary<Dock, List<ToolStrip>>();
        private readonly Dictionary<Dock, List<ToolStrip>> _leftToolStrips = new Dictionary<Dock, List<ToolStrip>>();
        private readonly Dictionary<Dock, List<ToolStrip>> _rightToolStrips = new Dictionary<Dock, List<ToolStrip>>();
        private readonly Dictionary<Dock, List<ToolStrip>> _topToolStrips = new Dictionary<Dock, List<ToolStrip>>();

        private ProjectManagementDock _projectManagementDock;

        public Window()
        {
            this.InitializeComponent();
        }

        /// <summary>
        ///     tool strip container to be mergeable with main window tool strip container
        /// </summary>
        [Browsable(true)]
        [EditorBrowsable(EditorBrowsableState.Always)]
        [DisplayName(@"ToolStripContainer")]
        [Category("Extended")]
        [Description("tool strip container to be mergeable")]
        public ToolStripContainer ToolStripContainer
        {
            get { return this.GetValue(ToolStripContainerProperty) as ToolStripContainer; }
            set { this.SetValue(ToolStripContainerProperty, value); }
        }

        /// <summary>
        ///     container to use for dockpanel
        /// </summary>
        [Browsable(true)]
        [EditorBrowsable(EditorBrowsableState.Always)]
        [DisplayName(@"DockPanelContainer")]
        [Category("Extended")]
        [Description("container to use for dockpanel")]
        public Control DockPanelContainer
        {
            get { return this.GetValue(DockPanelContainerProperty) as Control; }
            set { this.SetValue(DockPanelContainerProperty, value); }
        }

        /// <summary>
        ///     guid of the handeled project type
        /// </summary>
        [Browsable(true)]
        [EditorBrowsable(EditorBrowsableState.Always)]
        [DisplayName(@"ProjectType")]
        [Category("Extended")]
        [Description("guid of the handeled project type")]
        public Guid ProjectType
        {
            get { return (Guid) this.GetValue(ProjectTypeProperty); }
            set { this.SetValue(ProjectTypeProperty, value); }
        }

        /// <summary>
        ///     repositories to load separated by ;
        /// </summary>
        [Browsable(true)]
        [EditorBrowsable(EditorBrowsableState.Always)]
        [DisplayName(@"Repositories")]
        [Category("Extended")]
        [Description("repositories to load separated by ;")]
        public string Repositories
        {
            get { return this.GetValue(RepositoriesProperty) as string; }
            set { this.SetValue(RepositoriesProperty, value); }
        }

        public IDataService Workspace
        {
            get { return this.GetValue(WorkspaceProperty) as IDataService; }
            set { this.SetValue(WorkspaceProperty, value); }
        }

        public IProject Project
        {
            get { return this.GetValue(ProjectProperty) as IProject; }
            set { this.SetValue(ProjectProperty, value); }
        }

        public bool IsProjectDirty
        {
            get { return (bool) this.GetValue(IsProjectDirtyProperty); }
            set { this.SetValue(IsProjectDirtyProperty, value); }
        }

        public IProjectRepository Projects
        {
            get { return this.Workspace.GetRepository<IProject, IProjectRepository>(); }
        }

        protected override void OnCreateControl()
        {
            base.OnCreateControl();

            this.dockPanel.ContentAdded += this.DockPanel_ContentAdded;
            this.dockPanel.ContentRemoved += this.DockPanel_ContentRemoved;
        }

        public void ShowDock(Dock dock)
        {
            if (this.dockPanel.DocumentStyle == DocumentStyle.SystemMdi)
            {
                dock.MdiParent = this;
                dock.Show();
            }
            else
                dock.Show(this.dockPanel);
        }

        public void HideDock(Dock dock)
        {
            dock.Hide();
        }

        private void workspaceDock_ProjectDoubleClicked(object sender, ProjectDoubleClickedEventArgs e)
        {
            if (this.Project != e.Project)
                this.OpenProject(e.Project);
        }

        protected override void OnPropertyChanged(Property property, object newValue, object oldValue)
        {
            base.OnPropertyChanged(property, newValue, oldValue);

            if (property == DockPanelContainerProperty)
            {
                this.dockPanel.Parent = this.DockPanelContainer ?? this;
            }
            if (property == WorkspaceProperty)
            {
                if (this.Workspace != null)
                {
                    this.ShowProjectManagementDock();
                }
                else
                {
                    this.HideProjectManagementDock();
                }
            }
            else if (property == ProjectProperty)
            {
                if (this.Project != null)
                {
                    this.HideProjectManagementDock();
                }
                else
                {
                    this.ShowProjectManagementDock();
                }
            }
        }

        private void ShowProjectManagementDock()
        {
            this._projectManagementDock = new ProjectManagementDock(this);
            this._projectManagementDock.ProjectDoubleClicked += this.workspaceDock_ProjectDoubleClicked;
            this.ShowDock(this._projectManagementDock);
        }

        private void HideProjectManagementDock()
        {
            this.HideDock(this._projectManagementDock);
            this._projectManagementDock.ProjectDoubleClicked -= this.workspaceDock_ProjectDoubleClicked;
            this._projectManagementDock.Close();
        }

        private void DockPanel_ContentAdded(object sender, DockContentEventArgs e)
        {
            if (e.Content as Dock != null)
            {
                var dock = (Dock) e.Content;

                this.MergeDockToolStrips(dock);
                dock.VisibleChanged += this.dock_VisibleChanged;
            }
        }

        private void dock_VisibleChanged(object sender, EventArgs e)
        {
            if (((Dock) sender).Visible)
            {
                this.ShowContentDockToolStrips(sender as ContentDock);
            }
            else
            {
                this.HideContentDockToolStrips(sender as ContentDock);
            }
        }

        private void DockPanel_ContentRemoved(object sender, DockContentEventArgs e)
        {
            if (e.Content as Dock != null)
            {
                var dock = (Dock) e.Content;

                this.UnmergeDockToolStrips(dock);
                dock.VisibleChanged -= this.dock_VisibleChanged;
            }
        }

        private void MergeDockToolStripPanel(ToolStripPanel sourcePanel, ToolStripPanel targetPanel,
            Dictionary<Dock, List<ToolStrip>> dict, Dock dock)
        {
            if (!dict.ContainsKey(dock))
                dict.Add(dock, new List<ToolStrip>());

            var tmpList = targetPanel.Controls.OfType<Control>().ToList();
            foreach (ToolStrip strip in sourcePanel.Controls.OfType<Control>().Where(c => c is ToolStrip))
            {
                dict[dock].Add(strip);
                sourcePanel.Controls.Remove(strip);
                tmpList.Add(strip);
            }

            targetPanel.Controls.Clear();
            tmpList.Reverse();
            tmpList.ForEach(c => targetPanel.Controls.Add(c));
        }

        private void UnmergeDockToolStripPanel(ToolStripPanel sourcePanel, ToolStripPanel targetPanel,
            Dictionary<Dock, List<ToolStrip>> dict, Dock dock)
        {
            if (dict.ContainsKey(dock))
            {
                foreach (var strip in dict[dock])
                {
                    targetPanel.Controls.Remove(strip);
                    sourcePanel.Controls.Add(strip);
                }

                dict.Remove(dock);
            }
        }

        private void MergeDockToolStrips(Dock dock)
        {
            if (!this.DesignMode && this.ToolStripContainer != null && dock.ToolStripContainer != null)
            {
                this.MergeDockToolStripPanel(dock.ToolStripContainer.TopToolStripPanel,
                    this.ToolStripContainer.TopToolStripPanel, this._topToolStrips, dock);
                this.MergeDockToolStripPanel(dock.ToolStripContainer.RightToolStripPanel,
                    this.ToolStripContainer.RightToolStripPanel, this._rightToolStrips, dock);
                this.MergeDockToolStripPanel(dock.ToolStripContainer.BottomToolStripPanel,
                    this.ToolStripContainer.BottomToolStripPanel, this._bottomToolStrips, dock);
                this.MergeDockToolStripPanel(dock.ToolStripContainer.LeftToolStripPanel,
                    this.ToolStripContainer.LeftToolStripPanel, this._leftToolStrips, dock);

                dock.ToolStripContainer.TopToolStripPanelVisible =
                    dock.ToolStripContainer.RightToolStripPanelVisible =
                        dock.ToolStripContainer.BottomToolStripPanelVisible =
                            dock.ToolStripContainer.LeftToolStripPanelVisible = false;
            }
        }

        private void UnmergeDockToolStrips(Dock dock)
        {
            if (!this.DesignMode && this.ToolStripContainer != null && dock.ToolStripContainer != null)
            {
                this.UnmergeDockToolStripPanel(dock.ToolStripContainer.TopToolStripPanel,
                    this.ToolStripContainer.TopToolStripPanel, this._topToolStrips, dock);
                this.UnmergeDockToolStripPanel(dock.ToolStripContainer.RightToolStripPanel,
                    this.ToolStripContainer.RightToolStripPanel, this._rightToolStrips, dock);
                this.UnmergeDockToolStripPanel(dock.ToolStripContainer.BottomToolStripPanel,
                    this.ToolStripContainer.BottomToolStripPanel, this._bottomToolStrips, dock);
                this.UnmergeDockToolStripPanel(dock.ToolStripContainer.LeftToolStripPanel,
                    this.ToolStripContainer.LeftToolStripPanel, this._leftToolStrips, dock);

                dock.ToolStripContainer.TopToolStripPanelVisible =
                    dock.ToolStripContainer.RightToolStripPanelVisible =
                        dock.ToolStripContainer.BottomToolStripPanelVisible =
                            dock.ToolStripContainer.LeftToolStripPanelVisible = true;
            }
        }

        private void ShowContentDockToolStrips(ContentDock dock)
        {
            // dock != null means in this case, when the caller gave a PanelDock, dock is null and nothing should happen
            if (!this.DesignMode && dock != null && this.ToolStripContainer != null && dock.ToolStripContainer != null)
            {
                this._topToolStrips[dock].ForEach(ts => ts.Visible = true);
                this._rightToolStrips[dock].ForEach(ts => ts.Visible = true);
                this._bottomToolStrips[dock].ForEach(ts => ts.Visible = true);
                this._leftToolStrips[dock].ForEach(ts => ts.Visible = true);
            }
        }

        private void HideContentDockToolStrips(ContentDock dock)
        {
            // dock != null means in this case, when the caller gave a PanelDock, dock is null and nothing should happen
            if (!this.DesignMode && dock != null && this.ToolStripContainer != null && dock.ToolStripContainer != null)
            {
                this._topToolStrips[dock].ForEach(ts => ts.Visible = false);
                this._rightToolStrips[dock].ForEach(ts => ts.Visible = false);
                this._bottomToolStrips[dock].ForEach(ts => ts.Visible = false);
                this._leftToolStrips[dock].ForEach(ts => ts.Visible = false);
            }
        }

        protected virtual bool OnNewWorkspace(IDataService newWorkspace)
        {
            return true;
        }

        public void NewWorkspace()
        {
            this.OpenWorkspaceInternal(true);
        }

        public void OpenWorkspace()
        {
            this.OpenWorkspaceInternal(false);
        }

        private void OpenWorkspaceInternal(bool create)
        {
            if (this.Workspace == null || this.CloseWorkspace())
            {
                var dialog = create ? this.saveFileDialog : this.openFileDialog as FileDialog;

                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    if (create && File.Exists(dialog.FileName))
                        File.Delete(dialog.FileName);

                    ServiceProvider.ServiceConfigurations.Services =
                        Configuration.Load<ServicesConfiguration>(
                            Resources.DataServiceConfig.Replace("[FILENAME]", dialog.FileName)
                                .Replace("[REPOSITORIES]", string.Concat(";", this.Repositories)),
                            Resources.DataServiceTypes).Services;

                    var newWorkspace = ServiceProvider.GetService<IDataService>("workspace");

                    if (this.OnNewWorkspace(newWorkspace))
                    {
                        this.InitializeWorkspace(newWorkspace);
                    }
                }
            }
        }

        private void InitializeWorkspace(IDataService workspace)
        {
            this.Workspace = workspace;

            this.OnInitializeWorkspace();
        }

        protected virtual void OnInitializeWorkspace()
        {
        }

        public bool CloseWorkspace()
        {
            if (this.Workspace == null)
            {
                return true;
            }

            if (this.CloseProject() && this.OnCloseWorkspace())
            {
                this.Workspace.Dispose();

                this.Workspace = null;
                return true;
            }

            return false;
        }

        protected virtual bool OnCloseWorkspace()
        {
            return true;
        }

        public void NewProject()
        {
            var oldProject = this.Project;

            // if theres an open project, close it first if allowed
            if (oldProject == null || this.CloseProject())
            {
                // begin a transaction for the new project and create it
                var newProject = this.Projects.Create();
                this.OnCreateProject(newProject);
                this.Projects.SaveOrUpdate(newProject);
                this.Workspace.BeginTransaction();

                // ask the lower ones if they have a problem with creating a new project and if not do what they have to do
                if (this.OnNewProject(oldProject, newProject))
                {
                    this.IsProjectDirty = true;
                    // the lower ones agreed, so initialize the new project
                    this.InitializeProject(newProject);
                }
                else
                {
                    // the lower ones denied, so roll back the transaction of the new project and reinitialize the old one
                    this.Workspace.RollbackTransaction();

                    if (oldProject != null)
                        this.InitializeProject(oldProject);
                }
            }
        }

        protected virtual void OnCreateProject(IProject project)
        {
            project.Type = this.ProjectType;
            project.Name =
                "Untitled Project".GetUniqueName(this.Projects.GetOfType(this.ProjectType).Select(p => p.Name));
        }

        protected virtual bool OnNewProject(IProject oldProject, IProject newProject)
        {
            return true;
        }

        private void InitializeProject(IProject project)
        {
            this.Project = project;

            this.OnInitializeProject();
        }

        protected virtual void OnInitializeProject()
        {
        }

        public void OpenProject(IProject newProject)
        {
            var oldProject = this.Project;

            // if theres an open project, close it first
            if (oldProject == null || this.CloseProject())
            {
                // begin a transaction for the new project and create it
                this.Workspace.BeginTransaction();

                // ask the lower ones if they have a problem with creating a new project and if not do what they have to do
                if (this.OnOpenProject(oldProject, newProject))
                {
                    // the lower ones agreed, so initialize the new project
                    this.InitializeProject(newProject);
                }
                else
                {
                    // the lower ones denied, so roll back the transaction of the new project and reinitialize the old one
                    this.Workspace.RollbackTransaction();

                    if (oldProject != null)
                        this.InitializeProject(oldProject);
                }
            }
        }

        protected virtual bool OnOpenProject(IProject oldProject, IProject newProject)
        {
            return true;
        }

        public bool CloseProject()
        {
            if (this.OnCloseProject())
            {
                this.Project = null;

                if (this.IsProjectDirty)
                {
                    if (
                        MessageBox.Show(Resources.Window_CloseProject_UnsavedChanges_Text,
                            Resources.Window_CloseProject_UnsavedChanges_Title, MessageBoxButtons.YesNo,
                            MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        this.SaveProject();
                    }
                    else
                    {
                        this.Workspace.RollbackTransaction();
                    }
                }
                else
                {
                    this.Workspace.RollbackTransaction();
                }

                return true;
            }

            return false;
        }

        protected virtual bool OnCloseProject()
        {
            return true;
        }

        public void SaveProject()
        {
            if (this.IsProjectDirty)
            {
                this.Workspace.CommitTransaction();
                this.IsProjectDirty = false;
                this.Workspace.BeginTransaction();
            }
        }

        public void DeleteProject(IProject project)
        {
            if (this.Project == project)
                this.CloseProject();

            this.OnDeleteProject(project);
            this.Workspace.Delete(project);
        }

        protected virtual void OnDeleteProject(IProject project)
        {
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            base.OnClosing(e);

            e.Cancel = !this.CloseWorkspace();
        }
    }
}