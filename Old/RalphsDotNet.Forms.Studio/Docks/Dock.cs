﻿using System.ComponentModel;
using System.ComponentModel.Design;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;

namespace RalphsDotNet.Forms.Studio.Docks
{
    [DefaultEvent("Load")]
    [Designer("System.Windows.Forms.Design.FormDocumentDesigner, System.Design, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", typeof(IRootDesigner))]
    [DesignerCategory(@"Form")]
    [DesignTimeVisible(false)]
    [InitializationEvent("Load")]
    [ToolboxItem(false)]
    [ToolboxItemFilter("System.Windows.Forms.Control.TopLevel")]
    public partial class Dock : DockContent
    {
        public UserControl Control { get { return this.Controls.Count > 0 ? this.Controls[0] as UserControl : null; } }

        /// <summary>
        /// tool strip container to be mergeable with main window tool strip container
        /// </summary>
        [Browsable(true)]
        [EditorBrowsable(EditorBrowsableState.Always)]
        [DisplayName(@"ToolStripContainer")]
        [Category("Extended")]
        [Description("tool strip container to be mergeable with main window tool strip container")]
        public ToolStripContainer ToolStripContainer { get; set; }

        public Dock()
        {
            InitializeComponent();

            if (this.Control != null)
            {
                this.Control.PropertyChanged += Control_PropertyChanged;
                this.Text = this.Control.Text;
            }
        }

        void Control_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if(e.PropertyName == "Text")
                this.Text = this.Control.Text;
        }
    }
}
