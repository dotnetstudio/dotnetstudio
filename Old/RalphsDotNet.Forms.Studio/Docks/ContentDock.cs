﻿using System.ComponentModel;
using System.ComponentModel.Design;

namespace RalphsDotNet.Forms.Studio.Docks
{
    [DefaultEvent("Load")]
    [Designer("System.Windows.Forms.Design.FormDocumentDesigner, System.Design, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", typeof(IRootDesigner))]
    [DesignerCategory(@"Form")]
    [DesignTimeVisible(false)]
    [InitializationEvent("Load")]
    [ToolboxItem(false)]
    [ToolboxItemFilter("System.Windows.Forms.Control.TopLevel")]
    public partial class ContentDock : Dock
    {
        public ContentDock()
        {
            InitializeComponent();
        }
    }
}
