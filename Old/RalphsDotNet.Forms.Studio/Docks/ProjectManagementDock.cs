﻿using RalphsDotNet.Data.Projecting;
using RalphsDotNet.Forms.Studio.Controls;
using RalphsDotNet.Forms.Studio.Dialogs;
using System;
using System.ComponentModel;
using System.Linq;

namespace RalphsDotNet.Forms.Studio.Docks
{
    public partial class ProjectManagementDock : ToolDock
    {
        private readonly Window _window;

        public IQueryable<IProject> Projects
        {
            get { return this.projectManagementControl.Projects; }
            set { this.projectManagementControl.Projects = value; }
        }

        public ProjectManagementDock(Window window)
        {
            this._window = window;

            InitializeComponent();

            this.Projects = this._window.Projects.GetOfTypeOrderedByName(this._window.ProjectType);
            this.toolStripButton_NewProject.Enabled = this.Projects != null;

            window.PropertyChanged += window_PropertyChanged;
        }

        void window_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Project")
            {
                this.toolStripButton_CloseProject.Enabled = this.toolStripButton_RenameProject.Enabled = this._window.Project != null;
            }
            else if (e.PropertyName == "IsProjectDirty")
            {
                this.toolStripButton_SaveProject.Enabled = this._window.IsProjectDirty;
            }
        }

        private void workspaceControl_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "SelectedProject")
            {
                this.toolStripButton_OpenProject.Enabled = this.toolStripButton_DeleteProject.Enabled = this.projectManagementControl.SelectedProject != null;
            }
        }

        public void RenameProject()
        {
            if (this._window.Project != null)
            {
                this._window.Project.Name = RenameDialog.ShowDialog(this._window.Project.Name, this.Projects.Where(p => p != this._window.Project).Select(p => p.Name));
                this._window.Workspace.SaveOrUpdate(this._window.Project);
                this._window.IsProjectDirty = true;
                this.projectManagementControl.Reload();
            }
        }

        public event ProjectDoubleClickedEventHandler ProjectDoubleClicked;

        private void workspaceControl_ProjectDoubleClicked(object sender, ProjectDoubleClickedEventArgs e)
        {            
            if (this.ProjectDoubleClicked != null)
                this.ProjectDoubleClicked(sender, e);
        }

        private void toolStripButton_NewProject_Click(object sender, EventArgs e)
        {
            this._window.NewProject();
            this.projectManagementControl.Reload();
        }

        private void toolStripButton_OpenProject_Click(object sender, EventArgs e)
        {
            this._window.OpenProject(this.projectManagementControl.SelectedProject);
        }

        private void toolStripButton_SaveProject_Click(object sender, EventArgs e)
        {
            this._window.SaveProject();
        }

        private void toolStripButton_CloseProject_Click(object sender, EventArgs e)
        {
            this._window.CloseProject();
        }

        private void toolStripButton_DeleteSimulationProject_Click(object sender, EventArgs e)
        {
            this._window.DeleteProject(this.projectManagementControl.SelectedProject);

            this.projectManagementControl.Reload();
        }

        private void toolStripButton_RenameProject_Click(object sender, EventArgs e)
        {
            this.RenameProject();
        }
    }
}
