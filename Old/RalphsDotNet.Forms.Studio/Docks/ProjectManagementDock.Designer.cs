﻿namespace RalphsDotNet.Forms.Studio.Docks
{
    partial class ProjectManagementDock
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ProjectManagementDock));
            this.toolStripContainer = new System.Windows.Forms.ToolStripContainer();
            this.projectManagementControl = new RalphsDotNet.Forms.Studio.Controls.ProjectManagementControl();
            this.toolStrip = new System.Windows.Forms.ToolStrip();
            this.toolStripButton_NewProject = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton_OpenProject = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton_SaveProject = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton_RenameProject = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton_CloseProject = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton_DeleteProject = new System.Windows.Forms.ToolStripButton();
            this.toolStripContainer.ContentPanel.SuspendLayout();
            this.toolStripContainer.TopToolStripPanel.SuspendLayout();
            this.toolStripContainer.SuspendLayout();
            this.toolStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStripContainer
            // 
            // 
            // toolStripContainer.ContentPanel
            // 
            this.toolStripContainer.ContentPanel.Controls.Add(this.projectManagementControl);
            this.toolStripContainer.ContentPanel.Size = new System.Drawing.Size(256, 443);
            this.toolStripContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toolStripContainer.Location = new System.Drawing.Point(0, 0);
            this.toolStripContainer.Name = "toolStripContainer";
            this.toolStripContainer.Size = new System.Drawing.Size(256, 468);
            this.toolStripContainer.TabIndex = 1;
            // 
            // toolStripContainer.TopToolStripPanel
            // 
            this.toolStripContainer.TopToolStripPanel.Controls.Add(this.toolStrip);
            // 
            // projectManagementControl
            // 
            this.projectManagementControl.DataContext = null;
            this.projectManagementControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.projectManagementControl.Location = new System.Drawing.Point(0, 0);
            this.projectManagementControl.Name = "projectManagementControl";
            this.projectManagementControl.Projects = null;
            this.projectManagementControl.PropertyCheckInterval = 0;
            this.projectManagementControl.SelectedProject = null;
            this.projectManagementControl.Size = new System.Drawing.Size(256, 443);
            this.projectManagementControl.TabIndex = 1;
            this.projectManagementControl.ProjectDoubleClicked += new RalphsDotNet.Forms.Studio.Controls.ProjectDoubleClickedEventHandler(this.workspaceControl_ProjectDoubleClicked);
            this.projectManagementControl.PropertyChanged += new System.ComponentModel.PropertyChangedEventHandler(this.workspaceControl_PropertyChanged);
            // 
            // toolStrip
            // 
            this.toolStrip.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton_NewProject,
            this.toolStripButton_OpenProject,
            this.toolStripButton_SaveProject,
            this.toolStripButton_RenameProject,
            this.toolStripButton_CloseProject,
            this.toolStripButton_DeleteProject});
            this.toolStrip.Location = new System.Drawing.Point(3, 0);
            this.toolStrip.Name = "toolStrip";
            this.toolStrip.Size = new System.Drawing.Size(150, 25);
            this.toolStrip.TabIndex = 5;
            this.toolStrip.Text = "toolStrip1";
            // 
            // toolStripButton_NewProject
            // 
            this.toolStripButton_NewProject.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton_NewProject.Enabled = false;
            this.toolStripButton_NewProject.Image = global::RalphsDotNet.Forms.Studio.Properties.Resources.page_add;
            this.toolStripButton_NewProject.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_NewProject.Name = "toolStripButton_NewProject";
            this.toolStripButton_NewProject.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton_NewProject.Text = "New Project";
            this.toolStripButton_NewProject.Click += new System.EventHandler(this.toolStripButton_NewProject_Click);
            // 
            // toolStripButton_OpenProject
            // 
            this.toolStripButton_OpenProject.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton_OpenProject.Enabled = false;
            this.toolStripButton_OpenProject.Image = global::RalphsDotNet.Forms.Studio.Properties.Resources.page_edit;
            this.toolStripButton_OpenProject.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_OpenProject.Name = "toolStripButton_OpenProject";
            this.toolStripButton_OpenProject.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton_OpenProject.Text = "Open Project";
            this.toolStripButton_OpenProject.Click += new System.EventHandler(this.toolStripButton_OpenProject_Click);
            // 
            // toolStripButton_SaveProject
            // 
            this.toolStripButton_SaveProject.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton_SaveProject.Enabled = false;
            this.toolStripButton_SaveProject.Image = global::RalphsDotNet.Forms.Studio.Properties.Resources.page_save;
            this.toolStripButton_SaveProject.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_SaveProject.Name = "toolStripButton_SaveProject";
            this.toolStripButton_SaveProject.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton_SaveProject.Text = "Save Project";
            this.toolStripButton_SaveProject.Click += new System.EventHandler(this.toolStripButton_SaveProject_Click);
            // 
            // toolStripButton_RenameProject
            // 
            this.toolStripButton_RenameProject.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton_RenameProject.Enabled = false;
            this.toolStripButton_RenameProject.Image = global::RalphsDotNet.Forms.Studio.Properties.Resources.textfield_rename;
            this.toolStripButton_RenameProject.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_RenameProject.Name = "toolStripButton_RenameProject";
            this.toolStripButton_RenameProject.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton_RenameProject.Text = "Rename Project";
            this.toolStripButton_RenameProject.Click += new System.EventHandler(this.toolStripButton_RenameProject_Click);
            // 
            // toolStripButton_CloseProject
            // 
            this.toolStripButton_CloseProject.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton_CloseProject.Enabled = false;
            this.toolStripButton_CloseProject.Image = global::RalphsDotNet.Forms.Studio.Properties.Resources.page_red;
            this.toolStripButton_CloseProject.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_CloseProject.Name = "toolStripButton_CloseProject";
            this.toolStripButton_CloseProject.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton_CloseProject.Text = "Close Project";
            this.toolStripButton_CloseProject.Click += new System.EventHandler(this.toolStripButton_CloseProject_Click);
            // 
            // toolStripButton_DeleteProject
            // 
            this.toolStripButton_DeleteProject.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton_DeleteProject.Enabled = false;
            this.toolStripButton_DeleteProject.Image = global::RalphsDotNet.Forms.Studio.Properties.Resources.page_delete;
            this.toolStripButton_DeleteProject.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_DeleteProject.Name = "toolStripButton_DeleteProject";
            this.toolStripButton_DeleteProject.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton_DeleteProject.Text = "Delete Project";
            this.toolStripButton_DeleteProject.Click += new System.EventHandler(this.toolStripButton_DeleteSimulationProject_Click);
            // 
            // ProjectManagementDockDock
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(256, 468);
            this.Controls.Add(this.toolStripContainer);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ProjectManagementDockDock";
            this.Text = "Projects";
            this.ToolStripContainer = this.toolStripContainer;
            this.toolStripContainer.ContentPanel.ResumeLayout(false);
            this.toolStripContainer.TopToolStripPanel.ResumeLayout(false);
            this.toolStripContainer.TopToolStripPanel.PerformLayout();
            this.toolStripContainer.ResumeLayout(false);
            this.toolStripContainer.PerformLayout();
            this.toolStrip.ResumeLayout(false);
            this.toolStrip.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ToolStripContainer toolStripContainer;
        private RalphsDotNet.Forms.Studio.Controls.ProjectManagementControl projectManagementControl;
        private System.Windows.Forms.ToolStrip toolStrip;
        private System.Windows.Forms.ToolStripButton toolStripButton_NewProject;
        private System.Windows.Forms.ToolStripButton toolStripButton_OpenProject;
        private System.Windows.Forms.ToolStripButton toolStripButton_SaveProject;
        private System.Windows.Forms.ToolStripButton toolStripButton_CloseProject;
        private System.Windows.Forms.ToolStripButton toolStripButton_DeleteProject;
        private System.Windows.Forms.ToolStripButton toolStripButton_RenameProject;
    }
}