﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using RalphsDotNet.Extension;

namespace RalphsDotNet.Forms.Studio.Dialogs
{
    public partial class RenameDialog : Form
    {
        protected static Property NameProperty = new Property("Name", typeof (string), string.Empty);
        private bool _editing;

        public RenameDialog()
        {
            this.InitializeComponent();
        }

        public new string Name
        {
            get { return this.GetValue(NameProperty) as string; }
            set { this.SetValue(NameProperty, value); }
        }

        protected override void OnPropertyChanged(Property property, object newValue, object oldValue)
        {
            base.OnPropertyChanged(property, newValue, oldValue);

            if (property == NameProperty && !this._editing)
            {
                this.textBox_Name.Text = this.Name;
            }
        }

        private void Set()
        {
            this._editing = true;
            this.Name = this.textBox_Name.Text;
            this._editing = false;
        }

        private void textBox_Name_TextChanged(object sender, EventArgs e)
        {
            this.Set();
        }

        public static string ShowDialog(string name, IEnumerable<string> existingNames)
        {
            var dialog = new RenameDialog {Name = name};

            return dialog.ShowDialog() == DialogResult.OK
                ? dialog.Name.GetUniqueName(existingNames)
                : name.GetUniqueName(existingNames);
        }

        private void button_OK_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void button_Cancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }
    }
}