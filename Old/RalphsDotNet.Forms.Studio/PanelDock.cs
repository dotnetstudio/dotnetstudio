﻿using DockDotNET;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace RalphsDotNet.Forms.Studio
{
    [DefaultEvent("Load")]
    [Designer("System.Windows.Forms.Design.FormDocumentDesigner, System.Design, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", typeof(IRootDesigner))]
    [DesignerCategory("Form")]
    [DesignTimeVisible(false)]
    [InitializationEvent("Load")]
    [ToolboxItem(false)]
    [ToolboxItemFilter("System.Windows.Forms.Control.TopLevel")]
    public partial class PanelDock : Dock
    {
        public PanelDock() : base()
        {
            InitializeComponent();
        }
    }
}
