﻿namespace RalphsDotNet.Forms.Studio.Controls
{
    partial class ProjectManagementControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ProjectManagementControl));
            this.listView = new System.Windows.Forms.ListView();
            this.imageList_Small = new System.Windows.Forms.ImageList(this.components);
            this.imageList_Large = new System.Windows.Forms.ImageList(this.components);
            this.SuspendLayout();
            // 
            // listView
            // 
            this.listView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listView.Enabled = false;
            this.listView.LargeImageList = this.imageList_Small;
            this.listView.Location = new System.Drawing.Point(0, 0);
            this.listView.MultiSelect = false;
            this.listView.Name = "listView";
            this.listView.Size = new System.Drawing.Size(242, 487);
            this.listView.SmallImageList = this.imageList_Small;
            this.listView.TabIndex = 0;
            this.listView.UseCompatibleStateImageBehavior = false;
            this.listView.View = System.Windows.Forms.View.List;
            this.listView.SelectedIndexChanged += new System.EventHandler(this.listView_SelectedIndexChanged);
            this.listView.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.listView_MouseDoubleClick);
            // 
            // imageList_Small
            // 
            this.imageList_Small.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList_Small.ImageStream")));
            this.imageList_Small.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList_Small.Images.SetKeyName(0, "world.png");
            // 
            // imageList_Large
            // 
            this.imageList_Large.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList_Large.ImageStream")));
            this.imageList_Large.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList_Large.Images.SetKeyName(0, "world.png");
            // 
            // WorkspaceControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.listView);
            this.Name = "WorkspaceControl";
            this.Size = new System.Drawing.Size(242, 487);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView listView;
        private System.Windows.Forms.ImageList imageList_Small;
        private System.Windows.Forms.ImageList imageList_Large;
    }
}
