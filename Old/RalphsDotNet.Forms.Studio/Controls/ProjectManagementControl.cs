﻿using System;
using System.Linq;
using System.Windows.Forms;
using RalphsDotNet.Data.Projecting;

namespace RalphsDotNet.Forms.Studio.Controls
{
    public partial class ProjectManagementControl : UserControl
    {
        protected static Property ProjectsProperty = new Property("Projects", typeof(IQueryable<IProject>));

        public IQueryable<IProject> Projects
        {
            get { return this.GetValue(ProjectsProperty) as IQueryable<IProject>; }
            set { this.SetValue(ProjectsProperty, value); }
        }

        protected static Property SelectedProjectProperty = new Property("SelectedProject", typeof(IProject));

        public IProject SelectedProject
        {
            get { return this.GetValue(SelectedProjectProperty) as IProject; }
            set { this.SetValue(SelectedProjectProperty, value); }
        }

        public ProjectManagementControl()
        {
            InitializeComponent();
        }

        protected override void OnPropertyChanged(Property property, object newValue, object oldValue)
        {
            base.OnPropertyChanged(property, newValue, oldValue);

            if(property == ProjectsProperty)
            {
                this.listView.Enabled = newValue != null;

                if (newValue != null)
                    this.Reload();
            }
        }

        public void Reload()
        {
            var oldSelection = this.SelectedProject;
            this.SelectedProject = null;

            this.listView.Items.Clear();
            if (this.Projects != null)
                this.listView.Items.AddRange(this.Projects.Select(p => this.CreateListViewItemForProject(p)).ToArray());

// ReSharper disable once AssignNullToNotNullAttribute
            this.SelectedProject = oldSelection != null && this.Projects.Contains(oldSelection) ? oldSelection : null;
        }

        private ListViewItem CreateListViewItemForProject(IProject project)
        {
            return new ListViewItem(project.Name) { Tag = project, ImageIndex = 0 };
        }

        private void listView_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            var selectedItem = this.listView.SelectedItems.OfType<ListViewItem>().FirstOrDefault();
            if (selectedItem != null && this.ProjectDoubleClicked != null)
                this.ProjectDoubleClicked(this, new ProjectDoubleClickedEventArgs(selectedItem.Tag as IProject));
        }

        public event ProjectDoubleClickedEventHandler ProjectDoubleClicked;

        private void listView_SelectedIndexChanged(object sender, EventArgs e)
        {
            var selectedItem = this.listView.SelectedItems.OfType<ListViewItem>().FirstOrDefault();
            this.SelectedProject = selectedItem != null ? selectedItem.Tag as IProject : null;
        }
    }

    public delegate void ProjectDoubleClickedEventHandler(object sender, ProjectDoubleClickedEventArgs e);

    public class ProjectDoubleClickedEventArgs : EventArgs
    {
        public IProject Project { get; private set; }

        public ProjectDoubleClickedEventArgs(IProject project)
        {
            this.Project = project;
        }
    }
}
