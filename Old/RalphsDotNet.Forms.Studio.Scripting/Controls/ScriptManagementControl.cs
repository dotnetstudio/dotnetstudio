﻿using System;
using System.Linq;
using System.Windows.Forms;
using RalphsDotNet.Data.Scripting;

namespace RalphsDotNet.Forms.Studio.Scripting
{
    public partial class ScriptManagementControl : UserControl
    {
        protected static Property ScriptsProperty = new Property("Scripts", typeof(IQueryable<IScript>));

        public IQueryable<IScript> Scripts
        {
            get { return this.GetValue(ScriptsProperty) as IQueryable<IScript>; }
            set { this.SetValue(ScriptsProperty, value); }
        }

        protected static Property SelectedScriptProperty = new Property("SelectedScript", typeof(IScript));

        public IScript SelectedScript
        {
            get { return this.GetValue(SelectedScriptProperty) as IScript; }
            set { this.SetValue(SelectedScriptProperty, value); }
        }

        public ScriptManagementControl()
        {
            InitializeComponent();
        }

        protected override void OnPropertyChanged(Property property, object newValue, object oldValue)
        {
            base.OnPropertyChanged(property, newValue, oldValue);

            if (property == ScriptsProperty)
            {
                this.listView.Enabled = newValue != null;

                this.Reload();
            }
        }

        public void Reload()
        {
            this.listView.Items.Clear();
            if (this.Scripts != null)
                this.listView.Items.AddRange(this.Scripts.Select(p => this.CreateListViewItemForProject(p)).ToArray());
        }

        private ListViewItem CreateListViewItemForProject(IScript script)
        {
            return new ListViewItem(script.Name) { Tag = script, ImageIndex = 0 };
        }

        private void listView_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (this.ScriptDoubleClicked != null && this.SelectedScript != null)
                this.ScriptDoubleClicked(this, new ScriptDoubleClickedEventArgs(this.SelectedScript));
        }

        public event ScriptDoubleClickedEventHandler ScriptDoubleClicked;

        private void listView_SelectedIndexChanged(object sender, EventArgs e)
        {
            var selectedItem = this.listView.SelectedItems.OfType<ListViewItem>().FirstOrDefault();
            this.SelectedScript = selectedItem != null ? selectedItem.Tag as IScript : null;
        }
    }

    public delegate void ScriptDoubleClickedEventHandler(object sender, ScriptDoubleClickedEventArgs e);

    public class ScriptDoubleClickedEventArgs : EventArgs
    {
        public IScript Script { get; private set; }

        public ScriptDoubleClickedEventArgs(IScript script)
        {
            this.Script = script;
        }
    }
}
