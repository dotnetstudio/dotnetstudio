﻿namespace RalphsDotNet.Forms.Studio.Scripting.Docks
{
    partial class ScriptManagementDock
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ScriptManagementDock));
            this.toolStripContainer = new System.Windows.Forms.ToolStripContainer();
            this.scriptManagementControl = new RalphsDotNet.Forms.Studio.Scripting.ScriptManagementControl();
            this.toolStrip = new System.Windows.Forms.ToolStrip();
            this.toolStripButton_NewScript = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton_EditScript = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton_RenameScript = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton_DeleteScript = new System.Windows.Forms.ToolStripButton();
            this.toolStripContainer.ContentPanel.SuspendLayout();
            this.toolStripContainer.TopToolStripPanel.SuspendLayout();
            this.toolStripContainer.SuspendLayout();
            this.toolStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStripContainer
            // 
            // 
            // toolStripContainer.ContentPanel
            // 
            this.toolStripContainer.ContentPanel.Controls.Add(this.scriptManagementControl);
            this.toolStripContainer.ContentPanel.Size = new System.Drawing.Size(256, 443);
            this.toolStripContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toolStripContainer.Location = new System.Drawing.Point(0, 0);
            this.toolStripContainer.Name = "toolStripContainer";
            this.toolStripContainer.Size = new System.Drawing.Size(256, 468);
            this.toolStripContainer.TabIndex = 0;
            this.toolStripContainer.Text = "toolStripContainer1";
            // 
            // toolStripContainer.TopToolStripPanel
            // 
            this.toolStripContainer.TopToolStripPanel.Controls.Add(this.toolStrip);
            // 
            // scriptManagementControl
            // 
            this.scriptManagementControl.DataContext = null;
            this.scriptManagementControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.scriptManagementControl.Location = new System.Drawing.Point(0, 0);
            this.scriptManagementControl.Name = "scriptManagementControl";
            this.scriptManagementControl.PropertyCheckInterval = 0;
            this.scriptManagementControl.Scripts = null;
            this.scriptManagementControl.SelectedScript = null;
            this.scriptManagementControl.Size = new System.Drawing.Size(256, 443);
            this.scriptManagementControl.TabIndex = 0;
            this.scriptManagementControl.ScriptDoubleClicked += new RalphsDotNet.Forms.Studio.Scripting.ScriptDoubleClickedEventHandler(this.scriptManagementControl_ScriptDoubleClicked);
            this.scriptManagementControl.PropertyChanged += new System.ComponentModel.PropertyChangedEventHandler(this.scriptManagementControl_PropertyChanged);
            // 
            // toolStrip
            // 
            this.toolStrip.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton_NewScript,
            this.toolStripButton_EditScript,
            this.toolStripButton_RenameScript,
            this.toolStripButton_DeleteScript});
            this.toolStrip.Location = new System.Drawing.Point(3, 0);
            this.toolStrip.Name = "toolStrip";
            this.toolStrip.Size = new System.Drawing.Size(104, 25);
            this.toolStrip.TabIndex = 0;
            // 
            // toolStripButton_NewScript
            // 
            this.toolStripButton_NewScript.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton_NewScript.Enabled = false;
            this.toolStripButton_NewScript.Image = global::RalphsDotNet.Forms.Studio.Scripting.Properties.Resources.script_add;
            this.toolStripButton_NewScript.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_NewScript.Name = "toolStripButton_NewScript";
            this.toolStripButton_NewScript.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton_NewScript.Text = "Create Script";
            this.toolStripButton_NewScript.Click += new System.EventHandler(this.toolStripButton_NewScript_Click);
            // 
            // toolStripButton_EditScript
            // 
            this.toolStripButton_EditScript.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton_EditScript.Enabled = false;
            this.toolStripButton_EditScript.Image = global::RalphsDotNet.Forms.Studio.Scripting.Properties.Resources.script_edit;
            this.toolStripButton_EditScript.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_EditScript.Name = "toolStripButton_EditScript";
            this.toolStripButton_EditScript.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton_EditScript.Text = "Edit Script";
            this.toolStripButton_EditScript.Click += new System.EventHandler(this.toolStripButton_EditScript_Click);
            // 
            // toolStripButton_RenameScript
            // 
            this.toolStripButton_RenameScript.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton_RenameScript.Enabled = false;
            this.toolStripButton_RenameScript.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton_RenameScript.Image")));
            this.toolStripButton_RenameScript.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_RenameScript.Name = "toolStripButton_RenameScript";
            this.toolStripButton_RenameScript.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton_RenameScript.Text = "Rename Script";
            this.toolStripButton_RenameScript.Click += new System.EventHandler(this.toolStripButton_RenameScript_Click);
            // 
            // toolStripButton_DeleteScript
            // 
            this.toolStripButton_DeleteScript.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton_DeleteScript.Enabled = false;
            this.toolStripButton_DeleteScript.Image = global::RalphsDotNet.Forms.Studio.Scripting.Properties.Resources.script_delete;
            this.toolStripButton_DeleteScript.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_DeleteScript.Name = "toolStripButton_DeleteScript";
            this.toolStripButton_DeleteScript.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton_DeleteScript.Text = "Delete Script";
            this.toolStripButton_DeleteScript.Click += new System.EventHandler(this.toolStripButton_DeleteScript_Click);
            // 
            // ScriptManagementDock
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(256, 468);
            this.Controls.Add(this.toolStripContainer);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ScriptManagementDock";
            this.Text = "ScriptManagementDock";
            this.ToolStripContainer = this.toolStripContainer;
            this.toolStripContainer.ContentPanel.ResumeLayout(false);
            this.toolStripContainer.TopToolStripPanel.ResumeLayout(false);
            this.toolStripContainer.TopToolStripPanel.PerformLayout();
            this.toolStripContainer.ResumeLayout(false);
            this.toolStripContainer.PerformLayout();
            this.toolStrip.ResumeLayout(false);
            this.toolStrip.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ToolStripContainer toolStripContainer;
        private ScriptManagementControl scriptManagementControl;
        private System.Windows.Forms.ToolStrip toolStrip;
        private System.Windows.Forms.ToolStripButton toolStripButton_NewScript;
        private System.Windows.Forms.ToolStripButton toolStripButton_EditScript;
        private System.Windows.Forms.ToolStripButton toolStripButton_DeleteScript;
        private System.Windows.Forms.ToolStripButton toolStripButton_RenameScript;

    }
}