﻿namespace RalphsDotNet.Forms.Studio.Scripting.Docks
{
    partial class ScriptDock
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.toolStripContainer = new System.Windows.Forms.ToolStripContainer();
            this.scriptEditor = new RalphsDotNet.Forms.Scripting.ScriptEditor();
            this.toolStrip = new System.Windows.Forms.ToolStrip();
            this.toolStripButton_Undo = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton_Redo = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton_Cut = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton_Copy = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton_Paste = new System.Windows.Forms.ToolStripButton();
            this.toolStripContainer.ContentPanel.SuspendLayout();
            this.toolStripContainer.TopToolStripPanel.SuspendLayout();
            this.toolStripContainer.SuspendLayout();
            this.toolStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStripContainer
            // 
            // 
            // toolStripContainer.ContentPanel
            // 
            this.toolStripContainer.ContentPanel.Controls.Add(this.scriptEditor);
            this.toolStripContainer.ContentPanel.Size = new System.Drawing.Size(894, 427);
            this.toolStripContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toolStripContainer.Location = new System.Drawing.Point(0, 0);
            this.toolStripContainer.Name = "toolStripContainer";
            this.toolStripContainer.Size = new System.Drawing.Size(894, 452);
            this.toolStripContainer.TabIndex = 0;
            // 
            // toolStripContainer.TopToolStripPanel
            // 
            this.toolStripContainer.TopToolStripPanel.Controls.Add(this.toolStrip);
            // 
            // scriptEditor
            // 
            this.scriptEditor.DataContext = null;
            this.scriptEditor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.scriptEditor.IsDirty = false;
            this.scriptEditor.Location = new System.Drawing.Point(0, 0);
            this.scriptEditor.Name = "scriptEditor";
            this.scriptEditor.PropertyCheckInterval = 0;
            this.scriptEditor.Script = null;
            this.scriptEditor.Size = new System.Drawing.Size(894, 427);
            this.scriptEditor.TabIndex = 0;
            this.scriptEditor.SaveRequired += new System.EventHandler(this.scriptEditor_SaveRequired);
            this.scriptEditor.PropertyChanging += new System.ComponentModel.PropertyChangingEventHandler(this.scriptEditor_PropertyChanging);
            this.scriptEditor.PropertyChanged += new System.ComponentModel.PropertyChangedEventHandler(this.scriptEditor_PropertyChanged);
            // 
            // toolStrip
            // 
            this.toolStrip.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton_Undo,
            this.toolStripButton_Redo,
            this.toolStripSeparator1,
            this.toolStripButton_Cut,
            this.toolStripButton_Copy,
            this.toolStripButton_Paste});
            this.toolStrip.Location = new System.Drawing.Point(3, 0);
            this.toolStrip.Name = "toolStrip";
            this.toolStrip.Size = new System.Drawing.Size(133, 25);
            this.toolStrip.TabIndex = 2;
            this.toolStrip.Text = "Script Edititing Tools";
            // 
            // toolStripButton_Undo
            // 
            this.toolStripButton_Undo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton_Undo.Enabled = false;
            this.toolStripButton_Undo.Image = global::RalphsDotNet.Forms.Studio.Scripting.Properties.Resources.arrow_undo;
            this.toolStripButton_Undo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_Undo.Name = "toolStripButton_Undo";
            this.toolStripButton_Undo.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton_Undo.Text = "Undo";
            // 
            // toolStripButton_Redo
            // 
            this.toolStripButton_Redo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton_Redo.Enabled = false;
            this.toolStripButton_Redo.Image = global::RalphsDotNet.Forms.Studio.Scripting.Properties.Resources.arrow_redo;
            this.toolStripButton_Redo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_Redo.Name = "toolStripButton_Redo";
            this.toolStripButton_Redo.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton_Redo.Text = "Redo";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButton_Cut
            // 
            this.toolStripButton_Cut.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton_Cut.Enabled = false;
            this.toolStripButton_Cut.Image = global::RalphsDotNet.Forms.Studio.Scripting.Properties.Resources.cut;
            this.toolStripButton_Cut.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_Cut.Name = "toolStripButton_Cut";
            this.toolStripButton_Cut.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton_Cut.Text = "Cut";
            // 
            // toolStripButton_Copy
            // 
            this.toolStripButton_Copy.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton_Copy.Enabled = false;
            this.toolStripButton_Copy.Image = global::RalphsDotNet.Forms.Studio.Scripting.Properties.Resources.page_white_copy;
            this.toolStripButton_Copy.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_Copy.Name = "toolStripButton_Copy";
            this.toolStripButton_Copy.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton_Copy.Text = "Copy";
            // 
            // toolStripButton_Paste
            // 
            this.toolStripButton_Paste.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton_Paste.Enabled = false;
            this.toolStripButton_Paste.Image = global::RalphsDotNet.Forms.Studio.Scripting.Properties.Resources.page_white_paste;
            this.toolStripButton_Paste.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_Paste.Name = "toolStripButton_Paste";
            this.toolStripButton_Paste.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton_Paste.Text = "Paste";
            // 
            // ScriptDock
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(894, 452);
            this.Controls.Add(this.toolStripContainer);
            this.Name = "ScriptDock";
            this.Text = "";
            this.ToolStripContainer = this.toolStripContainer;
            this.toolStripContainer.ContentPanel.ResumeLayout(false);
            this.toolStripContainer.TopToolStripPanel.ResumeLayout(false);
            this.toolStripContainer.TopToolStripPanel.PerformLayout();
            this.toolStripContainer.ResumeLayout(false);
            this.toolStripContainer.PerformLayout();
            this.toolStrip.ResumeLayout(false);
            this.toolStrip.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip;
        private System.Windows.Forms.ToolStripButton toolStripButton_Undo;
        private System.Windows.Forms.ToolStripButton toolStripButton_Redo;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton toolStripButton_Cut;
        private System.Windows.Forms.ToolStripButton toolStripButton_Copy;
        private System.Windows.Forms.ToolStripButton toolStripButton_Paste;
        private RalphsDotNet.Forms.Scripting.ScriptEditor scriptEditor;
        public System.Windows.Forms.ToolStripContainer toolStripContainer;


    }
}