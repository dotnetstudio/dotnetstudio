﻿using RalphsDotNet.Data.Scripting;
using RalphsDotNet.Forms.Studio.Docks;
using RalphsDotNet.Service.Data.Interface;
using System;
using System.ComponentModel;

namespace RalphsDotNet.Forms.Studio.Scripting.Docks
{
    public partial class ScriptDock : ContentDock
    {
        private readonly IDataService _workspace;

        public IScript Script { get { return this.scriptEditor.Script; } set { this.scriptEditor.Script = value; } }

        public bool IsDirty { get { return this.scriptEditor.IsDirty; } private set { this.scriptEditor.IsDirty = value; } }

        public IScriptRepository Repository { get { return this._workspace != null ? this._workspace.GetRepository<IScript, IScriptRepository>() : null; } }

        public ScriptDock(IDataService workspace)
        {
            this._workspace = workspace;

            InitializeComponent();
        }

        private void scriptEditor_PropertyChanging(object sender, PropertyChangingEventArgs e)
        {
            if (e.PropertyName == "Script")
            {
                if (this.Script != null)
                    this.Script.PropertyChanged += Script_PropertyChanged;
            }
        }

        private void scriptEditor_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Script")
            {
                if (this.Script != null)
                {
                    this.Text = this.Script.Name;
                    this.toolStripButton_Undo.Enabled = this.toolStripButton_Redo.Enabled = this.toolStripButton_Cut.Enabled = this.toolStripButton_Copy.Enabled = this.toolStripButton_Paste.Enabled = true;

                    this.Script.PropertyChanged += Script_PropertyChanged;
                }
                else
                {
                    this.Text = string.Empty;
                    this.toolStripButton_Undo.Enabled = this.toolStripButton_Redo.Enabled = this.toolStripButton_Cut.Enabled = this.toolStripButton_Copy.Enabled = this.toolStripButton_Paste.Enabled = false;
                }
            }
        }

        void Script_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Name")
            {
                this.Text = this.Script.Name;
            }
        }

        private void SaveScript()
        {
            if (this._workspace != null)
            {
                this.Repository.SaveOrUpdate(this.Script);
                this.IsDirty = false;

                if (this.ProjectGotDirty != null)
                    this.ProjectGotDirty(this, new EventArgs());
            }
        }

        private void scriptEditor_SaveRequired(object sender, EventArgs e)
        {
            this.SaveScript();
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            base.OnClosing(e);

            this.scriptEditor.PrepareClose();
        }

        public event EventHandler ProjectGotDirty;
    }
}
