﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using RalphsDotNet.Data.Projecting;
using RalphsDotNet.Data.Scripting;
using RalphsDotNet.Extension;
using RalphsDotNet.Forms.Studio.Dialogs;
using RalphsDotNet.Forms.Studio.Docks;
using RalphsDotNet.Forms.Studio.Scripting.Data;
using RalphsDotNet.Service.Data.Interface;

namespace RalphsDotNet.Forms.Studio.Scripting.Docks
{
    public partial class ScriptManagementDock : ToolDock
    {
        private readonly List<ScriptDock> _scriptingDocks = new List<ScriptDock>();
        private readonly Window _window;

        public ScriptManagementDock(Window window)
        {
            this._window = window;

            this.InitializeComponent();

            this.Scripts = this._window.Project != null
                ? this.MappingRepository.GetScriptsOfProject(this.Project)
                : null;
        }

        public IQueryable<IScript> Scripts
        {
            get { return this.scriptManagementControl.Scripts; }
            set { this.scriptManagementControl.Scripts = value; }
        }

        public IScriptRepository ScriptRepository
        {
            get
            {
                return this._window.Workspace != null
                    ? this._window.Workspace.GetRepository<IScript, IScriptRepository>()
                    : null;
            }
        }

        public IScriptProjectMappingRepository MappingRepository
        {
            get
            {
                return this._window.Workspace != null
                    ? this._window.Workspace.GetRepository<IScriptProjectMapping, IScriptProjectMappingRepository>()
                    : null;
            }
        }

        public IDataService Workspace
        {
            get { return this._window.Workspace; }
        }

        public IProject Project
        {
            get { return this._window.Project; }
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            base.OnClosing(e);

            this._scriptingDocks.ToList().ForEach(d => this.CloseScript(d.Script));
        }

        private void scriptManagementControl_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Scripts")
            {
                this.toolStripButton_NewScript.Enabled = this.Scripts != null;
            }
            else if (e.PropertyName == "SelectedScript")
            {
                this.toolStripButton_EditScript.Enabled =
                    this.toolStripButton_RenameScript.Enabled =
                        this.toolStripButton_DeleteScript.Enabled = this.scriptManagementControl.SelectedScript != null;
            }
        }

        private void OpenScript(IScript script)
        {
            var dock = this._scriptingDocks.FirstOrDefault(d => d.Script == script);
            if (dock == null)
            {
                dock = new ScriptDock(this.Workspace) {Script = script};
                dock.ProjectGotDirty += this.dock_ProjectGotDirty;
                dock.FormClosed += this.dock_FormClosed;
                this._scriptingDocks.Add(dock);
            }

            if (!dock.Visible)
                this._window.ShowDock(dock);
        }

        public void RenameScript()
        {
            if (this.scriptManagementControl.SelectedScript != null)
            {
                this.scriptManagementControl.SelectedScript.Name =
                    RenameDialog.ShowDialog(this.scriptManagementControl.SelectedScript.Name,
                        this.Scripts.Where(s => s != this.scriptManagementControl.SelectedScript).Select(s => s.Name));
                this.ScriptRepository.SaveOrUpdate(this.scriptManagementControl.SelectedScript);
                this._window.IsProjectDirty = true;
                this.scriptManagementControl.Reload();
            }
        }

        private void dock_ProjectGotDirty(object sender, EventArgs e)
        {
            this._window.IsProjectDirty = true;
        }

        private void dock_FormClosed(object sender, FormClosedEventArgs e)
        {
            var dock = sender as ScriptDock;
            if (this._scriptingDocks.Contains(dock))
                this._scriptingDocks.Remove(dock);
        }

        private void CloseScript(IScript script)
        {
            var dock = this._scriptingDocks.FirstOrDefault(d => d.Script == script);
            if (dock != null)
            {
                dock.Close();
                this._scriptingDocks.Remove(dock);
            }
        }

        private void scriptManagementControl_ScriptDoubleClicked(object sender, ScriptDoubleClickedEventArgs e)
        {
            this.OpenScript(e.Script);
        }

        private void toolStripButton_EditScript_Click(object sender, EventArgs e)
        {
            if (this.scriptManagementControl.SelectedScript != null)
                this.OpenScript(this.scriptManagementControl.SelectedScript);
        }

        private void toolStripButton_NewScript_Click(object sender, EventArgs e)
        {
            var script = this.ScriptRepository.Create(
                "Untitled Script".GetUniqueName(this.Scripts.Select(s => s.Name)), ScriptLanguage.Python, null);
            this.ScriptRepository.SaveOrUpdate(script);
            this.MappingRepository.SaveOrUpdate(this.MappingRepository.Create(script, this.Project));
            this._window.IsProjectDirty = true;
            this.scriptManagementControl.Reload();
            this.OpenScript(script);
        }

        private void toolStripButton_DeleteScript_Click(object sender, EventArgs e)
        {
            if (this.scriptManagementControl.SelectedScript != null)
            {
                this.CloseScript(this.scriptManagementControl.SelectedScript);
                this.ScriptRepository.Delete(this.scriptManagementControl.SelectedScript);
                this._window.IsProjectDirty = true;
                this.scriptManagementControl.Reload();
            }
        }

        private void toolStripButton_RenameScript_Click(object sender, EventArgs e)
        {
            this.RenameScript();
        }
    }
}