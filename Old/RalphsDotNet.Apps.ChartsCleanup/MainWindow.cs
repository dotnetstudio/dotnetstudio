﻿using System;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using RalphsDotNet.Apps.ChartsCleanup.Properties;
using RalphsDotNet.Forms;
using Form = RalphsDotNet.Forms.Form;

namespace RalphsDotNet.Apps.ChartsCleanup
{
    public partial class MainWindow : Form
    {
        protected static Property ChartsPathProperty = new Property("ChartsPath", typeof(string), string.Empty);
        public string ChartsPath { get { return this.GetValue(ChartsPathProperty) as string; } set { this.SetValue(ChartsPathProperty, value); } }

        protected static Property OverallProgressProperty = new Property("OverallProgress", typeof(int), 0);
        public int OverallProgress { get { return (int)this.GetValue(OverallProgressProperty); } set { this.SetValue(OverallProgressProperty, value); } }

        protected static Property ProgressProperty = new Property("Progress", typeof(int), 0);
        public int Progress { get { return (int)this.GetValue(ProgressProperty); } set { this.SetValue(ProgressProperty, value); } }


        public MainWindow()
        {
            InitializeComponent();
        }

        protected override void OnPropertyChanged(Property property, object newValue, object oldValue)
        {
            base.OnPropertyChanged(property, newValue, oldValue);

            if (property == ChartsPathProperty && !this.InvokeRequired)
            {
                this.textBox_directory.Text = this.ChartsPath;
                this.button_start.Enabled = !string.IsNullOrEmpty(this.ChartsPath);
            }
            else if (property == OverallProgressProperty && !this.InvokeRequired)
            {
                this.progressBar.Maximum = this.OverallProgress;
            }
            else if (property == ProgressProperty && !this.InvokeRequired)
            {
                this.progressBar.Value = this.Progress;
            }
        }

        private void button_chooseDirectory_Click(object sender, EventArgs e)
        {
            if (DialogResult.OK == this.folderBrowserDialog.ShowDialog())
            {
                this.ChartsPath = this.folderBrowserDialog.SelectedPath;
            }
        }

        private void button_start_Click(object sender, EventArgs e)
        {
            this.button_chooseDirectory.Enabled = false;
            this.button_start.Enabled = false;

            this.backgroundWorker.RunWorkerAsync();
        }

        private void backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                var dir = new DirectoryInfo(this.ChartsPath);
                var files = dir.GetFiles();

                this.Progress = 0;
                this.OverallProgress = files.Length;

                foreach (var file in files)
                {
                    if (file.Extension.ToLower() == ".mp3")
                    {
                        var f = TagLib.File.Create(file.FullName);

                        var newName = string.Concat(f.Tag.FirstPerformer, " - ", f.Tag.Title, ".mp3");

                        Path.GetInvalidFileNameChars().ToList().ForEach(c => newName = newName.Replace(c, '_'));

                        if (!File.Exists(Path.Combine(dir.FullName, newName)))
                        {
                            file.MoveTo(Path.Combine(dir.FullName, newName));
                        }
                        else if (file.Name != newName)
                        {
                            file.Delete();
                        }
                    }
                    else
                    {
                        file.Delete();
                    }

                    this.Progress++;
                }

                MessageBox.Show(Resources.MainWindow_backgroundWorker_DoWork_SUCCESS_Text, Resources.MainWindow_backgroundWorker_DoWork_SUCCESS_Title, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, Resources.MainWindow_backgroundWorker_DoWork_FAILED_Title, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void backgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.button_chooseDirectory.Enabled = true;
            this.button_start.Enabled = true;
        }
    }
}
