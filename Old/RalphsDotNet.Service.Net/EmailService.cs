using System;
using System.Net;
using System.Net.Mail;
using RalphsDotNet.Service.Net.Interface;

namespace RalphsDotNet.Service.Net
{
    /// <summary>
    ///     Service object for sending emails.
    /// </summary>
    public class EmailService : IEmailService
    {
        public EmailService(IEmailService proxy, ServiceConfiguration configuration)
        {
            this.ServiceConfiguration = configuration;
        }

        public EmailServiceConfiguration EmailServiceConfiguration
        {
            get { return (EmailServiceConfiguration) this.ServiceConfiguration; }
        }

        public string Id
        {
            get { return this.ServiceConfiguration.Id; }
        }

        public ServiceConfiguration ServiceConfiguration { get; set; }

        /// <summary>
        ///     Sends an email.
        /// </summary>
        /// <param name='to'>
        ///     Receiver email address.
        /// </param>
        /// <param name='subject'>
        ///     Subject of the email.
        /// </param>
        /// <param name='body'>
        ///     Content of the email.
        /// </param>
        public void Send(string to, string subject, string body)
        {
            var message = new MailMessage();
            message.To.Add(to);
            message.Subject = subject;
            message.From = new MailAddress(this.EmailServiceConfiguration.SenderAdress);
            message.Body = body;
            var smtp = new SmtpClient(this.EmailServiceConfiguration.SmtpHost,
                this.EmailServiceConfiguration.SmtpPort)
            {
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(this.EmailServiceConfiguration.SmtpUsername,
                    this.EmailServiceConfiguration.SmtpPassword),
                EnableSsl = this.EmailServiceConfiguration.SmtpUsesSSL,
                DeliveryMethod = SmtpDeliveryMethod.Network
            };
            using (smtp)
                smtp.Send(message);
        }

        public void Dispose()
        {
        }
    }
}