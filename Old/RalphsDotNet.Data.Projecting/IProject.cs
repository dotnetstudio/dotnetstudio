﻿using System;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using RalphsDotNet.Data.Base;

namespace RalphsDotNet.Data.Projecting
{
    public interface IProject : IDataObject, IHasUniqueName
    {
        Guid Type { get; set; }

        DateTime LastAccess { get; set; }
        DateTime LastModification { get; set; }
    }

    public interface IProjectRepository : IDataRepository<IProject>
    {
        IProject Create();

        void UpdateAccess(IProject project);
        void UpdateModification(IProject project);
        IQueryable<IProject> GetOfType(Guid type);
        IQueryable<IProject> GetOfTypeOrderedByName(Guid type);
        IQueryable<IProject> GetOfTypeOrderedByLastAccess(Guid type);
    }
}
