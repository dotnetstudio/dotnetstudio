﻿using System;

namespace RalphsDotNet.Validation
{
    /// <summary>
    ///     Checks for a minimum value
    /// </summary>
    public class MaxValueAttribute : ValidatonAttribute
    {
        private readonly double _value;

        public MaxValueAttribute(double value)
        {
            this._value = value;
        }

        public override void Validate(object value, string parameterName)
        {
            if (!(value is IComparable) || this._value.CompareTo(value) > 0)
            {
                throw new ParameterValidatorException("the given value is higher than the validation value");
            }
        }
    }
}