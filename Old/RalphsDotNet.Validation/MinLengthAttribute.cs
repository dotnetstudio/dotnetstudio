namespace RalphsDotNet.Validation
{
    /// <summary>
    /// Checks a string for not being shorter than specified.
    /// </summary>
    public class MinLengthAttribute : ValidatonAttribute
    {
        private readonly int _minLength;

        public MinLengthAttribute(int minLength)
        {
            this._minLength = minLength;
        }

        #region implemented abstract members of Kollektiv.Validation.ParameterValidatorAttribute

        public override void Validate(object value, string parameterName)
        {
            if (value is string)
            {
                var s = value as string;
                if (!(string.IsNullOrEmpty(s) && s.Length >= this._minLength))
                {
                    throw (new ParameterValidatorException(
                        string.Format("\"{0}\" has less than \"{1}\" characters", parameterName, this._minLength)));
                }
            }
            else
            {
                throw (new ParameterValidatorException(
                    string.Format("parameter \"{0}\" has to be as string for use with {1}", parameterName,
                        this.GetType().Name)));
            }

        }

        #endregion
    }
}