namespace RalphsDotNet.Validation
{
    /// <summary>
    /// Checks a string for being not null or empty.
    /// </summary>
    public class NotNullOrEmptyAttribute : ValidatonAttribute
    {
        public override void Validate(object value, string parameterName)
        {
            if (value is string)
            {
                if (string.IsNullOrEmpty(value as string))
                {
                    throw (new ParameterValidatorException(
                        string.Format("\"{0}\" is null or empty", parameterName)));
                }
            }
            else
            {
                throw (new ParameterValidatorException(
                    string.Format("parameter \"{0}\" has to be as string for use with {1}", parameterName,
                        this.GetType().Name)));
            }
        }
    }
}