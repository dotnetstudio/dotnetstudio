namespace RalphsDotNet.Validation
{
    /// <summary>
    /// Checks a string for containing a valid email.
    /// </summary>
    public class EmailAdressAttribute : RegexAttribute
    {
        public EmailAdressAttribute() : base(
            @"^(([^<>()[\]\\.,;:\s@\""]+" +
            @"(\.[^<>()[\]\\.,;:\s@\""]+)*)|(\"".+\""))@" +
            @"((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}" +
            @"\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+" +
            @"[a-zA-Z]{2,}))$")
        {
        }
    }
}