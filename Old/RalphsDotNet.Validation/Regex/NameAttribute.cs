namespace RalphsDotNet.Validation
{
    /// <summary>
    /// Checks a string for containing a valid name.
    /// </summary>
    public class NameAttribute : RegexAttribute
    {
        public NameAttribute() : base(@"\b[A-Z][a-z]+\b")
        {
        }
    }
}