using System;
using System.Reflection;
using System.Linq;
using System.Collections.Generic;

namespace RalphsDotNet.Validation
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Parameter, AllowMultiple = true)]
    public abstract class ValidatonAttribute : Attribute
    {
        public abstract void Validate(object value, string parameterName);

        public static IEnumerable<ValidatonAttribute> GetValidatiorAttributes(ParameterInfo info)
        {
            return info.GetCustomAttributes(true)
                .Where(a => a.GetType().IsSubclassOf(typeof (ValidatonAttribute)))
                .Select(o => (ValidatonAttribute) o);
        }
    }

    public class ParameterValidatorException : Exception
    {
        public ParameterValidatorException(string message) : base(message)
        {
        }
    }
}

