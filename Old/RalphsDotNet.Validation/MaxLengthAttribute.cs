namespace RalphsDotNet.Validation
{
    /// <summary>
    /// Checks a string for not being longer than specified.
    /// </summary>
    public class MaxLengthAttribute : ValidatonAttribute
    {
        private readonly int _maxLength;

        public MaxLengthAttribute(int maxLength)
        {
            this._maxLength = maxLength;
        }

        #region implemented abstract members of Kollektiv.Validation.ParameterValidatorAttribute

        public override void Validate(object value, string parameterName)
        {
            if (value is string)
            {
                var s = value as string;
                if (!(string.IsNullOrEmpty(s) && s.Length <= this._maxLength))
                {
                    throw (new ParameterValidatorException(
                        string.Format("\"{0}\" has more than \"{1}\" characters", parameterName, this._maxLength)));
                }
            }
            else
            {
                throw (new ParameterValidatorException(
                    string.Format("parameter \"{0}\" has to be as string for use with {1}", parameterName,
                        this.GetType().Name)));
            }
        }

        #endregion
    }
}