namespace RalphsDotNet.Validation
{
    /// <summary>
    /// Checks a string by a regex pattern.
    /// </summary>
    public class RegexAttribute : ValidatonAttribute
    {
        public string Pattern { get; private set; }

        public RegexAttribute(string pattern)
        {
            this.Pattern = pattern;
        }

        public override void Validate(object value, string parameterName)
        {
            if (value is string)
            {
                var regex = new System.Text.RegularExpressions.Regex(this.Pattern);
                if (!regex.IsMatch(value as string))
                {
                    throw (new ParameterValidatorException(
                        string.Format("regex \"{0}\" fits not to parameter \"{1}\"", this.Pattern, parameterName)));
                }
            }
            else
            {
                throw (new ParameterValidatorException(
                    string.Format("parameter \"{0}\" has to be as string for use with {1}", parameterName,
                        this.GetType().Name)));
            }
        }
    }
}