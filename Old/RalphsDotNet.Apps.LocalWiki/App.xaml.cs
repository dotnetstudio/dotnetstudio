﻿using System.ComponentModel;
using System.Windows;
using RalphsDotNet.Service;
using RalphsDotNet.Service.Data.Interface;

namespace RalphsDotNet.Apps.LocalWiki
{
    /// <summary>
    ///     Interaction logic for App.xaml
    /// </summary>
    public partial class App : INotifyPropertyChanged
    {
        public const string DataServiceId = "Data";
        public const string DataServiceConfigurationFileNameKey = "Filename";

        public new static App Current
        {
            get { return Application.Current as App; }
        }

        public DataServiceConfiguration DataServiceConfiguration
        {
            get { return ServiceProvider.ServiceConfigurations[DataServiceId] as DataServiceConfiguration; }
            set
            {
                if (!Equals(this.DataServiceConfiguration, value))
                {
                    if (this.DataServiceConfiguration != null)
                        this.DataServiceConfiguration.PropertyChanged -= this.ServiceConfiguration_PropertyChanged;
                    this.DisposeDataService();
                    ServiceProvider.ServiceConfigurations.AddOrReplace(value);
                    Configuration.Save(ServiceProvider.ServiceConfigurations);

                    value.PropertyChanged += this.ServiceConfiguration_PropertyChanged;

                    this.OnPropertyChanged("DataService");
                    this.OnPropertyChanged("DataServiceConfiguration");
                }
            }
        }

        public IDataService DataService
        {
            get
            {
                return ServiceProvider.ServiceConfigurations[DataServiceId] != null
                    ? ServiceProvider.GetService<IDataService>(DataServiceId)
                    : null;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void ServiceConfiguration_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            Configuration.Save(ServiceProvider.ServiceConfigurations);
        }

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            if (!Configuration.Exists<ServicesConfiguration>())
            {
                Configuration.Save(new ServicesConfiguration {Services = new ServiceConfiguration[] {}});
            }
            else
            {
                if (this.DataServiceConfiguration != null)
                    this.DataServiceConfiguration.PropertyChanged += this.ServiceConfiguration_PropertyChanged;
            }
        }

        protected override void OnExit(ExitEventArgs e)
        {
            base.OnExit(e);

            this.DisposeDataService();
        }

        private void DisposeDataService()
        {
            if (this.DataService != null)
                ServiceProvider.DisposeService(DataServiceId);
        }

        protected void OnPropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}