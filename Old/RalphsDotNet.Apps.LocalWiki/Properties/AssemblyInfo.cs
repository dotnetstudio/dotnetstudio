﻿using System.Reflection;
using RalphsDotNet.Apps.LocalWiki.Activities;
using RalphsDotNet.Gui.ActivityModel;

[assembly: AssemblyTitle("Local Wiki")]
[assembly: AssemblyProduct("Local Wiki")]

[assembly: ActivityHostConfiguration("HeaderImageSource", "HeaderTextString", "SubHeaderTextString", "FooterTextString", typeof(EntryActivity))]