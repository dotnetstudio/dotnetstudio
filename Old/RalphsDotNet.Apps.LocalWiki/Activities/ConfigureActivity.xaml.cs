﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Linq;
using System.Windows.Input;
using Microsoft.Win32;
using RalphsDotNet.Gui.ActivityModel;
using RalphsDotNet.Service;
using RalphsDotNet.Service.Data;
using RalphsDotNet.Service.Data.Interface;

namespace RalphsDotNet.Apps.LocalWiki.Activities
{
    /// <summary>
    /// Interaction logic for ConfigureActivity.xaml
    /// </summary>
    public partial class ConfigureActivity
    {public ConfigureActivity()
        {
            this.References = new ActivityReferenceCollection
            {
                new ActivityReference {IsEnabled = false, Name = "Apply"},
                new ActivityReference {IsEnabled = false, Name = "Revert"}
            };

            this.DataContext = new ConfigureViewModel();

            this.LoadDatabaseFilename();

            this.InitializeComponent();
        }

        public ConfigureViewModel ViewModel
        {
            get { return this.DataContext as ConfigureViewModel; }
        }

        protected override void OnInitialized(EventArgs e)
        {
            base.OnInitialized(e);

            this.ViewModel.PropertyChanged += this.ViewModel_PropertyChanged;
        }

        private void ViewModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "DataDatabaseFilename")
            {
                this.CreateDatabaseConfiguration();
            }
        }

        private void CreateDatabaseConfiguration()
        {
            //this.ViewModel.App.DataServiceConfiguration = new DataServiceConfiguration
            //{
            //    Id = App.DataServiceId,
            //    ServiceAssemblyName = "RalphsDotNet.Service.Data",
            //    DataManagerType = typeof (NHibernateManager),
            //    RepositoryAssemblies = new[]
            //    {
            //        typeof (EmployeeRepository).Assembly
            //    },
            //    Values = new[]
            //    {
            //        new ConfigurationValue
            //        {
            //            Key = App.DataServiceConfigurationFileNameKey,
            //            Value = this.ViewModel.DataDatabaseFilename
            //        },
            //        new ConfigurationValue
            //        {
            //            Key = NHibernateManager.ConfigDbconfig,
            //            Value =
            //                "RalphsDotNet.Service.Data.MonoSqliteConfiguration, RalphsDotNet.Service.Data.NHibernate"
            //        },
            //        new ConfigurationValue
            //        {
            //            Key = NHibernateManager.ConfigDbvariant,
            //            Value = "Standard"
            //        },
            //        new ConfigurationValue
            //        {
            //            Key = NHibernateManager.ConfigConnectionstring,
            //            Value =
            //                string.Format(
            //                    "Data Source={0};Version=3;Pooling=true;FailIfMissing=false;Legacy Format=True",
            //                    this.ViewModel.DataDatabaseFilename)
            //        }
            //    }
            //};
        }

        private void LoadDatabaseFilename()
        {
            if (this.ViewModel.App.DataServiceConfiguration != null)
            {
                this.ViewModel.DataDatabaseFilename = this.ViewModel.App.DataServiceConfiguration.Values.Where(
                    cv => cv.Key == App.DataServiceConfigurationFileNameKey)
                    .Select(cv => cv.Value).FirstOrDefault();
            }
        }

        private void ChooseDatabaseFile_OnClick(object sender, RoutedEventArgs e)
        {
            var sfd = new SaveFileDialog
            {
                OverwritePrompt = false,
                AddExtension = true,
                DefaultExt = ".emdb",
                Filter = "Employee Manager Database (.emdb)|*.emdb"
            };
            if (sfd.ShowDialog() == true)
            {
                this.ViewModel.DataDatabaseFilename = sfd.FileName;
            }
        }
    }

    public class ConfigureViewModel : INotifyPropertyChanged
    {
        private string _dataDatabaseFilename;

        public string DataDatabaseFilename
        {
            get { return this._dataDatabaseFilename; }
            set
            {
                if (!Equals(this._dataDatabaseFilename, value))
                {
                    this._dataDatabaseFilename = value;

                    this.OnPropertyChanged("DataDatabaseFilename");
                }
            }
        }

        public App App
        {
            get { return Application.Current as App; }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null) this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
