﻿using System.Windows;
using RalphsDotNet.Gui.ActivityModel;

namespace RalphsDotNet.Apps.LocalWiki.Activities
{
    /// <summary>
    /// Interaction logic for EntryActivity.xaml
    /// </summary>
    public partial class EntryActivity
    {
        public EntryActivity()
        {
            this.References = new ActivityReferenceCollection
            {
                new ActivityReference {Activity = typeof (ConfigureActivity), IsEnabled = true, Name = "ConfigureActivityTextString"}
            };

            InitializeComponent();
        }

        public App App
        {
            get { return Application.Current as App; }
        }
    }
}
