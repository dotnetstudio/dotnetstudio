using System.Collections.Generic;
using RalphsDotNet.Data.Base;

namespace RalphsDotNet.Data.Web
{
    public interface IArticle : IContent
	{
		string Type{ get; set; }
	}

    public interface IArticleRepository : IDataRepository<IArticle>
    {
        IArticle Create(string type, ILanguage language, string title, string text, IEnumerable<IAttachment> attachments);
    }
}

