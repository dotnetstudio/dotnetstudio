using System.Collections.Generic;
using RalphsDotNet.Data.Base;

namespace RalphsDotNet.Data.Web
{
	public interface IEmailContent:IContent
	{
		string Type{ get; set; }
	}

    public interface IEmailContentRepository : IDataRepository<IEmailContent>
    {
        IEmailContent Create(string type, ILanguage language, string title, string text, IEnumerable<IAttachment> attachments);
    }
}

