using RalphsDotNet.Data.Base;

namespace RalphsDotNet.Data.Web
{
    [Abstract]
    public interface IContent : IDataObject, IHasAttachments, IHasLanguage
    {
        string Title { get; set; }

        string Text { get; set; }
    }
}

