﻿using System;

namespace RalphsDotNet.Forms
{
    public class Property
    {
        public string Name { get; private set; }
        public Type Type { get; private set; }
        public object DefaultValue { get; private set; }

        public Property(string name, Type type)
        {
            if (type.IsValueType)
                throw (new ArgumentException("a property of a value type requires a default value"));

            this.Name = name;
            this.Type = type;
            this.DefaultValue = null;
        }

        public Property(string name, Type type, object defaultValue)
        {
            this.Name = name;
            this.Type = type;
            this.DefaultValue = defaultValue;
        }
    }
}
