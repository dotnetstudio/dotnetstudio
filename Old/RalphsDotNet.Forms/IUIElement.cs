﻿using System.ComponentModel;

namespace RalphsDotNet.Forms
{
    public interface IUIElement : INotifyPropertyChanging, INotifyPropertyChanged
    {
        
    }
}
