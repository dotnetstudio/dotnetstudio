﻿using System;
using System.Windows.Forms;

namespace RalphsDotNet.Forms
{
    public partial class EditorControl : UserControl
    {
        protected static Property IsDirtyProperty = new Property("IsDirty", typeof(bool), false);

        public bool IsDirty
        {
            get { return (bool)this.GetValue(IsDirtyProperty); }
            set { this.SetValue(IsDirtyProperty, value); }
        }

        public bool IsReloadAllowed
        {
            get
            {
                return !this.IsDirty || MessageBox.Show("document changed outside editor, use new version?", "document changed", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes;
            }
        }

        public void PrepareClose()
        {
            if (this.IsDirty)
                if (MessageBox.Show("document contains changes, do you want to save?", "document contains changes", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    this.SendSaveRequired();
                    
        }

        public EditorControl()
        {
            InitializeComponent();
        }

        protected void SendSaveRequired()
        {
            if (this.SaveRequired != null)
                this.SaveRequired(this, EventArgs.Empty);
        }

        public event EventHandler SaveRequired;
    }
}
