﻿namespace RalphsDotNet.Forms
{
    public enum NotificationType
    {
        CollectionChanged,
        PropertyChanging,
        PropertyChanged
    }
}
