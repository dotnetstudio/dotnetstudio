﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Linq;

namespace RalphsDotNet.Forms
{
    [DefaultEvent("Load")]
    [Designer("System.Windows.Forms.Design.ControlDesigner, System.Design, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")]
    [Designer("System.Windows.Forms.Design.UserControlDocumentDesigner, System.Design, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", typeof(IRootDesigner))]
    [DesignerCategory("UserControl")]
    public class UserControl : System.Windows.Forms.UserControl, IUIElement
    {
        private static readonly Dictionary<Type, List<Property>> PropertyRegister = new Dictionary<Type, List<Property>>();
        protected static Property DataContextProperty = new Property("DataContext", typeof(object));
        protected static Property PropertyCheckIntervalProperty = new Property("PropertyCheckInterval", typeof(int), 0);

        public object DataContext { get { return this.GetValue(DataContextProperty); } set { this.SetValue(DataContextProperty, value); } }
// ReSharper disable once InconsistentNaming
        protected dynamic dataContext { get { return this.DataContext; } }
        
        /// <summary>
        /// if &gt; 0 a timer will tick in this intervall(miliseconds) and trigger for each property the virtual OnPropertyCheck method
        /// </summary>
        [Browsable(true)]
        [EditorBrowsable(EditorBrowsableState.Always)]
        [DisplayName("PropertyCheckInterval")]
        [Category("Extended")]
        [Description("if > 0 a timer will tick in this intervall(miliseconds) and trigger for each property the virtual OnPropertyCheck method")]
        public int PropertyCheckInterval { get { return (int)this.GetValue(PropertyCheckIntervalProperty); } set { this.SetValue(PropertyCheckIntervalProperty, value); } }

        private readonly System.Windows.Forms.Timer _propertyCheckTimer = new System.Windows.Forms.Timer();

        private readonly Hashtable _properties = new Hashtable();

        public UserControl()
        {
            var thisType = this.GetType();

            if (!PropertyRegister.ContainsKey(thisType))
                PropertyRegister.Add(thisType, new List<Property>());

            foreach (var p in thisType.GetFields().Where(f => f.FieldType == typeof(Property)).Select(f => f.GetValue(this) as Property).Where(p => p != null && !PropertyRegister[thisType].Contains(p)))
            {
                PropertyRegister[thisType].Add(p);
            }

            this._propertyCheckTimer.Tick += propertyCheckTimer_Tick;
        }

        void propertyCheckTimer_Tick(object sender, EventArgs e)
        {
            foreach (var p in PropertyRegister[this.GetType()])
                this.OnPropertyCheck(p, this.GetValue(p));
        }

        private delegate void SetValueDelegate(Property property, object value);
        protected void SetValue(Property property, object value)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new SetValueDelegate(this.SetValue), property, value);
            }
            else
            {
                var oldValue = this.GetValue(property);
                if (oldValue != value)
                {
                    if (this.OnPropertyChanging(property, value, oldValue))
                    {
                        if (!this._properties.ContainsKey(property))
                            this._properties.Add(property, value);
                        else
                            this._properties[property] = value;

                        this.OnPropertyChanged(property, value, oldValue);
                    }
                }
            }
        }

        protected object GetValue(Property property)
        {
            return !this._properties.ContainsKey(property) ? property.DefaultValue : this._properties[property];
        }

        protected virtual bool OnPropertyChanging(Property property, object newValue, object oldValue)
        {
            if (oldValue as INotifyCollectionChanged != null)
                ((INotifyCollectionChanged)oldValue).CollectionChanged -= Property_CollectionChanged;

            if (oldValue as INotifyPropertyChanging != null)
                ((INotifyPropertyChanging)oldValue).PropertyChanging -= Property_PropertyChanging;

            if (oldValue as INotifyPropertyChanged != null)
                ((INotifyPropertyChanged)oldValue).PropertyChanged -= Property_PropertyChanged;

            this.OnPropertyChanging(property.Name);

            return true;
        }

        protected virtual void OnPropertyChanged(Property property, object newValue, object oldValue)
        {
            if (newValue as INotifyCollectionChanged != null)
                ((INotifyCollectionChanged)newValue).CollectionChanged += Property_CollectionChanged;

            if (newValue as INotifyPropertyChanging != null)
                ((INotifyPropertyChanging)newValue).PropertyChanging += Property_PropertyChanging;

            if (newValue as INotifyPropertyChanged != null)
                ((INotifyPropertyChanged)newValue).PropertyChanged += Property_PropertyChanged;

            if (property == DataContextProperty)
                this.OnDataContextChanged(newValue, oldValue);
            else if (property == PropertyCheckIntervalProperty)
            {
                if (this.PropertyCheckInterval > 0)
                {
                    this._propertyCheckTimer.Interval = this.PropertyCheckInterval;
                    this._propertyCheckTimer.Enabled = true;
                }
                else
                {
                    this._propertyCheckTimer.Enabled = false;
                }
            }

            this.OnPropertyChanged(property.Name);
        }

        void Property_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            this.PropertyNotification(sender, NotificationType.CollectionChanged);
        }

        void Property_PropertyChanging(object sender, PropertyChangingEventArgs e)
        {
            this.PropertyNotification(sender, NotificationType.PropertyChanging);
        }

        void Property_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            this.PropertyNotification(sender, NotificationType.PropertyChanged);
        }

        private delegate void PropertyNotificationDelegate(object sender, NotificationType type);
        private void PropertyNotification(object sender, NotificationType type)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new PropertyNotificationDelegate(this.PropertyNotification), sender, type);
            }
            else
            {
                foreach (var property in this._properties.Keys.Cast<Property>().Where(property => this.GetValue(property) == sender))
                {
                    this.OnPropertyNotification(property, type);
                    break;
                }
            }
        }

        protected virtual void OnPropertyNotification(Property property, NotificationType type)
        {
        }

        protected virtual void OnPropertyCheck(Property property, object value)
        {
        }

        protected virtual void OnDataContextChanged(object newValue, object oldValue)
        {
            this.Refresh();
        }

        protected void OnPropertyChanging(string propertyName)
        {
            if (this.PropertyChanging != null)
                this.PropertyChanging(this, new PropertyChangingEventArgs(propertyName));
        }

        protected void OnPropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        public event PropertyChangingEventHandler PropertyChanging;

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
