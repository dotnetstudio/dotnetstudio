using System;
using System.Collections.Generic;
using System.Linq;

namespace RalphsDotNet.Data
{
    public interface IDataManager : IDisposable
    {
        /// <summary>
        /// Gets if there is an active transaction.
        /// </summary>
        /// <value>
        /// <c>true</c> if there is an active transaction, otherwise <c>false</c>.
        /// </value>
        bool ActiveTransaction { get; }

        /// <summary>
        /// Gets if there is an active transaction.
        /// </summary>
        /// <value>
        /// <c>true</c> if there is an active transaction, otherwise <c>false</c>.
        /// </value>
        bool ActiveBatchTransaction { get; }

        /// <summary>
        /// Gets a repository.
        /// </summary>
        /// <returns>
        /// The repository.
        /// </returns>
        /// <typeparam name='TO'>
        /// The object type parameter.
        /// </typeparam>
        /// <typeparam name='TR'>
        /// The repository type parameter.
        /// </typeparam>
        TR GetRepository<TO, TR>()
            where TO : IDataObject
            where TR : IDataRepository<TO>;

        /// <summary>
        /// Begin a transaction.
        /// </summary>
        void BeginTransaction();

        /// <summary>
        /// Begin a batch transaction.
        /// </summary>
        void BeginBatchTransaction();

        /// <summary>
        /// Commit an active transaction.
        /// </summary>
        void CommitTransaction();

        /// <summary>
        /// Commit an active batch transaction.
        /// </summary>
        void CommitBatchTransaction();

        /// <summary>
        /// Rollback an active transaction.
        /// </summary>
        void RollbackTransaction();

        /// <summary>
        /// Rollback an active batch transaction.
        /// </summary>
        void RollbackBatchTransaction();

        /// <summary>
        /// Saves or updates a data object.
        /// </summary>
        /// <param name='obj'>
        /// The affecting data object.
        /// </param>
        void SaveOrUpdate(IDataObject obj);

        /// <summary>
        /// Saves or updates a data object.
        /// </summary>
        /// <param name='obj'>
        /// The affecting data object.
        /// </param>
        void BatchSaveOrUpdate(IDataObject obj);

        /// <summary>
        /// Saves or updates a data object.
        /// </summary>
        /// <param name='objs'>
        /// The affecting data objects.
        /// </param>
        void SaveOrUpdate(IEnumerable<IDataObject> objs);

        /// <summary>
        /// Saves or updates a data object.
        /// </summary>
        /// <param name='objs'>
        /// The affecting data objects.
        /// </param>
        void BatchSaveOrUpdate(IEnumerable<IDataObject> objs);

        /// <summary>
        /// Deletes a data object.
        /// </summary>
        /// <param name='obj'>
        /// The affecting data object.
        /// </param>
        void Delete(IDataObject obj);

        /// <summary>
        /// Deletes a data object.
        /// </summary>
        /// <param name='obj'>
        /// The affecting data object.
        /// </param>
        void BatchDelete(IDataObject obj);

        /// <summary>
        /// Deletes a data object.
        /// </summary>
        /// <param name='objs'>
        /// The affecting data objects.
        /// </param>
        void Delete(IEnumerable<IDataObject> objs);

        /// <summary>
        /// Deletes a data object.
        /// </summary>
        /// <param name='objs'>
        /// The affecting data objects.
        /// </param>
        void BatchDelete(IEnumerable<IDataObject> objs);

        /// <summary>
        /// Reloads an object
        /// </summary>
        /// <param name="obj"></param>
        void Reload(IDataObject obj);

        /// <summary>
        /// Reloads multiple objects
        /// </summary>
        /// <param name="objs"></param>
        void Reload(IEnumerable<IDataObject> objs);

        /// <summary>
        /// Gets a query of all data objects of a given type.
        /// </summary>
        /// <typeparam name='T'>
        /// The data object type.
        /// </typeparam>
        IQueryable<T> Get<T>() where T : IDataObject;

        /// <summary>
        /// Gets a query of all data objects of a given type.
        /// </summary>
        /// <typeparam name='T'>
        /// The data object type.
        /// </typeparam>
        IQueryable<T> BatchGet<T>() where T : IDataObject;
    }
}

