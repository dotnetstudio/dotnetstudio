﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;

namespace RalphsDotNet.Data
{
    [DataContract]
    public class DataObject : IDataObject
    {
        public static T Create<T>() where T : IDataObject
        {
            // ensure data assembly is already emitted
            var ass = typeof (T).Assembly;
            if (ModelEmitter.ResolveAssembly(ass) == null)
            {
                ModelEmitter.EmitModelFromDataAssembly(ass);
            }

            return (T)Activator.CreateInstance(ModelEmitter.ResolveType(typeof(T)));
        }

        private Guid _id;

        private bool _isDirty;

        private TransactionState _state;

        protected DataObject()
        {
            this.PropertyChanged += this.DataObject_PropertyChanged;
        }

        public virtual Guid Id
        {
            get { return this._id; }
            set
            {
                if (this._id != value)
                {
                    this.OnPropertyChanging("Id");

                    this._id = value;

                    this.OnPropertyChanged("Id");
                }
            }
        }

        [IgnorePersistance]
        public virtual bool IsDirty
        {
            get { return this._isDirty; }
            set
            {
                if (this._isDirty != value)
                {
                    this.OnPropertyChanging("IsDirty");

                    this._isDirty = value;

                    this.OnPropertyChanged("IsDirty");
                }
            }
        }

        [IgnorePersistance]
        public virtual TransactionState State
        {
            get { return this._state; }
            set
            {
                if (this._state != value)
                {
                    this.OnPropertyChanging("State");

                    this._state = value;

                    this.OnPropertyChanged("State");
                }
            }
        }

        public virtual event PropertyChangedEventHandler PropertyChanged;
        public virtual event PropertyChangingEventHandler PropertyChanging;

        private void DataObject_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName != "Id" && e.PropertyName != "IsDirty" && e.PropertyName != "State")
                this.IsDirty = true;
        }

        protected virtual void OnPropertyChanging(string propertyName)
        {
            if (this.PropertyChanging != null)
                this.PropertyChanging(this, new PropertyChangingEventArgs(propertyName));
        }

        protected virtual void OnPropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        public override string ToString()
        {
            var np =
                this.GetType()
                    .GetProperties()
                    .FirstOrDefault(p => p.PropertyType == typeof (string) && p.Name == "Name");

            if (np != null)
                return np.GetValue(this, null) as string;
            else
                return base.ToString();
        }

        public virtual void Dispose()
        {
            this.Id = Guid.Empty;
            this.IsDirty = false;
            this.State = TransactionState.None;
        }
    }
}