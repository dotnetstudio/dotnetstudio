using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace RalphsDotNet.Data
{
    public interface IDataRepository : INotifyPropertyChanging, INotifyPropertyChanged, IDisposable
    {
        /// <summary>
        /// Gets the type of the data object.
        /// </summary>
        Type DataType { get; }

        /// <summary>
        /// Gets the related data manager instance.
        /// </summary>
        IDataManager Manager { get; }
    }

    /// <summary>
    /// Repository managing a persistent data object.
    /// </summary>
    public interface IDataRepository<T> : IDataRepository where T : IDataObject
    {
        /// <summary>
        /// Gets a query of all data objects in this repository.
        /// </summary>
        IQueryable<T> All { get; }

        /// <summary>
        /// Gets a query of all data objects in this repository.
        /// </summary>
        IQueryable<T> BatchAll { get; }

        /// <summary>
        /// Saves or updates a data object.
        /// </summary>
        /// <param name='obj'>
        /// Affecting data object.
        /// </param>
        void SaveOrUpdate(T obj);

        /// <summary>
        /// Saves or updates a data object.
        /// </summary>
        /// <param name='obj'>
        /// Affecting data object.
        /// </param>
        void BatchSaveOrUpdate(T obj);

        /// <summary>
        /// Saves or updates a data object.
        /// </summary>
        /// <param name='objs'>
        /// Affecting data objects.
        /// </param>
        void SaveOrUpdate(IEnumerable<T> objs);

        /// <summary>
        /// Saves or updates a data object.
        /// </summary>
        /// <param name='objs'>
        /// Affecting data objects.
        /// </param>
        void BatchSaveOrUpdate(IEnumerable<T> objs);

        /// <summary>
        /// Deletes a data object.
        /// </summary>
        /// <param name='obj'>
        /// Affecting data object.
        /// </param>
        void Delete(T obj);

        /// <summary>
        /// Deletes a data object.
        /// </summary>
        /// <param name='obj'>
        /// Affecting data object.
        /// </param>
        void BatchDelete(T obj);

        /// <summary>
        /// Deletes a data object.
        /// </summary>
        /// <param name='objs'>
        /// Affecting data object.
        /// </param>
        void Delete(IEnumerable<T> objs);

        /// <summary>
        /// Deletes a data object.
        /// </summary>
        /// <param name='objs'>
        /// Affecting data object.
        /// </param>
        void BatchDelete(IEnumerable<T> objs);
        
        /// <summary>
        /// Reloads an object
        /// </summary>
        /// <param name="obj"></param>
        void Reload(T obj);

        /// <summary>
        /// Reloads multiple objects
        /// </summary>
        /// <param name="objs"></param>
        void Reload(IEnumerable<T> objs);
    }
}

