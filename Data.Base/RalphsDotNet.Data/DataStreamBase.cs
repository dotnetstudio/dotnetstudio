﻿using System.IO;

namespace RalphsDotNet.Data
{
    public abstract class DataStreamBase : Stream
    {
        public string Id { get; private set; }

        public IDataObject Last { get; protected set; }

        protected DataStreamBase(string id)
        {
            this.Id = id;
        }

        public abstract void Write<T>(T obj) where T : IDataObject;
        public abstract IDataObject Read();

        public virtual void WaitForQueueToFinish()
        {
        }
    }
}