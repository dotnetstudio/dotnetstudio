using System;
using System.ComponentModel;

namespace RalphsDotNet.Data
{
    /// <summary>
    /// a presistent data object.
    /// </summary>
    public interface IDataObject : INotifyPropertyChanging, INotifyPropertyChanged, IDisposable
    {
        /// <summary>
        /// Gets the unique identifier of the persistent data object instance.
        /// </summary>
        Guid Id { get; set; }

        [IgnorePersistance]
        bool IsDirty { get; set; }

        [IgnorePersistance]
        TransactionState State { get; set; }
    }

    public enum TransactionState
    {
        None,
        ForSaveOrUpdate,
        ForDelete
    }
}

