﻿using System;

namespace RalphsDotNet.Data
{
    [AttributeUsage(AttributeTargets.Interface)]
    public class AbstractAttribute : Attribute
    {
        public string Name { get; set; }
    }

    [AttributeUsage(AttributeTargets.Property)]
    public class IndexAttribute : Attribute
    {
        public string Name { get; set; }
    }

    [AttributeUsage(AttributeTargets.Property)]
    public class UniqueAttribute : Attribute
    {
        public string Name { get; set; }
    }

    [AttributeUsage(AttributeTargets.Property)]
    public class TextAttribute : Attribute
    {
    }

    /// <summary>
    ///     assoc object gets also deleted (only valid on the many side of the assoc)
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class CascadeAllAttribute : Attribute
    {
    }

    [AttributeUsage(AttributeTargets.Interface)]
    public class DeleteOrphansAttribute : Attribute
    {
    }

    [AttributeUsage(AttributeTargets.Property)]
    public class IgnorePersistanceAttribute : Attribute
    {
    }

    [AttributeUsage(AttributeTargets.Property)]
    public class ForeignKeyAttribute : Attribute
    {
        public ForeignKeyAttribute(string name)
        {
            this.Name = name;
        }

        public string Name { get; set; }
    }

    [AttributeUsage(AttributeTargets.Property)]
    public class EagerLoadingAttribute : Attribute
    {
    }

    [AttributeUsage(AttributeTargets.Property)]
    public class OrderByAttribute : Attribute
    {
        public string Property { get; set; }
        public Order Order { get; set; }
    }

    public enum Order
    {
        Ascending,
        Descending
    }
}