﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;

namespace RalphsDotNet.Data
{
    public static class ModelEmitter
    {
        private static readonly Dictionary<Assembly, Assembly> DataAssemblies = new Dictionary<Assembly, Assembly>();
        private static readonly Dictionary<Type, Type> DataTypes = new Dictionary<Type, Type>();
        private static readonly Dictionary<Type, Type> ReverseDataTypes = new Dictionary<Type, Type>();
        private static ulong _typeCounter;

        static ModelEmitter()
        {
            AppDomain.CurrentDomain.AssemblyResolve += CurrentDomain_AssemblyResolve;
        }

        public static Type ResolveType(Type iFace)
        {
            return DataTypes[iFace];
        }

        public static Type ReverseResolveType(Type type)
        {
            return ReverseDataTypes[type];
        }

        public static Assembly ResolveAssembly(Assembly dataAssembly)
        {
            Assembly ass;
            DataAssemblies.TryGetValue(dataAssembly, out ass);
            return ass;
        }

        private static Assembly CurrentDomain_AssemblyResolve(object sender, ResolveEventArgs args)
        {
            var assPath = Path.Combine(Path.GetTempPath(), args.Name.Split(',').First() + ".dll");
            return File.Exists(assPath) ? Assembly.LoadFrom(assPath) : null;
        }

        public static Assembly EmitModelFromDataAssembly(Assembly dataAssembly)
        {
            lock (DataTypes)
            {
                var interfaces =
                    dataAssembly.GetTypes()
                        .Where(
                            x =>
                                x.GetInterfaces().Any(i => i == (typeof (IDataObject))) &&
                                x.GetCustomAttributes(true).All(a => a as AbstractAttribute == null))
                        .ToArray();

                if (interfaces.Any(t => !t.IsInterface))
                    throw (new ArgumentException("all types have to be interfaces"));

                var assName = new AssemblyName(string.Concat("E_", Guid.NewGuid().ToString()));
                var assPath = Path.GetTempPath();
                var assBuilder = AppDomain.CurrentDomain.DefineDynamicAssembly(assName, AssemblyBuilderAccess.Run,
                    assPath);

                var moduleBuilder = new Dictionary<string, ModuleBuilder>();
                var dynamicModule = assBuilder.DefineDynamicModule(assName.Name);

                var isNotifyPropertyChanging =
                    typeof (DataObject).GetInterfaces().Any(i => i == typeof (INotifyPropertyChanging));
                var isNotifyPropertyChanged =
                    typeof (DataObject).GetInterfaces().Any(i => i == typeof (INotifyPropertyChanged));

                foreach (var iFace in interfaces.Where(iFace => !DataTypes.ContainsKey(iFace)))
                {
                    if (iFace.Namespace != null && !moduleBuilder.ContainsKey(iFace.Namespace))
                        moduleBuilder.Add(iFace.Namespace, dynamicModule);

                    _typeCounter++;

                    if (iFace.Namespace != null && moduleBuilder.ContainsKey(iFace.Namespace))
                    {
                        var mBuilder = moduleBuilder[iFace.Namespace];
                        var tBuilder = mBuilder.DefineType(
                            string.Concat("E_", _typeCounter.ToString(), "_", iFace.Name),
                            TypeAttributes.Public | TypeAttributes.Serializable, typeof (DataObject));

                        tBuilder.AddInterfaceImplementation(iFace);

                        tBuilder.DefineDefaultConstructor(MethodAttributes.Public | MethodAttributes.HideBySig |
                                                          MethodAttributes.SpecialName |
                                                          MethodAttributes.RTSpecialName);

                        var builderInfo =
                            new Dictionary
                                <string,
                                    Tuple
                                        <Type, MethodAttributes, MethodAttributes, MethodInfo, MethodInfo,
                                            PropertyInfo>
                                    >();

                        iFace.CollectionOverridePropertiesOfInterfaceInfos(typeof (DataObject), builderInfo);

                        foreach (var propertyName in builderInfo.Keys)
                        {
                            propertyName.EmitOverrideProperty(typeof (DataObject), tBuilder, builderInfo,
                                isNotifyPropertyChanging,
                                isNotifyPropertyChanged);
                        }

                        DataTypes.Add(iFace, tBuilder.CreateType());
                        ReverseDataTypes.Add(DataTypes[iFace], iFace);
                    }
                }

                DataAssemblies.Add(dataAssembly, assBuilder);

                return assBuilder;
            }
        }

        private static void CollectionOverridePropertiesOfInterfaceInfos(this Type iFace, Type baseType,
            Dictionary<string, Tuple<Type, MethodAttributes, MethodAttributes, MethodInfo, MethodInfo, PropertyInfo>>
                builderInfo)
        {
            // emiting properties of the implementing interfaces
            foreach (var implIFace in iFace.GetInterfaces().Where(i => !baseType.GetInterfaces().Contains(i)))
                implIFace.CollectionOverridePropertiesOfInterfaceInfos(baseType, builderInfo);

            // emiting properties of the type itself
            foreach (var property in iFace.GetProperties())
            {
                property.CollectionOverridePropertiesOfInterfaceInfo(builderInfo);
            }
        }

        private static void CollectionOverridePropertiesOfInterfaceInfo(this PropertyInfo property,
            Dictionary<string, Tuple<Type, MethodAttributes, MethodAttributes, MethodInfo, MethodInfo, PropertyInfo>>
                builderInfo)
        {
            if (!builderInfo.ContainsKey(property.Name))
            {
                var mAttributesGet = CalculateGetterAttributes(property, false);
                var mAttributesSet = CalculateSetterAttributes(property, false);

                builderInfo.Add(property.Name,
                    new Tuple<Type, MethodAttributes, MethodAttributes, MethodInfo, MethodInfo, PropertyInfo>(
                        property.PropertyType, mAttributesGet, mAttributesSet,
                        property.CanRead ? property.GetGetMethod() : null,
                        property.CanWrite ? property.GetSetMethod() : null, property));
            }
            else
            {
                var mAttributesGet = CalculateGetterAttributes(property,
                    (builderInfo[property.Name].Item2 & MethodAttributes.Public) == MethodAttributes.Public);
                var mAttributesSet = CalculateSetterAttributes(property,
                    (builderInfo[property.Name].Item3 & MethodAttributes.Public) == MethodAttributes.Public);

                builderInfo[property.Name] =
                    new Tuple<Type, MethodAttributes, MethodAttributes, MethodInfo, MethodInfo, PropertyInfo>(
                        property.PropertyType, mAttributesGet, mAttributesSet,
                        property.CanRead ? property.GetGetMethod() : builderInfo[property.Name].Item4,
                        property.CanWrite ? property.GetSetMethod() : builderInfo[property.Name].Item5, property);
            }
        }

        private static void EmitOverrideProperty(this string propertyName, Type baseType, TypeBuilder tBuilder,
            Dictionary<string, Tuple<Type, MethodAttributes, MethodAttributes, MethodInfo, MethodInfo, PropertyInfo>>
                builderInfo, bool isNotifyPropertyChanging, bool isNotifyPropertyChanged)
        {
            var fBuilder = tBuilder.DefineField(string.Concat("_", propertyName),
                builderInfo[propertyName].Item1, FieldAttributes.Private);

            var pBuilder = tBuilder.DefineProperty(propertyName, PropertyAttributes.None,
                builderInfo[propertyName].Item1, null);

            builderInfo[propertyName].Item6.GetCustomAttributesData()
                .Select(d => d.ToAttributeBuilder()).ToList()
                .ForEach(pBuilder.SetCustomAttribute);

            var mAttributesGet = builderInfo[propertyName].Item2;
            var mAttributesSet = builderInfo[propertyName].Item3;

            var pType = builderInfo[propertyName].Item1;
            var mBuilderGet = tBuilder.DefineMethod(string.Concat("get_", propertyName), mAttributesGet, pType,
                Type.EmptyTypes);

            var genGet = mBuilderGet.GetILGenerator();
            genGet.Emit(OpCodes.Ldarg_0);
            genGet.Emit(OpCodes.Ldfld, fBuilder);
            genGet.Emit(OpCodes.Ret);

            pBuilder.SetGetMethod(mBuilderGet);

            if ((builderInfo[propertyName].Item2 & MethodAttributes.Public) == MethodAttributes.Public)
                tBuilder.DefineMethodOverride(mBuilderGet, builderInfo[propertyName].Item4);

            var mBuilderSet = tBuilder.DefineMethod(string.Concat("set_", propertyName), mAttributesSet, null,
                new[] {builderInfo[propertyName].Item1});
            var genSet = mBuilderSet.GetILGenerator();

            var endOfSet = genSet.DefineLabel();

            var compareMethod = pType.GetMethod("Equals",
                BindingFlags.Public | BindingFlags.FlattenHierarchy | BindingFlags.Instance, null,
                CallingConventions.Standard, new[] {typeof (object)}, null);

            /* just samples
               genSet.Emit(OpCodes.Stloc_0);                   // store result to local0
               genSet.Emit(OpCodes.Ldloc_0);                   // load local0
            */

            genSet.Emit(OpCodes.Nop);

            // store arguments into local variables
            genSet.Emit(OpCodes.Ldarg_0); // load argumentTHIS for usage by ldfld
            genSet.Emit(OpCodes.Ldfld, fBuilder); // load field from argumentTHIS
            if (pType.IsValueType) genSet.Emit(OpCodes.Box, pType); // box field

            genSet.Emit(OpCodes.Ldarg_1); // load argumentVALUE for compare
            if (pType.IsValueType) genSet.Emit(OpCodes.Box, pType); // box argumentVALUE

            // if interface 'if(==)' else 'if(x.Equals(y))'
            if (pType.IsValueType)
            {
                genSet.Emit(OpCodes.Callvirt, compareMethod);
                // call Equals method on value from field with argumentVALUE as argument, returns true(1) or false(0)
            }
            else
            {
                genSet.Emit(OpCodes.Ceq); // compare value from field with argumentVALUE, returns true(1) or false(0)
            }
            genSet.Emit(OpCodes.Brtrue_S, endOfSet);

            if (isNotifyPropertyChanging)
            {
                genSet.Emit(OpCodes.Ldarg_0);
                genSet.Emit(OpCodes.Ldstr, propertyName);
                genSet.Emit(OpCodes.Callvirt,
                    baseType.GetMethod("OnPropertyChanging", BindingFlags.NonPublic | BindingFlags.Instance, null,
                        new[] {typeof (string)}, null));
            }

            genSet.Emit(OpCodes.Ldarg_0); // load argumentTHIS for usage by stfld
            genSet.Emit(OpCodes.Ldarg_1); // load argumentVALUE
            //if (pType.IsValueType) genSet.Emit(OpCodes.Box, pType);     // box arguemntVALUE
            genSet.Emit(OpCodes.Stfld, fBuilder);

            if (isNotifyPropertyChanged)
            {
                genSet.Emit(OpCodes.Ldarg_0);
                genSet.Emit(OpCodes.Ldstr, propertyName);
                genSet.Emit(OpCodes.Callvirt,
                    baseType.GetMethod("OnPropertyChanged", BindingFlags.NonPublic | BindingFlags.Instance, null,
                        new[] {typeof (string)}, null));
            }

            genSet.MarkLabel(endOfSet);
            genSet.Emit(OpCodes.Ret);

            pBuilder.SetSetMethod(mBuilderSet);

            if ((builderInfo[propertyName].Item3 & MethodAttributes.Public) == MethodAttributes.Public)
                tBuilder.DefineMethodOverride(mBuilderSet, builderInfo[propertyName].Item5);
        }

        private static CustomAttributeBuilder ToAttributeBuilder(this CustomAttributeData data)
        {
            if (data == null)
            {
                throw new ArgumentNullException("data");
            }

            var propertyArguments = new List<PropertyInfo>();
            var propertyArgumentValues = new List<object>();
            var fieldArguments = new List<FieldInfo>();
            var fieldArgumentValues = new List<object>();
            foreach (var namedArg in data.NamedArguments)
            {
                var fi = namedArg.MemberInfo as FieldInfo;
                var pi = namedArg.MemberInfo as PropertyInfo;

                if (fi != null)
                {
                    fieldArguments.Add(fi);
                    fieldArgumentValues.Add(namedArg.TypedValue.Value);
                }
                else if (pi != null)
                {
                    propertyArguments.Add(pi);
                    propertyArgumentValues.Add(namedArg.TypedValue.Value);
                }
            }
            return new CustomAttributeBuilder(
                data.Constructor,
                data.ConstructorArguments.Select(ctorArg => ctorArg.Value).ToArray(),
                propertyArguments.ToArray(),
                propertyArgumentValues.ToArray(),
                fieldArguments.ToArray(),
                fieldArgumentValues.ToArray());
        }

        private static MethodAttributes CalculateGetterAttributes(PropertyInfo property, bool forcePublic)
        {
            var mAttributesGet = MethodAttributes.SpecialName | MethodAttributes.HideBySig | MethodAttributes.Virtual;

            if (property.CanRead || forcePublic)
                mAttributesGet |= MethodAttributes.Public;
            else
                mAttributesGet |= MethodAttributes.Private;

            return mAttributesGet;
        }

        private static MethodAttributes CalculateSetterAttributes(PropertyInfo property, bool forcePublic)
        {
            var mAttributesSet = MethodAttributes.SpecialName | MethodAttributes.HideBySig | MethodAttributes.Virtual;

            if (property.CanWrite || forcePublic)
                mAttributesSet |= MethodAttributes.Public;
            else
                mAttributesSet |= MethodAttributes.Private;

            return mAttributesSet;
        }
    }
}